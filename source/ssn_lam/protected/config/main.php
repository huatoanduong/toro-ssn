<?php

include_once 'config.local.php';

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

Yii::setPathOfAlias('bootstrap', dirname(__FILE__) . '/../extensions/bootstrap');

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'SSN',
    'theme' => $THEME,
    'language' => 'en',
    // preloading 'log' component
//    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.models.core.*',
        'application.models.forms.*',
        'application.models.menus.*',
        'application.models.cms.*',
        'application.models.base.*',
        'application.models.lookup.*',
        'application.models.newsletter.*',
        'application.models.contactus.*',
        'application.components.*',
        'application.components.customFunction.*',
        'application.components.format.*',
        'application.components.helper.*',
        'application.components.widget.*',
        'application.extensions.yii-mail.*',
        'application.extensions.EUploadedImage.*',
        'application.extensions.EPhpThumb.*',
        'application.extensions.MyDebug.*',
        //'application.extensions.editMe.*',
        'application.extensions.ControllerActionsName.*',
        'application.modules.auditTrail.models.AuditTrail',
        'application.extensions.toSlug.*',
        'application.extensions.fbLikeBox.*',
        'application.extensions.multicomplete.*',
    ),
    'modules' => array(
        'gii' => array(
            'class'=>'system.gii.GiiModule',
//            'class' => 'application.modules.gii.GiiModule',
            'password' => '123123',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
//            'generatorPaths' => array(
//                'application.modules.gii', // a path alias
//            ),
        ),
        'admin',
        'member',
        'auditTrail' => array(
            'userClass' => 'Users', // the class name for the user object
            'userIdColumn' => 'id', // the column name of the primary key for the user
            'userNameColumn' => 'username', // the column name of the primary key for the user
        ),
    ),
    // application components
    'components' => array(
        'session' => array(
            'class' => 'CDbHttpSession',
            'timeout' => 84600,
        ),
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
            'class' => 'WebUser',
            'loginUrl' => array('/admin/site/login'),
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'rules' => array(
				'admin/login' => 'admin/site/login',
                        array('class' => 'application.components.CmsUrlRule'),
                                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                            ),
            'showScriptName' => false,
        ),
        'db' => array(
            'connectionString' => "mysql:host=localhost;dbname=dihub",
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'tablePrefix' => $TABLE_PREFIX,
            'charset' => 'utf8',
		   'enableProfiling'=>true,
		   'enableParamLogging'=>true,
        ),
        'authManager' => array(
            'class' => 'CDbAuthManager',
            'connectionID' => 'db',
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
//				array(
//					'class' => 'CFileLogRoute',
//				   'levels'=>'error, warning',
//				),
//				array(
//					'class' => 'DbLogRoute',
//					'connectionID' => 'db',
//					'autoCreateLogTable' => false,
//					'logTableName' => $TABLE_PREFIX . "_logger",
//					'levels' => 'info, error'
//				),
                array(
                    'class' => 'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
                    'ipFilters'=>array('*'),
                ),
            ),
        ),
        'mail' => array(
            'class' => 'application.extensions.yii-mail.YiiMail',
            'transportType' => 'smtp', /// case sensitive!
            'transportOptions' => array(
                'host' => '',
                'username' => '',
                'password' => '',
                'port' => '465',
                'encryption' => 'ssl',
                'timeout' => '120',
            ),
            'viewPath' => 'application.mail',
            'logging' => true,
            'dryRun' => false
        ),
        'setting' => array(
            'class' => 'application.extensions.MyConfig.MyConfig',
            'cacheId' => null,
            'useCache' => false,
            'cacheTime' => 0,
            'tableName' => $TABLE_PREFIX . '_settings',
            'createTable' => false,
            'loadDbItems' => true,
            'serializeValues' => true,
            'configFile' => '',
        ),
        'format' => array(
            'class' => 'CustomBaseFormatter',
        ),
//        'ELangHandler' => array(
//            'class' => 'application.extensions.langhandler.ELangHandler',
//            'languages' => array('en', 'cn'),
//            'strict' => true,
//        ),
        'events' => array(
            'class' => 'CmsEventList'
        ),
        'widgetFactory' => array(
            'widgets' => array(
                'CGridView' => array(
//                    'cssFile'=>'/NoisyRadar/css/gridview.css',
                ),
            ),
        ),
        'bootstrap' => array(
            'class' => 'bootstrap.components.Bootstrap',
        ),
        'metadata' => array('class' => 'Metadata'),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        'ckeditor_full' => "[['Source', 'Bold', 'Italic', 'Underline', 'RemoveFormat', 'Table', 'PasteText', 'PasteFromWord'],['NumberedList', 'BulletedList', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],['Link', 'Unlink', 'Image'],['Styles', 'Format', 'Font', 'FontSize'],['TextColor', 'BGColor'],['Youtube', 'oembed', 'filebrowser', 'Video']]",
        'ckeditor_basic' => "[
                            [ 'Bold', 'Italic','Underline', '-', 'Table', 'JustifyLeft', 'JustifyCenter', 'JustifyRight','JustifyBlock', 'RemoveFormat', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ],
                            [ 'FontSize', 'TextColor', 'BGColor' ]
                    ]",
        'niceditor' => array('bold', 'italic', 'underline', 'ol', 'ul'),
        'niceditor_v_1' => array('bold', 'italic', 'underline', 'ol', 'ul', 'fontSize', 'left', 'center', 'right', 'justify', 'forecolor', 'bgcolor', 'image', 'upload', 'link', 'unlink'),
        'adminEmail' => '',
        'autoEmail' => '',
        'dateFormat' => 'd/m/Y',
        'timeFormat' => 'H:i:s',
        'paypalURL' => '',
        'paypalURL_sandbox' => '',
        'paypal_email_address' => '',
        'is_paypal_sandbox' => 1,
        'image_watermark' => '',
        'defaultPageSize' => 20,
        'twitter' => '',
        'facebook' => '',
        'linkedin' => '',
        'wordpress' => '',
        'rss' => '',
        'meta_description' => '',
        'meta_keywords' => '',
        'reCaptcha' => array(
            'publicKey' => '6Lfmj9ASAAAAAM2b4ePzdByLBIrX6bSU32ZnLgIR',
            'privateKey' => '6Lfmj9ASAAAAAAiZVwboS55FW1sKY1QWm-lGEEAV',
        ),
    ),
);
<?php
$controllerName = Yii::app()->controller->id;
$actionName = Yii::app()->controller->action->id;
$hasHomeBanner = array(
	'site/index',
);
$current_action = $controllerName . '/' . $actionName;
?>
<!DOCTYPE html>
<html lang="en">
<?php include '_head.php'; ?>
<body class="skin-red-light sidebar-mini sidebar-collapse">
	<div class="wrapper">
        <?php include '_header.php'; ?>

        <?php $this->widget('MainMenu'); ?>

        <div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1><?php echo CHtml::encode($this->pageTitle) ?></h1>
				<?php $this->widget('zii.widgets.CBreadcrumbs', array(
					'links' => $this->breadcrumbs,
					'separator' => '',
					'homeLink'=> CHtml::tag('li', array(), CHtml::link('<i class="fa fa-dashboard"></i> Home', Yii::app()->homeUrl)),
					'tagName' => 'ol',
					'activeLinkTemplate' => '<li><a href="{url}">{label}</a></li>',
					'inactiveLinkTemplate' => '<li class="active">{label}</li>',
					'htmlOptions'=>array('class'=>'breadcrumb')
				)); ?>
			</section>

			<!-- Main content -->
			<section class="content">
				<?php $this->widget('application.components.widget.Notification'); ?>
				<?= $content ?>
			</section><!-- /.content -->
        </div>
			
        <?php include '_footer.php'; ?>
	</div>
</body>
</html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="copyright" content="<?php echo CHtml::encode(Yii::app()->params['projectName']); ?>" />
    <?php if (!defined('YII_DEBUG')): ?>
    <meta content="index, follow" name="robots" />
    <?php endif ?>

    <title><?php echo $this->pageTitle . ' - ' . Yii::app()->params['projectName'] ?></title>

    <link rel="shortcut icon" type="image/ico" href="<?php echo Yii::app()->theme->baseUrl; ?>favicon.ico" />
    <link rel="apple-touch-icon" href="<?php echo Yii::app()->theme->baseUrl; ?>favicon.png" />
	
	<?php Yii::app()->getClientScript()->registerCssFile(
		Yii::app()->theme->baseUrl.'css/bootstrap.min.css'); ?>
	<?php Yii::app()->getClientScript()->registerCssFile(
		Yii::app()->theme->baseUrl.'css/AdminLTE.min.css'); ?>
	<?php Yii::app()->getClientScript()->registerCssFile(
		Yii::app()->theme->baseUrl.'css/skins/_all-skins.min.css'); ?>
	<?php Yii::app()->getClientScript()->registerCssFile(
		Yii::app()->theme->baseUrl.'css/custom.css'); ?>
	<?php Yii::app()->getClientScript()->registerCssFile(
		'https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'); ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

	<?php
	Yii::app()->clientScript->scriptMap = array(
		'jquery.js' => Yii::app()->theme->baseUrl.'js/jQuery-2.1.4.min.js',
		'jquery.min.js' => Yii::app()->theme->baseUrl.'js/jQuery-2.1.4.min.js',
	);?>
    <?php Yii::app()->getClientScript()->registerCoreScript('jquery'); ?>
	<?php Yii::app()->clientScript->registerScriptFile(
		Yii::app()->theme->baseUrl.'js/bootstrap.min.js', CClientScript::POS_END) ?>
	<?php Yii::app()->clientScript->registerScriptFile(
		Yii::app()->theme->baseUrl.'js/app.min.js', CClientScript::POS_END) ?>
	<?php Yii::app()->clientScript->registerScriptFile(
		Yii::app()->theme->baseUrl.'js/custom.js', CClientScript::POS_END) ?>

</head>
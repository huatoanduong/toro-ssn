<?php $this->beginClip('banner'); ?>
<?php $this->Widget('PageBannerWidget',array('apply_for_page' => $model->slug)); ?>    
<?php $this->endClip(); ?>

<div class="wrapper container">
	<div class="about-page">
		<div class="row">
			<div class="sidebar col-md-3">
				<?php $this->widget('PageNavigation', array(
					'page' => $model,
				)); ?>
			</div>
			<div class="main-wrap col-md-9">
				<ol class="breadcrumb">
					<li><a href="<?php echo Yii::app()->homeUrl ?>"><span class="icon icon-home"></span></a></li>
					<li class="active"><?php echo $model->title ?></li>
				</ol>
				
				<h2 class="title-2"><?php echo $model->title ?></h2>
				<div class="main-content"><?php echo $model->content ?></div>
			</div><!-- Main wrap -->
		</div><!-- Content page -->
	</div><!-- Row -->
</div>

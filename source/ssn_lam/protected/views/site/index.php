<div class="wrapper container">
    <div class="about-page">
        <div class="row">
            <div class="main-wrap col-md-9">
                <ol class="breadcrumb">
                        <li><a href="<?php echo Yii::app()->homeUrl ?>"><span class="icon icon-home"></span></a></li>
                        <li class="active"><?php echo 'Trang chủ' ?></li>
                </ol>
                <h2 class="title-2"><?php echo 'Sản phẩm chất lượng cao' ?></h2>
                <ul class="list-service clearfix">
                    <?php if (count($hotProducts) > 0): ?>
                        <?php foreach ($hotProducts as $hproduct): ?>			
                            <li>
                                <a href="<?php echo Yii::app()->createAbsoluteUrl('product/detail', array('slug' => $hproduct->slug)); ?>">
                                    <?php echo $hproduct->getImage() ?>
                                    <span><?php echo $hproduct->name; ?></span>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
                <h2 class="title-2"><?php echo 'Sản phẩm bình dân' ?></h2>
                <ul class="list-service clearfix">
                    <?php if (count($products) > 0): ?>
                        <?php foreach ($products as $product): ?>			
                            <li>
                                <a href="<?php echo Yii::app()->createAbsoluteUrl('product/detail', array('slug' => $product->slug)); ?>">
                                    <?php echo $product->getImage() ?>
                                    <span><?php echo $product->name; ?></span>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
                
            </div><!-- Main wrap -->
            <div class="sidebar col-md-3">
                <?php 
                    $this->widget('SidebarWidget', array()
                  );
                ?>
            </div>
        </div>
    </div>
</div>
<style>
    .main-wrap{
        float: left !important;
    }
    .sidebar{
        float: right !important;
    }
    .img-prd{
/*        width: 177px !important;
        height: 185px !important;*/
    }
</style>
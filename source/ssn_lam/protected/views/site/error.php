<?php
$this->bodyClass .= 'login-page ';
$this->pageTitle = 'Error';
?>

<div class="login-box">
	<div class="login-logo">
	Error
	</div>
	<div class="login-box-body"><p><?php echo CHtml::encode($message); ?></p></div>
</div>


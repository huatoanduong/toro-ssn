<?php

class ShippingController extends MemberController {

	public function actionComment($id) {
		$model = $this->loadModel($id);
		if (!$model->canView())
			throw new CHttpException(404, 'The requested page does not exist.');
		
		if (isset($_POST['message'])) {
			$comment = new SsnShippingComment();
			$comment->comment = $_POST['message'];
			$comment->user_id = Yii::app()->user->id;
			$comment->shipping_id = $model->id;
			$comment->save();
			Yii::app()->user->setFlash('success', 'Your comment has been added.');
			$this->redirect(array('comment', 'id'=>$model->id));
		}
			
		$comments = SsnShippingComment::model()->findByShipping($id);
		$this->render('comment', array(
			'comments' => $comments,
			'model' => $model
		));
	}
	
	public function actionPartners($term) {
		$p = SsnPartner::model()->tableName();
		$u = Yii::app()->user->id;
		$condition = "name LIKE :s AND (
			user_id in (
			select to_user_id
			from $p
			where from_user_id=$u
		) or user_id in (
			select from_user_id
			from $p
			where to_user_id=$u
		))";
		$comps = SsnCompany::model()->findAll(
			$condition, array(
			':s' => "%$term%"
		));

		$data = array();
		foreach ($comps as $comp) {
			$data[] = array(
				'value' => $comp->user_id,
				'name' => $comp->name,
				'address'=>$comp->address,
				'label' => $this->renderPartial('_company', array('model' => $comp), true)
			);
		}
		echo json_encode($data);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id) {
		$model = $this->loadModel($id);
		if (!$model->canView()) {
			throw new CHttpException(403, 'You do not have permission to access this page.');
		}
		
		if (!$model->canEdit()) {
			$model->form_items = $model->items;
			Yii::app()->clientScript->registerScript(time(), "
				$('#ssn-shipping-form').find('input, textarea').prop('disabled', true);
				$('#ssn-shipping-form').find('button,input[type=\"file\"]').remove();
			", CClientScript::POS_READY);
			$this->render('view', array(
				'model' => $model,
			));
			return;
		}
		
		foreach ($model->items as $item) {
			$model->form_items[$item->id] = $item;
		}

		$noItemError = true;
		if (isset($_POST['SsnShippingItem'])) {
			$model->form_items = array();
			foreach($_POST['SsnShippingItem'] as $index => $itemData) {
				$item = SsnShippingItem::model()->findByPk($index);
				if (!$item) {
					$item = new SsnShippingItem('create');
				}
				$item->attributes = $itemData;
				$item->documentFile = CUploadedFile::getInstanceByName("SsnShippingItem[$index][document]");
				$noItemError = $noItemError && $item->validate();
				$model->form_items[$index] = $item;
			}
		}

		if (isset($_POST['SsnShipping'])) {
			$model->attributes = $_POST['SsnShipping'];
			$model->documentFile = CUploadedFile::getInstance($model, 'document');
			
			if ($noItemError && $model->save()) {
				$model->saveDocument();
				$savedIds = array();
				foreach($model->form_items as $item) {
					$item->shipping_id = $model->id;
					$item->save();
					$item->saveDocument();
					$savedIds[] = $item->id;
				}
				if ($savedIds) {
					$s = implode(',', $savedIds);
					SsnShippingItem::model()->safeDeleteAll("id NOT IN ({$s}) AND shipping_id={$model->id}");
				} else {
					SsnShippingItem::model()->safeDeleteAll("shipping_id={$model->id}");
				}
				SendEmail::updateShippingRecipientNotify($model);
				Yii::app()->user->setFlash('success', 'Shipping has been updated successfully.');
				$this->redirect(array('view', 'id'=>$model->id));
			}
		}
		
		$this->render('view', array(
			'model' => $model,
		));

	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() {
		$model = new SsnShipping('create');
		$model->dateLoadText = date('d/m/Y');
		$model->from_user_id = Yii::app()->user->id;
		$model->form_items = array('n0' => new SsnShippingItem('create'));

		$noItemError = true;
		if (isset($_POST['SsnShippingItem'])) {
			foreach($_POST['SsnShippingItem'] as $index => $itemData) {
				$item = SsnShippingItem::model()->findByPk($index);
				if (!$item) {
					$item = new SsnShippingItem('create');
				}
				$item->attributes = $itemData;
				$item->documentFile = CUploadedFile::getInstanceByName("SsnShippingItem[$index][document]");
				$noItemError = $noItemError && $item->validate();
				$model->form_items[$index] = $item;
			}
		}

		if (isset($_POST['SsnShipping'])) {
			$model->attributes = $_POST['SsnShipping'];
			$model->documentFile = CUploadedFile::getInstance($model, 'document');
			
			if ($noItemError && $model->save()) {
				$model->saveDocument();
				foreach($model->form_items as $item) {
					$item->shipping_id = $model->id;
					$item->save();
					$item->saveDocument();
				}
				SendEmail::newShippingRecipientNotify($model);
				Yii::app()->user->setFlash('success', 'Shipping has been created successfully.');
				$this->redirect(array('view', 'id'=>$model->id));
			}
		}


		$this->render('create', array(
			'model' => $model,
		));
	}

	public function actionAddItem() {
		$index = isset($_POST['index']) ? $_POST['index'] : time();
		$item = new SsnShippingItem();
		$this->renderPartial('_shipping-item', array(
			'index' => $index,
			'model' => $item,
		), false, true);
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id) {
		
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id) {
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionAdmin() {
		$dataProvider = new CActiveDataProvider('SsnShipping');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	/**
	 * Received Shippings
	 */
	public function actionIndex() {
		$model = new SsnShippingSearch();
		$model->unsetAttributes();  // clear any default values
		$model->user_id = Yii::app()->user->id;
		$model->mode = SsnShippingSearch::MODE_RECV;

		if (Yii::app()->session['shipping-search']) {
			$model->attributes = Yii::app()->session['shipping-search'];
		}
		
		if (isset($_POST['SsnShippingSearch']))
			$model->attributes = $_POST['SsnShippingSearch'];
		
		Yii::app()->session['shipping-search'] = $model->attributes;
		$this->render('index', array(
			'model' => $model,
		));
	}

	/**
	 * Sent Shippings
	 */
	public function actionSent() {
		$model = new SsnShippingSearch();
		$model->unsetAttributes();  // clear any default values
		$model->user_id = Yii::app()->user->id;
		$model->mode = SsnShippingSearch::MODE_SENT;

		if (Yii::app()->session['shipping-sent-search']) {
			$model->attributes = Yii::app()->session['shipping-sent-search'];
		}
		
		if (isset($_POST['SsnShippingSearch']))
			$model->attributes = $_POST['SsnShippingSearch'];

		Yii::app()->session['shipping-sent-search'] = $model->attributes;
		$this->render('sent', array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return SsnShipping the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id) {
		$model = SsnShipping::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		$model->scenario = 'update';
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param SsnShipping $model the model to be validated
	 */
	protected function performAjaxValidation($model) {
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'ssn-shipping-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}


<?php

class PartnerController extends MemberController {

	public $pluralTitle = 'Partners';
	public $singleTitle = 'Partner';
	public $cannotDelete = array();

	/**
	 * Declares class-based actions.
	 */
	public function accessRules() {
		return array();
	}

	public function actions() {
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page' => array(
				'class' => 'CViewAction',
			),
		);
	}

	public function actionCheckEmailExist() {
		if (isset($_POST['email']) && !empty($_POST['email'])) {
			$user = Users::model()->findByAttributes(array('email' => $_POST['email']));
			if ($user) {
				echo '1';
			} else {
				echo '2';
			}
		}
	}

	public function actionCreate() {
		$model = new SsnPartner('create');
		if (isset($_POST['SsnPartner'])) {
			$model->attributes = $_POST['SsnPartner'];
			$email = trim($_POST['SsnPartner']['user_email']);
			$user = Users::model()->findByAttributes(array('email' => $email));
			$model->status = SsnPartner::STATUS_SEND;
			$model->from_user_id = Yii::app()->user->id;
			$model->to_user_id = $user ? $user->id : 0;
			$model->partner_outsite_system = $user ? '' : $email;
			if ($model->validate()) {
				$model->created_date = date('Y-m-d H:i:s');
				if ($model->save()) {
					SendEmail::partnerPendingRequest($model);
					$this->redirect(Yii::app()->createAbsoluteUrl('member/partner/index'));
				}
			} elseif (!$user) {
				$fromUser = Users::model()->findByPk(Yii::app()->user->id);
				SendEmail::sendInviteRequest(array(
					'from'=>$fromUser->full_name,
					'to'=>$email
				));
				Yii::app()->user->setFlash('success', "An invite email has been sent to $email");
				$this->redirect('create');
			}
		}
		
		$this->pageTitle = 'Make a Request';
		$this->breadcrumbs['Partner'] = array('index');
		$this->breadcrumbs[] = 'Make a Request';
		$this->render('create', array(
			'model' => $model,
		));
	}

	public function actionApprove($status, $id) {
		$partner = SsnPartner::model()->findByPk($id);
		if ($partner) {
			$partner->status = SsnPartner::STATUS_APPROVE;
			$partner->approve_by = Yii::app()->user->id;
			if ($partner->update(array('status'))) {
				SendEmail::partnerApproveRequest($model);
			}

			$this->redirect(Yii::app()->createAbsoluteUrl('member/partner/index'));
		}
	}

	public function actionDelete($id) {
		try {
			if (Yii::app()->request->isPostRequest) {
				// we only allow deletion via POST request
				if (!in_array($id, $this->cannotDelete)) {
					if ($model = $this->loadModel($id)) {
						if ($model->delete())
							Yii::log("Delete record " . print_r($model->attributes, true), 'info');
					}

					// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
					if (!isset($_GET['ajax']))
						$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
				}
			} else {
				Yii::log("Invalid request. Please do not repeat this request again.");
				throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
			}
		} catch (Exception $e) {
			Yii::log("Exception " . print_r($e, true), 'error');
			throw new CHttpException($e);
		}
	}

	public function actionIndex($t = 2) {
		$t_approve = SsnPartner::getTotalPartner(SsnPartner::STATUS_APPROVE);
		$t_pending = SsnPartner::getTotalPartner(SsnPartner::STATUS_PENDING);
		$t_sent = SsnPartner::getTotalPartner(SsnPartner::STATUS_SEND);
		$model = new SsnPartner('search');
		$model->unsetAttributes();
		$view = '_approved';
		$status = 'Approved';
		switch ($t) {
			case SsnPartner::STATUS_APPROVE:
				$view = '_approved';
				$status = 'Approved';
				$model->status = SsnPartner::STATUS_APPROVE;
				break;
			case SsnPartner::STATUS_SEND:
				$view = '_sent';
				$status = 'Sent';
				$model->from_user_id = Yii::app()->user->id;
				$model->status = SsnPartner::STATUS_SEND;
				break;
			case SsnPartner::STATUS_PENDING:
				$view = '_received';
				$status = 'Received';
				$model->to_user_id = Yii::app()->user->id;
				$model->status = SsnPartner::STATUS_SEND;
				break;
		}
		if (isset($_GET['SsnPartner']))
			$model->attributes = $_GET['SsnPartner'];

		$this->pageTitle = 'Partner';
		$this->pluralTitle = 'Partner';
		$this->breadcrumbs[] = 'Partner';
		$this->render('index', array(
			'model' => $model,
			'view' => $view,
			'status' => $status,
			't_approve' => $t_approve,
			't_pending' => $t_pending,
			't_sent' => $t_sent
		));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id);
		if (isset($_POST['SsnPartner'])) {
			$model->attributes = $_POST['SsnPartner'];
			if ($model->save()) {
				$this->setNotifyMessage(NotificationType::Success, $this->singleTitle . ' has been updated');
				$this->redirect(array('view', 'id' => $model->id));
			} else
				$this->setNotifyMessage(NotificationType::Error, $this->singleTitle . ' cannot be updated for some reasons');
		}
		//$model->beforeRender();
		$this->render('update', array(
			'model' => $model,
			'actions' => $this->listActionsCanAccess,
			'title_name' => $model->id));
	}

	public function actionView($id) {
		$this->pageTitle = 'Partner Info';
		try {
			$model = Users::model()->findByPk($id);
			$partner = SsnPartner::getListPartner();
			if (!key_exists($id, $partner))
				throw new CHttpException(404, 'The requested page does not exist.');
			$this->render('view', array(
				'model' => $model,
				'title_name' => $model->id));
		} catch (Exception $exc) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
	}

	public function actionViewCompany($id) {
		$partner = SsnPartner::getListPartner();
		if (!key_exists($id, $partner))
			throw new CHttpException(403, 'Access Denied.');
		
		$user = Users::model()->getInforUser($id);
		$comp = SsnCompany::model()->getCompanyByUser($id);
		
		$this->breadcrumbs[] = 'Partner Info';
		$this->pageTitle = 'Partner Info';
		$this->render('view_company', array(
			'comp' => $comp,
			'user' => $user,
		));
	}

	/*
	 * Bulk delete
	 * If you don't want to delete some specified record please configure it in global $cannotDelete variable
	 */

	public function actionDeleteAll() {
		$deleteItems = $_POST['ssn-partner-grid_c0'];
		$shouldDelete = array_diff($deleteItems, $this->cannotDelete);

		if (!empty($shouldDelete)) {
			SsnPartner::model()->deleteAll('id in (' . implode(',', $shouldDelete) . ')');
			$this->setNotifyMessage(NotificationType::Success, 'Your selected records have been deleted');
		} else
			$this->setNotifyMessage(NotificationType::Error, 'No records was deleted');

		if (!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	public function loadModel($id) {
		//need this define for inherit model case. Form will render parent model name in control if we don't have this line
		$initMode = new SsnPartner();
		$model = $initMode->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

}

<?php

class SampleController extends MemberController {

    public $pluralTitle = 'Samples';
    public $singleTitle = 'Sample';
    public $cannotDelete = array();

	public function actionPartnerTemplate() {
		$p = SsnPartner::model()->tableName();
		$u = Yii::app()->user->id;
		$condition = "user_id in (
			select to_user_id
			from $p
			where from_user_id=$u
		) or user_id in (
			select from_user_id
			from $p
			where to_user_id=$u
		)";
		$comps = SsnCompany::model()->findAll($condition);
		$data = array();
		foreach ($comps as $comp) {
			$data[$comp->id] = $this->renderPartial('_company', array('model' => $comp), true);
		}
		return json_encode($data);
	}
	
	public function actionCreate() {
		$model = new SsnSample('create');
		$model->dateSentText = date('d/m/Y');
		$model->from_user_id = Yii::app()->user->id;
		$model->form_items = array('n0' => new SsnSampleItem('create'));

		$noItemError = true;
		if (isset($_POST['SsnSampleItem'])) {
			foreach($_POST['SsnSampleItem'] as $index => $itemData) {
				$item = SsnSampleItem::model()->findByPk($index);
				if (!$item) {
					$item = new SsnSampleItem('create');
				}
				$item->attributes = $itemData;
				$item->documentFile = CUploadedFile::getInstanceByName("SsnSampleItem[$index][document]");
				$noItemError = $noItemError && $item->validate();
				$model->form_items[$index] = $item;
			}
		}

		if (isset($_POST['SsnSample'])) {
			$model->attributes = $_POST['SsnSample'];
			$model->documentFile = CUploadedFile::getInstance($model, 'document');
			
			if ($noItemError && $model->save()) {
				$model->saveDocument();
				$model->saveReceivers();
				foreach($model->form_items as $item) {
					$item->sample_id = $model->id;
					$item->save();
					$item->saveDocument();
				}
				SendEmail::newSampleRecipientNotify($model);
				Yii::app()->user->setFlash('success', 'Sample has been created successfully.');
				$this->redirect(array('sent'));
			}
		}

		$this->pageTitle = 'New Sample';
		$this->breadcrumbs['Sample'] = array('index');
		$this->breadcrumbs[] = 'Add New';
		$this->render('create', array(
			'model' => $model,
		));
	}
	
    public function actionDelete($id) {
        try {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                if (!in_array($id, $this->cannotDelete)) {
                    if ($model = $this->loadModel($id)) {
                        if ($model->delete())
                            Yii::log("Delete record " . print_r($model->attributes, true), 'info');
                    }

                    // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                    if (!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
                }
            } else {
                Yii::log("Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } catch (Exception $e) {
            Yii::log("Exception " . print_r($e, true), 'error');
            throw new CHttpException($e);
        }
    }

	public function actionIndex() {
		$model = new SsnSampleSearch();
		$model->unsetAttributes();  // clear any default values
		$model->user_id = Yii::app()->user->id;
		$model->mode = SsnSampleSearch::MODE_RECV;

		if (Yii::app()->session['sample-search']) {
			$model->attributes = Yii::app()->session['sample-search'];
		}
		
		if (isset($_POST['SsnSampleSearch']))
			$model->attributes = $_POST['SsnSampleSearch'];

		Yii::app()->session['sample-search'] = $model->attributes;

		$this->pageTitle = 'Sample';
		$this->breadcrumbs[] = 'Sample';
		$this->render('index', array(
			'model' => $model,
		));
	}
	
	public function actionAll() {
		$model = new SsnSampleSearch();
		$model->unsetAttributes();  // clear any default values
		$model->user_id = Yii::app()->user->id;
		$model->mode = SsnSampleSearch::MODE_ALL;

		if (Yii::app()->session['sample-search-all']) {
			$model->attributes = Yii::app()->session['sample-search-all'];
		}
		
		if (isset($_POST['SsnSampleSearch']))
			$model->attributes = $_POST['SsnSampleSearch'];

		Yii::app()->session['sample-search'] = $model->attributes;
		
		$this->pageTitle = 'Sample';
		$this->breadcrumbs[] = 'Sample';
		$this->render('all', array(
			'model' => $model,
		));
	}


	/**
	 * Sent Samples
	 */
	public function actionSent() {
		$model = new SsnSampleSearch();
		$model->unsetAttributes();  // clear any default values
		$model->user_id = Yii::app()->user->id;
		$model->mode = SsnSampleSearch::MODE_SENT;

		if (Yii::app()->session['sample-sent-search']) {
			$model->attributes = Yii::app()->session['sample-sent-search'];
		}
		
		if (isset($_POST['SsnSampleSearch']))
			$model->attributes = $_POST['SsnSampleSearch'];

		Yii::app()->session['sample-sent-search'] = $model->attributes;
		$this->pageTitle = 'Sample';
		$this->breadcrumbs[] = 'Sample';
		$this->render('sent', array(
			'model' => $model,
		));
	}

    public function actionSearchTable(){
        $model = new SsnSample('search');
        $model->unsetAttributes();  // clear any default values
        if($_GET['sample_type_search'] == SsnSample::STATUS_SEND)
            $model->from_user_id = Yii::app()->user->id;
        else
            $model->to_user_id = Yii::app()->user->id;
        if (isset($_GET['SsnSample']))
            $model->attributes = $_GET['SsnSample'];
        $this->renderPartial('_table_item', array('data' => $model->searchSample(), 't' => $_GET['sample_type_search']));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        if (isset($_POST['SsnSample'])) {
            $model->attributes = $_POST['SsnSample'];
            if ($model->save()) {
                $this->setNotifyMessage(NotificationType::Success, $this->singleTitle . ' has been updated');
                $this->redirect(array('view', 'id' => $model->id));
            } else
                $this->setNotifyMessage(NotificationType::Error, $this->singleTitle . ' cannot be updated for some reasons');
        }
		
		$this->pageTitle = 'Sample Detail';
		$this->breadcrumbs['Sample'] = array('index');
		$this->breadcrumbs[] = 'Sample Detail';
        $this->render('update', array(
            'model' => $model,
            'actions' => $this->listActionsCanAccess,
            'title_name' => $model->id));
    }

	public function actionView($id) {
		$this->pageTitle = 'Sample Detail';
		$this->breadcrumbs['Sample'] = array('index');
		$this->breadcrumbs[] = 'Sample Detail';
		
		$model = $this->loadModel($id);
		if (!$model->canView()) {
			throw new CHttpException(403, 'You do not have permission to access this page.');
		}
		
		if (!$model->canEdit()) {
			$model->form_items = $model->items;
			$this->render('view', array(
				'model' => $model,
			));
			return;
		}
		
		foreach ($model->items as $item) {
			$model->form_items[$item->id] = $item;
		}

		$noItemError = true;
		if (isset($_POST['SsnSampleItem'])) {
			$model->form_items = array();
			foreach($_POST['SsnSampleItem'] as $index => $itemData) {
				$item = SsnSampleItem::model()->findByPk($index);
				if (!$item) {
					$item = new SsnSampleItem('create');
				}
				$item->attributes = $itemData;
				$item->documentFile = CUploadedFile::getInstanceByName("SsnSampleItem[$index][document]");
				$noItemError = $noItemError && $item->validate();
				$model->form_items[$index] = $item;
			}
		}

		if (isset($_POST['SsnSample'])) {
			$model->attributes = $_POST['SsnSample'];
			$model->documentFile = CUploadedFile::getInstance($model, 'document');
			
			if ($noItemError && $model->save()) {
				$model->saveDocument();
				$model->saveReceivers();
				$savedIds = array();
				foreach($model->form_items as $item) {
					$item->sample_id = $model->id;
					$item->save();
					$item->saveDocument();
					$savedIds[] = $item->id;
				}
				if ($savedIds) {
					$s = implode(',', $savedIds);
					SsnSampleItem::model()->safeDeleteAll("id NOT IN ({$s}) AND sample_id={$model->id}");
				} else {
					SsnSampleItem::model()->safeDeleteAll("sample_id={$model->id}");
				}
				Yii::app()->user->setFlash('success', 'Sample has been updated successfully.');
				SendEmail::updateSampleRecipientNotify($model);
				$this->redirect(array('sent'));
			}
		}
		
		$this->render('update', array(
			'model' => $model,
		));
	}
	
    public function actionComment($id){
		$model = $this->loadModel($id);
		if (!$model->canView())
			throw new CHttpException(404, 'The requested page does not exist.');
		
		if (isset($_POST['message'])) {
			$comment = new SsnSampleComment();
			$comment->comment = $_POST['message'];
			$comment->user_id = Yii::app()->user->id;
			$comment->sample_id = $model->id;
			$comment->save();
			Yii::app()->user->setFlash('success', 'Your comment has been added.');
			$this->redirect(array('comment', 'id'=>$model->id));
		}
			
		$comments = SsnSampleComment::model()->findBySample($id);
		$this->render('comment', array(
			'comments' => $comments,
			'model' => $model
		));
    }
    
    public function actionInsertComment(){
        $comment = new SsnSampleComment();
        $comment->sample_id = $_POST['sampleId'];
        $comment->user_id = Yii::app()->user->id;
        $comment->comment = $_POST['comment'];
        $comment->created_date = date('Y-m-d H:i:s');
        echo $comment->save() ? 1:0;
    }

	/*
     * Bulk delete
     * If you don't want to delete some specified record please configure it in global $cannotDelete variable
     */

    public function actionDeleteAll() {
        $deleteItems = $_POST['ssn-sample-grid_c0'];
        $shouldDelete = array_diff($deleteItems, $this->cannotDelete);

        if (!empty($shouldDelete)) {
            SsnSample::model()->deleteAll('id in (' . implode(',', $shouldDelete) . ')');
            $this->setNotifyMessage(NotificationType::Success, 'Your selected records have been deleted');
        } else
            $this->setNotifyMessage(NotificationType::Error, 'No records was deleted');

        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    /*
	 * @return SsnSample
	 */
	public function loadModel($id) {
        //need this define for inherit model case. Form will render parent model name in control if we don't have this line
        $model = SsnSample::model()->findByPk($id);
		$model->scenario = 'update';
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
    
	public function actionAddItem() {
		$index = isset($_POST['index']) ? $_POST['index'] : time();
		$item = new SsnSampleItem();
		$this->renderPartial('_sample-item', array(
			'index' => $index,
			'model' => $item,
		), false, true);
	}


}

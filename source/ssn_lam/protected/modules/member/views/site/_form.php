<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-model-form',
	'enableAjaxValidation'=>false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('role' => 'form', 'enctype' => 'multipart/form-data'),
)); ?>

<h3>Company Information</h3>
<div class="form-group">
	<?php echo $form->labelEx($comp, 'image', array('class' => '')); ?>
	
	<?php if (!$comp->isNewRecord && $comp->is_img_active == 1) : ?>
		<?php if (!empty($comp->image)): ?>
			<div class="thumbnail" id="thumbnail-<?php echo $comp->id; ?>">
				<?php echo CHtml::image($comp->getLogoUrl(), '', array()); ?>
			</div>
		<?php endif; ?>
	<?php endif; ?>
	<?php if($comp->is_img_active == 0):?>
		<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo.png" alt="Blocked Image"/>
	<?php else:?>
	<?php echo $form->fileField($comp, 'image', array('title' => "Upload " . $comp->getAttributeLabel('image'))); ?>
	<div class='notes'>Recommended Size: <?php echo $comp->imageSize ?> (width x height), Allow file type  <?php echo '*.' . str_replace(',', ', *.', $comp->allowImageType); ?> - Maximum file size : <?php echo ($comp->maxImageFileSize / 1024) / 1024; ?>M </div>
	<?php echo $form->error($comp, 'image'); ?>
	<?php endif;?>
</div>
<div class="form-group">
	<?php echo $form->labelEx($comp,'name',array()); ?>
	<?php echo $form->textField($comp,'name',array('class' => 'form-control')); ?>
	<?php echo $form->error($comp,'name'); ?>
</div>
<div class="form-group">
	<?php echo $form->labelEx($comp,'address', array('class' => '')); ?>
	<?php echo $form->textField($comp,'address',array('class' => 'form-control')); ?>
	<?php echo $form->error($comp,'address'); ?>
</div>
<div class="form-group">
	<?php echo $form->labelEx($comp,'area_code_id', array('class' => '')); ?>
	<?php echo $form->dropDownList($comp,'area_code_id', AreaCode::loadArrArea(),array('class' => 'form-control')); ?>
	<?php echo $form->error($comp,'area_code_id'); ?>
</div>
<div class="form-group">
	<?php echo $form->labelEx($comp,'city_id', array('class' => '')); ?>
	<?php echo $form->dropDownList($comp,'city_id', array(),array('class' => 'form-control')); ?>
	<?php echo $form->error($comp,'city_id'); ?>
</div>
<div class="form-group">
	<?php echo $form->labelEx($comp,'website', array('class' => '')); ?>
	<?php echo $form->textField($comp,'website',array('class' => 'form-control')); ?>
</div>
<div class="form-group">
	<?php echo $form->labelEx($comp,'phone', array('class' => '')); ?>
	<?php echo $form->textField($comp,'phone',array('class' => 'form-control')); ?>
</div>
<div class="form-group">
	<?php echo $form->labelEx($comp,'size', array('class' => '')); ?>
	<?php echo $form->textField($comp,'size',array('class' => 'form-control')); ?>
</div>
<div class="form-group">
	<?php echo $form->labelEx($comp,'about', array('class' => '')); ?>
	<?php echo $form->textArea($comp,'about',array('class' => 'form-control')); ?>
</div>
<br/>

<h3>Admin Information</h3>
<div class="form-group">
	<?php echo $form->labelEx($user,'first_name',array('label'=>'First Name', 'class' => '')); ?>
	<?php echo $form->textField($user,'first_name',array('class' => 'form-control')); ?>
	<?php echo $form->error($user,'first_name'); ?>
</div>
<div class="form-group">
	<?php echo $form->labelEx($user,'last_name',array('label'=>'Last Name', 'class' => '')); ?>
	<?php echo $form->textField($user,'last_name',array('class' => 'form-control')); ?>
	<?php echo $form->error($user,'last_name'); ?>
</div>
<div class="form-group">
	<a href="javascript:;" class="changePassword" data-id="0">Change Password</a>
</div>
<div class="psw hidden">
	<div class="form-group">
		<?php echo $form->labelEx($user,'newPassword', array('class' => '')); ?>
		<?php echo $form->passwordField($user,'newPassword',array('class' => 'form-control')); ?>
		<?php echo $form->error($user,'newPassword'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($user,'password_confirm', array('class' => '')); ?>
		<?php echo $form->passwordField($user,'password_confirm',array('maxlength'=>30, 'class' => 'form-control')); ?>
		<?php echo $form->error($user,'password_confirm'); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($user,'email', array('class' => '')); ?>
	<?php echo $form->textField($user,'email',array('class' => 'form-control', 'disabled' => true)); ?>
	<?php echo $form->error($user,'email'); ?>
</div>

<div class="form-group">
	<?php echo $form->labelEx($user,'phone_public', array('label'=>'CellPhone', 'class' => '')); ?>
	<?php echo $form->textField($user,'phone_public',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
	<?php echo $form->error($user,'phone_public'); ?>
</div>
<div class="form-group">
	<?php echo $form->labelEx($user,'phone_private', array('label'=>'Phone', 'class' => '')); ?>
	<?php echo $form->textField($user,'phone_private',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
	<?php echo $form->error($user,'phone_private'); ?>
</div>
<div class="form-group">
	<?php echo $form->labelEx($user,'skype', array('label'=>'Skype', 'class' => '')); ?>
	<?php echo $form->textField($user,'skype',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
	<?php echo $form->error($user,'skype'); ?>
</div>
<div class="form-group">
	<?php echo $form->labelEx($user,'whatapps', array('label'=>'Whatapps', 'class' => '')); ?>
	<?php echo $form->textField($user,'whatapps',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
	<?php echo $form->error($user,'whatapps'); ?>
</div>
<div class="form-group">
	<?php echo $form->labelEx($user,'line', array('label'=>'Line', 'class' => '')); ?>
	<?php echo $form->textField($user,'line',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
	<?php echo $form->error($user,'line'); ?>
</div>
<div class="form-group">
	<?php echo $form->labelEx($user,'viber', array('label'=>'Viber', 'class' => '')); ?>
	<?php echo $form->textField($user,'viber',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
	<?php echo $form->error($user,'viber'); ?>
</div>
<div class="form-group">
	<?php echo $form->labelEx($user,'wechat', array('label'=>'Wechat', 'class' => '')); ?>
	<?php echo $form->textField($user,'wechat',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
	<?php echo $form->error($user,'wechat'); ?>
</div>

<div class="form-group">
	<input type="submit" name="yt0" value="Save" class="btn btn-primary">
</div>
<?php $this->endWidget(); ?>

<script>
    $(document).ready(function() {
       $('.changePassword').on('click', function() {
           if ($(this).text()=='Change password') {
               $('.psw').removeClass('hidden');
               $('#Users_newPassword').prop('disabled', false);
               $('#Users_password_confirm').prop('disabled', false);
               $(this).text('Unchange password');
           } else {
               $('.psw').addClass('hidden');
               $('#Users_newPassword').prop('disabled', true);
               $('#Users_password_confirm').prop('disabled', true);
               $(this).text('Change password');
           }
       });

   });
   
   $(window).load(function() {
       $('.changePassword').trigger('click');
       getModule();
   });

   $("#SsnCompany_area_code_id").change(getModule);

   function getModule() {
       var url = "<?php echo $this->createUrl('getListByArea') ?>";
       var request = $.ajax({
           type: "post",
           url: url,
           data: {
               module: $("#SsnCompany_area_code_id").val(),
               selected: ''
           }
       }).done(function(msg) {
           $("#SsnCompany_city_id").html(msg);
       });
   }	
</script>

<style>
.box-body h3 {
	margin-top: 0px
}
</style>
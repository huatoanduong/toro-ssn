<section class="content-header">
    <h1>
        Partner Info
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo Yii::app()->createAbsoluteUrl('member/partner/index')?>">Partner</a></li>
        <li class="active">Partner Info</li>
    </ol>
</section>
<section class="content">

    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="<?php echo Yii::app()->createAbsoluteUrl('/member/partner/view', array('id' => $_GET['id']));?>" class="tab" >Admin Info</a></li>
                    <li class=""><a href="<?php echo Yii::app()->createAbsoluteUrl('/member/partner/viewCompany', array('id' => $_GET['id']));?>" class="tab" >Company</a></li>
                </ul>
                <div class="tab-content">
                    <table class="table table-hover">
                        <tr>
                            <td style="width: 15%;">Full name</td>
                            <td><?php echo $model->full_name?></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td><?php echo $model->email?></td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td><?php echo $model->address?></td>
                        </tr>
                        <tr>
                            <td>Cellphone</td>
                            <td><?php echo $model->phone_public?></td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td><?php echo $model->phone_private?></td>
                        </tr>
                        <tr>
                            <td>Skype</td>
                            <td><?php echo $model->skype?></td>
                        </tr>
                        <tr>
                            <td>Whatapps</td>
                            <td><?php echo $model->whatapps?></td>
                        </tr>
                        <tr>
                            <td>Line</td>
                            <td><?php echo $model->line?></td>
                        </tr>
                        <tr>
                            <td>Viber</td>
                            <td><?php echo $model->viber?></td>
                        </tr>
                        <tr>
                            <td>Wechat</td>
                            <td><?php echo $model->wechat?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
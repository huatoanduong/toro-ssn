<?php $f = Yii::app()->format ?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-model-form',
	'enableAjaxValidation'=>false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('role' => 'form', 'enctype' => 'multipart/form-data'),
)); ?>

<h3>Company Information</h3>
<div class="form-group">
	<?php echo $form->labelEx($comp, 'image', array('class' => 'control-label')); ?>
	<div>
	<?php if($comp->is_img_active == 0):?>
		<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo.png" alt="Blocked Image"/>
	<?php else:?>
		<?php echo CHtml::image(ImageHelper::getImageUrl($comp, "image", "large"), '', array()); ?>
	<?php endif;?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($comp,'name',array()); ?>
	<p class="control-stastic"><?= InputHelper::display($comp->name) ?></p>
</div>
<div class="form-group">
	<?php echo $form->labelEx($comp,'address', array('class' => '')); ?>
	<p class="control-stastic"><?= InputHelper::display($comp->address) ?></p>
</div>
<div class="form-group">
	<?php echo $form->labelEx($comp,'area_code_id', array('class' => '')); ?>
	<p class="control-stastic"><?= $f->formatEnum($comp->area_code_id, AreaCode::loadArrArea()); ?></p>
</div>
<div class="form-group">
	<?php echo $form->labelEx($comp,'city_id', array('class' => '')); ?>
	<p class="control-stastic"><?= $f->formatEnum($comp->city_id, array()); ?></p>
</div>
<div class="form-group">
	<?php echo $form->labelEx($comp,'website', array('class' => '')); ?>
	<p class="control-stastic"><?= InputHelper::display($comp->website) ?></p>
</div>
<div class="form-group">
	<?php echo $form->labelEx($comp,'phone', array('class' => '')); ?>
	<p class="control-stastic"><?= InputHelper::display($comp->phone) ?></p>
</div>
<div class="form-group">
	<?php echo $form->labelEx($comp,'size', array('class' => '')); ?>
	<p class="control-stastic"><?= InputHelper::display($comp->size) ?></p>
</div>
<div class="form-group">
	<?php echo $form->labelEx($comp,'about', array('class' => '')); ?>
	<p class="control-stastic"><?= InputHelper::display($comp->about) ?></p>
</div>
<br/>


<h3>Admin Information</h3>
<div class="form-group">
	<?php echo $form->labelEx($user,'full_name',array('class' => '')); ?>
	<p class="control-stastic"><?= InputHelper::display($user->full_name) ?></p>
</div>
<div class="form-group">
	<?php echo $form->labelEx($user,'email', array('class' => '')); ?>
	<p class="control-stastic"><?= InputHelper::display($user->email) ?></p>
</div>

<div class="form-group">
	<?php echo $form->labelEx($user,'phone_public', array('label'=>'CellPhone', 'class' => '')); ?>
	<p class="control-stastic"><?= InputHelper::display($user->phone_public) ?></p>
</div>
<div class="form-group">
	<?php echo $form->labelEx($user,'phone_private', array('label'=>'Phone', 'class' => '')); ?>
	<p class="control-stastic"><?= InputHelper::display($user->phone_private) ?></p>
</div>
<div class="form-group">
	<?php echo $form->labelEx($user,'skype', array('label'=>'Skype', 'class' => '')); ?>
	<p class="control-stastic"><?= InputHelper::display($user->skype) ?></p>
</div>
<div class="form-group">
	<?php echo $form->labelEx($user,'whatapps', array('label'=>'Whatapps', 'class' => '')); ?>
	<p class="control-stastic"><?= InputHelper::display($user->whatapps) ?></p>
</div>
<div class="form-group">
	<?php echo $form->labelEx($user,'line', array('label'=>'Line', 'class' => '')); ?>
	<p class="control-stastic"><?= InputHelper::display($user->line) ?></p>
</div>
<div class="form-group">
	<?php echo $form->labelEx($user,'viber', array('label'=>'Viber', 'class' => '')); ?>
	<p class="control-stastic"><?= InputHelper::display($user->viber) ?></p>
</div>
<div class="form-group">
	<?php echo $form->labelEx($user,'wechat', array('label'=>'Wechat', 'class' => '')); ?>
	<p class="control-stastic"><?= InputHelper::display($user->wechat) ?></p>
</div>
<?php $this->endWidget(); ?>

<style>
.box-body h3 {
	margin-top: 0px
}
</style>
<?php
/* @var $model SsnShippingItem */
/* @var $index int */

$form= new CActiveForm;
$form->enableClientValidation = true;
?>
<div class="item">
	<h4>Item <?= str_replace('n', '', $index) + 1 ?></h4>
	<div class="row form-group">
		<div class="col-sm-12">
			<?= $form->textField($model, 'name', array(
				'name'=>"SsnShippingItem[$index][name]",
				'class'=>'form-control',
				'placeHolder'=>'Name'
			)) ?>
			<?= $form->error($model, 'name', array('id'=>"SsnShippingItem_{$index}_name_em_")) ?>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-sm-9">
			<?= $form->textField($model, 'color', array(
				'name'=>"SsnShippingItem[$index][color]",
				'class'=>'form-control',
				'placeHolder'=>'Color'
			)) ?>
			<?= $form->error($model, 'color', array('id'=>"SsnShippingItem_{$index}_color_em_")) ?>
		</div>
		<div class="col-sm-3">
			<?= $form->textField($model, 'quantity', array(
				'name'=>"SsnShippingItem[$index][quantity]",
				'class'=>'form-control',
				'placeHolder'=>'Quantity'
			)) ?>
			<?= $form->error($model, 'quantity', array('id'=>"SsnShippingItem_{$index}_quantity_em_")) ?>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-sm-8">
			<?= $form->fileField($model, 'document', array(
				'name'=>"SsnShippingItem[$index][document]",
			)) ?>
			<?= $form->error($model, 'document', array('id'=>"SsnShippingItem_{$index}_document_em_")) ?>
			<?php if ($url = $model->getDocumentDownloadUrl()): ?>
			<a href="<?= $url ?>"><?= $model->document ?></a> 
			<?php else: ?>
			<p class="form-control-static">No document uploaded.</p>
			<?php endif ?>
		</div>
		<div class="col-sm-4">
			<button class="btn btn-danger pull-right btn-remove" type="button">Remove</button>
		</div>
	</div>
</div>

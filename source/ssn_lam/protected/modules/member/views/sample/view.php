<div class="nav-tabs-custom">
	<ul class="nav nav-tabs">
		<li class="active"><a href="javascript:void(0)">Detail</a></li>
		<!--<li><a href="<?= $this->createUrl('comment', array('id'=>$model->id)) ?>">View Comment</a></li>-->
	</ul>
	<div class="tab-content">
		<div class="tab-pane active">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'ssn-sample-form',
			'enableAjaxValidation'=>false,
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
			),
			'htmlOptions' => array(
				'role' => 'form', 
				'enctype' => 'multipart/form-data'
			),
		)); ?>
			<div class="box-body">
				<div class="form-group">
					<?php echo $form->labelEx($model,'dateSentText', array('class'=>'control-label')); ?>
					<p class="form-control-static"><?= $model->dateSentText ?></p>
				</div>

				<?php if ($model->isRecipient()): ?>
				<!--send from-->
				<div class="form-group">
					<?php echo $form->labelEx($model,'from_user_id', array('class'=>'control-label')); ?>
					<p class="form-control-static">
						<a href="<?= $this->createUrl('partner/viewCompany', array('id'=> $model->from_user_id)) ?>"
							class="notranslate"><?= $model->sender->company->name ?></a>
					</p>
				</div>
				<?php endif ?>

				<div class="form-group">
					<?php echo $form->labelEx($model,'attention', array('class'=>'control-label')); ?>
					<p class="form-control-static"><?= InputHelper::display($model->attention) ?></p>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<?php echo $form->labelEx($model,'awbn', array('class'=>'control-label')); ?>
							<?php if ($model->getTrackingLink()): ?>
							<p class="form-control-static"><a href="<?= $model->getTrackingLink() ?>" target="_blank"><?= $model->awbn ?></a></p>
							<?php else: ?>
							<p class="form-control-static"><?= InputHelper::display($model->awbn) ?></p>
							<?php endif ?>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<?php echo $form->labelEx($model,'courier_id', array('class'=>'control-label')); ?>
							<p class="form-control-static"><?= $model->getCourierName() ?></p>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label">Item</label>
					<div class="table-responsive">
						<table class="item-list table table-bordered table-hover">
							<thead>
								<tr>
									<th>Name</th>
									<th>Color</th>
									<th>Qty</th>
									<th>Unit</th>
									<th>Document</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($model->form_items as $index => $item): ?>
								<tr>
									<td><?= $item->name ?></td>
									<td><?= $item->color ?></td>
									<td><?= $item->quantity ?></td>
									<td><?= $item->unit ?></td>
									<td>
										<?php if ($url = $item->getDocumentDownloadUrl()): ?>
										<a href="<?= $url ?>"><?= $item->document ?></a> 
										<?php else: ?>
										No document uploaded.
										<?php endif ?>
									</td>
								</tr>
								<?php endforeach ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="form-group">
					<?php echo $form->labelEx($model,'remark', array('class'=>'control-label')); ?>
					<p class="form-control-static"><?= InputHelper::display($model->remark) ?></p>
				</div>
				<div class="form-group">
					<?php echo $form->labelEx($model,'document', array('class'=>'control-label')); ?>
					<?php if ($url = $model->getDocumentDownloadUrl()): ?>
					<p><a href="<?= $url ?>" class="notranslate"><?= $model->document ?></a></p>
					<?php else: ?>
					<p class="form-control-static"><em>No document uploaded.</em></p>
					<?php endif ?>
				</div>	
			</div>
		<?php $this->endWidget(); ?>
		</div><!-- /.tab-pane -->
	</div>
	<!-- /.tab-content -->
</div>
<!-- nav-tabs-custom -->


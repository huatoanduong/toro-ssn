<?php
?>
<div class="nav-tabs-custom">
	<ul class="nav nav-tabs">
		<li class="active"><a href="javascript:void(0)">Detail</a></li>
		<!--<li><a href="<?= $this->createUrl('comment', array('id'=>$model->id)) ?>">View Comment</a></li>-->
	</ul>
	<div class="tab-content">
		<div class="tab-pane active">
			<?php $this->renderPartial('_form', array('model'=>$model)) ?>
		</div><!-- /.tab-pane -->
	</div>
	<!-- /.tab-content -->
</div>
<!-- nav-tabs-custom -->


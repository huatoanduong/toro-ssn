<?php
/* @var $this SampleController */
/* @var $model SsnSample */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerScriptFile(
	Yii::app()->theme->baseUrl.'/select2/select2.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(
	Yii::app()->theme->baseUrl.'/select2/select2.css');

$addItemUrl = $this->createUrl('addItem');
$autoCompleteUrl = $this->createUrl('partners');
Yii::app()->clientScript->registerScript(time(), "
	ssn.setupSampleDetailPage('{$addItemUrl}',{$this->actionPartnerTemplate()});
", CClientScript::POS_LOAD);
?>
<?php $this->widget('application.components.widget.Notification'); ?>				

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ssn-sample-form',
	'enableAjaxValidation'=>false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'validateOnChange'=>false,
	),
	'htmlOptions' => array(
		'role' => 'form', 
		'enctype' => 'multipart/form-data'
	),
)); ?>
	<div class="box-body">
		<div class="form-group">
			<?php echo $form->labelEx($model,'dateSentText', array('class'=>'control-label')); ?>
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'model' => $model,
				'attribute' => 'dateSentText',
				'options' => array(
					'dateFormat' => 'dd/mm/yy',
				),
				'htmlOptions'=>array(
					'class'=>'form-control'
				),
			)); ?>
			<?php echo $form->error($model,'dateSentText'); ?>
		</div>
		
		<?php if ($model->isRecipient()): ?>
		<!--send from-->
		<div class="form-group">
			<?php echo $form->labelEx($model,'from_user_id', array('class'=>'control-label')); ?>
			<p class="form-control-static">
				<a href="<?= $this->createUrl('partner/view', array('id'=> $model->from_user_id)) ?>"
				    class="notranslate"><?= $model->sender->company->name ?></a>
			</p>
		</div>
		<?php endif ?>
		
		<?php if ($model->isSender()): ?>
		<!--send to-->
		<div class="form-group">
			<?php echo $form->labelEx($model,'to_user_ids', array('class'=>'control-label')); ?>
			<div class="">
				<?php echo $form->dropDownList($model, 'to_user_ids', 
					SsnPartner::getPartnerListData(Yii::app()->user->id), 
					array(
						'multiple'=>true, 
						'class'=>'company-select form-control',
						'style'=>'width: 100%'
					)
				); ?>
			</div>
			<?php echo $form->error($model,'to_user_ids'); ?>
		</div>
		<?php endif ?>
		
		<div class="form-group">
			<?php echo $form->labelEx($model,'attention', array('class'=>'control-label')); ?>
			<?php echo $form->textField($model,'attention',array('class'=>'form-control','maxlength'=>255)); ?>
			<?php echo $form->error($model,'attention'); ?>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<?php echo $form->labelEx($model,'awbn', array('class'=>'control-label')); ?>
					<?php echo $form->textField($model,'awbn',array('class'=>'form-control','maxlength'=>255)); ?>
					<?php echo $form->error($model,'awbn'); ?>
					<?php if ($model->getTrackingLink()): ?>
					<p><a href="<?= $model->getTrackingLink() ?>" target="_blank">Go to tracking page</a></p>
					<?php endif ?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<?php echo $form->labelEx($model,'courier_id', array('class'=>'control-label')); ?>
					<?php echo $form->dropDownList($model, 'courier_id', SsnCourier::getListData(),
						array('class' => 'form-control', 'empty'=>'Other')); ?>
					<?php echo $form->error($model,'courier_id'); ?>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="" class="control-label">Item</label>
			<div class="table-responsive">
				<table class="item-list table table-bordered table-hover">
					<thead>
						<tr>
							<th>Name</th>
							<th>Color</th>
							<th>Qty</th>
							<th>Unit</th>
							<th>Document</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					<?php foreach($model->form_items as $index => $item): ?>
						<?php $this->renderPartial('_sample-item', array(
							'index' => $index,
							'model' => $item,
						)) ?>
					<?php endforeach ?>
					</tbody>
				</table>
			</div>
			<p><button type="button" class="btn btn-default btn-add">Add item</button></p>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'remark', array('class'=>'control-label')); ?>
			<?php echo $form->textArea($model,'remark',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'remark'); ?>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'document', array('class'=>'control-label')); ?>
			<?php echo $form->fileField($model, 'document'); ?>
			<?php echo $form->error($model, 'document'); ?>
			<?php if ($url = $model->getDocumentDownloadUrl()): ?>
			<p><a href="<?= $url ?>" class="notranslate"><?= $model->document ?></a></p>
			<?php else: ?>
			<p class="form-control-static">No document uploaded.</p>
			<?php endif ?>
		</div>	
	</div>
	<!-- /.box-body -->

	<div class="box-footer">
		<button type="submit" class="btn btn-primary"><?php echo $model->isNewRecord ? 'Create' : 'Update' ?></button>
	</div>

<?php $this->endWidget(); ?>

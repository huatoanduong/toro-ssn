<?php
/* @var $this ShippingController */
/* @var $model SsnShipping */
?>
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">Information</h3>
	</div>
	<!-- /.box-header -->
	<!-- form start -->
	<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>
<!-- /.box -->
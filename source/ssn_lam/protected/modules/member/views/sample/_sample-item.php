<?php
/* @var $model SsnSampleItem */
/* @var $index int */

$form= new CActiveForm;
$form->enableClientValidation = true;
?>
<tr class="item">
	<td>
		<h4 class="hide">Item <?= str_replace('n', '', $index) + 1 ?></h4>
		<?= $form->textField($model, 'name', array(
			'name'=>"SsnSampleItem[$index][name]",
			'class'=>'form-control input-sm',
			'placeHolder'=>'Name'
		)) ?>
		<?= $form->error($model, 'name', array('id'=>"SsnSampleItem_{$index}_name_em_")) ?>
	</td>
	<td>
		<?= $form->textField($model, 'color', array(
			'name'=>"SsnSampleItem[$index][color]",
			'class'=>'form-control input-sm',
			'placeHolder'=>'Color'
		)) ?>
		<?= $form->error($model, 'color', array('id'=>"SsnSampleItem_{$index}_color_em_")) ?>
	</td>
	<td>
		<?= $form->textField($model, 'quantity', array(
			'name'=>"SsnSampleItem[$index][quantity]",
			'class'=>'form-control input-sm qty',
			'placeHolder'=>'Quantity'
		)) ?>
		<?= $form->error($model, 'quantity', array('id'=>"SsnSampleItem_{$index}_quantity_em_")) ?>
	</td>
	<td>
		<?= $form->dropDownList($model, 'unit', SsnSampleItem::getUnitList(), array(
			'name'=>"SsnSampleItem[$index][unit]",
			'class'=>'form-control input-sm',
			'empty'=>'N/A',
		)) ?>
		<?= $form->error($model, 'unit', array('id'=>"SsnSampleItem_{$index}_unit_em_")) ?>
	</td>
	<td>
		<?= $form->fileField($model, 'document', array(
			'name'=>"SsnSampleItem[$index][document]",
		)) ?>
		<?= $form->error($model, 'document', array('id'=>"SsnSampleItem_{$index}_document_em_")) ?>
		<?php if ($url = $model->getDocumentDownloadUrl()): ?>
		<a href="<?= $url ?>"><?= $model->document ?></a> 
		<?php endif ?>
	</td>
	<td>
		<button class="btn btn-danger btn-xs btn-remove" type="button">Remove</button>
	</td>
</tr>

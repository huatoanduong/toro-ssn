<div class="item item-<?php echo $key?> col-md-12 clearfix">
    <h4 class="item-title">Item <?php echo $key+1?></h4>
    <div class="form-group col-md-10" style="padding: 0 15px">
        <?php echo $form->textField($item, "[$key]name", array('class' => 'form-control', 'maxlength' => 255, 'placeholder' => 'Name')); ?>
        <?php echo $form->error($item, "[$key]name"); ?>
    </div>
    <div class="form-group col-md-1">
        &nbsp;
    </div>
    <div class="form-group col-md-3">
        <?php echo $form->dropDownList($item, "[$key]unit", SsnSampleItem::unit(),array('class' => 'form-control', 'maxlength' => 255, 'placeholder' => 'Color')); ?>
        <?php echo $form->error($item, "[$key]unit"); ?>
    </div>
    <div class="form-group col-md-10">
        <?php echo $form->textField($item, "[$key]color", array('class' => 'form-control', 'maxlength' => 255, 'placeholder' => 'Color')); ?>
        <?php echo $form->error($item, "[$key]color"); ?>
    </div>
    <div class="form-group col-md-1">
        &nbsp;
    </div>
    <div class="form-group col-md-3">
        <?php echo $form->textField($item, "[$key]quantity", array('class' => 'form-control', 'maxlength' => 255, 'placeholder' => 'Quantity')); ?>
        <?php echo $form->error($item, "[$key]quantity"); ?>
    </div>
    <div class="form-group col-md-12">
        <?php echo $form->labelEx($item, "document", array('class' => '')); ?>
            <?php if (!$item->isNewRecord) : ?>
                <?php if (!empty($item->document)): ?>
                    <div id="del-file-<?php echo $item->id ?>">
                            <?php echo $item->showUploadFile($item); ?>
<!--                            - 
                            <a href="javascript:;" id="del-file" data-id="<?php echo $item->id; ?>" style="color: red;">delete</a>-->
                    </div><br>
                <?php endif; ?>
        <?php endif; ?>
        
        <?php echo $form->fileField($item, "[$key]document", array('title' => "Upload " . $item->getAttributeLabel('document'))); ?>
        
        <div class='notes'>Allow file type  <?php echo '*.' . str_replace(',', ', *.', $item->allowUploadType); ?> - Maximum file size : <?php echo ($item->maxUploadFileSize / 1024) / 1024; ?>M </div>
		<div style="display: none;" class="errorMessage"
			 id="SsnSampleItem_document_em_<?php echo $key ?>"></div>
    </div>
    <?php if($key != 0):?>
        <div class="form-group col-md-12">
            <a href="javascript:;" class="btn btn-warning remove-item" data-item-id="<?php echo $key;?>" style="margin-top: 10px;">Remove</a>
        </div>
    <?php endif;?>
</div>

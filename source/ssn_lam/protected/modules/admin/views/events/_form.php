<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><span class="<?php echo $model->isNewRecord ? $this->iconCreate : $this->iconEdit; ?>"></span> <?php echo $model->isNewRecord ? 'Create' : 'Update'; ?> <?php echo $this->singleTitle ?></h3>
    </div>
    <div class="panel-body">
        <div class="form">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'tb-events-form',
                'enableAjaxValidation' => false,
                'htmlOptions' => array('class' => 'form-horizontal', 'role' => 'form', 'enctype' => 'multipart/form-data'),
            ));
            ?>
            <div class='form-group form-group-sm'>
                <?php echo $form->labelEx($model, 'title', array('label' => 'Training Name', 'class' => 'col-sm-1 control-label')); ?>
                <div class="col-sm-3">
                    <?php echo $form->textField($model, 'title', array('class' => 'form-control', 'maxlength' => 255)); ?>
                    <?php echo $form->error($model, 'title'); ?>
                </div>
            </div>

            <div class='form-group form-group-sm'>
                    <?php echo $form->labelEx($model, 'feature_image', array('class' => 'col-sm-1 control-label')); ?>
                <div class="col-sm-1">
                    <?php if ($model->feature_image != '') { ?>
                        <div class="thumbnail" id="thumbnail-<?php echo $model->id; ?>">
                            <div class="caption">
                                <h4><?php echo $model->getAttributeLabel('feature_image'); ?></h4>
                                <p>Click on remove button to remove <?php echo $model->getAttributeLabel('feature_image'); ?></p>
                                <p><a href="<?php echo $this->baseControllerIndexUrl(); ?>/removeimage/fieldName/feature_image/id/<?php echo $model->id; ?>" class="label label-danger removedfile" rel="tooltip" title="Remove">Remove</a>
                            </div>
                            <img src="<?php echo Yii::app()->createAbsoluteUrl($model->uploadImageFolder . '/' . $model->id . '/' . $model->feature_image); ?>"  style="width:100%;" />
                        </div>
                    <?php } ?>
                    <?php echo $form->fileField($model, 'feature_image', array('title' => "Upload " . $model->getAttributeLabel('feature_image'))); ?>
                    <div class='notes'>Allow file type  <?php echo '*.' . str_replace(',', ', *.', $model->allowImageType); ?> - Maximum file size : <?php echo ($model->maxImageFileSize / 1024) / 1024; ?>M </div>
                    <?php echo $form->error($model, 'feature_image'); ?>
                </div>
            </div>



            <div class="form-group form-group-sm">
                <label class="col-sm-1 control-label">&nbsp;</label>
                <div class="col-sm-10">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab5" data-toggle="tab"><?php echo $model->getAttributeLabel('short_content'); ?></a></li>
                        <li><a href="#tab1" data-toggle="tab"><?php echo $model->getAttributeLabel('content'); ?></a></li>
                    </ul>
                    <br />
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab5">
                            <div class='form-group form-group-sm'>
                                <div class="col-sm-9">
                                    <?php echo $form->textArea($model, 'short_content', array('class' => 'my-editor-full', 'cols' => 63, 'rows' => 5)); ?>
                                    <?php echo $form->error($model, 'short_content'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab1">
                            <div class='form-group form-group-sm'>
                                <div class="col-sm-9">
                                    <?php echo $form->textArea($model, 'content', array('class' => 'my-editor-full', 'cols' => 63, 'rows' => 5)); ?>
                                    <?php echo $form->error($model, 'content'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class='form-group form-group-sm'>
                <?php echo $form->labelEx($model, 'country_id', array('class' => 'col-sm-1 control-label')); ?>
                <div class="col-sm-3">
                    <?php echo $form->dropDownList($model, 'country_id', MfCountry::getListData(), array('empty'=>'--Select--', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'country_id'); ?>
                </div>
            </div>
            
            <div class='form-group form-group-sm'>
                <?php echo $form->labelEx($model, 'city_id', array('class' => 'col-sm-1 control-label')); ?>
                <div class="col-sm-3">
                    <?php
                        $arrCities = $model->isNewRecord ? array(): MfCity::getListData($model->country_id);
                    ?>
                    <?php echo $form->dropDownList($model, 'city_id', $arrCities, array('empty'=>'--Select--', 'class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'city_id'); ?>
                </div>
            </div>

            <div class='form-group form-group-sm'>
                <?php echo $form->labelEx($model, 'program_id', array('class' => 'col-sm-1 control-label')); ?>
                <div class="col-sm-3">
                    <?php echo $form->dropDownList($model, 'program_id', MfProgram::getListData(), array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'program_id'); ?>
                </div>
            </div>

            <div class='form-group form-group-sm'>
                <?php echo $form->labelEx($model, 'course_fee', array('class' => 'col-sm-1 control-label')); ?>
                <div class="col-sm-3">
                    <?php echo $form->textField($model, 'course_fee', array('class' => 'form-control', 'maxlength' => 255)); ?>
                    <?php echo $form->error($model, 'course_fee'); ?>
                </div>
            </div>

            <div class='form-group form-group-sm'>
                <?php echo $form->labelEx($model, 'currency', array('class' => 'col-sm-1 control-label')); ?>
                <div class="col-sm-3">
                    <?php echo $form->textField($model, 'currency', array('class' => 'form-control', 'maxlength' => 255)); ?>
                    <?php echo $form->error($model, 'currency'); ?>
                </div>
            </div>

            <div class='form-group form-group-sm'>
                <?php echo $form->labelEx($model, 'start_date', array('class' => 'col-sm-1 control-label')); ?>
                <div class="col-sm-3">
                    <?php echo $form->textField($model, 'start_date', array('class' => 'ver_datetimepicker', 'cols' => 63, 'rows' => 5)); ?>
                    <?php echo $form->error($model, 'start_date'); ?>
                </div>
            </div>
            
            <div class='form-group form-group-sm'>
                <?php echo $form->labelEx($model, 'end_date', array('class' => 'col-sm-1 control-label')); ?>
                <div class="col-sm-3">
                    <?php echo $form->textField($model, 'end_date', array('class' => 'ver_datetimepicker', 'cols' => 63, 'rows' => 5)); ?>
                    <?php echo $form->error($model, 'end_date'); ?>
                </div>
            </div>


<!--            <div class='form-group form-group-sm'>
                <?php echo $form->labelEx($model, 'status', array('class' => 'col-sm-1 control-label')); ?>
                <div class="col-sm-3">
                    <?php echo $form->dropDownList($model, 'status', $model->optionActive, array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'status'); ?>
                </div>
            </div>-->

            <div class="clr"></div>
            <div class="well">
                <?php echo CHtml::htmlButton($model->isNewRecord ? '<span class="' . $this->iconCreate . '"></span> Create' : '<span class="' . $this->iconSave . '"></span> Save', array('class' => 'btn btn-primary', 'type' => 'submit')); ?> &nbsp;  
                <?php echo CHtml::htmlButton('<span class="' . $this->iconCancel . '"></span> Cancel', array('class' => 'btn btn-default', 'onclick' => 'javascript: location.href=\'' . $this->baseControllerIndexUrl() . '\'')); ?>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>
<script>
    $("#TbEvents_country_id").change(getCities);
    window.onload = function ()
    {
            <?php 
            if (!$model->isNewRecord):?>
            getCities();
            <?php endif;?>
    }
    function getCities(){
        var url = "<?php echo Yii::app()->createAbsoluteUrl('admin/events/getCities');?>";
        var cityId = "<?php echo $model->city_id;?>";
        if(cityId === ''){
            cityId = 0;
        }
        $.ajax({
                type: "post",
                url: url,
                data: {'areaCode' : $('#TbEvents_country_id').val(), 'cityId' : cityId}
              }).done(function(msg) {
                    $("#TbEvents_city_id").html(msg);       
              });
    }
</script>
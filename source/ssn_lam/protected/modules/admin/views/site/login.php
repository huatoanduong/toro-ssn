<?php 
$this->pageTitle = Yii::app()->name . ' - Login';
Yii::app()->clientScript->registerScriptFile(
	Yii::app()->theme->baseUrl.'/plugins/iCheck/icheck.min.js', CClientScript::POS_END) ;
Yii::app()->clientScript->registerScript('icheck-init', "
$(function() {
	$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_square-blue',
		increaseArea: '20%' // optional
	});
});
");
?>
<div class="login-box">
	<div class="login-logo"><b>SSN</b> Admin</div>
	<!-- /.login-logo -->
	<div class="login-box-body">
		<?php $this->widget('application.components.widget.Notification'); ?>
		
		<?php
		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'admin-login-form',
			'enableClientValidation' => true,
			'clientOptions' => array(
				'validateOnSubmit' => true,
			),
			'htmlOptions' => array('role' => "form")
		));
		?>
			<div class="form-group has-feedback">
				<?php echo $form->textField($model, 'username', array('size' => 40, 
					'class' => "form-control", 'placeholder'=>'Email')); ?>
				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				<?php echo $form->error($model, 'username');?>
			</div>
			<div class="form-group has-feedback">
				<?php echo $form->passwordField($model, 'password', array('size' => 33,
					'class' => "form-control", 'placeholder'=>'Password')); ?>
				<span class="glyphicon glyphicon-lock form-control-feedback"></span> 
				<?php echo $form->error($model, 'password');?>
			</div>
			<div class="row">
				<div class="col-xs-8">
					<div class="checkbox icheck">
						<label>
							<?php echo $form->checkBox($model, 'rememberMe'); ?> Remember me
						</label>
					</div>
				</div>
				<!-- /.col -->
				<div class="col-xs-4">
					<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
				</div>
				<!-- /.col -->
			</div>
		<?php $this->endWidget(); ?>
		<a href="<?php echo Yii::app()->createAbsoluteUrl('admin/site/ForgotPassword'); ?>">I forgot my password</a>
<!--		<br> <a href="register.html" class="text-center">Register a new membership</a> -->
	</div>
	<!-- /.login-box-body -->
</div>

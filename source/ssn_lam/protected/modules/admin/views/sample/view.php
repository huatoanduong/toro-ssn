<?php
$this->breadcrumbs = array(
    'Samples' => array('index'),
    'Sample Detail',
);
?>
<div class="box">
	<div class="box-body">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'ssn-sample-form',
			'enableAjaxValidation'=>false,
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
			),
			'htmlOptions' => array(
				'role' => 'form', 
				'enctype' => 'multipart/form-data'
			),
		)); ?>
		<div class="form-group">
			<?php echo $form->labelEx($model,'dateSentText', array('class'=>'control-label')); ?>
			<p class="form-control-static"><?= $model->dateSentText ?></p>
		</div>

		<!--send from-->
		<div class="form-group">
			<?php echo $form->labelEx($model,'from_user_id', array('class'=>'control-label')); ?>
			<p class="form-control-static">
				<a href="<?= $this->createUrl('company/view', array('id'=> $model->sender->company->id)) ?>"
					class="notranslate"><?= $model->sender->company->name ?></a>
			</p>
		</div>

		<!--send to-->
		<div class="form-group">
			<label for="">Recipient</label>
			<p class="form-control-static">
				<?php foreach ($model->receivers as $receiver): ?>
				<a href="<?= $this->createUrl('company/view', array('id'=> $receiver->company->id)) ?>">
					<?= $receiver->company->name ?>
				</a><br/>
				<?php endforeach ?>
			</p>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'attention', array('class'=>'control-label')); ?>
			<p class="form-control-static"><?= $model->attention ?: 'Not set.' ?></p>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'awbn', array('class'=>'control-label')); ?>
			<?php if ($model->getTrackingLink()): ?>
			<p class="form-control-static"><a href="<?= $model->getTrackingLink() ?>" target="_blank"><?= $model->awbn ?></a></p>
			<?php else: ?>
			<p class="form-control-static"><?= $model->awbn ?: 'Not set.' ?></p>
			<?php endif ?>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model,'courier_id', array('class'=>'control-label')); ?>
			<p class="form-control-static"><?= $model->getCourierName() ?></p>
		</div>

		<div class="form-group">
			<label for="" class="control-label">Item</label>
			<div class="table-responsive">
				<table class="item-list table table-bordered table-hover">
					<thead>
						<tr>
							<th>Name</th>
							<th>Color</th>
							<th>Qty</th>
							<th>Unit</th>
							<th>Document</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($model->items as $index => $item): ?>
						<tr>
							<td><?= $item->name ?></td>
							<td><?= $item->color ?></td>
							<td><?= $item->quantity ?></td>
							<td><?= $item->unit ?></td>
							<td>
								<?php if ($url = $item->getDocumentDownloadUrl()): ?>
								<a href="<?= $url ?>"><?= $item->document ?></a> 
								<?php else: ?>
								No document uploaded.
								<?php endif ?>
							</td>
						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'remark', array('class'=>'control-label')); ?>
			<p class="form-control-static"><?= $model->remark ?: 'Not set.' ?></p>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'document', array('class'=>'control-label')); ?>
			<?php if ($url = $model->getDocumentDownloadUrl()): ?>
			<p><a href="<?= $url ?>" class="notranslate"><?= $model->document ?></a></p>
			<?php else: ?>
			<p class="form-control-static">No document uploaded.</p>
			<?php endif ?>
		</div>	
		<?php $this->endWidget(); ?>
	</div>
</div>

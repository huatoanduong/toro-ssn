<?php
$form = $this->beginWidget('CActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
	'htmlOptions' => array('class' => 'form-horizontal', 'role' => 'form', 'id' => 'search-form'),
));
?>
<div class="row">
	<div class="col-md-10">
		<div class="form-group">
			<?php echo $form->labelEx($model, 'status', array('class' => 'col-sm-2 control-label')); ?>
			<div class="col-sm-10">
				<?php echo $form->dropDownList($model, 'status', $model->optionActive, array('class' => 'form-control')); ?>
				<?php echo $form->error($model, 'status'); ?>
			</div>
		</div>
		
	</div>
	<div class="col-md-2">
		<button type="submit" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-search"></span> Search</button>
	</div>
</div>
<?php $this->endWidget(); ?>

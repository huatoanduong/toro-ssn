<?php
$this->breadcrumbs = array(
    $this->pluralTitle,
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('ssn-partner-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});

$('#clearsearch').click(function(){
	var id='search-form';
	var inputSelector='#'+id+' input, '+'#'+id+' select';
	$(inputSelector).each( function(i,o) {
		 $(o).val('');
	});
	var data=$.param($(inputSelector));
	$.fn.yiiGridView.update('ssn-partner-grid', {data: data});
	return false;
});

$('.deleteall-button').click(function(){
        var atLeastOneIsChecked = $('input[name=\"ssn-partner-grid_c0[]\"]:checked').length > 0;
        if (!atLeastOneIsChecked)
        {
                alert('Please select at least one record to delete');
        }
        else if (window.confirm('Are you sure you want to delete the selected records?'))
        {
                document.getElementById('ssn-partner-grid-bulk').action='" . Yii::app()->createAbsoluteUrl('admin/' . Yii::app()->controller->id . '/deleteall') . "';
                document.getElementById('ssn-partner-grid-bulk').submit();
        }
});

");

Yii::app()->clientScript->registerScript('ajaxupdate', "
    $('#ssn-partner-grid a.ajaxupdate').on('click', function() {
        $.fn.yiiGridView.update('ssn-partner-grid', {
            type: 'POST',
            url: $(this).attr('href'),
            success: function() {
                $.fn.yiiGridView.update('ssn-partner-grid');
            }
        });
        return false;
    });
");
?>

<div class="box">
	<div class="box-body">
		<div class='search-form'>
			<?php $this->renderPartial('_search', array(
				'model' => $model,
			)); ?>
		</div>
		
		<p>
			<?= CHtml::htmlButton('<span class="glyphicon glyphicon-trash"></span> Bulk Delete', array(
			'class' => 'btn btn-danger deleteall-button', 
			'type' => 'button')); ?>
		</p>
		
		<div class="table-responsive">
        <?php
        $allowAction = in_array("delete", $this->listActionsCanAccess) ? 'CCheckBoxColumn' : '';
        $columnArray = array();
        if (in_array("Delete", $this->listActionsCanAccess)) {
            $columnArray[] = array(
                'value' => '$data->id',
                'class' => "CCheckBoxColumn",
            );
        }
        $columnArray = array_merge($columnArray, array(
            array(
                'header' => 'S/N',
                'type' => 'raw',
                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                'headerHtmlOptions' => array('width' => '30px', 'style' => 'text-align:center;'),
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name' => 'from_user_id',
                'value' => '$data->from_user_fk ? $data->from_user_fk->companyName:"-"',
            ),
            array(
                'name' => 'to_user_id',
                'value' => '$data->to_user_fk ? $data->to_user_fk->companyName:"-"',
            ),
            array(
                'name' => 'created_date',
                'type' => 'date',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name' => 'status',
                'type' => 'status',
                'value' => 'array("id"=>$data->id,"status"=>$data->status)',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'name' => 'approve_by',
                'value' => '$data->approve_user_fk ? $data->approve_user_fk->full_name:"-"',
                'htmlOptions' => array('style' => 'text-align:right;')
            ),
            array(
                'header' => 'Approve',
                'name' => 'status',
                'type' => 'approvePartnerByAdmin',
                'value' => 'array("id"=>$data->id,"status"=>$data->status)',
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'header' => 'Actions',
                'class' => 'CButtonColumn',
                'template' => '{delete}',
            ),
        ));
		
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'ssn-partner-grid-bulk',
            'enableAjaxValidation' => false,
            'htmlOptions' => array('enctype' => 'multipart/form-data')));

        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'ssn-partner-grid',
            'dataProvider' => $model->search(),
            'selectableRows' => 2,
            'columns' => $columnArray,
			'cssFile'=>false,
			'itemsCssClass'=>'table table-striped table-bordered',
			'pagerCssClass'=>'pag-container',
			'pager' => array(
				'header'=>'',
				'cssFile' => false,
				'nextPageLabel' => 'Next',
				'prevPageLabel' => 'Previous',
				'firstPageLabel' => 'First',
				'lastPageLabel' => 'Last',
				'selectedPageCssClass'=>'active',
				'maxButtonCount' => 10,
				'htmlOptions'=>array('class' => 'pagination pull-right',)
			),
        ));
        $this->endWidget();
        ?>			
		</div>
	</div>
<div>

<?php

class CompanyController extends AdminController {

	public $pluralTitle = 'Company Profile';
	public $singleTitle = 'Company';
	public $cannotDelete = array();

	public function actionSetDefaultLogo($id) {
		$model = $this->loadModel($id);
		$model->image = '';
		$model->update('image');
		$this->redirect(array('view', 'id'=>$id));
	}
	
	public function actionIndex() {
		$model = new SsnCompany('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['SsnCompany']))
			$model->attributes = $_GET['SsnCompany'];

		$this->pageTitle = 'Company';
		$this->render('index', array(
			'model' => $model, 'actions' => $this->listActionsCanAccess,
		));
	}
	
	public function actionView($id) {
		$model = $this->loadModel($id);
        if (isset($_POST['SsnCompany'])) {
            $model->attributes = $_POST['SsnCompany'];
            if ($model->save()) {
                $model->saveImage('image');
                Yii::app()->user->setFlash('success', "Your profile has been successfully changed.");
				$this->redirect(array('view', 'id'=>$id));
            }
        }
		$this->pageTitle = "Company '{$model->name}'";
		$this->render('view', array('model'=>$model));
	}

	public function actionPartner($id) {
		$model = $this->loadModel($id);
		$search = new SsnPartner('search');
		$search->from_user_id = $search->to_user_id = $model->user_id;
		$model->searchModel = $search;
		$this->pageTitle = "Company '{$model->name}'";
		$this->render('partner', array('model'=>$model));
	}

	public function actionSample($id) {
		$model = $this->loadModel($id);
		$search = new SsnSampleSearch();
		$search->user_id = $model->user_id;
		$model->searchModel = $search;
		$this->pageTitle = "Company '{$model->name}'";
		$this->render('sample', array('model'=>$model));
	}

	public function actionDeleteSample($id) {
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			if (!in_array($id, $this->cannotDelete)) {
				if ($model = SsnSample::model()->findByPk($id)) {
					$model->delete();
				}

				// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
				if (!isset($_GET['ajax']))
					$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
			}
		}
	}
	
	public function actionDelete($id) {
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			if (!in_array($id, $this->cannotDelete)) {
				if ($model = SsnPartner::model()->findByPk($id)) {
					$model->delete();
				}

				// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
				if (!isset($_GET['ajax']))
					$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
			}
		} else {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}
	}

    /*
     * Bulk delete
     * If you don't want to delete some specified record please configure it in global $cannotDetele variable
     */

    public function actionDeleteAll() {
        $shouldDelete = $_POST['ssn-company-grid_c0'];

        if (!empty($shouldDelete)) {
            SsnCompany::model()->deleteAll('id in (' . implode(',', $shouldDelete) . ')');
            Yii::app()->user->setFlash('success', 'Your selected records have been deleted.');
        }
        else
            Yii::app()->user->setFlash('error', 'No records deleted.');

        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }
	
	public function actionApprove($id) {
		$partner = SsnPartner::model()->findByPk($id);
		if ($partner) {
			$partner->status = SsnPartner::STATUS_APPROVE;
			$partner->approve_by = Yii::app()->user->id;
			if ($partner->update(array('status'))) {
				SendEmail::partnerApproveRequest($partner);
			}
		}
		echo '1';
	}
	
	public function loadModel($id) {
		//need this define for inherit model case. Form will render parent model name in control if we don't have this line
		$model = SsnCompany::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

}

<?php

/*
 * Display menu and child menu items in sidebar
 * @author Lam Huynh
 */

class BimCapNav extends CWidget {

    /**
     * @var Menuitem active menuitem
     */
    public $model;

    public function init() {
        return parent::init();
    }

    public function run() {
        if (!$this->model)
            return;
        $model = $this->model;

        $this->render("bim_cap_nav", array(
            'model' => $model,
        ));
    }

    public function getCssClass($model) {
        if ($this->model->id == $model->id ||
                ($this->model->parent && $model->id == $this->model->parent->id) // menuitem has active child menu
        )
            return 'active';
        return;
    }

    public function getSubMenuCssClass($model) {
        if ($this->model->id == $model->id ||
                ($this->model->parent && $model->id == $this->model->parent->id) // menuitem has active child menu
        )
            return 'in';
        return '';
    }

}

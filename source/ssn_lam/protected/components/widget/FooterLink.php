<?php
/*
 * Display menu and child menu items in sidebar
 * @author Lam Huynh
 */
class FooterLink extends CWidget {

	public function init() {
		return parent::init();
	}

	public function run() {
		$items = Menuitem::model()->getFooterItems();
		$this->render("footer_link", array(
			'items' => $items,
		));
	}

}

<?php
//call: $this->widget('FEMenus', array('group_id' => <group_id>, 'layout' => <'layout1', 'layout2', 'layout3', 'layout4'>, 'current_page' => <current_page>, 'active_class' => <'module/controller/action'>));
class FEMenus extends CWidget{
    public $current_page;
    public $group_name;
    public $group_id;
    public $menu_root;
    public $layout;
    public $active_class;
    public $active_action;
    private $group_ids;
    private $hierarchy_tree;
    public static $layouts = array('layout1', 'layout2', 'layout3');
    public $highlight;
    public $external_highlight;
    public function init() {
        $this->highlight = Menuitem::highlight($this->external_highlight);
        $this->group_ids = CHtml::listData(Menu::model()->findAll(), 'id', 'id');
        $this->hierarchy_tree = array();
        return parent::init();
    }
    public function readyToBuild() {
        return in_array($this->layout, self::$layouts) && in_array($this->group_id, $this->group_ids);
    }
    public function setHierarchyTree() {
        if($this->readyToBuild())
            $this->hierarchy_tree = Menuitem::buildMenuData($this->group_id, $this->menu_root, $this->current_page, $this->active_class);
    }
    public function getHierarchyTree(){
        $this->setHierarchyTree();
        return $this->hierarchy_tree;
    }
    public function isHighlight($link, $menu_id) {
        if(!empty($this->highlight))
            foreach ($this->highlight as $item)
                if(isset($item[$link])&&$item[$link]==$menu_id)
                    return true;
        return false;
    }
    public function run(){
        $this->render("femenus/$this->layout",array(
            'menus' => $this->getHierarchyTree(), 
        ));
    }
}
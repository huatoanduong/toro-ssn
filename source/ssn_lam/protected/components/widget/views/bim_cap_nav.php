<?php
/* @var $model BimCapability */
/* @var $this BimCapNav */
$cats = BimCapability::getCategories();
?>
<div class="menu-sidebar">
	<h3 class="title-5">Tản mạn về rượu</h3>
	<ul class="nav accordion">
		<?php foreach($cats as $cat): ?>
		<?php $class = $this->getCssClass($cat) ?>
		<li class="<?php echo $class ?>"><a href="<?php echo $cat->getUrl() ?>"><?php echo $cat->title ?></a>
			<div class="collapse <?php echo $this->getSubMenuCssClass($cat) ?>">
				<?php if (!empty($cat->childs)): ?>
				<ul>
					<?php foreach($cat->childs as $child): ?>
					<li class="<?php echo $this->getCssClass($child) ?>"><a href="<?php echo $child->getUrl() ?>"><?php echo $child->title ?></a></li>
					<?php endforeach; ?>
				</ul>
				<?php endif ?>
			</div>
		</li>
		<?php endforeach; ?>
	</ul>
</div><!-- Menu bar -->

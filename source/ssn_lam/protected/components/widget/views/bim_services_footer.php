<h3 class="title-4">BIM Services</h3>
<?php foreach($data as $cols) : ?>
<ul class="list-link col-md-6" style="float: left">
	<?php foreach($cols as $item): ?>
	<li><a href="<?php echo Yii::app()->createAbsoluteUrl('service/detail', array('slug'=>$item->slug));?>"><?php echo $item->title;?></a></li>
	<?php endforeach;?>
</ul>
<?php endforeach; ?>
<div style="clear:both"></div>

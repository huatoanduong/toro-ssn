<?php if (count($models) > 0): ?>
    <?php foreach ($models as $model): ?>
        <div class="bn bn-inner">
            <?php echo CHtml::image(ImageHelper::getImageUrl($model, "image", 'large'), $model->title); ?>
            <div class="carousel-caption">
                <div class="content">
                    <h1><?php echo $model->title ?></h1>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
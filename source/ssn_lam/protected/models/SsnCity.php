<?php

/**
 * This is the model class for table "{{_ssn_sample_item}}".
 *
 * The followings are the available columns in table '{{_ssn_sample_item}}':
 * @property integer $id
 * @property string $courier
 */
class SsnCity extends _BaseModel {
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{_ssn_city}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'length', 'max' => 255),
            array('id, name, order_by, status, area_code_id', 'safe')
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Yii::t('translation', 'ID'),
            'name' => Yii::t('translation', 'City'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->params['defaultPageSize'],
            ),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SsnSampleItem the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function nextOrderNumber() {
        return self::model()->count() + 1;
    }
    
    public static function getListData(){
        $data = self::model()->findAll();
        return CHtml::listData($data, 'id', 'name');
    }
    public static function getListDataByArea($area = 0){
        $data = self::model()->findAll(array('area_code_id' => $area));
        return CHtml::listData($data, 'id', 'name');
    }

}

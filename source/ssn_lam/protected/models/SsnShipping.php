<?php

/**
 * This is the model class for table "{{_ssn_shipping}}".
 *
 * The followings are the available columns in table '{{_ssn_shipping}}':
 * @property integer $id
 * @property integer $from_user_id
 * @property integer $to_user_id
 * @property string $order_number
 * @property string $remark
 * @property string $document
 * @property string $date_load
 * @property string $created_date
 * @property string $updated_date
 */
class SsnShipping extends CActiveRecord {

	public $to_company;
	public $form_items=array();
	public $documentFile;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SsnShipping the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{_ssn_shipping}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		Yii::import('application.components.validator.FileUploadClientValidator');

		return array(
			array('dateLoadText, order_number, to_user_id', 'required', 'on' => 'create, update', 'message' => 'You can not leave this field empty.'),
			array('document', 'application.components.validator.FileUploadClientValidator',
				'types' => 'doc, docx, xls, xlsx, pdf', 'allowEmpty' => true, 'maxSize'=>3000000,
				'on'=>'create, update'),
			
			array('from_user_id, to_user_id', 'numerical', 'integerOnly' => true),
			array('order_number, document', 'length', 'max' => 255),
			array('remark, created_date, updated_date, date_load, to_company', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, from_user_id, to_user_id, order_number, remark, document, created_date, updated_date', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'receiver' => array(self::BELONGS_TO, 'Users', 'to_user_id'),
			'sender' => array(self::BELONGS_TO, 'Users', 'from_user_id'),
			'items' => array(self::HAS_MANY, 'SsnShippingItem', 'shipping_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'from_user_id' => 'Send From',
			'to_company' => 'Send To',
			'to_user_id' => 'Send To',
			'order_number' => 'Order Code',
			'remark' => 'Remark',
			'document' => 'Document',
			'created_date' => 'Created Date',
			'updated_date' => 'Updated Date',
			'date_load' => 'Date Load',
			'dateLoadText' => 'Date Load',
		);
	}

	public function behaviors() {
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'created_date',
				'updateAttribute' => 'updated_date',
			)
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('from_user_id', $this->from_user_id);
		$criteria->compare('to_user_id', $this->to_user_id);
		$criteria->compare('order_number', $this->order_number, true);
		$criteria->compare('remark', $this->remark, true);
		$criteria->compare('document', $this->document, true);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('updated_date', $this->updated_date, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function validateCompanyName($attribute) {
		$val = $this->$attribute;
		$p = SsnPartner::model()->tableName();
		$u = $this->from_user_id;
		$cond = "t.name=:name AND (
			t.user_id IN ( 
				SELECT to_user_id 
				from $p 
				where from_user_id=$u 
			) OR t.user_id IN ( 
				SELECT from_user_id 
				from $p 
				where to_user_id=$u 
			)
		)";
		$comp = SsnCompany::model()->find($cond, array(
			':name' => $val
		));
		if ($comp) {
			$this->to_user_id = $comp->user_id;
		} else {
			$this->addError($attribute, 'Company name is not valid.');
		}
	}

	public function getDocumentDirectory() {
		return Yii::getPathOfAlias('webroot') . '/upload/shipping/';
	}

	public function saveDocument() {
		if (!$this->documentFile) return;
		
		$saveName = time() . '-' . $this->documentFile->getName();
		$savePath = $this->getDocumentDirectory() . $saveName;
		if (!file_exists(dirname($savePath))) {
			mkdir(dirname($savePath));
		}
		$this->documentFile->saveAs($savePath);
		$this->removeDocumentFile();
		$this->document = $saveName;
		$this->update('document');
	}

	public function getDocumentDownloadUrl() {
		$file = $this->getDocumentDirectory() . $this->document;
		return is_file($file) ? Yii::app()->baseUrl .'/upload/shipping/' . $this->document : null;
	}
	
	public function getDateLoadText() {
		return date('d/m/Y', strtotime($this->date_load));
	}

	public function setDateLoadText($value) {
		$this->date_load = DateHelper::toDbDateFormat($value);
	}

	public function getReceiverName() {
		if ($this->receiver->company)
			return $this->receiver->company->name;
		return '';
	}
	
	public function getSenderName() {
		if ($this->sender->company)
			return $this->sender->company->name;
		return '';
	}
	
	public function getRemarkExcerpt() {
		return StringHelper::limitStringLength($this->remark, 30);
	}
	
	public function canView($userId=null) {
		return $this->isSender($userId) || $this->isRecipient($userId);
	}
	
	public function canEdit($userId=null) {
		return $this->isSender($userId);
	}
	
	public function isSender($userId=null) {
		if (!$userId)
			$userId = Yii::app ()->user->id;
		return $userId==$this->from_user_id;
	}
	
	public function isRecipient($userId=null) {
		if (!$userId)
			$userId = Yii::app ()->user->id;
		return $userId==$this->to_user_id;
	}
	
	protected function beforeDelete() {
		$this->removeDocumentFile();
		return true;
	}
	
	public function removeDocumentFile() {
		if (is_file($this->getDocumentDirectory().$this->document))
			unlink ($this->getDocumentDirectory().$this->document);
	}

	public function safeDeleteAll($condition='',$params=array()) {
		$models = self::model()->findAll($condition, $params);
		foreach($models as $model) {
			$model->delete();
		}
	}
}

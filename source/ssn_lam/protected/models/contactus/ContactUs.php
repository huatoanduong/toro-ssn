<?php

/**
 * This is the model class for table "{{_contactus}}".
 *
 * The followings are the available columns in table '{{_contactus}}':
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $content
 * @property string $type
 * @property string $file_upload
 * @property integer $status
 * @property string $company
 * @property integer $option
 */
class ContactUs extends _ContactUsBaseModel {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{_contactus}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, email, phone, content', 'required'),
            array('status, option', 'numerical', 'integerOnly' => true),
            array('name, email, phone, file_upload, company', 'length', 'max' => 255),
            array('type', 'length', 'max' => 45),
            array('email', 'email'),
            array('content', 'safe'),
            array('file_upload', 'file', 'on' => 'create,update',
                'allowEmpty' => true,
                'types' => $this->allowUploadType,
                'wrongType' => 'Only ' . $this->allowUploadType . ' are allowed.',
                'maxSize' => $this->maxUploadFileSize, // 3MB
                'tooLarge' => 'The file was larger than ' . ($this->maxUploadFileSize / 1024) / 1024 . 'MB. Please upload a smaller file.',
            ),
            array(
                'phone',
                'match', 'not' => true, 'pattern' => '/[^0-9\+\-\)\( ]/',
                'message' => 'Phone must be numerical and allow input (),+,-',
            ),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, email, phone, content, type, file_upload, status, company, option', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Yii::t('translation', 'ID'),
            'name' => Yii::t('translation', 'Name'),
            'email' => Yii::t('translation', 'Email'),
            'phone' => Yii::t('translation', 'Phone'),
            'content' => Yii::t('translation', 'Enquiry'),
            'type' => Yii::t('translation', 'Type'),
            'file_upload' => Yii::t('translation', 'File Upload'),
            'status' => Yii::t('translation', 'Status'),
            'company' => Yii::t('translation', 'Company'),
            'option' => Yii::t('translation', 'Option'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('content', $this->content, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('company', $this->company, true);
        $criteria->compare('option', $this->option);
        
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
			'sort' => array(
                'defaultOrder' => 't.created_date desc'),
            'pagination' => array(
                'pageSize' => Yii::app()->params['defaultPageSize'],
            ),
        ));
    }

    public function activate() {
        $this->status = 1;
        $this->update();
    }

    public function deactivate() {
        $this->status = 0;
        $this->update();
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ContactUs the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function nextOrderNumber() {
        return self::model()->count() + 1;
    }

//    public function showUploadFile($model) {
//        if (!empty($model->file_upload)) {
//            $temp = explode('.', $model->file_upload);
//            $fileType = $temp[count($temp) - 1 ];
//            if ($fileType == 'pdf'){
//              return CHtml::link (CHtml::image (Yii::app ()->theme->baseUrl . "/img/pdf.png"), Yii::app ()->createAbsoluteUrl ($model->uploadFileFolder.'/'.$model->id.'/'.$model->file_upload), array('target' => '_blank'));
//            }else{
//             return CHtml::link (CHtml::image (Yii::app ()->createAbsoluteUrl ($model->uploadFileFolder.'/'.$model->id.'/'.$model->file_upload),'',array('style' => 'width:50px;height:50px;')),
//                     Yii::app ()->createAbsoluteUrl ($model->uploadFileFolder.'/'.$model->id.'/'.$model->file_upload), 
//                     array('target' => '_blank'));   
//            }
//        }
//    }

    public function showUploadFile($model) {
        if (!empty($model->file_upload)) {
            return CHtml::link($model->file_upload, Yii::app()->createAbsoluteUrl($model->uploadFileFolder . '/' . $model->id . '/' . $model->file_upload), array('target' => '_blank'));
        } else {
            return '';
        }
    }

}

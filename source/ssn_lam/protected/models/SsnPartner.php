<?php

/**
 * This is the model class for table "{{_ssn_partner}}".
 *
 * The followings are the available columns in table '{{_ssn_partner}}':
 * @property integer $id
 * @property integer $from_user_id
 * @property integer $to_user_id
 * @property string $created_date
 * @property integer $status
 * @property integer $approve_by
 */
class SsnPartner extends _BaseModel {

	const STATUS_SEND = 0;
	const STATUS_PENDING = 1;
	const STATUS_APPROVE = 2;
	public static $STATUSES = array(
		self::STATUS_SEND => 'Waiting',
		self::STATUS_PENDING => 'Waiting',
		self::STATUS_APPROVE => 'Approved',
	);

	public $user_email;

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{_ssn_partner}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('from_user_id, status, approve_by', 'numerical', 'integerOnly' => true),
			array('created_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_email, from_user_id, to_user_id, created_date, status, approve_by', 'safe', 'on' => 'search'),
			array('user_email', 'required'),
			array('user_email', 'checkUserExist', 'on' => 'create'),
		);
	}

	public function checkUserExist() {
		$email = $this->user_email;
		$fuid = $this->from_user_id;
		$tuid = $this->to_user_id;
		$getEmail = Users::checkUserByEmail($email);
		$partner = self::model()->count(array('condition' => "from_user_id = $fuid AND to_user_id = $tuid"));
		if ($partner > 0) {
			$this->addError('user_email', 'Your request had been sent before. Please don\'t resend it.');
		}
		if (strcmp($email, Yii::app()->user->email) == 0) {
			$this->addError('user_email', 'You can not add yourself');
		}
		if ($getEmail <= 0) {
			$this->addError('user_email', 'This email not exist, please try again with another email');
		}
	}

	public function checkEmailExist() {
		
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'from_user_fk' => array(self::BELONGS_TO, 'Users', 'from_user_id'),
			'to_user_fk' => array(self::BELONGS_TO, 'Users', 'to_user_id'),
			'approve_user_fk' => array(self::BELONGS_TO, 'Users', 'approve_by'),
			
			'from_user' => array(self::BELONGS_TO, 'Users', 'from_user_id'),
			'to_user' => array(self::BELONGS_TO, 'Users', 'to_user_id'),
			'approve_user' => array(self::BELONGS_TO, 'Users', 'approve_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => Yii::t('translation', 'ID'),
			'from_user_id' => Yii::t('translation', 'Company Name'),
			'to_user_id' => Yii::t('translation', 'To Company'),
			'created_date' => Yii::t('translation', 'Send Date'),
			'status' => Yii::t('translation', 'Status'),
			'statusText' => Yii::t('translation', 'Status'),
			'approve_by' => Yii::t('translation', 'Approve By'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

//        $criteria->compare('id', $this->id);
		if ($this->status == self::STATUS_APPROVE) {
			$uid = Yii::app()->user->id;
			$criteria->addCondition("t.from_user_id  = '{$uid}' OR t.to_user_id = '{$uid}'");
		}
		if (!empty($this->from_user_id))
			$criteria->compare('from_user_id', $this->from_user_id);
		if (!empty($this->to_user_id))
			$criteria->compare('to_user_id', $this->to_user_id);
//        $criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('status', $this->status);
//        $criteria->compare('approve_by', $this->approve_by);


		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => Yii::app()->params['defaultPageSize'],
			),
		));
	}
	
	public function searchAdmin() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$conds = array();
		$conds[] = 'from_user_id = :user_id';
		$conds[] = 'to_user_id = :user_id';
		$criteria->addCondition(implode(' OR ', $conds));
		$criteria->params[':user_id'] = $this->from_user_id;

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => Yii::app()->params['defaultPageSize'],
			),
		));
	}

	public function activate() {
		$this->status = 1;
		$this->update();
	}

	public function deactivate() {
		$this->status = 0;
		$this->update();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsnPartner the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function nextOrderNumber() {
		return SsnPartner::model()->count() + 1;
	}

	public static function getTotalPartner($t) {
		$criteria = new CDbCriteria;
		$uid = Yii::app()->user->id;
		switch ($t) {
			case SsnPartner::STATUS_APPROVE:
				$criteria->addCondition("t.from_user_id  = '{$uid}' OR t.to_user_id = '{$uid}'");
				break;
			case SsnPartner::STATUS_SEND:
				$criteria->compare('from_user_id', $uid);
				break;
			case SsnPartner::STATUS_PENDING:
				$criteria->compare('to_user_id', $uid);
				$t = SsnPartner::STATUS_SEND;
				break;
		}

		$criteria->compare('status', $t);

		return self::model()->count($criteria);
	}

	public static function getListPartner() {
		$criteria = new CDbCriteria;

		$uid = Yii::app()->user->id;
		$criteria->addCondition("from_user_id  = '{$uid}' OR to_user_id = '{$uid}'");
		$criteria->compare('status', self::STATUS_APPROVE);

		$data = self::model()->findAll($criteria);
		$arr = array();
		if ($data) {
			foreach ($data as $k => $i) {
				if ($i->to_user_id == Yii::app()->user->id)
					$arr[$i->from_user_id] = $i->from_user_fk ? $i->from_user_fk->full_name . ' - ' . $i->from_user_fk->email : '';
				if ($i->from_user_id == Yii::app()->user->id)
					$arr[$i->to_user_id] = $i->to_user_fk ? $i->to_user_fk->full_name . ' - ' . $i->to_user_fk->email : '';
			}
		}
		return $arr;
	}

	public function getStatusText() {
		return Yii::app()->format->formatEnum($this->status, self::$STATUSES);
	}
	public function getFromCompany() {
		return $this->from_user ? $this->from_user->company->name : '';
	}
	public function getToCompany() {
		return $this->to_user ? $this->to_user->company->name : '';
	}
	
	static public function getPartnerListData($userId) {
		$p = SsnPartner::model()->tableName();
		$u = $userId;
		$condition = "user_id in (
			select to_user_id
			from $p
			where from_user_id=$u
		) or user_id in (
			select from_user_id
			from $p
			where to_user_id=$u
		)";
		$comps = SsnCompany::model()->findAll($condition);
		$data = array();
		foreach ($comps as $comp) {
			$data[$comp->id] = "{$comp->name} ({$comp->user->email})";
		}
		return $data;
	}
}

function initialize() {
	var myLatlng = new google.maps.LatLng(1.3199536,103.8442129);
	var myOptions = {
	  zoom: 17,
	  center: myLatlng,
	  mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	
	var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	
	var contentString = '<div id="content">'+
		'<h1>DiHub Singapore</h1>'+
		'<div>'+
		'<p>238B Thomson Road #17-00 Tower B Novena Square Singapore 307685</p>'+
		'</div>'+
		'</div>';

	var infowindow = new google.maps.InfoWindow({
		content: contentString,
		maxWidth: 400
	});
	
	var companyImage = new google.maps.MarkerImage('themes/dihub/img/point.png',
		new google.maps.Size(100,48),
		new google.maps.Point(0,0)
	);

	var marker = new google.maps.Marker({
		position: myLatlng,
		map: map,
		icon: companyImage,
		title: 'DiHub Singapore'
	});
	google.maps.event.addListener(marker, 'click', function() {
	  infowindow.open(map,marker);
	});

  }
	
 initialize();     
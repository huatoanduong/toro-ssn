//Haidt
(function ($) {
	var data = {
		id: 'data-id',
		class: 'data-class',
		wrap: '.bg-mask',
		closeBtn: '.fancybox-close',
		active: 'active',
		nav_li: '.carousel-navigation ul li',
		nav_ul: '.carousel-navigation ul',
		img_ul: '.carousel-stage ul',
		img_li: '.carousel-stage ul li',
		nav_wrap: '.navigation',
		nav_v: '.carousel-navigation',
		x: 0,
		y: 0,
		next: '<a href="#" class="next next-navigation"> &raquo; </a>',
		prev: '<a href="#" class="prev prev-navigation"> &laquo; </a>',
		next_class: '.next-navigation',
		prev_class: '.prev-navigation',
		footer_class : '.footer-container',
		overlay_class: '.bg_overlay',
		div_bg_overlay : '<div class="bg_overlay"></div>',
	};
	var index = {};
	$.fn.galleries = function (options) {
		var defaults = {
		};
		var settings = $.extend({}, defaults, options);
		var selector = $(this);
		clicker(selector);
		
		navArrowNextClicker();
		navArrowPrevClicker();
		closeBtn();
		return this.each(function () {

		});
	};

	function clicker(selector) {
		selector.on('click', function () {
			getIndex($(this));
			openPopup();
			showNav(0);
			$(data.footer_class).after(data.div_bg_overlay);
		});
	}

	function debug(el) {
		console.log(el);
	}

	function getIndex(target) {
		index.id = $('#' + target.attr(data.id));
		index.id_s = '#' + target.attr(data.id);
		index.class = $('.' + target.attr(data.class));
		index.wrap = $(data.wrap);
		index.img_w = $(index.id_s + ' ' + data.img_li + ':first').outerWidth();
		index.nav_item_w = $(index.id_s + ' ' + data.nav_li + ':first').outerWidth();
		index.total_item = index.id.find(data.nav_li).size();
		index.nav_w = index.total_item * index.nav_item_w;
		index.nav_v_w = index.nav_item_w * 5;
		var fli = index.id.find(data.nav_li);
		fli.removeClass(data.active);
		fli.each(function (i) {
			if (i == 0) {
				$(this).addClass(data.active);
			}
		});

	}

	function openPopup() {		
		index.class.hide();
		index.wrap.show();
		index.id.show();
		index.id.addClass(data.active);
		setPosition();
		index.id.find(data.nav_ul).css({'left': 0});
		index.id.find(data.img_ul).css({'left': 0});
		navClicker();
	}

	closePopup = function () {
		index.class.hide();
		index.wrap.hide();
		index.id.removeClass(data.active);
		data.x = 0;
	}

	function closeBtn() {
		$(data.closeBtn).on('click', function () {
			closePopup();
			$(data.overlay_class).remove();
		});
	}

	function setPosition() {
		var w = $(data.wrap).width();
		var h = $(data.wrap).height();
		var aw = index.class.width();
		var x = (((w / 2) - (aw / 2)) * 100) / w;
		index.class.css({'left': x + '%', 'top': 100});
	}


	function navClicker() {
		index.Nav = index.id.find(data.nav_li);
		index.Nav.each(function (i) {
			var that = $(this);
			$(this).click(function () {
				if (!that.hasClass(data.active)) {
					index.Nav.removeClass(data.active);
					slideTo(i, that);
				}
			});
		});
	}

	function slideTo(i, item) {
		var x = 0 - i * index.img_w;
		index.id.find(data.img_ul).stop().animate({left: x}, 800);
		item.addClass(data.active);
	}

	function showNav(left) {
		var right = left + index.nav_w;
		if (right > index.nav_v_w) {
			index.id.find(data.next_class).show();
		} else {
			index.id.find(data.next_class).hide();
		}
		if (left < 0 - index.nav_item_w + 10) {
			index.id.find(data.prev_class).show();
		} else {
			index.id.find(data.prev_class).hide();
		}
	}

	function navArrowNextClicker() {
		$(data.next_class).click(function () {
			if (data.x + index.nav_w > index.nav_v_w) {
				data.x = data.x - index.nav_item_w - 10;
				index.id.find(data.nav_ul).stop().animate({left: data.x}, 800);
				showNav(data.x);
			}
		});
	}

	function navArrowPrevClicker() {
		$(data.prev_class).click(function () {
			if (data.x < 0) {
				data.x = data.x + index.nav_item_w + 10;
				index.id.find(data.nav_ul).stop().animate({left: data.x}, 800);
				showNav(data.x);
			}
		});
	}


})(jQuery);

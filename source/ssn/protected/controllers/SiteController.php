<?php

class SiteController extends FrontController {
    public $attempts = MAX_TIME_TO_SHOW_CAPTCHA;
    public $counter;

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page           
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('captcha'),
                'users' => array('*'),
            ),
        );
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    protected function performAjaxValidation($model) {
        try {
            if (isset($_POST['ajax'])) {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
        } catch (Exception $e) {
            Yii::log("Exception " . print_r($e, true), 'error');
            throw new CHttpException("Exception " . print_r($e, true));
        }
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        if(isset(Yii::app()->user->id))
            $this->redirect(Yii::app()->createAbsoluteUrl('/site/dashboard'));
        $this->redirect(Yii::app()->createAbsoluteUrl('/site/login'));
    }

    public function actionCheckCompany() {
        $companyName = $_POST['companyName'];
        $isExist = $this->isCompanyNameExist($companyName);
        $tmp = array();
        if ($isExist) {
            $tmp = $this->getCompanyByName($companyName);
        }
        echo json_encode($tmp);
        die;
    }

    public function isCompanyNameExist($companyName) {
        $company = SsnCompany::model()->findAll("name = '{$companyName}'");
        $r = FALSE;
        if ($company)
            $r = TRUE;
        return $r;
    }

    public function getCompanyByName($companyName) {
        $company = SsnCompany::model()->findAll(array('condition' => "name = '{$companyName}'"));
        $tmp = array();
        if ($company) {
            foreach ($company as $k => $item) {
                $tmp[$item->id] = "<div style='display: absolute; margin-top: -41px; margin-left: 20px'>". "Company Name: ".$item->name . '</br>' .'Company Phone ' . $item->phone . '</br>' .'Company Address ' . $item->address."</div>";
            }
        }
        return $tmp;
    }

    public function actionRegister() {
        $this->pageTitle = 'Register';
        $this->layout='column1';
        $model = new Users('createMemberFE');
        $company = new SsnCompany();
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);
        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
            $company->attributes = $_POST['SsnCompany'];
            if ($model->validate() && $company->validate()) {
                $model->status = STATUS_ACTIVE;
                $model->role_id = ROLE_MEMBER;
                $model->application_id = FE;
                $model->password_hash = md5($model->temp_password);
                $model->verify_code = Users::model()->checkVerifyCode(rand(100000, 1000000)); // Gen verify code and send qua mail or sms
                if ($model->save()) {
                    $c = new SsnCompany();
                    $c->user_id = $model->id;
                    if ($_POST['SsnCompany']['companyId'] == 0) {
                        $c->attributes = $_POST['SsnCompany'];
                        $c->save();
                    } else {
                        $oldCompany = SsnCompany::model()->findByPk($_POST['SsnCompany']['companyId']);
                        if ($oldCompany) {
                            $c->attributes = $oldCompany->attributes;
                            $c->save();
//                            $c->saveImage($fieldName);
                        }
                    }
                    $this->redirect(Yii::app()->createAbsoluteUrl('/member/site/updateProfile'));
                }
            }
        }
        $this->render('/partials/register', array(
            'model' => $model,
            'company' => $company
        ));
    }

    private function captchaRequired() {
        return Yii::app()->session->itemAt('captchaRequired') >= $this->attempts;
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        if(isset(Yii::app()->user->id))
            $this->redirect(Yii::app()->createAbsoluteUrl('/site/dashboard'));
        $this->layout='column1';
        $model = $this->captchaRequired() ? new LoginForm('captchaRequired') : new LoginForm();
        $model->login_by = 'username';
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            $user = new Users();
            $user->email = $model->login_by;
            $user->password_hash = $model->password;
            if ($model->validate()) {
                $this->redirect(Yii::app()->createAbsoluteUrl('member/site/updateProfile'));
                if($user->checkUser()){
                    Yii::app()->session->add('captchaRequired', 0);
                    Yii::app()->end();
                }
            } else {
                $this->counter = Yii::app()->session->itemAt('captchaRequired') + 1;
                Yii::app()->session->add('captchaRequired', $this->counter);
            }
        }
        $this->render('/partials/login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->createAbsoluteUrl('site/login'));
    }

    /**
     * send an email to enable member to reset password - bb - 27/7/2014
     */
    public function actionForgotPassword() {
        $model = new Users('forgotPassword');
        if (isset($_POST['Users'])) {
            $model->email = $_POST['Users']['email'];
            if ($model->validate()) {
                $model = Users::model()->findByAttributes(array('email' => trim($model->email)));
                SendEmail::forgotPasswordToUser($model);
                Yii::app()->user->setFlash('msg', "Email sent! You'll receive an email with instructions on how to set a new password.");
                $this->redirect(array('forgotPassword'));
            }
        }
        $this->render('forgot_password', array('model' => $model));
    }

    public function actionResetPassword() {
        $this->pageTitle = 'Reset password ' . ' - ' . Yii::app()->params['title'];
        try {
            $verify_code = trim($_GET['verify_code']);
            $model = Users::model()->findByAttributes(array('verify_code' => $verify_code));
            if (!$model)
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            $model->scenario = 'fe_reset_password';

            if (isset($_POST['Users'])) {
                $model->attributes = $_POST['Users'];

                if ($model->validate()) {
                    $newPass = $model->newPassword;
                    $model->password_hash = md5($newPass);
                    $model->temp_password = $newPass;
                    $model->verify_code = '';
                    $model->update(array('password_hash', 'temp_password', 'verify_code'));
                    $this->gotoPage(PAGE_SUCCESS_RESET_PASSWORD);
                }
            }

            $this->render('resetPassword', array(
                'model' => $model
            ));
        } catch (Exception $e) {
            Yii::log("Exception " . print_r($e, true), 'error');
            throw new CHttpException("Exception " . print_r($e, true));
        }
    }

    public function actionContactUs() {
        $contact_page_id = 87;
        $this->pageTitle = 'Liên hệ ';
        $general_enquiry = new ContactGeneralEnquiry('create');

        if (isset($_POST['ContactGeneralEnquiry'])) {
            $general_enquiry->attributes = $_POST['ContactGeneralEnquiry'];
            if ($general_enquiry->save()) {
                
            } else {
                Yii::app()->user->setFlash('error', 'General Enquiry cannot be submitted for some reasons');
                Yii::log(print_r($general_enquiry->getErrors(), true), 'error', 'SiteController.actionContact');
            }
        }


        $this->render('contact_us', array(
            'general_enquiry' => $general_enquiry,
        ));
    }

    public function actionUnderConstruction() {
        $this->render('underconstruction');
    }
    
    public function actionDashboard(){
        $this->pageTitle = 'Dashboard';
        $this->render('dashboard', array(
            
        ));
    }

}

<?php
$this->pageTitle = 'Error';
?>
<div class="container">
	<h2 class="title-2">Error</h2>
	<div class="main-content">                            
		<p><?php echo CHtml::encode($message); ?></p>
	</div>
</div><!-- Row -->

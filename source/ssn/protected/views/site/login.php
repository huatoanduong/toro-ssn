<div class="wrapper container">
    <div class="header">
        <div class="row">
            <div class="col-md-7">
                <ol class="breadcrumb">
                    <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/'); ?>"><span class="icon icon-home"></span></a></li>
                    <li class="active">Sign in</li>
                </ol>
                <h2 class="title-2">Sign in</h2>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-md-offset-3">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'login-form',
            'htmlOptions' => array('class' => 'form-horizontal', 'role' => 'form'),
            'enableClientValidation' => false,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
                ));
        ?>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'username', array('class' => 'col-sm-2')); ?>
                <div class="col-sm-3">
                    <?php echo $form->textField($model, 'username', array('class' => 'form-control', 'placeholder' => 'Username')); ?>
                    <?php echo $form->error($model, 'username'); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model, 'password', array('class' => 'col-sm-2')); ?>
                <div class="col-sm-3">
                    <?php echo $form->passwordField($model, 'password', array('class' => 'form-control', 'placeholder' => 'Password')); ?>
                    <?php echo $form->error($model, 'password'); ?>
                </div>
            </div>

            <?php if ($model->scenario == 'captchaRequired'): ?>
            <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'verifyCode', array('class' => 'col-sm-1')); ?>
                <div class="col-sm-6">
                    <?php $this->widget('CCaptcha'); ?>
                    <?php echo CHtml::activeTextField($model, 'verifyCode', array('class' => 'form-control')); ?>
                    <?php echo $form->error($model, 'verifyCode'); ?>
                    <div class="hint">Please enter the letters as they are shown in the image above.
                        <br/>Letters are not case-sensitive.</div>
                    </div>
                </div>
            <?php endif; ?>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-6">
                    <label>
                        <?php echo $form->checkBox($model, 'rememberMe'); ?>
                        Remember me
                    </label>
                    <?php echo $form->error($model, 'rememberMe'); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-6">
                    <button type="submit" class="btn btn-primary">Sign in</button>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-6">
                    <a class="default-link" href="<?php echo Yii::app()->createAbsoluteUrl('site/forgotPassword') ?>">I forgot my password</a>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-6">
                    <a class="default-link" href="<?php echo Yii::app()->createAbsoluteUrl('site/notReciverConfirmation') ?>">Not reciver confirmation?</a>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-6">
                    <label>
                        Not a member?
                        <a class="default-link" href="<?php echo Yii::app()->createAbsoluteUrl('site/register'); ?>">Register new membership</a>
                    </label>
                </div>
            </div>
        <?php $this->endWidget(); ?>
    </div>
</div>
<style>
    #login-form{
    }
</style>
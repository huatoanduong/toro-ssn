<!DOCTYPE html>
<html lang="en">
    <?php include '_head.php'; ?>
    <body class="skin-red-light sidebar-mini">
        <div class="wrapper">
        <?php include '_header.php'; ?>

        <?php $this->widget('MainMenu', array(
                            )); ?>
        <?php
        $controllerName = Yii::app()->controller->id;
        $actionName = Yii::app()->controller->action->id;
        $hasHomeBanner = array(
                'site/index',
        );
        $current_action = $controllerName . '/' . $actionName;
        ?>
        <div class="content-wrapper">
            <?php echo $content; ?>
        </div>
        <?php include '_footer.php'; ?>
        </div>
    </body>
</html>

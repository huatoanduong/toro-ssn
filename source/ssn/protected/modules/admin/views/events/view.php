<?php
$this->breadcrumbs = array(
    $this->pluralTitle => array('index'),
    'View ' . $this->singleTitle . ' : ' . $title_name,
);

$this->menu = array(
    array('label' => $this->pluralTitle, 'url' => array('index'), 'icon' => $this->iconList),
    array('label' => 'Update ' . $this->singleTitle, 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Create ' . $this->singleTitle, 'url' => array('create')),
);
?>
<h1>View <?php echo $this->singleTitle . ' : ' . $title_name; ?></h1>

<?php
//for notify message
$this->renderNotifyMessage();
//for list action button
echo $this->renderControlNav();
?><div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><span class="glyphicon glyphicon-list-alt"></span> View <?php echo $this->singleTitle ?></h3>
    </div>
    <div class="panel-body">
        <?php
        $this->widget('zii.widgets.CDetailView', array(
            'data' => $model,
            'attributes' => array(
                array('name' => 'Training Name'
                    ,'value' => $model->title),
                array(
                    'name' => 'country_id',
                    'type' => 'countryId',
                ),
                array(
                    'name' => 'City',
                    'type' => 'cityId',
                    'value' => $model->city_id
                ),
                array(
                    'name' => 'program_id',
                    'type' => 'programId',
                ),
                array(
                    'name' => 'Course Fee & Currency',
                    'type' => 'html',
                    'value' => $model->course_fee." ".$model->currency
                ),
                array(
                    'name' => 'Course Period',
                    'value' => DateHelper::toDateTimePickerFormat($model->start_date)." - ".DateHelper::toDateTimePickerFormat($model->end_date)
                ),
                array(
                    'name' => 'feature_image',
                    'type' => 'raw',
                    'value' => $model->feature_image != '' ? '<div class="thumbnail col-sm-3">' . CHtml::image(
                                    Yii::app()->createAbsoluteUrl($model->uploadImageFolder . '/' . $model->id . '/' . $model->feature_image), '', array(
                                'style' => 'width :100%',
                            )) . '</div>' : ''
                ),
                array(
                    'name' => 'short_content',
                    'type' => 'html',
                ),
                array(
                    'name' => 'content',
                    'type' => 'html',
                ),
                array(
                    'name' => 'created_date',
                    'type' => 'date',
                ),
            ),
        ));
        ?>
        <div class="well">
            <?php echo CHtml::htmlButton('<span class="' . $this->iconBack . '"></span> Back', array('class' => 'btn btn-default', 'onclick' => 'javascript: location.href=\'' . $this->baseControllerIndexUrl() . '\'')); ?>    </div>
    </div>
</div>

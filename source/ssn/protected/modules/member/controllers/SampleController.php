<?php

class SampleController extends MemberController {

    public $pluralTitle = 'Samples';
    public $singleTitle = 'Sample';
    public $cannotDelete = array();

    public function actionCreate() {
        $this->pageTitle = 'Create Sample';
        try {
            $model = new SsnSample('create');
            $model->created_date = date('d/m/Y');
            $model->from_user_id = Yii::app()->user->id;
            $item = new SsnSampleItem();
            $existItem = array();
            if (isset($_POST['SsnSample'])) {
                $model->attributes = $_POST['SsnSample'];
                $model->from_user_id = Yii::app()->user->id;
                $flag = true;
                foreach($_POST['SsnSampleItem'] as $nk => $ni){
                    $n = new SsnSampleItem('create');
                    $n->attributes = $ni;
                    $n->sample_id = 0;
                    $flag = $flag && $n->validate();
                    $existItem[] = $n;
                }
                $flag = $flag && $model->validate();
                if ($flag && $model->save()) {
                    $model->saveFile('document');
                    foreach($existItem as $k => $i){
                        $i->sample_id = $model->id;
                        if($i->save()){
                            $document = $_FILES['SsnSampleItem']['name'][$k]['document'];
                            $file = $_FILES['SsnSampleItem']["tmp_name"][$k]['document'];
                            @mkdir(Yii::getPathOfAlias('webroot').'/upload/item/files'."/".$i->id, 0777);
                            move_uploaded_file($file, Yii::getPathOfAlias('webroot').'/upload/item/files/'.$i->id.'/'.$document);
                            $i->document = $document;
                            $i->update(array('document'));
                            $i->saveFile('document');
                        }
                    }
                    $this->redirect(Yii::app()->createAbsoluteUrl('member/sample/index'));
                } 
            }
            $this->render('create', array(
                'model' => $model,
                'item' => $item,
                'key' => 0,
                'existItem' => $existItem
            ));
        } catch (exception $e) {
            Yii::log("Exception " . print_r($e, true), 'error');
            throw new CHttpException($e);
        }
    }

    public function actionDelete($id) {
        try {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                if (!in_array($id, $this->cannotDelete)) {
                    if ($model = $this->loadModel($id)) {
                        if ($model->delete())
                            Yii::log("Delete record " . print_r($model->attributes, true), 'info');
                    }

                    // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                    if (!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
                }
            } else {
                Yii::log("Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } catch (Exception $e) {
            Yii::log("Exception " . print_r($e, true), 'error');
            throw new CHttpException($e);
        }
    }

    public function actionIndex($t = SsnSample::STATUS_RECEIVED) {
        $this->pageTitle = 'Sample List';
        $model = new SsnSample('search');
        $model->unsetAttributes();  // clear any default values
        if($t == SsnSample::STATUS_SEND)
            $model->from_user_id = Yii::app()->user->id;
        else
            $model->to_user_id = Yii::app()->user->id;
        if (isset($_GET['SsnSample']))
            $model->attributes = $_GET['SsnSample'];    

        $this->render('index', array(
            'model' => $model,
            'data' => $model->searchSample(),
            'key' => 0,
            't' => $t,
        ));
    }
    
    public function actionSearchTable(){
        $model = new SsnSample('search');
        $model->unsetAttributes();  // clear any default values
        if($_GET['sample_type_search'] == SsnSample::STATUS_SEND)
            $model->from_user_id = Yii::app()->user->id;
        else
            $model->to_user_id = Yii::app()->user->id;
        if (isset($_GET['SsnSample']))
            $model->attributes = $_GET['SsnSample'];
        $this->renderPartial('_table_item', array('data' => $model->searchSample()));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        if (isset($_POST['SsnSample'])) {
            $model->attributes = $_POST['SsnSample'];
            if ($model->save()) {
                $this->setNotifyMessage(NotificationType::Success, $this->singleTitle . ' has been updated');
                $this->redirect(array('view', 'id' => $model->id));
            } else
                $this->setNotifyMessage(NotificationType::Error, $this->singleTitle . ' cannot be updated for some reasons');
        }
        //$model->beforeRender();
        $this->render('update', array(
            'model' => $model,
            'actions' => $this->listActionsCanAccess,
            'title_name' => $model->id));
    }

    public function actionView($id) {
        $this->pageTitle = 'View Sample';
            $model = $this->loadModel($id);
            if (isset($_POST['SsnSample'])) {
                $model->attributes = $_POST['SsnSample'];
                $model->from_user_id = Yii::app()->user->id;
                if ($model->save()) {
                    $item = SsnSampleItem::model()->findAll(array('condition' => "sample_id = {$model->id}"));
                    if($item){
                        $count = count($item);
                        for ($c = 0; $c < $count; $c++){
                            $item[$c]->attributes = $_POST['SsnSampleItem'][$c];
                            $item[$c]->save();
                        }
                    }
//                    foreach($_POST['SsnSampleItem'] as $k => $i){
//                        $newItem = new SsnSampleItem();
//                        $newItem->attributes = $i;
//                        $newItem->sample_id = $model->id;
//                        $newItem->save();
//                    }
                    Yii::app()->user->setFlash('successSample', "Your sample has been successfully changed.");
                    $this->redirect(Yii::app()->createAbsoluteUrl('member/sample/view', array('id' => $id)));
                } 
            }
            if($model->from_user_id == Yii::app()->user->id || $model->to_user_id ==  Yii::app()->user->id ){
                $item = new SsnSampleItem();
                $existItem = SsnSampleItem::model()->findAll(array('condition' => "sample_id = $id"));
                $this->render('view', array(
                    'model' => $model,
                    'title_name' => $model->id,
                    'item' => $item,
                    'existItem' => $existItem,
                    'key' => 0
                ));
            }
            else{
                throw new CHttpException(404, 'The requested page does not exist.');
            }
    }
    
    public function actionComment($id){
        $this->pageTitle = 'View Comment';
        try {
            $model = $this->loadModel($id);
            $comments = SsnSampleComment::getCommentBySample($id);
            $item = new SsnSampleItem();
            $this->render('_comment', array(
                'model' => $model,
                'title_name' => $model->id,
                'item' => $item,
                'comments' => $comments
                ));
        } catch (Exception $exc) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
    }
    
    public function actionInsertComment(){
        $comment = new SsnSampleComment();
        $comment->sample_id = $_POST['sampleId'];
        $comment->user_id = Yii::app()->user->id;
        $comment->comment = $_POST['comment'];
        $comment->created_date = date('Y-m-d H:i:s');
        echo $comment->save() ? 1:0;
    }

    /*
     * Bulk delete
     * If you don't want to delete some specified record please configure it in global $cannotDelete variable
     */

    public function actionDeleteAll() {
        $deleteItems = $_POST['ssn-sample-grid_c0'];
        $shouldDelete = array_diff($deleteItems, $this->cannotDelete);

        if (!empty($shouldDelete)) {
            SsnSample::model()->deleteAll('id in (' . implode(',', $shouldDelete) . ')');
            $this->setNotifyMessage(NotificationType::Success, 'Your selected records have been deleted');
        } else
            $this->setNotifyMessage(NotificationType::Error, 'No records was deleted');

        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    public function loadModel($id) {
        //need this define for inherit model case. Form will render parent model name in control if we don't have this line
        $initMode = new SsnSample();
        $model = $initMode->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
    
    public function actionAddNewItem() {
        $key = isset($_POST['key']) ? (int) $_POST['key'] : '999';
        $item = new SsnSampleItem();
        $form = new CActiveForm();
        $this->renderPartial('_item', array(
            'item' => $item,
            'form' => $form,
            'key' => $key,
            ), false, true);
    }

}

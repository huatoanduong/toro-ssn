<?php

class SiteController extends MemberController {

    /**
     * Declares class-based actions.
     */
    public function accessRules() {
        return array();
    }

    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function actionIndex() {
        $this->render('index');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionChangePassword() {
        $model = Users::getInforUser(Yii::app()->user->id);
        $model->scenario = 'changeMyPassword';

        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
            if ($model->validate()) {
                $model->password_hash = md5($model->newPassword);
                $model->temp_password = $model->newPassword;
                if ($model->update(array('password_hash', 'temp_password'))) {
                    SendEmail::changePassMailToUser($model);
                    Yii::app()->user->setFlash('successChangeMyPassword', "Your password has been successfully changed.");
                }
            }
        }

        $this->render('changePassword', array(
            'model' => $model,
        ));
    }

    public function actionUpdateProfile() {
        $this->pageTitle = 'Update Profile';
        $model = Users::getInforUser(Yii::app()->user->id);
        $tmpEmail = $model->email;
        $model->scenario = 'updateMyProfile';
        $changePass = 1;
        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
            $model->email = $tmpEmail;
            if(isset($_POST['Users']['temp_password']) || isset($_POST['Users']['password_confirm'])){
                $model->scenario = 'createAdmin';
                $changePass = 0;
                $model->password_confirm = $_POST['Users']['password_confirm'];
            }
            if($model->validate()){
                if(!empty($model->temp_password))
                    $model->password_hash = md5(trim($model->temp_password));
                if ($model->save()) {
                    $model->saveImage('image');
                    $changePass = 1;
                    Yii::app()->user->setFlash('success', "Your profile has been successfully changed.");
                }
            }
        }
        $this->render('updateProfile', array(
            'model' => $model,
            'view' => '_user',
            'changePass' => $changePass,
        ));
    }
    public function actionCompanyProfile() {
        $this->pageTitle = 'Company Profile';
        $model = SsnCompany::getCompanyByUser(Yii::app()->user->id);
        if (isset($_POST['SsnCompany'])) {
            $model->attributes = $_POST['SsnCompany'];
            $model->user_id = Yii::app()->user->id;
            if ($model->save()) {
                $model->saveImage('image');
                Yii::app()->user->setFlash('success', "Your profile has been successfully changed.");
            }
        }
        $this->render('updateProfile', array(
            'model' => $model,
            'view' => '_company'
        ));
    }
    public function actionCompanyProfileBK() {
        $this->pageTitle = 'Company Profile';
        $model = SsnCompany::getCompanyByUser(Yii::app()->user->id);
        if (isset($_POST['SsnCompany'])) {
            $model->attributes = $_POST['SsnCompany'];
            $model->user_id = Yii::app()->user->id;
            if ($model->save()) {
                $model->saveImage('image');
                Yii::app()->user->setFlash('success', "Your profile has been successfully changed.");
            }
        }
        $this->render('companyProfile', array(
            'model' => $model,
        ));
    }
    public function actionInvitePartner() {
        $model = SsnCompany::getCompanyByUser(Yii::app()->user->id);
        $model->scenario = 'updateMyProfile';

        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', "Your profile has been successfully changed.");
            }
        }
        $this->render('invitePartner', array(
            'model' => $model,
        ));
    }
    

}
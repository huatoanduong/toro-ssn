<?php
/* @var $model SsnSampleItem */
/* @var $index int */

$form= new CActiveForm;
$form->enableClientValidation = true;
?>
<div class="item">
	<h4>Item <?= str_replace('n', '', $index) + 1 ?></h4>
	<div class="row form-group">
		<div class="col-sm-9">
			<?= $form->textField($model, 'name', array(
				'name'=>"SsnSampleItem[$index][name]",
				'class'=>'form-control',
				'placeHolder'=>'Name'
			)) ?>
			<?= $form->error($model, 'name', array('id'=>"SsnSampleItem_{$index}_name_em_")) ?>
		</div>
		<div class="col-sm-3">
			<?= $form->dropDownList($model, 'unit', SsnSampleItem::getUnitList(), array(
				'name'=>"SsnSampleItem[$index][unit]",
				'class'=>'form-control',
				'empty'=>'N/A',
			)) ?>
			<?= $form->error($model, 'unit', array('id'=>"SsnSampleItem_{$index}_unit_em_")) ?>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-sm-9">
			<?= $form->textField($model, 'color', array(
				'name'=>"SsnSampleItem[$index][color]",
				'class'=>'form-control',
				'placeHolder'=>'Color'
			)) ?>
			<?= $form->error($model, 'color', array('id'=>"SsnSampleItem_{$index}_color_em_")) ?>
		</div>
		<div class="col-sm-3">
			<?= $form->textField($model, 'quantity', array(
				'name'=>"SsnSampleItem[$index][quantity]",
				'class'=>'form-control qty',
				'placeHolder'=>'Quantity'
			)) ?>
			<?= $form->error($model, 'quantity', array('id'=>"SsnSampleItem_{$index}_quantity_em_")) ?>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-sm-8">
			<?= $form->fileField($model, 'document', array(
				'name'=>"SsnSampleItem[$index][document]",
			)) ?>
			<?= $form->error($model, 'document', array('id'=>"SsnSampleItem_{$index}_document_em_")) ?>
			<?php if ($url = $model->getDocumentDownloadUrl()): ?>
			<a href="<?= $url ?>"><?= $model->document ?></a> 
			<?php else: ?>
			<p class="form-control-static">No document uploaded.</p>
			<?php endif ?>
		</div>
		<div class="col-sm-4">
			<button class="btn btn-danger pull-right btn-remove" type="button">Remove</button>
		</div>
	</div>
</div>

<link href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
<?php
Yii::app()->clientScript->registerScript('ajaxupdate', "
    $('#ssn-sample-grid a.ajaxupdate').on('click', function() {
        $.fn.yiiGridView.update('ssn-sample-grid', {
            type: 'POST',
            url: $(this).attr('href'),
            success: function() {
                $.fn.yiiGridView.update('ssn-sample-grid');
            }
        });
        return false;
    });
");
?>
<section class="content-header">
    <h1>
        Sample List
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Sample</li>
    </ol>
</section>
<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('ssn-sample-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});

$('#clearsearch').click(function(){
	var id='search-form';
	var inputSelector='#'+id+' input, '+'#'+id+' select';
	$(inputSelector).each( function(i,o) {
		 $(o).val('');
	});
	var data=$.param($(inputSelector));
	$.fn.yiiGridView.update('ssn-sample-grid', {data: data});
	return false;
});


");

?>

                    
<section class="content">
    <div class='search-form'>
        <?php $this->renderPartial('_search',array(
                'model'=>$model,

        )); ?>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="<?php echo (!isset($_GET['t']) || $_GET['t']  == SsnSample::STATUS_RECEIVED) ? 'active':''?>"><a href="<?php echo Yii::app()->createAbsoluteUrl('member/sample/index', array('t' => SsnSample::STATUS_RECEIVED)) ?>">Received</a></li>
                    <li class="<?php echo (isset($_GET['t']) && $_GET['t']  == SsnSample::STATUS_SEND) ? 'active':''?>"><a href="<?php echo Yii::app()->createAbsoluteUrl('member/sample/index', array('t' => SsnSample::STATUS_SEND)) ?>">Sent</a></li>
                    <li class="pull-right"><a href="<?php echo Yii::app()->createAbsoluteUrl('/member/sample/create') ?>" class="btn btn-primary">New</a></li>
                </ul>
                <div class="tab-content">
                    <?php
                    $columnArray = array();
                    $columnArray = array_merge($columnArray, array(
                        array(
                            'header' => 'S/N',
                            'type' => 'raw',
                            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                            'headerHtmlOptions' => array('width' => '30px', 'style' => 'text-align:center;'),
                            'htmlOptions' => array('style' => 'text-align:left;'),
                        ),
                        array(
                            'name' => 'from_user_id',
                            'value' => '$data->from_user_fk ? $data->from_user_fk->full_name:"-"',
                            'htmlOptions' => array('style' => 'text-align:left;')
                        ),
                        array(
                            'name' => 'to_user_id',
                            'value' => '$data->to_user_fk ? $data->to_user_fk->full_name:"-"',
                            'htmlOptions' => array('style' => 'text-align:left;')
                        ),
                        array(
                            'name' => 'to_user_id',
                            'value' => '$data->to_user_fk ? $data->to_user_fk->full_name:"-"',
                            'htmlOptions' => array('style' => 'text-align:left;')
                        ),
                        array(
                            'name' => 'to_user_id',
                            'value' => '$data->to_user_fk ? $data->to_user_fk->full_name:"-"',
                            'htmlOptions' => array('style' => 'text-align:left;')
                        ),
                        'awbn',
                        array(
                            'name' => 'created_date',
                            'type' => 'date',
                            'htmlOptions' => array('style' => 'text-align:left;')
                        ),
                        array(
                            'header' => 'Actions',
                            'class' => 'CButtonColumn',
                            'template' => '{view}{delete}',
                        ),
                    ));
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'ssn-sample-grid-bulk',
                        'enableAjaxValidation' => false,
                        'htmlOptions' => array('enctype' => 'multipart/form-data')));

                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'ssn-sample-grid',
                        'itemsCssClass' => 'table',
                        'htmlOptions' => array('class' => 'table-stripe', 'style' => 'width: 100%'),
                        'dataProvider' => $model->search(),
                        'pager' => array(
                            'header' => '',
                            'prevPageLabel' => 'Prev',
                            'firstPageLabel' => 'First',
                            'lastPageLabel' => 'Last',
                            'nextPageLabel' => 'Next',
                        ),
                        'selectableRows' => 2,
                        'columns' => $columnArray,
                    ));
                    $this->endWidget();
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    .breadcrumb > li + li:before {
        color: #CCCCCC;
        content: " | ";
        padding: 0 5px;
    }
</style>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
<script>
    $('document').ready(function(){
        $(".datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
        
        $('#SsnSample__search_time').on('change', function(){
            if($(this).val() === '4'){
                $('.start_date').removeClass('hidden');
                $('.end_date').removeClass('hidden');
            }else{
                $('.start_date').addClass('hidden');
                $('.end_date').addClass('hidden');
            }
        });
    });
</script>
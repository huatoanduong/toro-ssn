<?php
/* @var $this SampleController */
/* @var $model SsnSample */
/* @var $form CActiveForm */
?>
<?php $this->widget('application.components.widget.Notification'); ?>				

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ssn-sample-form',
	'enableAjaxValidation'=>false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array(
//		'class' => 'form-horizontal', 
		'role' => 'form', 
		'enctype' => 'multipart/form-data'
	),
));
$addItemUrl = $this->createUrl('addItem');
$autoCompleteUrl = $this->createUrl('partners');
Yii::app()->clientScript->registerScript(time(), "
	ssn.setupSampleDetailPage('{$addItemUrl}','{$autoCompleteUrl}');
", CClientScript::POS_LOAD);
?>
	<div class="box-body">
		<div class="form-group">
			<?php echo $form->labelEx($model,'dateSentText', array('class'=>'control-label')); ?>
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'model' => $model,
				'attribute' => 'dateSentText',
				'options' => array(
					'dateFormat' => 'dd/mm/yy',
				),
				'htmlOptions'=>array(
					'class'=>'form-control'
				),
			)); ?>
			<?php echo $form->error($model,'dateSentText'); ?>
		</div>
		<?php if (Yii::app()->user->id != $model->from_user_id): ?>
		<div class="form-group">
			<label for="" class="control-label">Send from</label>
			<p class="form-control-static">
				<a href="<?= $this->createUrl('partner/view', array('id'=> $model->from_user_id)) ?>"><?= $model->sender->company->name ?></a>
			</p>
		</div>
		<?php endif ?>
		<div class="form-group">
			<?php echo $form->labelEx($model,'to_company', array('class'=>'control-label')); ?>
			<?php $this->widget('zii.widgets.jui.CJuiAutoComplete',array(
				'model' => $model,
				'attribute' => 'to_company',
				'sourceUrl'=> $this->createUrl('partners'),
				'options'=>array(
					'minLength'=>'2',
					'appendTo' => '#ac-container'
				),
				'htmlOptions'=>array(
					'class'=>'form-control'
				),
			)); ?>
			<?php echo $form->error($model,'to_company'); ?>
			
			<div id="ac-container" style="position:absolute; width: 500px;"></div>
			<div class="hide user-token-tpl user-token">
				<span class="comp-name"></span>
				<a href="javascript:void(0)" class="btn-remove"><i class="fa fa-close text-danger"></i></a>
				<input type="hidden" name="SsnSample[to_user_ids][]" disabled="disabled" />
			</div>
			<div class="user-ids">
				<?php foreach ($model->receivers as $user): ?>
				<div class="user-token">
					<span class="comp-name"><?= $user->company->name ?> (<?= $user->company->address ?>)</span>
					<a href="javascript:void(0)" class="btn-remove"><i class="fa fa-close text-danger"></i></a>
					<input type="hidden" name="SsnSample[to_user_ids][]" value="<?= $user->id ?>" />
				</div>
				<?php endforeach ?>
			</div>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'attention', array('class'=>'control-label')); ?>
			<?php echo $form->textField($model,'attention',array('class'=>'form-control','maxlength'=>255)); ?>
			<?php echo $form->error($model,'attention'); ?>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<?php echo $form->labelEx($model,'awbn', array('class'=>'control-label')); ?>
					<?php echo $form->textField($model,'awbn',array('class'=>'form-control','maxlength'=>255)); ?>
					<?php echo $form->error($model,'awbn'); ?>
					<?php if (!$model->isNewRecord): ?>
					<p><a href="<?= $model->getTrackingLink() ?>" target="_blank">Go to tracking page</a></p>
					<?php endif ?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<?php echo $form->labelEx($model,'courier_id', array('class'=>'control-label')); ?>
					<?php echo $form->dropDownList($model, 'courier_id', SsnCourier::getListData(),
						array('class' => 'form-control', 'empty'=>'Other')); ?>
					<?php echo $form->error($model,'courier_id'); ?>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="" class="control-label">Item</label>
			<div class="list-item">
				<?php foreach($model->form_items as $index => $item): ?>
				<?php $this->renderPartial('_sample-item', array(
					'index' => $index,
					'model' => $item,
				)) ?>
				<?php endforeach ?>
			</div>
			<p><button type="button" class="btn btn-primary btn-add">Add item</button></p>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'remark', array('class'=>'control-label')); ?>
			<?php echo $form->textArea($model,'remark',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'remark'); ?>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'document', array('class'=>'control-label')); ?>
			<?php echo $form->fileField($model, 'document'); ?>
			<?php echo $form->error($model, 'document'); ?>
			<?php if ($url = $model->getDocumentDownloadUrl()): ?>
			<a href="<?= $url ?>"><?= $model->document ?></a>
			<?php else: ?>
			<p class="form-control-static">No document uploaded.</p>
			<?php endif ?>
		</div>	
	</div>
	<!-- /.box-body -->

	<div class="box-footer">
		<button type="submit" class="btn btn-primary"><?php echo $model->isNewRecord ? 'Add' : 'Save' ?></button>
	</div>

<?php $this->endWidget(); ?>

<?php
/* @var $this ShippingController */
/* @var $model SsnShipping */
$this->pageTitle = 'New Sample';
?>

<section class="content-header">
    <h1>New Sample</h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo Yii::app()->homeUrl ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?= $this->createUrl('index') ?>">Sample</a></li>
        <li class="active">New</li>
    </ol>
</section>

<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Information</h3>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
	</div>
	<!-- /.box -->
</section>


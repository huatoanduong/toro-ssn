<?php
$this->pageTitle = 'Sample Detail';
?>
<section class="content-header">
    <h1>Sample Detail</h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo Yii::app()->homeUrl ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?= $this->createUrl('index') ?>">Sample List</a></li>
        <li class="active">Sample Detail</li>
    </ol>
</section>
<section class="content-header">
<?php if(Yii::app()->user->hasFlash('successSample')):?>
    <div class="alert alert-success" role="alert">
        <?php echo Yii::app()->user->getFlash('successSample'); ?>
    </div>
<?php endif; ?>
</section>
<section class="content">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li class="active"><a href="javascript:void(0)">Detail</a></li>
			<li><a href="<?= $this->createUrl('comment', array('id'=>$model->id)) ?>">Comment</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active">
				<?php $this->renderPartial('_form', array('model'=>$model)) ?>
			</div><!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- nav-tabs-custom -->
</section>


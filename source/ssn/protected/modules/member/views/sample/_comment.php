<section class="content-header">
    <h1>
        Sample
        <small>Comment</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo Yii::app()->createAbsoluteUrl('member/sample/index')?>">Sample List</a></li>
        <li class="active">View Comment</li>
    </ol>
</section>
<section class="content">

    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li><a href="<?php echo Yii::app()->createAbsoluteUrl('member/sample/view', array('id' =>  $model->id, 't' => $t))?>">Detail</a></li>
                    <li class="active"><a href="<?php echo Yii::app()->createAbsoluteUrl('member/sample/comment', array('id' =>  $model->id))?>">Comment</a></li>
                </ul>
                    <div class="box-body">
                    <div class="direct-chat-messages">
                        <?php foreach($comments as $k => $comment):?>
                            <?php 
                                $alignName = $comment->user_id != Yii::app()->user->id ? 'left':'right';
                                $alignTime = $comment->user_id != Yii::app()->user->id ? 'right':'left';
                            ?>
                            <div class="direct-chat-msg <?php echo $alignName?>">
                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name pull-<?php echo $alignName?>"><?php echo $comment->user_fk ? $comment->user_fk->full_name:''?></span>
                                    <span class="direct-chat-timestamp pull-<?php echo $alignTime?>"><?php echo $comment->created_date?></span>
                                </div>
                                <!--<img class="direct-chat-img" src="dist/img/user1-128x128.jpg" alt="message user image" /> /.direct-chat-img -->
                                <div class="direct-chat-text">
                                    <?php echo nl2br($comment->comment)?>
                                </div>
                            </div>
                        <?php endforeach;?>


                    </div>
                    </div>
                    <div class="box-footer">
                      <form action="#" method="post">
                        <div class="input-group">
                        <textarea type="text" id="message" name="message" placeholder="Type Message ..." class="form-control" style="height: 34px;"/></textarea>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-warning btn-flat send">Send</button>
                            </span>
                        </div>
                      </form>
                    </div>
                </div>
            </div>
        </div>
</section>
<script>
    $(document).ready(function(){
        $('.send').on('click',function(ev){
            ev.preventDefault();
            var uname = '<?php echo Yii::app()->user->full_name?>';
            var message = $('#message').val();
            var currentdate = new Date(); 
            var datetime = currentdate.getDate() + "-"
                + (currentdate.getMonth()+1)  + "-" 
                + currentdate.getFullYear() + " "  
                + currentdate.getHours() + ""  
                + currentdate.getMinutes() + "-" 
                + currentdate.getSeconds();
            if(message.trim() == ''){
                alert('Please input comment');
            }else{
                $.ajax({
                    type: 'POST',
                    url: '<?php echo Yii::app()->createAbsoluteUrl('member/sample/insertComment')?>',
                    data: {"sampleId":'<?php echo $model->id?>', "comment":message},
                    success: function(data){
                        var data = "<div class='direct-chat-msg right'>"
                                +"<div class='direct-chat-info clearfix'>"
                                +"    <span class='direct-chat-name pull-right'>"+uname+"</span>"
                                +"    <span class='direct-chat-timestamp pull-left'>"+datetime+"</span>"
                                +"</div>"
                                
                                +"<div class='direct-chat-text'>"
                                +message+"                                </div>"
                                +"</div>";
                        $('.direct-chat-messages').append(data);
                        window.location.reload();
                    }
                });
            }
        });
    });
</script>
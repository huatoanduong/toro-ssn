<?php
/* @var $model SsnSampleSearch */
$items = $model->search();
?>
<?php if ($items): ?>
<div class="table-responsive">
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th rowspan="2" class="text-center">Date Sent</th>
				<th rowspan="2" class="text-center"><?= $model->mode==SsnSampleSearch::MODE_RECV ? 
	'Sender' : 'Receiver' ?></th>
				<th rowspan="2" class="text-center">AWBN</th>
				<th colspan="3" class="text-center">Item</th>
				<th rowspan="2" class="text-center">Remark</th>
			</tr>
			<tr>
				<th class="text-center">Name</th>
				<th class="text-center">Color</th>
				<th class="text-center">Qty</th>
			</tr>
		</thead>
		
		<tbody>
		<?php foreach($items as $sample): ?>
		<?php $rs = count($sample->items)>0 ?count($sample->items) : 1 ?>
		<tr>
			<td rowspan="<?= $rs ?>" class="text-center">
				<a href="<?= $this->createUrl('view', array('id'=> $sample->id)) ?>">
					<?= $sample->dateSentText ?>
				</a>
			</td>
			<td rowspan="<?= $rs ?>">
				<?php if ($model->mode== SsnSampleSearch::MODE_RECV): ?>
				<a href="<?= $this->createUrl('partner/view', array('id'=>$sample->sender->id)) ?>>
					<?= $sample->senderName ?>
				 </a>
				<?php else: ?>
					<?php foreach($sample->receivers as $receiver): ?>
					<p><a href="<?= $this->createUrl('partner/view', array('id'=>$receiver->id)) ?>">
						<?= $receiver->company->name ?>
					</a></p>
					<?php endforeach ?>
				<?php endif ?>
				</a>
			</td>
			
			<td rowspan="<?= $rs ?>" class="text-center">
				<a href="<?= $sample->getTrackingLink() ?>" target="_blank"><?= $sample->awbn ?></a>
			</td>
			
			<?php if (count($sample->items)>0): ?>
			<?php $item = $sample->items[0] ?>
				<td><?= $item->name ?></td>
				<td><?= $item->color ?></td>
				<td class="text-center"><?= $item->quantity ?></td>
			<?php else: ?>
				<td></td>
				<td></td>
				<td></td>
			<?php endif ?>
			
			<td rowspan="<?= $rs ?>"><span data-toggle="tooltip" data-placement="bottom" title="<?= $sample->remark ?>"><?= $sample->remarkExcerpt ?></span></td>
		</tr>
		
		<?php for($i=1; $i<count($sample->items); $i++): ?>
		<?php $item = $sample->items[$i] ?>
		<tr>
			<td><?= $item->name ?></td>
			<td><?= $item->color ?></td>
			<td class="text-center"><?= $item->quantity ?></td>
		</tr>
		<?php endfor ?>
		<?php endforeach ?>
		
		</tbody>
	</table>
</div>

<div class="clearfix">
<?php $this->widget('CLinkPager', array(
	'pages' => $model->pagination,
	'header' => '',
	'nextPageLabel' => 'Next',
	'prevPageLabel' => 'Previous',
	'firstPageLabel' => 'First',
	'lastPageLabel' => 'Last',
	'selectedPageCssClass'=>'active',
	'maxButtonCount' => 10,
	'htmlOptions'=>array(
		'class' => 'pagination pull-right',
	)
)) ?>
</div>

<?php else: ?>
<p>There are no items.</p>
<?php endif ?>

<?php
/* @var $this ShippingController */
/* @var $model SsnShipping */

$this->breadcrumbs=array(
	'Ssn Shippings'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SsnShipping', 'url'=>array('index')),
	array('label'=>'Create SsnShipping', 'url'=>array('create')),
	array('label'=>'View SsnShipping', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SsnShipping', 'url'=>array('admin')),
);
?>

<h1>Update SsnShipping <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this ShippingController */
/* @var $model SsnShipping */
$this->pageTitle = 'Shipping Detail';
?>

<section class="content-header">
    <h1>Shipping Detail</h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo Yii::app()->homeUrl ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?= $this->createUrl('index') ?>">Shipping Schedule</a></li>
        <li class="active">Shipping Detail</li>
    </ol>
</section>

<section class="content">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li class="active"><a href="javascript:void(0)">Detail</a></li>
			<li><a href="<?= $this->createUrl('comment', array('id'=>$model->id)) ?>">Comment</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active">
				<?php $this->renderPartial('_form', array('model'=>$model)) ?>
			</div><!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- nav-tabs-custom -->
</section>


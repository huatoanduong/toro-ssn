<?php
/* @var $this ShippingController */
/* @var $model SsnShipping */
/* @var $form CActiveForm */
?>
<?php $this->widget('application.components.widget.Notification'); ?>				

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ssn-shipping-form',
	'enableAjaxValidation'=>false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array(
//		'class' => 'form-horizontal', 
		'role' => 'form', 
		'enctype' => 'multipart/form-data'
	),
));
$addItemUrl = $this->createUrl('addItem');
$autoCompleteUrl = $this->createUrl('partners');
Yii::app()->clientScript->registerScript(time(), "
	ssn.setupShippingDetailPage('{$addItemUrl}','{$autoCompleteUrl}');
", CClientScript::POS_LOAD);
?>
	<div class="box-body">
		<!--date load-->
		<div class="form-group">
			<?php echo $form->labelEx($model,'dateLoadText', array('class'=>'control-label')); ?>
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'model' => $model,
				'attribute' => 'dateLoadText',
				'options' => array(
					'dateFormat' => 'dd/mm/yy',
				),
				'htmlOptions'=>array(
					'class'=>'form-control'
				),
			)); ?>
			<?php echo $form->error($model,'dateLoadText'); ?>
		</div>
		
		<?php if ($model->isRecipient()): ?>
		<!--send from-->
		<div class="form-group">
			<label for="" class="control-label">Send from</label>
			<p class="form-control-static">
				<a href="<?= $this->createUrl('partner/view', array('id'=> $model->from_user_id)) ?>">
					<?= $model->sender->company->name ?>
				</a>
			</p>
		</div>
		<?php endif ?>
		
		<?php if ($model->isSender()): ?>
		<!--send to-->
		<div class="form-group">
			<?php echo $form->labelEx($model,'to_company', array('class'=>'control-label')); ?>
			<?php $this->widget('zii.widgets.jui.CJuiAutoComplete',array(
				'model' => $model,
				'attribute' => 'to_company',
				'sourceUrl'=> $this->createUrl('partners'),
				'options'=>array(
					'minLength'=>'2',
					'appendTo' => '#ac-container'
				),
				'htmlOptions'=>array(
					'class'=>'form-control'
				),
			)); ?>
			<?php echo $form->error($model,'to_company'); ?>

			<div id="ac-container" style="position:absolute; width: 500px;"></div>
			<div class="hide user-token-tpl user-token">
				<span class="comp-name"></span>
				<a href="javascript:void(0)" class="btn-remove"><i class="fa fa-close text-danger"></i></a>
				<input type="hidden" name="SsnShipping[to_user_id]" disabled="disabled" />
			</div>
			<!-- recipient list-->
			<div class="user-ids">
				<?php if ($model->receiver): ?>
				<?php $user = $model->receiver ?>
				<div class="user-token">
					<span class="comp-name"><?= $user->company->name ?> (<?= $user->company->address ?>)</span>
					<a href="javascript:void(0)" class="btn-remove"><i class="fa fa-close text-danger"></i></a>
					<input type="hidden" name="SsnShipping[to_user_id]" value="<?= $user->id ?>" />
				</div>
				<?php endif ?>
			</div>
		</div>
		<?php endif ?>
		
		<!--order number-->
		<div class="form-group">
			<?php echo $form->labelEx($model,'order_number', array('class'=>'control-label')); ?>
			<?php echo $form->textField($model,'order_number',array('class'=>'form-control','maxlength'=>255)); ?>
			<?php echo $form->error($model,'order_number'); ?>
		</div>
		
		<!--item list-->
		<div class="form-group">
			<label for="" class="control-label">Item</label>
			<div class="item-list">
				<?php foreach($model->form_items as $index => $item): ?>
				<?php $this->renderPartial('_shipping-item', array(
					'index' => $index,
					'model' => $item,
				)) ?>
				<?php endforeach ?>
			</div>
			<p><button type="button" class="btn btn-primary btn-add">Add item</button></p>
		</div>
		
		<!--remark-->
		<div class="form-group">
			<?php echo $form->labelEx($model,'remark', array('class'=>'control-label')); ?>
			<?php echo $form->textArea($model,'remark',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'remark'); ?>
		</div>
		
		<!--document-->
		<div class="form-group">
			<?php echo $form->labelEx($model,'document', array('class'=>'control-label')); ?>
			<?php echo $form->fileField($model, 'document'); ?>
			<?php echo $form->error($model, 'document'); ?>
			<?php if ($url = $model->getDocumentDownloadUrl()): ?>
			<a href="<?= $url ?>"><?= $model->document ?></a>
			<?php else: ?>
			<p class="form-control-static">No document uploaded.</p>
			<?php endif ?>
		</div>	
	</div>
	<!-- /.box-body -->

	<div class="box-footer">
		<button type="submit" class="btn btn-primary"><?php echo $model->isNewRecord ? 'Add' : 'Save' ?></button>
	</div>

<?php $this->endWidget(); ?>
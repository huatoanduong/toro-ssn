<section class="content-header">
    <h1>
        Profile
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Profile</li>
    </ol>
</section>
<section class="content-header">
<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-success" role="alert">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>
</section>

<section class="content">

    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="<?php echo $view == '_user' ? 'active':''?>"><a href="<?php echo Yii::app()->createAbsoluteUrl('/member/site/updateProfile');?>" class="tab" >Admin Info</a></li>
                    <li class="<?php echo $view == '_user' ? '':'active'?>"><a href="<?php echo Yii::app()->createAbsoluteUrl('/member/site/companyProfile');?>" class="tab" >Company</a></li>
                </ul>
                <div class="tab-content">
                    <?php $this->renderPartial("_profile/{$view}", array('model' => $model, 'changePass' => $changePass))?> 
                </div>
            </div>
        </div>
    </div>
</section>
<script>
</script>
<script>
    $(document).ready(function(){
        $('.changePassword').on('click', function(){
            var id = $('#change_pass').val();
            if(id === '0'){
                $('.psw').removeClass('hidden');
                $('#Users_temp_password').prop('disabled', false);
                $('#Users_password_confirm').prop('disabled', false);
                $(this).text('Unchange password');
                $('#change_pass').val(1);
            }else{
                $('.psw').addClass('hidden');
                $('#Users_temp_password').prop('disabled', true);
                $('#Users_password_confirm').prop('disabled', true);
                $(this).text('Change password');
                $('#change_pass').val(0);
            }
        });
        
    });
    $('#SsnCompany_area_code_id').on('change', function(){
        
    });
    $(window).load(function() {
        $('.changePassword').trigger('click');
        getModule();
    });
    
    
    
    $("#SsnCompany_area_code_id").change(getModule);
    
	function getModule()
	{
            var url = "http://ssn.trandoanphilong.com/member/site/getListByArea";
            var request = $.ajax({
                type: "post",
                url: url,
                data: { module: $("#SsnCompany_area_code_id").val(), selected: ''}
              }).done(function(msg) {
                $("#SsnCompany_city_id").html(msg);                
              });
	}
	
	
</script>
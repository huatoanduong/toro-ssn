<div class="tab-pane active">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'users-model-form',
        'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
            'htmlOptions' => array('class' => 'form-horizontal', 'role' => 'form', 'enctype' => 'multipart/form-data'),
    )); ?>
        <div class="box-body">
            <div class="col-md-6">
            <p class="note">Fields with <span class="required">*</span> are required.</p>
            <div class="form-group">
                <?php echo $form->labelEx($model,'first_name',array('label'=>'First Name', 'class' => '')); ?>
                <?php echo $form->textField($model,'first_name',array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'first_name'); ?>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model,'last_name',array('label'=>'Last Name', 'class' => '')); ?>
                <?php echo $form->textField($model,'last_name',array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'last_name'); ?>
            </div>
            <div class="form-group">
                <a href="javascript:;" class="changePassword" data-id="0">Change Password</a>
            </div>
            <div class="psw hidden">
                <input type="hidden" name="change_pass" value="<?php echo $changePass;?>" id="change_pass"/>
                <div class="form-group">
                    <?php echo $form->labelEx($model,'temp_password', array('class' => '')); ?>
                    <?php echo $form->passwordField($model,'temp_password',array('value'=>'', 'class' => 'form-control', 'disabled' => true)); ?>
                    <?php echo $form->error($model,'temp_password'); ?>
                </div>

                <div class="form-group">
                    <?php echo $form->labelEx($model,'password_confirm', array('class' => '')); ?>
                    <?php echo $form->passwordField($model,'password_confirm',array('maxlength'=>30,'value'=>'', 'class' => 'form-control', 'disabled' => true)); ?>
                    <?php echo $form->error($model,'password_confirm'); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model,'email', array('class' => '')); ?>
                <?php echo $form->textField($model,'email',array('class' => 'form-control', 'disabled' => true)); ?>
                <?php echo $form->error($model,'email'); ?>
            </div>

            <div class="form-group">
                <?php echo $form->labelEx($model,'phone_public', array('label'=>'CellPhone', 'class' => '')); ?>
                <?php echo $form->textField($model,'phone_public',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'phone_public'); ?>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model,'phone_private', array('label'=>'Phone', 'class' => '')); ?>
                <?php echo $form->textField($model,'phone_private',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'phone_private'); ?>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model,'skype', array('label'=>'Skype', 'class' => '')); ?>
                <?php echo $form->textField($model,'skype',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'skype'); ?>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model,'whatapps', array('label'=>'Whatapps', 'class' => '')); ?>
                <?php echo $form->textField($model,'whatapps',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'whatapps'); ?>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model,'line', array('label'=>'Line', 'class' => '')); ?>
                <?php echo $form->textField($model,'line',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'line'); ?>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model,'viber', array('label'=>'Viber', 'class' => '')); ?>
                <?php echo $form->textField($model,'viber',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'viber'); ?>
            </div>
            <div class="form-group">
                <?php echo $form->labelEx($model,'wechat', array('label'=>'Wechat', 'class' => '')); ?>
                <?php echo $form->textField($model,'wechat',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'wechat'); ?>
            </div>


            <div class="form-group">
                    <input type="submit" name="yt0" value="Save" class="btn btn-primary">
            </div>
        </div>
    <?php $this->endWidget(); ?>
</div>
<div class="tab-pane">
</div>
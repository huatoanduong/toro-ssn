<section class="content-header">
    <h1>
        Company Profile
        <small>Update</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Company Profile</li>
    </ol>
</section>
<section class="content-header">
<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-success" role="alert">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>
</section>
<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Information</h3>
            </div>
                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'users-model-form',
                        'enableAjaxValidation'=>false,
                        'enableClientValidation'=>true,
                        'clientOptions'=>array(
                            'validateOnSubmit'=>true,
                        ),
                            'htmlOptions' => array('class' => 'form-horizontal', 'role' => 'form', 'enctype' => 'multipart/form-data'),
                    )); ?>
                    <div class="box-body">
                <div class="col-md-6">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'image', array('class' => '')); ?>
                            <div class="col-sm-12">
                                <?php if (!$model->isNewRecord && $model->is_img_active == 1) : ?>
                                    <?php if (!empty($model->image)): ?>
                                        <div class="thumbnail" id="thumbnail-<?php echo $model->id; ?>">
                                            <button href="" type="button" class="btn pull-right img-del" data-id="<?php echo $model->id; ?>"></button>
                                            <?php echo CHtml::image(ImageHelper::getImageUrl($model, "image", "large"), '', array()); ?>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php if($model->is_img_active == 0):?>
                                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo.png" alt="Blocked Image"/>
                                <?php else:?>
                                <?php echo $form->fileField($model, 'image', array('title' => "Upload " . $model->getAttributeLabel('image'))); ?>
                                <div class='notes'>Recommended Size: <?php echo $model->imageSize ?> (width x height), Allow file type  <?php echo '*.' . str_replace(',', ', *.', $model->allowImageType); ?> - Maximum file size : <?php echo ($model->maxImageFileSize / 1024) / 1024; ?>M </div>
                                <?php echo $form->error($model, 'image'); ?>
                                <?php endif;?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo $form->labelEx($model,'name',array()); ?>
                            <?php echo $form->textField($model,'name',array('class' => 'form-control')); ?>
                            <?php echo $form->error($model,'name'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $form->labelEx($model,'address', array('class' => '')); ?>
                            <?php echo $form->textField($model,'address',array('class' => 'form-control')); ?>
                            <?php echo $form->error($model,'address'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $form->labelEx($model,'website', array('class' => '')); ?>
                            <?php echo $form->textField($model,'website',array('class' => 'form-control')); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $form->labelEx($model,'phone', array('class' => '')); ?>
                            <?php echo $form->textField($model,'phone',array('class' => 'form-control')); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $form->labelEx($model,'size', array('class' => '')); ?>
                            <?php echo $form->textField($model,'size',array('class' => 'form-control')); ?>
                        </div>
                        <div class="row form-group">
                            <?php echo $form->labelEx($model,'about', array('class' => '')); ?>
                            <?php echo $form->textArea($model,'about',array('class' => 'form-control')); ?>
                        </div>
                        <div class="form-group">>
                            <input type="submit" name="yt0" value="Save" class="btn btn-primary">
                        </div>
                    </div>
                <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
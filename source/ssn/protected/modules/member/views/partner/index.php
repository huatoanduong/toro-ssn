<?php
Yii::app()->clientScript->registerScript('ajaxupdate', "
    $('#ssn-partner-grid a.ajaxupdate').on('click', function() {
        $.fn.yiiGridView.update('ssn-partner-grid', {
            type: 'POST',
            url: $(this).attr('href'),
            success: function() {
                $.fn.yiiGridView.update('ssn-partner-grid');
            }
        });
        return false;
    });
");
?>
<section class="content-header">
    <h1>
        Partner
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Partner</li>
    </ol>
</section>
<section class="content">

    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="<?php echo $view == '_approved' ? 'active':''?>"><a href="<?php echo Yii::app()->createAbsoluteUrl('member/partner/index', array('t' =>  SsnPartner::STATUS_APPROVE))?>">Partners (<?php echo $t_approve?>)</a></li>
                    <li class="<?php echo $view == '_sent' ? 'active':''?>"><a href="<?php echo Yii::app()->createAbsoluteUrl('member/partner/index', array('t' =>  SsnPartner::STATUS_SEND))?>">Request Sent (<?php echo $t_sent?>)</a></li>
                    <li class="<?php echo $view == '_received' ? 'active':''?>"><a href="<?php echo Yii::app()->createAbsoluteUrl('member/partner/index', array('t' =>  SsnPartner::STATUS_PENDING))?>">Waiting for Response (<?php echo $t_pending?>)</a></li>
                    <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/member/partner/create')?>" style="background: #3c8dbc; color: #FFF">New Request</a></li>
                </ul>
                <div class="tab-content">
                    <?php
                        $this->renderPartial("_status/$view", array('model' => $model));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>

<style>
    .breadcrumb > li + li:before {
        color: #CCCCCC;
        content: " | ";
        padding: 0 5px;
    }
</style>
<script>
    $(document).ready(function(){
        $('body').on('click','#ssn-partner-grid a.delete',function() {
                if(!confirm('Are you sure you want to delete this item?')) return false;
                alert(11);
                var th = this,
                        afterDelete = function(){};
                jQuery('#ssn-partner-grid').yiiGridView('update', {
                        type: 'POST',
                        url: jQuery(this).attr('href'),
                        success: function(data) {
                                window.location.reload();
                        },
                        error: function(XHR) {
                                window.location.reload();
                        }
                });
                return false;
        });
        
    });
</script>

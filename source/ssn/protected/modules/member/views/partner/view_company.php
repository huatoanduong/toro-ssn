<section class="content-header">
    <h1>
        Partner Info
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo Yii::app()->createAbsoluteUrl('member/partner/index')?>">Partner</a></li>
        <li class="active">Partner Info</li>
    </ol>
</section>
<section class="content">

    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class=""><a href="<?php echo Yii::app()->createAbsoluteUrl('/member/partner/view', array('id' => $_GET['id']));?>" class="tab" >Admin Info</a></li>
                    <li class="active"><a href="<?php echo Yii::app()->createAbsoluteUrl('/member/partner/viewCompany', array('id' => $_GET['id']));?>" class="tab" >Company</a></li>
                </ul>
                <div class="tab-content">
                    <table class="table table-hover">
                        <tr>
                            <td style="width: 15%;">&nbsp;</td>
                            <td>
                                <?php if($model->is_img_active == 0):?>
                                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo.png" alt="Blocked Image"/>
                                <?php else:?>
                                    <?php echo CHtml::image(ImageHelper::getImageUrl($model, "image", "large"), '', array()); ?>
                                <?php endif;?>
                            </td>
                        </tr>
                        <tr>
                            <td>Company Name</td>
                            <td><?php echo $model->name?></td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td><?php echo $model->address?></td>
                        </tr>
                        <tr>
                            <td>Website</td>
                            <td><?php echo $model->website?></td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td><?php echo $model->phone?></td>
                        </tr>
                        <tr>
                            <td>Size</td>
                            <td><?php echo $model->size?></td>
                        </tr>
                        <tr>
                            <td>About</td>
                            <td><?php echo $model->about?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
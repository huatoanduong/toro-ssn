<?php
$allowAction = in_array("delete", array()) ? 'CCheckBoxColumn' : '';
$columnArray = array();
if (in_array("Delete", array())) {
    $columnArray[] = array(
        'value' => '$data->id',
        'class' => "CCheckBoxColumn",
    );
} 

$columnArray = array_merge($columnArray, array(
    array(
        'header' => 'S/N',
        'type' => 'raw',
        'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
        'headerHtmlOptions' => array('width' => '30px', 'style' => 'text-align:center;'),
        'htmlOptions' => array('style' => 'text-align:left;')
    ),
    array(
        'name' => 'from_user_id',
        'type' => 'userNameFormat',
        'value' => 'array($data->from_user_id, $data->to_user_id, $data->from_user_fk ? $data->from_user_fk->full_name:"-", $data->to_user_fk ? $data->to_user_fk->full_name:"-")',
        'htmlOptions' => array('style' => 'text-align:left;')
    ),
    array(
        'name' => 'from_user_id',
        'header' => 'User Admin',
        'type' => 'userLink',
        'value' => 'array($data->from_user_id, $data->to_user_id, $data->from_user_fk ? $data->from_user_fk->full_name:"-", $data->to_user_fk ? $data->to_user_fk->full_name:"-")',
        'htmlOptions' => array('style' => 'text-align:left;')
    ),
    array(
        'header' => 'Admin Email',
        'value' => '$data->to_user_fk ? $data->to_user_fk->email:""',
        'htmlOptions' => array('style' => 'text-align:left;')
    ),
    array(
        'header' => 'Actions',
        'class' => 'CButtonColumn',
        'template' => '{delete}',
        'buttons' => array(
            'delete' => array(
                'label'=>'',
                'imageUrl'=>'',
                'options' => array('class' => 'glyphicon glyphicon-remove', 'style' => 'color: red', 'title' => 'Remove'),
                'click' => 'function(e){e.preventDefault();
                            if(!confirm("Are you sure you want to delete this item?")) return false;
                            var th = this,
                                    afterDelete = function(){};
                            jQuery("#ssn-partner-grid").yiiGridView("update", {
                                    type: "POST",
                                    url: jQuery(this).attr("href"),
                                    success: function(data) {
                                            window.location.reload();
                                    },
                                    error: function(XHR) {
                                            window.location.reload();
                                    }
                            });
                            return false;}'
            ),
        )
    ),
));
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'ssn-partner-grid-bulk',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('enctype' => 'multipart/form-data')));


$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'ssn-partner-grid',
    'itemsCssClass' => 'table',
    'htmlOptions' => array('class' => 'table-stripe', 'style' => 'width: 100%'),
    'dataProvider' => $model->search(),
    'pager' => array(
        'header' => '',
        'prevPageLabel' => 'Prev',
        'firstPageLabel' => 'First',
        'lastPageLabel' => 'Last',
        'nextPageLabel' => 'Next',
    ),
    'selectableRows' => 2,
    'columns' => $columnArray,
));
$this->endWidget();
?>

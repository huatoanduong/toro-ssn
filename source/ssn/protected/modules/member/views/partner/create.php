<section class="content-header">
    <h1>
        Make a Request
        <small>Create</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo Yii::app()->createAbsoluteUrl('member/partner/index')?>">Partner</a></li>
        <li class="active">Make a Request</li>
    </ol>
</section> 
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Information</h3>
                </div>
                <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Notice</h4>
            </div>
            <div class="modal-body">
                <p>This email not exist on SSN system. Are you sure to send partner request ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-send-outsystem">Send</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('body').on('click', '.btn-send-outsystem', function(){
        
            $("form").submit();
        });
        $('body').on('click', '.sbm', function(e){
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->createAbsoluteUrl('member/partner/checkEmailExist')?>',
                data: "email="+$('#SsnPartner_user_email').val(),
                success: function(data){
                    if(data === '2'){
                        $('#myModal').modal('show');
                    }else{
                        $("form").submit();
                        return false;
                    }                    
                }
            });
        });
    });
</script> 
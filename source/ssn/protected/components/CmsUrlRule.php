<?php
/**
 * @author Lam Huynh
 */
class CmsUrlRule extends CBaseUrlRule {

	protected $_urlSuffix='';
	
	public function createUrl($manager, $route, $params, $ampersand) {
		// url rewrite for cms page
		if (!isset($params['slug'])) return false;
		$slug = $params['slug'];
		if ($this->hasPage($slug))
			return $slug . $this->_urlSuffix;

		// no content found
		return false;
	}
	
	public function parseUrl($manager, $request, $pathInfo, $rawPathInfo) {
		// url rewrite for cms page
		$slug = rtrim($pathInfo,$this->_urlSuffix);
		if ($this->hasPage($slug))
			return "cms/index/slug/$slug";

		// no content found
		return false;
	}
	
	public function hasPage($slug) {
		if (empty($slug))
			return false;
		$c = new CDbCriteria();
		$c->compare('slug', $slug);
		$c->compare('status', 1);
		return Page::model()->exists($c);
	}
}
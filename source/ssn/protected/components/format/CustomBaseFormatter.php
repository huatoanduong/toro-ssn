<?php

//haidt
class CustomBaseFormatter extends BaseFormatter {

	public function formatStatus($value) {
		if (is_array($value)) {
			return (($value['status'] == STATUS_INACTIVE) ?
					CHtml::link(
						"Hidden", array("ajaxActivate", "id" => $value['id']), array(
						"class" => "ajaxupdate",
						"title" => "Click here to " . CustomDeclareHelper::$statusFormat[STATUS_ACTIVE],
						)
					) :
					CHtml::link(
						"Shown", array("ajaxDeactivate", "id" => $value['id']), array(
						"class" => "ajaxupdate",
						"title" => "Click here to " . CustomDeclareHelper::$statusFormat[STATUS_INACTIVE],
						)
					)
				);
		} else
			return $value == 0 ? CustomDeclareHelper::$statusFormat[STATUS_INACTIVE] : CustomDeclareHelper::$statusFormat[STATUS_ACTIVE];
	}

}

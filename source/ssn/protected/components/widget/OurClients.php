<?php
class OurClients extends CWidget {
	
	public function init() {
		return parent::init();
	}

	public function run() {
		$models = Clients::model()->findAll();
		$this->render("our_clients", array(
			'models' => $models,
		));
	}
}

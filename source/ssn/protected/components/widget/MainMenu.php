<?php
/*
 * Display menu and child menu items in sidebar
 * @author Lam Huynh
 */
class MainMenu extends CWidget {

	public function init() {
		return parent::init();
	}

	public function run() {
		$items = Menuitem::showMenus(4,0);
		$this->render("main_menu/main", array(
			'items' => $items,
		));
	}

	public function getCssClass($menuItem) {
		if ($this->menuItem && $menuItem->id==$this->menuItem->id)
			return 'active';
		return;
	}
}

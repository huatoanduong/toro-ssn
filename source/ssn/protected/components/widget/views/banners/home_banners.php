
<div id="banner" class="carousel slide bn" data-ride="carousel">
    <?php if (count($models) > 0): ?>
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <?php foreach ($models as $key => $model): ?>
                <li data-target="#banner" data-slide-to="<?php echo $key; ?>" class="<?php echo $key == 0 ? 'active' : ''; ?>"></li>
            <?php endforeach; ?>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <?php foreach ($models as $key => $model): ?>
                <div class="item <?php echo $key == 0 ? 'active' : ''; ?>">
                    <?php echo CHtml::image(ImageHelper::getImageUrl($model, "image", 'large'), $model->title); ?>
                    <div class="carousel-caption">
                        <div class="content">
                            <h1><?php echo $model->title ?></h1>
                            <p><?php echo $model->description; ?></p>
                            <?php if (!empty($model->link)): ?>
                                <a target="_blank" href="<?php echo $model->link; ?>" class="btn btn-warning">LEARN MORE</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div><!-- Banner -->
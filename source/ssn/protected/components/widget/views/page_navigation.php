<?php
/* @var $model Page */
/* @var $this PageNavigation */
?>
<div class="menu-sidebar">
	<h3 class="title-5"><?php echo $model->title ?></h3>
	<ul class="nav">
		<?php foreach($model->childs as $child): 
		?>
		<li class="<?php echo $this->getCssClass($child) ?>"><a href="<?php echo $child->getUrl() ?>"><?php echo $child->title ?></a></li>
		<?php endforeach ?>
	</ul>
</div>


<aside>
    <h4 class="title-2"><?php echo $this->group_name;?></h4>
    <?php if (!empty($menus)): ?>
    <ul class="nav-list">
    <?php foreach ($menus as $key => $item): ?>
        <?php
        $class = '';
        $link = $this->active_action=='/cms/index'?$this->current_page:$this->active_action;
        if($this->isHighlight($link, $item['id']))
            $class = $this->active_class;
        ?>
        <li class="<?php echo $class;?>"><a href="<?php echo $item['link'];?>"><?php echo $item['title'];?></a></li>
    <?php endforeach; ?>
    </ul>
    <?php endif;?>
</aside>
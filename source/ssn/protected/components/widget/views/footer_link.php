<?php
/* @var $model Menuitem */
/* @var $this MainMenu */
?>
<ul class="list-link">
	<?php foreach($items as $item): ?>
	<li>
		<a href="<?php echo $item->createLink() ?>"><?php echo $item->name ?></a>
	</li>
	<?php endforeach ?>
</ul>

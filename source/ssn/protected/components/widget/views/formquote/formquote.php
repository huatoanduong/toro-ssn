<div class="col-md-6 form-register">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'quote-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('class' => 'form-horizontal clearfix', 'role' => 'form', 'enctype' => 'multipart/form-data'),
        'action' => Yii::app()->createAbsoluteUrl('service/FormRequest'),
    ));
    ?>
    <div class="alert-sv alert-success-sv" role="alert">
        <button type="button" class="close" data-dismiss="alert">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span></button>
        Request sent successfully!
    </div>
    <h3 class="title-7">Request for Quote</h3>

    <div class="form-group">
        <label for="inputEmail" class="col-sm-3 control-label">Name:</label>
        <div class="col-sm-9">
            <?php echo $form->textField($model, 'name', array('class' => 'form-control', 'maxlength' => 255)); ?>
        </div>
    </div>

    <div class="form-group">
        <label for="inputEmail" class="col-sm-3 control-label">Email:</label>
        <div class="col-sm-9">
            <?php echo $form->textField($model, 'email', array('class' => 'form-control', 'maxlength' => 255)); ?>
        </div>
    </div>
    <div class="form-group address">
        <label for="inputAddress" class="col-sm-3 control-label">Address:</label>
        <div class="col-sm-9">
            <div class="form-group">
                <?php echo $form->textField($model, 'address_1', array('class' => 'form-control', 'maxlength' => 255)); ?>
            </div>
            <?php echo $form->textField($model, 'address_2', array('class' => 'form-control', 'maxlength' => 255)); ?>
        </div>
    </div>

    <div class="form-group">
        <label for="inputEmail" class="col-sm-3 control-label">Contact:</label>
        <div class="col-sm-9">
            <?php echo $form->textField($model, 'contact', array('class' => 'form-control', 'maxlength' => 255)); ?>
        </div>
    </div>
    <?php echo $form->hiddenField($model, 'bim_service_id', array('class' => 'form-control', 'value' => $service_id)); ?>
    <div class="form-group pull-right">
        <div class="col-md-12">
            <img class="loading" style="width:20px;height:20px;display: none;position: absolute;left: -8px;top: 3px;" src="<?php echo Yii::app()->theme->baseUrl . "/admin/images/loading.gif"; ?>">
            <button id="btn-quote-sm" type="button" class="btn btn-info btn-lg">Submit</button>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>

<script>
    $(document).ready(function () {
        var formObj = getFormObject();
        $('#btn-quote-sm').on('click', function () {
            hideErrors();
            var validate = formValidate(formObj);
            if (validate.length > 0) {
                showErrors(validate);
            } else {
                submitForm(formObj);
            }

        });
    });

    function getFormObject() {
        var formObj = {};
        formObj.name = $('#BimRequestQuote_name');
        formObj.email = $('#BimRequestQuote_email');
        formObj.address1 = $('#BimRequestQuote_address_1');
        formObj.address2 = $('#BimRequestQuote_address_2');
        formObj.contact = $('#BimRequestQuote_contact');
        return formObj;
    }

    function formValidate(formObj) {
        var errors = [];
        if (formObj.name.val() == '') {
            errors.push({id: formObj.name, msg: '<div class="errorMessage name_error">Name cannot be blank.</div>'});
        }
        if (formObj.email.val() == '') {
            errors.push({id: formObj.email, msg: '<div class="errorMessage email_error">Email cannot be blank.</div>'});
        }else{
            if( !isValidEmailAddress( formObj.email.val() ) ) { 
                errors.push({id: formObj.email, msg: '<div class="errorMessage email_error">Email is not a valid email address.</div>'});
            };
        }
        
        if (formObj.contact.val() == '') {
//            errors.push({id: formObj.contact, msg: '<div class="errorMessage contact_error">Contact cannot be blank.</div>'});
        }else{
			 if( !isValidPhone( formObj.contact.val() ) ) { 
                errors.push({id: formObj.contact, msg: '<div class="errorMessage email_error">Phone must be numerical and allow input (),+,-</div>'});
            };
		}
        return errors;
    }

    function showErrors(validate) {
        $.each(validate, function (i, item) {
            item.id.after(item.msg);
        });
    }

    function hideErrors() {
        $('.errorMessage').remove();
    }

    function submitForm(formObj) {
        var params = {};
        params.data = new FormData($("#quote-form")[0]);
        params.url = '<?php echo Yii::app()->createAbsoluteUrl('service/ajaxformrequest'); ?>';
        var promise = requests(params);
        promise.done(function (data) {
            if (data.status) {
                cleanData(formObj);
                $('.loading').hide();
                $('.alert-success-sv').show();
                setTimeout(function () {
                    $('.alert-success-sv').fadeOut();
                }, 5000);
            }
        });
    }

    function requests(params) {
        var promise = $.ajax({
            url: params.url,
            type: 'POST',
            dataType: 'json',
            data: params.data,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('.loading').show();
            }
        });
        return promise;
    }

    function cleanData(formObj) {
        formObj.name.val('');
        formObj.email.val('');
        formObj.address1.val('');
        formObj.address2.val('');
        formObj.contact.val('');
    }

    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailAddress);
    }
    
	function isValidPhone(phone){
		var pattern = new RegExp(/^[\d|\(|\)|\+|\-]+$/);
        return pattern.test(phone);
	}
</script>
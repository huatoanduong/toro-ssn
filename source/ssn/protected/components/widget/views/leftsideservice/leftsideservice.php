<div class="sidebar col-md-3">
    <div class="menu-sidebar">
        <h3 class="title-5">BIM Services</h3>
        <ul class="nav">
			<?php if (count($models) > 0): ?>
				<?php foreach ($models as $model): ?>
					<?php if ($model->type == Service::BIM_SERVICE): ?>
						<li class="<?php echo $model->slug == $slug ? 'active' : ''; ?>">
							<a href="<?php echo Yii::app()->createAbsoluteUrl('service/detail', array('slug' => $model->slug)); ?>">
								<?php echo $model->title; ?>
							</a>
						</li>
					<?php endif; ?>
				<?php endforeach; ?>

				<li><a href="#">Other Value Added Services</a>
					<ul>
						<?php foreach ($models as $model): ?>
							<?php if ($model->type == Service::OTHER_SERVICE): ?>
								<li class="<?php echo $model->slug == $slug ? 'active' : ''; ?>">
									<a href="<?php echo Yii::app()->createAbsoluteUrl('service/detail', array('slug' => $model->slug)); ?>">
										<?php echo $model->title; ?>
									</a>
								</li>
							<?php endif; ?>
						<?php endforeach; ?>
					</ul>
				</li>
			<?php endif; ?>
        </ul>
    </div><!--Menu bar-->
    <ul class="quick-link">
        <!--<li class="side-train clearfix">
                <a href="training.html">
                        <img src="img/side-nav1.jpg" alt="ATC (BIM Training)" />
                        <span class="desc">
                                <span>Learn more about</span>
                                <strong>ATC(BIM Training)</strong>
                        </span>
                </a>
        </li>-->
        <li class="side-contact clearfix">
            <a href="<?php echo Yii::app()->createAbsoluteUrl('site/contactus/#2'); ?>">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/side-nav2.jpg" alt="Enquiry Form" />
                <span class="desc">
                    <span>Contact us by </span>
                    <strong>Enquiry Form</strong>
                </span>
            </a>
        </li>
    </ul><!-- quick-link -->
</div><!-- Sidebar -->
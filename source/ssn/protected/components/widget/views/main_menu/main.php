<?php
    $company = SsnCompany::model()->findByAttributes(array("user_id" => Yii::app()->user->id));
    $user = Users::model()->findByPk(Yii::app()->user->id);
?>
<aside class="main-sidebar">
    <section class="sidebar" style="height: auto">
        <div class="user-panel" style="">
            <div class="pull-left image" style="height: 50px">
                <?php if($company->is_img_active == 0):?>
                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo.png" alt="Blocked Image" width="50" height="50"/>
                <?php else:?>
                    <?php echo CHtml::image(ImageHelper::getImageUrl($company, "image", "thumb"), '', array('width' => 50, 'height' => 50)); ?>
                <?php endif;?>
            </div>
            <div class="pull-left info" style="">
                <p><?php echo Yii::app()->user->full_name; ?></p>
                <p><?php echo $user->company ? $user->company->name:''; ?></p>
                <!--<p><?php echo $user->address; ?></p>-->
                <!--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>-->
            </div>
        </div>
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..." />
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <ul class="sidebar-menu">
            <?php foreach ($items as $item): ?>
                <li class="">
                    <a href="<?php echo $item->createLink() ?>">
                        <i class="<?php echo $item->css_class ?>"></i>
                        <span><?php echo $item->name ?></span>
                    </a>
                </li>
            <?php endforeach ?>
        </ul>
    </section>
</aside>
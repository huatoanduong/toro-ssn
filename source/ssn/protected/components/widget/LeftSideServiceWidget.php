<?php

class LeftSideServiceWidget extends CWidget {

    public $slug;

    public function run() {
        $this->getDropDownList();
    }

    public function getDropDownList() {
        $init = new Service();
		$models = $init->getAll();
        $this->render("leftsideservice/leftsideservice", array(
            'models' => $models,
            'slug' => $this->slug
        ));
    }

}

?>
<?php

/**
 * This is the model class for table "{{_ssn_sample_comment}}".
 *
 * The followings are the available columns in table '{{_ssn_sample_comment}}':
 * @property integer $id
 * @property integer $sample_id
 * @property integer $user_id
 * @property string $comment
 * @property integer $order_by
 * @property string $created_date
 */
class SsnSampleComment extends _BaseModel {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{_ssn_sample_comment}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('sample_id, user_id, order_by', 'numerical', 'integerOnly' => true),
            array('comment, created_date', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, sample_id, user_id, comment, order_by, created_date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user_fk'=>array(self::BELONGS_TO, 'Users', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Yii::t('translation', 'ID'),
            'sample_id' => Yii::t('translation', 'Sample'),
            'user_id' => Yii::t('translation', 'User'),
            'comment' => Yii::t('translation', 'Comment'),
            'order_by' => Yii::t('translation', 'Order By'),
            'created_date' => Yii::t('translation', 'Created Date'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('sample_id', $this->sample_id);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('comment', $this->comment, true);
        $criteria->compare('order_by', $this->order_by);
        $criteria->compare('created_date', $this->created_date, true);


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->params['defaultPageSize'],
            ),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SsnSampleComment the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function nextOrderNumber() {
        return SsnSampleComment::model()->count() + 1;
    }
    
    public static function getCommentBySample($id) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('sample_id', $id);

        $criteria->order = 't.created_date';

        return self::model()->findAll($criteria);
    }

}

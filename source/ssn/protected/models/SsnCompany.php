<?php

/**
 * This is the model class for table "{{_ssn_company}}".
 *
 * The followings are the available columns in table '{{_ssn_company}}':
 * @property integer $id
 * @property string $name
 * @property string $image
 * @property string $address
 * @property string $website
 * @property string $phone
 * @property string $size
 * @property string $about
 * @property integer $status
 * @property integer $order_by
 * @property string $created_date
 * @property string $updated_date
 */
class SsnCompany extends _BaseModel {
    public $companyId = 0;
    
    public $maxImageFileSize = 3145728; //3MB
    public $allowImageType = 'jpg,gif,png';
    
    public $uploadImageFolder = 'upload/company'; 
    public $defineImageSize = array(
        'image' => array(
            array('alias' => 'logo', 'size' => '50x50'),
            array('alias' => 'thumb', 'size' => '300x150'),
            array('alias' => 'large', 'size' => '1400x466')
        )
    );
    public $imageSize = '1400px x 466px';

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{_ssn_company}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, is_img_active, image, address, website, phone, size, about, status, order_by, created_date, updated_date', 'safe'),
            array('image', 'file', 'on' => 'create,update',
                'allowEmpty' => true,
                'types' => $this->allowImageType,
                'wrongType' => 'Only ' . $this->allowImageType . ' are allowed.',
                'maxSize' => $this->maxImageFileSize, // 3MB
                'tooLarge' => 'The file was larger than' . ($this->maxImageFileSize / 1024) / 1024 . 'MB. Please upload a smaller file.',
            ),
            array('name, address', 'required'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id,is_img_active, name, image, address, website, phone, size, about, status, order_by, created_date, updated_date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Yii::t('translation', 'ID'),
            'name' => Yii::t('translation', 'Name'),
            'image' => Yii::t('translation', 'Image'),
            'address' => Yii::t('translation', 'Address'),
            'website' => Yii::t('translation', 'Website'),
            'phone' => Yii::t('translation', 'Phone'),
            'size' => Yii::t('translation', 'Size'),
            'about' => Yii::t('translation', 'About'),
            'status' => Yii::t('translation', 'Status'),
            'order_by' => Yii::t('translation', 'Order By'),
            'created_date' => Yii::t('translation', 'Created Date'),
            'updated_date' => Yii::t('translation', 'Updated Date'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('image', $this->image, true);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('website', $this->website, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('size', $this->size, true);
        $criteria->compare('about', $this->about, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('order_by', $this->order_by);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('updated_date', $this->updated_date, true);


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->params['defaultPageSize'],
            ),
        ));
    }

    public function activate() {
        $this->status = 1;
        $this->update();
    }

    public function deactivate() {
        $this->status = 0;
        $this->update();
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SsnCompany the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function nextOrderNumber() {
        return SsnCompany::model()->count() + 1;
    }
    
    public function getCompanyByUser($userId){
        $user = self::model()->findByAttributes(array('user_id'=>$userId));
        if($user)
            return $user;
        else
            return new SsnCompany();
    }

}

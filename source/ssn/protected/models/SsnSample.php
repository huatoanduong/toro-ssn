<?php

/**
 * This is the model class for table "{{_ssn_sample}}".
 *
 * The followings are the available columns in table '{{_ssn_sample}}':
 * @property integer $id
 * @property integer $item_id
 * @property integer $from_user_id
 * @property integer $to_user_id
 * @property string $awbn
 * @property string $remark
 * @property string $document
 * @property integer $status
 * @property integer $order_by
 * @property string $created_date
 * @property string $updated_date
 */
class SsnSample extends _BaseModel {
    public $uploadFileFolder = 'upload/sample/files';
    public $_search_time;
    public $_search_start_date;
    public $_search_end_date;
    public $maxUploadFileSize = 3145728;
    public $allowUploadType = 'doc,docx,xls,xlsx,pdf';
    
    const STATUS_SEND = 1;
    const STATUS_RECEIVED = 0;
    public static function _SEARCH_TIME_OPTIONS(){
        return array(
            '1' => 'Lastest 7 days',
            '2' => 'Current month',
            '3' => 'Lastest 3 months',
            '4' => 'Range',
        );
    }
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{_ssn_sample}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('item_id, from_user_id, to_user_id, status, order_by', 'numerical', 'integerOnly' => true),
            array('awbn', 'length', 'max' => 50),
            array('document', 'length', 'max' => 255),
            array('remark, created_date, updated_date', 'safe'),
            array('awbn, created_date, to_user_id', 'required'),
            array('document', 'file', 'on' => 'create,update',
                'allowEmpty' => true,
                'types' => $this->allowUploadType,
                'wrongType' => 'Only ' . $this->allowUploadType . ' are allowed.',
                'maxSize' => $this->maxUploadFileSize, // 3MB
                'tooLarge' => 'The file was larger than ' . ($this->maxUploadFileSize / 1024) / 1024 . 'MB. Please upload a smaller file.',
                "enableClientValidation"=>true
            ),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, item_id, from_user_id, to_user_id, awbn, remark, document, status, order_by, created_date, updated_date', 'safe', 'on' => 'search'),
            array('_search_time, _search_start_date, _search_end_date', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'from_user_fk'=>array(self::BELONGS_TO, 'Users', 'from_user_id'),
            'to_user_fk'=>array(self::BELONGS_TO, 'Users', 'to_user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Yii::t('translation', 'ID'),
            'item_id' => Yii::t('translation', 'Item'),
            'from_user_id' => Yii::t('translation', 'From User'),
            'to_user_id' => Yii::t('translation', 'To User'),
            'awbn' => Yii::t('translation', 'AWBN'),
            'remark' => Yii::t('translation', 'Remark'),
            'document' => Yii::t('translation', 'Document'),
            'status' => Yii::t('translation', 'Status'),
            'order_by' => Yii::t('translation', 'Order By'),
            'created_date' => Yii::t('translation', 'Send Date'),
            'updated_date' => Yii::t('translation', 'Updated Date'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('awbn', $this->awbn, true);
        $criteria->compare('from_user_id', $this->from_user_id);
        $criteria->compare('to_user_id', $this->to_user_id);
        switch ($this->_search_time){
            case 1:
                $currentDate = date('Y-m-d');
                $last7Date = date('Y-m-d', strtotime('-7 day', strtotime($currentDate)));
                $criteria->addCondition("DATE(created_date) BETWEEN '$last7Date' AND '$currentDate'");
                break;
            case 2:
                $currentMonth = date('m');
                $criteria->addCondition("MONTH(created_date) = '$currentMonth'");
                break;
            case 3:
                $currentMonth = date('m');
                $pastMonth = $currentMonth - 3;
                $criteria->addCondition("MONTH(created_date) BETWEEN '$pastMonth' AND '$currentMonth'");
                break;
            case 4:
                if(!empty($this->_search_start_date) || !empty($this->_search_end_date)){
                    $startDate = empty($this->_search_start_date) ? date('Y-m-d') : DateHelper::toDbDateFormat($this->_search_start_date);
                    $endDate = empty($this->_search_end_date) ? date('Y-m-d') : DateHelper::toDbDateFormat($this->_search_end_date);
                    $criteria->addCondition("DATE(created_date) BETWEEN '$startDate' AND '$endDate'");
                }
                break;
        }


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->params['defaultPageSize'],
            ),
        ));
    }
    public function searchSample() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

//        $criteria->compare('awbn', $this->awbn, true, 'OR');
//        $criteria->compare('remark', $this->awbn, true, 'OR');
        $criteria->addCondition("awbn LIKE '%{$this->awbn}%'", 'OR');
        $criteria->addCondition("remark LIKE '%{$this->awbn}%'", 'OR');
        $criteria->compare('from_user_id', $this->from_user_id);
        $criteria->compare('to_user_id', $this->to_user_id);
        switch ($this->_search_time){
            case 1:
                $currentDate = date('Y-m-d');
                $last7Date = date('Y-m-d', strtotime('-7 day', strtotime($currentDate)));
                $criteria->addCondition("DATE(created_date) BETWEEN '$last7Date' AND '$currentDate'");
                break;
            case 2:
                $currentMonth = date('m');
                $criteria->addCondition("MONTH(created_date) = '$currentMonth'");
                break;
            case 3:
                $currentMonth = date('m');
                $pastMonth = $currentMonth - 3;
                $criteria->addCondition("MONTH(created_date) BETWEEN '$pastMonth' AND '$currentMonth'");
                break;
            case 4:
                if(!empty($this->_search_start_date) || !empty($this->_search_end_date)){
                    $startDate = empty($this->_search_start_date) ? date('Y-m-d') : DateHelper::toDbDateFormat($this->_search_start_date);
                    $endDate = empty($this->_search_end_date) ? date('Y-m-d') : DateHelper::toDbDateFormat($this->_search_end_date);
                    $criteria->addCondition("DATE(created_date) BETWEEN '$startDate' AND '$endDate'");
                }
                break;
        }


        return self::model()->findAll($criteria);
    }

    public function activate() {
        $this->status = 1;
        $this->update();
    }

    public function deactivate() {
        $this->status = 0;
        $this->update();
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SsnSample the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function nextOrderNumber() {
        return SsnSample::model()->count() + 1;
    }
    public function showUploadFile($model) {
        if (!empty($model->document)) {
            return CHtml::link($model->document, Yii::app()->createAbsoluteUrl($model->uploadFileFolder . '/' . $model->id . '/' . $model->document), array('target' => '_blank'));
        } else {
            return '';
        }
    }
    protected function beforeSave() {
        $this->created_date = DateHelper::toDbDateFormat($this->created_date);
        return parent::beforeSave();
    }
    
    protected function afterFind() {
        $this->created_date = date('d/m/Y', strtotime($this->created_date));
        return parent::afterFind();
    }

}

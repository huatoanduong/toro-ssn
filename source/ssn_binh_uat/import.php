<?php
if (!isset($_FILES["zip_file"])) {
    exit('missing file data in request.');
}

$zipFile = $_FILES["zip_file"]["tmp_name"];
$zip = new ZipArchive; 
if ($zip->open($zipFile) != TRUE) {
    exit('invalid zip archive file.');
}
echo "zip file uploaded.\n";

// list files to backup before extract
$basePath = realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR;
$filesToBackup = array();
for ($i = 0; $i < $zip->numFiles; $i++) {
    $file = $zip->getNameIndex($i);
    if (is_file($basePath.$file)) {
        $filesToBackup[$file] = $basePath.$file;
    }
}

// create backup file
if ($filesToBackup) {
    echo "these files will be backup:\n";
    $backup = new ZipArchive;
    $bname = "backup-".date('Y-m-d-H-i').".zip";
    if ($backup->open($bname, ZipArchive::CREATE)!==TRUE) {
        echo("cannot create backup file.\n");
    }

    foreach($filesToBackup as $dst => $src) {
        $backup->addFile($src, $dst);
        echo $src."\n";
    }
    $backup->close();
    echo("backup created: $bname\n");
}

if (!$zip->extractTo($basePath)) {
    echo "cannot extract update file.\n";
} else {
    echo "server updated.\n";
}

$zip->close();

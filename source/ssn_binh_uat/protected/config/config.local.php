<?php 
$THEME_NAME = '';
$THEME		= '';
$TABLE_PREFIX = 'mind';
   
//All defined items in Yii-core
//Please do not change if not require
define('BE', 1);
define('FE', 2);

define('ROLE_MANAGER', 1);
define('ROLE_ADMIN', 2);
define('ROLE_MEMBER', 3);

//max records in logger table
define('LOGGER_TABLE_MAX_RECORDS', 2000);

define('PASSW_LENGTH_MIN', 6);
define('PASSW_LENGTH_MAX', 32);

define('VERZ_COOKIE_ADMIN', md5('verz_cookie_admin'));
define('VERZ_COOKIE', md5('verz_cookie'));
define('VERZLOGIN', md5('verz_login'));
define('VERZLPASS', md5('verz_pass'));

define('VERZ_COOKIE_MEMBER', md5('verz_cookie_member'));
define('VERZLOGIN_MEMBER', md5('verz_login_member'));
define('VERZLPASS_MEMBER', md5('verz_pass_member'));

define('STATUS_INACTIVE', 0);
define('STATUS_ACTIVE', 1);
define('STATUS_NEW', 2);
define('STATUS_WAIT_ACTIVE_CODE', 3);
define('STATUS_AUTOCREATE_INACTIVE', 4);

define('TYPE_YES', 1);
define('TYPE_NO', 0);

define('HOME_IMAGE_BLOCK', 10);
define('SERVICE_IMAGE_BLOCK', 11);

//max time for failed login to show captcha required
define('MAX_TIME_TO_SHOW_CAPTCHA', 2);

//Email 
define('MAIL_CONTACT_US_TO_ADMIN',4);

//Contact Us Type
define('CONTACT_US_GENERAL_ENQUIRY',1);
define('CONTACT_US_TRAINING_REQUEST',2);
define('CONTACT_US_BIM_SERVICE_ENQUIRY',3);
define('CONTACT_US_CAREER',4);

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Log in - SSN</title>
    <link rel="shortcut icon" type="image/ico" href="<?php echo Yii::app()->theme->baseUrl; ?>favicon.ico" />
    <link rel="apple-touch-icon" href="<?php echo Yii::app()->theme->baseUrl; ?>favicon.png" />

    <!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.4 -->
	<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<!-- Font Awesome Icons -->
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<!-- Theme style -->
	<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
	<!-- iCheck -->
	<link href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login-page">
	<div class="login-box">
		<div class="login-logo">
			<a href="<?php echo Yii::app()->createAbsoluteUrl('site/updateProfile')?>"><b>SSN</b></a>
		</div>
		<!-- /.login-logo -->
		<div class="login-box-body">
			<p class="login-box-msg">Sign in to start your session</p>

			<?php $this->widget('application.components.widget.Notification'); ?>
			
			<?php $form = $this->beginWidget('CActiveForm', array( 
				'id' => 'login-form', 
				'htmlOptions' => array(
					'class' => '', 
					'role' => 'form'), 
				'enableClientValidation' => false, 
				'clientOptions' => array( 'validateOnSubmit' => true, ),
			)); ?>
			<div class="form-group has-feedback">
				<?php echo $form->textField($model, 'username', array('class' => 'form-control', 'placeholder' => 'Username')); ?>
				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				<?php echo $form->error($model, 'username'); ?>
			</div>
			<div class="form-group has-feedback">
				<?php echo $form->passwordField($model, 'password', array('class' => 'form-control', 'placeholder' => 'Password')); ?>
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
			</div>
			<?php if ($model->scenario == 'captchaRequired'): ?>
			<div class="form-group clearfix">
				<div class="col-sm-12">
					<?php echo CHtml::activeLabelEx($model, 'verifyCode', array( 'class'=> 'col-sm-6')); ?>
				</div>
				<div class="col-sm-12">
					<?php $this->widget('CCaptcha'); ?>
					<?php echo CHtml::activeTextField($model, 'verifyCode', array( 'class'=> 'form-control')); ?>
					<?php echo $form->error($model, 'verifyCode'); ?>
					<div class="hint">Please enter the letters as they are shown in the image above.
						<br/>Letters are not case-sensitive.
					</div>
				</div>
			</div>
			<?php endif; ?>
			<div class="row">
				<div class="col-xs-8">
					<div class="checkbox icheck">
						<label>
							<?php echo $form->checkBox($model, 'rememberMe'); ?> Remember Me
						</label>
					</div>
				</div>
				<!-- /.col -->
				<div class="col-xs-4">
					<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
				</div>
				<!-- /.col -->
			</div>
			<?php $this->endWidget(); ?>

			<a href="<?php echo Yii::app()->createAbsoluteUrl('site/forgotPassword') ?>#">I forgot my password</a>
			<br>

            <a href="<?php echo Yii::app()->createAbsoluteUrl('site/reSendVerification') ?>#">Not receive confirmation?</a>
            <br>

			<a href="<?php echo Yii::app()->createAbsoluteUrl('site/register'); ?>" class="text-center">Register a new membership</a>

		</div>
	</div>

	<!-- jQuery 2.1.4 -->
	<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jQuery-2.1.4.min.js" type="text/javascript"></script>
	<!-- Bootstrap 3.3.2 JS -->
	<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap.min.js" type="text/javascript"></script>
	<!-- iCheck -->
	<script src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
	<script>
		$(function() {
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
				increaseArea: '20%' // optional
			});
		});
	</script>
</body>

</html>
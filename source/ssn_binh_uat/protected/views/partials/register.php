<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Register - SSN</title>
      <link rel="shortcut icon" type="image/ico" href="<?php echo Yii::app()->theme->baseUrl; ?>favicon.ico" />
      <link rel="apple-touch-icon" href="<?php echo Yii::app()->theme->baseUrl; ?>favicon.png" />

      <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
    <?php Yii::app()->getClientScript()->registerCoreScript('jquery'); ?>

  <body class="register-page">
    <div class="register-box">
      <div class="register-logo">
        <a href="#"><b>SSN</b></a>
      </div>

      <div class="register-box-body">
        <p class="login-box-msg">Register a new membership</p>

          <?php $this->widget('application.components.widget.Notification'); ?>

                        <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'users-model-form',
                            'enableClientValidation' => true,
                            'enableAjaxValidation' => false,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                        )); ?>


                    <div class="form-group has-feedback">
                            <?php echo $form->textField($model,'first_name',array('class' => 'form-control', 'placeholder' => 'First Name')); ?>
                            <?php echo $form->error($model,'first_name'); ?>
                    </div>
                    <div class="form-group has-feedback">
                            <?php echo $form->textField($model,'last_name',array('class' => 'form-control', 'placeholder' => 'Last Name')); ?>
                            <?php echo $form->error($model,'last_name'); ?>
                    </div>

                  <div class="form-group has-feedback">
                      <?php echo $form->textField($model,'username',array('class' => 'form-control', 'placeholder' => 'Email')); ?>
                      <?php echo $form->error($model,'username'); ?>
                  </div>

                    <div class="form-group has-feedback">
                            <?php echo $form->passwordField($model,'temp_password',array('value'=>'', 'class' => 'form-control', 'placeholder' => 'Password')); ?>
                            <?php echo $form->error($model,'temp_password'); ?>
                    </div>

                    <div class="form-group has-feedback">
                            <?php echo $form->passwordField($model,'password_confirm',array('maxlength'=>30,'value'=>'', 'class' => 'form-control', 'placeholder' => 'Confirm Password')); ?>
                            <?php echo $form->error($model,'password_confirm'); ?>
                    </div>


                    <div class="form-group has-feedback">
                            <?php echo $form->textField($model,'phone_public',array('size'=>18,'maxlength'=>30, 'class' => 'form-control', 'placeholder' => 'Cellphone')); ?>
                            <?php echo $form->error($model,'phone_public'); ?>
                    </div>
                    <div class="form-group has-feedback">
                            <?php echo $form->textField($model,'phone_private',array('size'=>18,'maxlength'=>30, 'class' => 'form-control', 'placeholder' => 'Phone')); ?>
                            <?php echo $form->error($model,'phone_private'); ?>
                    </div>
                    <div class="form-group has-feedback">
                            <?php echo $form->textField($model,'skype',array('size'=>18,'maxlength'=>30, 'class' => 'form-control', 'placeholder' => 'Skype')); ?>
                            <?php echo $form->error($model,'skype'); ?>
                    </div>
                    <div class="form-group has-feedback">
                            <?php echo $form->textField($model,'whatapps',array('size'=>18,'maxlength'=>30, 'class' => 'form-control', 'placeholder' => 'Whatapps')); ?>
                            <?php echo $form->error($model,'whatapps'); ?>
                    </div>
                    <div class="form-group has-feedback">
                            <?php echo $form->textField($model,'line',array('size'=>18,'maxlength'=>30, 'class' => 'form-control', 'placeholder' => 'Line')); ?>
                            <?php echo $form->error($model,'line'); ?>
                    </div>
                    <div class="form-group has-feedback">
                            <?php echo $form->textField($model,'viber',array('size'=>18,'maxlength'=>30, 'class' => 'form-control', 'placeholder' => 'Viber')); ?>
                            <?php echo $form->error($model,'viber'); ?>
                    </div>
                    <div class="form-group has-feedback">
                            <?php echo $form->textField($model,'wechat',array('size'=>18,'maxlength'=>30, 'class' => 'form-control', 'placeholder' => 'Wechat')); ?>
                            <?php echo $form->error($model,'wechat'); ?>
                    </div>
                    <div style="border-bottom: 1px solid #CCC; margin-bottom: 10px;">
                        <h4>Company</h4>
                    </div>    
                    <div class="form-group has-feedback">
                            <?php echo $form->textField($company,'name',array('class' => 'form-control', 'placeholder' => 'Name')); ?>
                            <?php echo $form->error($company,'name'); ?>
                    </div>
                    <div class="form-group has-feedback">
                            <?php echo $form->textField($company,'address',array('class' => 'form-control', 'placeholder' => 'Address')); ?>
                            <?php echo $form->error($company,'address'); ?>
                    </div>
                    <div class="form-group has-feedback">
                            <?php echo $form->textField($company,'website',array('class' => 'form-control', 'placeholder' => 'Website')); ?>
                    </div>
                    <div class="form-group has-feedback">
                            <?php echo $form->textField($company,'phone',array('class' => 'form-control', 'placeholder' => 'Phone')); ?>
                            <?php echo $form->error($company,'phone'); ?>
                    </div>
                    <div class="form-group has-feedback">
                            <?php echo $form->textField($company,'size',array('class' => 'form-control', 'placeholder' => 'Size')); ?>
                    </div>
                    <div class="form-group has-feedback">
                            <?php echo $form->textArea($company,'about',array('class' => 'form-control', 'placeholder' => 'About')); ?>
                    </div>
                    <?php echo $form->hiddenField($company,'companyId',array('class' => 'form-control')); ?>
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="checkbox icheck">
                                <label>
                                    <?php echo $form->checkbox($model,'agreeTermOfUse', array('class' => 'col-sm-1')); ?>
                                    <?php echo $form->label($model,'agreeTermOfUse',array('label'=>'I agree with '.CHtml::link('Term Of Use',array('/term-of-use'), array('target'=>'_blank')))); ?>
                                <?php echo $form->error($model,'agreeTermOfUse'); ?>
                                </label>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                        </div><!-- /.col -->
                    </div>

                        <?php $this->endWidget(); ?>
                    <div class="social-auth-links text-center">
        </div>

        <a href="<?php echo Yii::app()->createAbsoluteUrl('site/login')?>" class="text-center">I already have a membership</a>
      </div><!-- /.form-box -->
    </div><!-- /.register-box -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Notice</h4>
            </div>
          <div class="modal-body">
              <p>Your company has exist, please choice one option below:</p>
              <br/>
              <div class="data">
                  
              </div>
          </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary saveChange">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap 3.3.2 JS -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap.min.js" type="text/javascript"></script>
<!-- iCheck -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>



<script>
    $('document').ready(function(){
        $('.save').on('click',function(ev){
            ev.preventDefault();
            var error = $('body').find('.errorMessage:visible').length;
            var name = $('#SsnCompany_name').val();
            var i = 0;
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->createAbsoluteUrl('site/checkCompany')?>',
                data: "companyName="+name,
                success: function(data){
                    json = jQuery.parseJSON(data);
                    $('.data').html(' ');
                    $.each(json, function (key, data) {
                        i++;
                        var a = $('<input>').attr({
                                    type: 'radio',
                                    id: key,
                                    name: 'bar[]',
                                    class: 'rd',
                                    value: key
                                }).after(" "+data)
                        $('.data').append(a);
                        $('.data').append('</br>');
                    });
                    var a = $('<input>').attr({
                                    type: 'radio',
                                    id: 0,
                                    name: 'bar[]',
                                    class: 'rd',
                                    checked: 'true',
                                    value: 0
                                }).after(" Not above");
                        $('.data').append(a);
                    if(i > 0){
                        $('#myModal').modal('show');
                    }else{
                        $('form').submit();
                        
                    }
                }
            });
        });
    });
    $(document).on('click','.saveChange',function(e){
        var abc = $('.modal-body').find('input[type=radio]:checked').val();
        $('#SsnCompany_companyId').val(abc);
        $('form').submit();
    });
    
</script>
</body>

</html>
<style>
    label > .required{
        color: red;
    }
    .errorMessage{
        color: red !important;
    }
</style>
<?php 
    $company = SsnCompany::model()->findByAttributes(array("user_id" => Yii::app()->user->id));
    $user = Users::model()->findByPk(Yii::app()->user->id);
?>
<header class="main-header">
<!--    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">-->
<!--        <span class="sr-only">Toggle navigation</span>-->
<!--    </a>-->

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas"
           role="button" >
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
				<li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-user"></i>&nbsp;
                        <span class="hidden-xs notranslate"><?php echo $user->full_name;?>
							(<?php echo $company->name ?>)</span>
                    </a>
                    <ul class="dropdown-menu user-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <?php if($company->is_img_active == 0):?>
                                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo.png" alt="Blocked Image" width="5gif0" height="50"/>
                            <?php else:?>
                                <?php echo CHtml::image(ImageHelper::getImageUrl($company, "image", "thumb"), '', array('width' => 50, 'height' => 50)); ?>
                            <?php endif;?>
                            <p class="notranslate">
                                <?php echo $user->full_name;?> - <?php echo $company->name;?>
                                <small><?php echo $company->address;?></small>
                                <!--<small>Member since Nov. 2012</small>-->
                            </p>
                          </li>
        
						<!-- Menu Footer-->
						<li class="user-footer">
							<div class="pull-left">
								<a href="<?php echo Yii::app()->createAbsoluteUrl('member/site/updateProfile')?>" class="btn btn-default btn-flat">Profile</a>
							</div>
							<div class="pull-right">
								<a href="<?php echo Yii::app()->createAbsoluteUrl('/site/logout')?>" class="btn btn-default btn-flat">Sign out</a>
							</div>
						</li>
                    </ul>
                  </li>
                <li class="translator">
                    <div id="google_translate_element" style="padding: 10px 15px"></div>
                </li>

            </ul>
        </div>
    </nav>
</header>
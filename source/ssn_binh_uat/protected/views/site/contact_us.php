<div class="wrapper container">

    <div class="contact-page">

        <div class="header">
            <div class="row">
                <div class="col-md-7">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/'); ?>"><span class="icon icon-home"></span></a></li>
                        <li class="active">Liên hệ</li>
                    </ol>
                    <h2 class="title-2">Liên hệ</h2>
                </div>
            </div>
        </div>
        <?php if (Yii::app()->user->hasFlash('msg')) : ?>
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <?php echo Yii::app()->user->getFlash('msg'); ?>
            </div>
        <?php elseif (Yii::app()->user->hasFlash('error')): ?>
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <?php echo Yii::app()->user->getFlash('error'); ?>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-md-6">
                <div class="tab-content"> 

                    <div class="tab-pane active" id="0">
                        <h3 class="title-7">Email liên hệ</h3>     
                        <?php
                        $form1 = $this->beginWidget('CActiveForm', array(
                            'id' => 'general-enquiry-form',
                            'enableAjaxValidation' => false,
                            'htmlOptions' => array('class' => 'form-upload form-contact', 'role' => 'form', 'enctype' => 'multipart/form-data'),
                        ));
                        ?>
                        <div class="form-group">
                            <?php echo $form1->textField($general_enquiry, 'name', array('class' => 'form-control', 'id' => 'exampleInputName1', 'placeholder' => 'Tên', 'maxlength' => 50)); ?>
                            <?php echo $form1->error($general_enquiry, 'name') ?> 
                        </div>
                        <div class="form-group">
                            <?php echo $form1->textField($general_enquiry, 'email', array('class' => 'form-control', 'id' => 'exampleInputEmail1', 'placeholder' => 'Email', 'maxlength' => 50)); ?>
                            <?php echo $form1->error($general_enquiry, 'email') ?> 
                        </div>
                        <div class="form-group">
                            <?php echo $form1->textField($general_enquiry, 'phone', array('class' => 'form-control', 'id' => 'exampleInputTel', 'placeholder' => 'Số điện thoại', 'maxlength' => 50)); ?>
                            <?php echo $form1->error($general_enquiry, 'phone') ?> 
                        </div>
						
                        <div class="form-group">
                            <?php echo $form1->textArea($general_enquiry, 'content', array('class' => 'form-control', 'id' => 'exampleInputEnquiry', 'placeholder' => 'Nội dung', 'cols' => 30, 'rows' => 10)); ?>
                            <?php echo $form1->error($general_enquiry, 'content') ?> 
                        </div>

                        <div class="form-group captcha clearfix">
                                <div class="group">
                                    <div class="col-1">
                                        <?php $this->widget('CCaptcha'); ?>
                                    </div>
                                    <div class="col-1">
                                        <?php echo $form1->textField($general_enquiry,'grVerifyCode',array('class' => 'form-control')); ?>
                                        <?php echo $form1->error($general_enquiry,'grVerifyCode'); ?>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <button type="submit" class="btn btn-info">submit</button>
                                </div>
                        </div>
                        <?php $this->endWidget(); ?>  
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="contact-info">
                    <h5>Hải sản miền biển</h5>
                    <div class="clearfix">
                        <div class="col-md-6">
                            <div class="address">
                                <br>
                                <p>
                                    <span class="address">Địa chỉ:  <?php echo Yii::app()->params['companyAddress'];?></span>
                                    <span class="tel">Số điện thoại:  <?php echo Yii::app()->params['companyPhone'];?></span>
                                    <span class="mail">Email: <a href="mailto:<?php echo Yii::app()->params['companyEmail'];?>"><?php echo Yii::app()->params['companyEmail'];?></a></span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

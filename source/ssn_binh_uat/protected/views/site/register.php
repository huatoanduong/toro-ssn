<div class="wrapper container">
    <div class="header">
        <div class="row">
            <div class="col-md-7">
                <ol class="breadcrumb">
                    <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/'); ?>"><span class="icon icon-home"></span></a></li>
                    <li class="active">Register</li>
                </ol>
                <h2 class="title-2">Register</h2>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-md-offset-3">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'users-model-form',
            'enableClientValidation' => false,
            'enableAjaxValidation' => false,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        )); ?>

        <p class="note">Fields with <span class="required">*</span> are required.</p>
        <div style="border-bottom: 1px solid #CCC; margin-bottom: 10px;">
            <h4>User Information</h4>
        </div>
        <?php echo $form->errorSummary($model); ?>

    <div class="row form-group">
        <?php echo $form->labelEx($model,'first_name',array('label'=>'First Name', 'class' => 'col-sm-3')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model,'first_name',array('class' => 'form-control')); ?>
            <?php echo $form->error($model,'first_name'); ?>
        </div>
    </div>
    <div class="row form-group">
        <?php echo $form->labelEx($model,'last_name',array('label'=>'Last Name', 'class' => 'col-sm-3')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model,'last_name',array('class' => 'form-control')); ?>
            <?php echo $form->error($model,'last_name'); ?>
        </div>
    </div>
    <div class="row form-group">
        <?php echo $form->labelEx($model,'temp_password', array('class' => 'col-sm-3')); ?>
        <div class="col-sm-6">
            <?php echo $form->passwordField($model,'temp_password',array('value'=>'', 'class' => 'form-control')); ?>
            <?php echo $form->error($model,'temp_password'); ?>
        </div>
    </div>

    <div class="row form-group">
        <?php echo $form->labelEx($model,'password_confirm', array('class' => 'col-sm-3')); ?>
        <div class="col-sm-6">
            <?php echo $form->passwordField($model,'password_confirm',array('maxlength'=>30,'value'=>'', 'class' => 'form-control')); ?>
            <?php echo $form->error($model,'password_confirm'); ?>
        </div>
    </div>
    <div class="row form-group">
        <?php echo $form->labelEx($model,'email', array('class' => 'col-sm-3')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model,'email',array('class' => 'form-control')); ?>
            <?php echo $form->error($model,'email'); ?>
        </div>
    </div>

    <div class="row form-group">
        <?php echo $form->labelEx($model,'phone_public', array('label'=>'CellPhone', 'class' => 'col-sm-3')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model,'phone_public',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
            <?php echo $form->error($model,'phone_public'); ?>
        </div>
    </div>
    <div class="row form-group">
        <?php echo $form->labelEx($model,'phone_private', array('label'=>'Phone', 'class' => 'col-sm-3')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model,'phone_private',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
            <?php echo $form->error($model,'phone_private'); ?>
        </div>
    </div>
    <div class="row form-group">
        <?php echo $form->labelEx($model,'skype', array('label'=>'Skype', 'class' => 'col-sm-3')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model,'skype',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
            <?php echo $form->error($model,'skype'); ?>
        </div>
    </div>
    <div class="row form-group">
        <?php echo $form->labelEx($model,'whatapps', array('label'=>'Whatapps', 'class' => 'col-sm-3')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model,'whatapps',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
            <?php echo $form->error($model,'whatapps'); ?>
        </div>
    </div>
    <div class="row form-group">
        <?php echo $form->labelEx($model,'line', array('label'=>'Line', 'class' => 'col-sm-3')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model,'line',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
            <?php echo $form->error($model,'line'); ?>
        </div>
    </div>
    <div class="row form-group">
        <?php echo $form->labelEx($model,'viber', array('label'=>'Viber', 'class' => 'col-sm-3')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model,'viber',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
            <?php echo $form->error($model,'viber'); ?>
        </div>
    </div>
    <div class="row form-group">
        <?php echo $form->labelEx($model,'wechat', array('label'=>'Wechat', 'class' => 'col-sm-3')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model,'wechat',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
            <?php echo $form->error($model,'wechat'); ?>
        </div>
    </div>
    <div style="border-bottom: 1px solid #CCC; margin-bottom: 10px;">
        <h4>Company</h4>
    </div>    
    <div class="row form-group">
        <?php echo $form->labelEx($company,'name',array('label'=>'Name', 'class' => 'col-sm-3')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($company,'name',array('class' => 'form-control')); ?>
        </div>
    </div>
    <div class="row form-group">
        <?php echo $form->labelEx($company,'address', array('class' => 'col-sm-3')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($company,'address',array('class' => 'form-control')); ?>
        </div>
    </div>
    <div class="row form-group">
        <?php echo $form->labelEx($company,'website', array('class' => 'col-sm-3')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($company,'website',array('class' => 'form-control')); ?>
        </div>
    </div>
    <div class="row form-group">
        <?php echo $form->labelEx($company,'phone', array('class' => 'col-sm-3')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($company,'phone',array('class' => 'form-control')); ?>
        </div>
    </div>
    <div class="row form-group">
        <?php echo $form->labelEx($company,'size', array('class' => 'col-sm-3')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($company,'size',array('class' => 'form-control')); ?>
        </div>
    </div>
    <div class="row form-group">
        <?php echo $form->labelEx($company,'about', array('class' => 'col-sm-3')); ?>
        <div class="col-sm-6">
            <?php echo $form->textArea($company,'about',array('class' => 'form-control')); ?>
        </div>
    </div>
    <?php echo $form->hiddenField($company,'companyId',array('class' => 'form-control')); ?>

        <div class="row form-group">
            <div class="col-sm-6">
            <?php echo $form->checkbox($model,'agreeTermOfUse', array('class' => 'col-sm-1')); ?>
                <?php echo $form->label($model,'agreeTermOfUse',array('label'=>'I agree with '.CHtml::link('Term Of Use',array('/term-of-use'), array('target'=>'_blank')))); ?>
                <?php echo $form->error($model,'agreeTermOfUse'); ?>
            </div>
        </div>


        <div class="row form-group">
            <div class="col-sm-offset-2 col-sm-6">
                <button type="button" class="btn btn-primary save" name="abc">Save</button>
            </div>
        </div>
        <?php $this->endWidget(); ?>

    </div>
    
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Notice</h4>
            </div>
          <div class="modal-body">
              <p>Your company has exist, please choice one option below:</p>
              <br/>
              <div class="data">
                  
              </div>
          </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary saveChange">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('document').ready(function(){
        $('.save').on('click',function(ev){
            ev.preventDefault();
            var error = $('body').find('.errorMessage:visible').length;
            var name = $('#SsnCompany_name').val();
            var i = 0;
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->createAbsoluteUrl('site/checkCompany')?>',
                data: "companyName="+name,
                success: function(data){
                    json = jQuery.parseJSON(data);
                    $('.data').html(' ');
                    $.each(json, function (key, data) {
                        i++;
                        var a = $('<input>').attr({
                                    type: 'radio',
                                    id: key,
                                    name: 'bar[]',
                                    class: 'rd',
                                    value: key
                                }).after(" "+data)
                        $('.data').append(a);
                        $('.data').append('</br>');
                    });
                    var a = $('<input>').attr({
                                    type: 'radio',
                                    id: 0,
                                    name: 'bar[]',
                                    class: 'rd',
                                    checked: 'true',
                                    value: 0
                                }).after(" Not above");
                        $('.data').append(a);
                    if(i > 0){
                        $('#myModal').modal('show');
                    }else{
                        $('form').submit();
                        
                    }
                }
            });
        });
    });
    $(document).on('click','.saveChange',function(e){
        var abc = $('.modal-body').find('input[type=radio]:checked').val();
        $('#SsnCompany_companyId').val(abc);
        $('form').submit();
    });
    
</script>
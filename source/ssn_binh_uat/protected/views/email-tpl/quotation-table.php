<?php
/* @var $model SsnShipping */
?>
<?php if ($model->items): ?>
<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border:1px solid #EAEAEA;">
	<thead>
		<tr>
			<th align="left" bgcolor="#EAEAEA" style="padding:3px 9px">Item name</th>
			<th align="left" bgcolor="#EAEAEA" style="padding:3px 9px">Color</th>
            <th align="left" bgcolor="#EAEAEA" style="padding:3px 9px">Price</th>
            <th align="left" bgcolor="#EAEAEA" style="padding:3px 9px">Unit</th>

		</tr>
	</thead>
	<tbody bgcolor="#F6F6F6">
	<?php foreach($model->items as $item): ?>
		<tr>
			<td align="left" valign="top" style="padding:3px 9px; border-bottom:1px dotted #CCCCCC;">
				<?= $item->itemdetail->name ?>
			</td>
			<td align="left" valign="top" style="padding:3px 9px; border-bottom:1px dotted #CCCCCC;">
				<?= $item->itemdetail->color ?>
			</td>
            <td align="left" valign="top" style="padding:3px 9px; border-bottom:1px dotted #CCCCCC;">
                <?= $item->price ?>
            </td>
            <td align="left" valign="top" style="padding:3px 9px; border-bottom:1px dotted #CCCCCC;">
                <?= $item->unit==""? "N/A" : $item->unit  ?>
            </td>

		</tr>
	<?php endforeach ?>
	</tbody>
</table>
<?php endif; ?>

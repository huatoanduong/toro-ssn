<?php
/*
 * Display menu and child menu items in sidebar
 * @author Lam Huynh
 */
class Breadcrumb extends CWidget {

	/**
	 * @var Menuitem active menuitem
	 */
	public $menuItem;

	public function init() {
		return parent::init();
	}

	public function run() {
		if (!$this->menuItem) return;

		$navItems = array();
		$item = $this->menuItem;
		do {
			array_unshift($navItems, $item);
		} while ($item = $item->parent);
			
		$this->render("breadcrumb", array(
			'items' => $navItems,
		));
	}

}

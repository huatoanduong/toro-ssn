<?php
class SlideshowWidget extends CWidget {

	public function run() {
		$banners = Banners::getActiveBanner();
		$this->render("slideshow", array('banner' => $banners));
	}

}

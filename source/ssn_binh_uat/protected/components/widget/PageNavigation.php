<?php
/*
 * Display menu and child menu items in sidebar
 * @author Lam Huynh
 */
class PageNavigation extends CWidget {

	/**
	 * @var Menuitem active menuitem
	 */
	public $page;

	public function init() {
		return parent::init();
	}

	public function run() {
		if (!$this->page) return;
		if (!$this->page->parent) {
			$page = $this->page;
		} else {
			$page = $this->page->parent;
		}

		$this->render("page_navigation", array(
			'model' => $page,
		));
	}

	public function getCssClass($page) {
		$firstChild = Page::model()->getFirstChild($page->parent->id);
		if(!isset($this->page->parent) && $firstChild->id == $page->id) {
			return 'active';
		} else {
			if ($this->page && $this->page->id==$page->id)
				return 'active';
			return;
		}
		return;
	}
}

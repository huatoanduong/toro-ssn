<?php
class BimServicesList extends CWidget {

	public $type = 'home';
	
	public function init() {
		return parent::init();
	}

	public function run() {
		if($this->type == 'home') {
			$models = Service::model()->getAllLimit(12);
			$data = $this->array_chunk_vertical($models, 2, false, false);
			$this->render("bim_services_home", array(
				'data' => $data,
			));
		} elseif($this->type == 'footer') {
			$models = Service::model()->getAllLimit(20);
			$data = array_chunk($models, 10);
			$this->render("bim_services_footer", array(
				'data' => $data,
			));
		}
	}
	
	public function array_chunk_vertical($input, $size, $preserve_keys = false, $size_is_horizontal = true) {
		$chunks = array();
	   
		if ($size_is_horizontal) {
			$chunk_count = ceil(count($input) / $size);
		} else {
			$chunk_count = $size;
		}
	   
		for ($chunk_index = 0; $chunk_index < $chunk_count; $chunk_index++) {
			$chunks[] = array();
		}

		$chunk_index = 0;
		foreach ($input as $key => $value)
		{
			if ($preserve_keys) {
				$chunks[$chunk_index][$key] = $value;
			} else {
				$chunks[$chunk_index][] = $value;
			}
		   
			if (++$chunk_index == $chunk_count) {
				$chunk_index = 0;
			}
		}
	   
		return $chunks;
	}
}

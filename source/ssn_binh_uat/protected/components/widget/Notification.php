<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Notification
 *
 * @author Lam
 */
class Notification extends CWidget {

	public function run() {
		if ($flashes = Yii::app()->user->getFlashes()): ?>
			<?php foreach($flashes as $type => $message): ?>
			<div class="alert alert-<?= $type ?>" role="alert">
				<button type="button" class="close" data-dismiss="alert">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<?= $message ?>
			</div>			
			<?php endforeach; ?>
		<?php endif;
	}
}

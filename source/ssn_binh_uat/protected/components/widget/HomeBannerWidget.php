<?php 
class HomeBannerWidget extends CWidget
{
    
    public function run()
    {
        $this->getBanners();
    }
    public function getBanners()
    {
       $init = new Banners();
       $models = $init->getActiveHomeBanners();
       $this->render("banners/home_banners",array('models'=>$models));
    }
}
?>
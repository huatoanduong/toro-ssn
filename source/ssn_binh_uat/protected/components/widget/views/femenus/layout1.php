<?php
$link = $this->active_action=='/cms/index'||$this->active_action=='/cms/viewGroup'?$this->current_page:$this->active_action;
?>
<nav class="menu">
    <?php if (!empty($menus)): ?>
    <ul>
        <?php foreach ($menus as $key => $item): ?>
            <?php
            $class = '';
            if($this->isHighlight($link, $item['id']))
                $class = $this->active_class;
            ?>
            <li class="<?php if($key==0) echo 'first'?> <?php if(!isset($menus[$key+1])) echo 'last'?> <?php echo $class; ?>"><a href="<?php echo $item['link'];?>"><?php echo $item['title'];?></a>
                <?php if (!empty($item['child'])): ?>
                    <ul>
                        <?php foreach ($item['child'] as $sub_item): ?>
                            <li><a href="<?php echo $sub_item['link'] ?>"><?php echo $sub_item['title'] ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
    </ul>
    <?php endif; ?>
</nav>
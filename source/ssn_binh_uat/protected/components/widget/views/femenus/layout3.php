<div class="group-1">
    <h4><?php echo $this->group_name?></h4>
    <?php if (!empty($menus)): ?>
    <ul class="link">
        <?php foreach ($menus as $key => $item): ?>
        <?php
        $class = '';
        $link = $this->active_action=='/cms/index'?$this->current_page:$this->active_action;
        if($this->isHighlight($link, $item['id']))
            $class = $this->active_class;
        ?>
        <li class="<?php echo $class?>"><a href="<?php echo $item['link'] ?>"><?php echo $item['title'] ?></a></li>
        <?php if($key==3):?>
        </ul>
        <ul class="link">
        <?php endif;?>
        <?php endforeach; ?>
    </ul>
    <?php endif; ?>
</div>
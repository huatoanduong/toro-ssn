<div class="services col-md-4">
	<h2 class="title-2">BIM services</h2>
	<div class="clearfix list-services">
		<?php foreach($data as $cols) : 
		?>
			<ul>
				<?php foreach($cols as $item): ?>
				<li><a href="<?php echo Yii::app()->createAbsoluteUrl('service/detail', array('slug'=>$item->slug));?>"><?php echo $item->title;?></a></li>
				<?php endforeach;?>
			</ul>
		<?php endforeach; ?>
	</div>
	<a href="<?php echo Yii::app()->createAbsoluteUrl('service/index');?>" class="btn btn-danger">more services</a>
</div><!-- Services -->
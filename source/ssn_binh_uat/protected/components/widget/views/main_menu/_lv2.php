<?php
/* @var $model Menuitem */
/* @var $this MainMenu */
?>
<?php foreach($items as $k => $item): ?>
<?php if (($k % 4)==0): ?><ul><?php endif ?>
	<li><a href="<?php echo $item->createLink() ?>"><?php echo $item->name ?></a></li>
<?php if (($k % 4)==3 || $k==(count($items)-1)): ?></ul><?php endif ?>
<?php endforeach ?>

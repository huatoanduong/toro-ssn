<?php

/**
 * Render a Ccustomizable select box with support for searching, 
 * tagging, remote data sets, infinite scrolling
 *
 * @author Lam
 */
class Select2 extends CInputWidget {

	public function run() {
		list($name, $id) = $this->resolveNameID();

		if (isset($this->htmlOptions['id']))
			$id = $this->htmlOptions['id'];
		else
			$this->htmlOptions['id'] = $id;
		if (isset($this->htmlOptions['name']))
			$name = $this->htmlOptions['name'];
		else
			$this->htmlOptions['name'] = $name;

		if ($this->hasModel())
			echo CHtml::activeDropDownList($this->model, $this->attribute, $this->htmlOptions);
		else
			echo CHtml::textField($name, $this->value, $this->htmlOptions);

		if (isset($this->defaultLanguage) || isset($this->language))
			$this->registerScriptFile($this->i18nScriptFile);

		if (isset($this->defaultLanguage)) {
			if (!isset($this->defaultOptions['showMonthAfterYear']))
				$this->defaultOptions['showMonthAfterYear'] = false;
			Yii::app()->getClientScript()->registerScript(__CLASS__ . 'defaultI18n', "jQuery.datepicker.setDefaults(jQuery.extend(" . CJavaScript::encode($this->defaultOptions) . ", jQuery.datepicker.regional['{$this->defaultLanguage}']));");
		} else {
			Yii::app()->getClientScript()->registerScript(__CLASS__ . 'defaultOptions', "jQuery.datepicker.setDefaults(" . CJavaScript::encode($this->defaultOptions) . ");");
		}

		if (isset($this->language)) {
			if (!isset($this->options['showMonthAfterYear']))
				$this->options['showMonthAfterYear'] = false;
			$js = "jQuery('#{$id}').datepicker(jQuery.extend(" . CJavaScript::encode($this->options) . ", jQuery.datepicker.regional['{$this->language}']));";
		} else {
			(!$this->options) ? $option = '' : $option = CJavaScript::encode($this->options);
			$js = "jQuery('#{$id}').datepicker($option);";
		}
		Yii::app()->getClientScript()->registerScript(__CLASS__ . '#' . $id, $js);
	}

}

<?php

class CmsController extends FrontController {

	private $_viewingObject = null;

	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page' => array(
				'class' => 'CViewAction',
			),
		);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		$error = Yii::app()->errorHandler->error;

		if (Yii::app()->request->isAjaxRequest)
			echo $error['message'];
		else
			$this->render('error', $error);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex($slug) {
		$model = Page::model()->find("slug=:slug and status=1", array(':slug' => $slug));
		$this->pageTitle = $model->title;
		$init = new Ads();
		$banner = $init->getAdsBanners($slug);
		if (!$model)
			throw new CHttpException(404, 'The requested page does not exist.');

		$this->_viewingObject = $model;
		$layout = empty($model->content) ? 'first_child' : 'tree';
		if (empty($model->childs) && $model->parent_id==0)
			$layout = 'no_nav';
		
		$this->render('page/' . $layout, array(
			'model' => $model,
			'banner' => $banner
		));
	}

	public function getActiveMenuItem() {
		if ($this->action->id == 'index') {
			if (!$this->_viewingObject)
				return null;
			$page = $this->_viewingObject;
			$i = 0;
			do {
				$c = new CDbCriteria();
				$c->compare('type', Menuitem::TYPE_CMS_PAGE);
				$c->compare('link', $page->id);
				$c->order = 'parent_id desc';
				$menuItem = Menuitem::model()->find($c);
				$page = $page->parent;
				$i++;
			} while ((!$menuItem && $page) || $i > 5);
			return $menuItem;
		}
		return null;
	}

}

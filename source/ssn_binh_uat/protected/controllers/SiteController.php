<?php

class SiteController extends FrontController {
    public $attempts = MAX_TIME_TO_SHOW_CAPTCHA;
    public $counter;

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page           
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('captcha'),
                'users' => array('*'),
            ),
        );
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
			} else {
				$this->layout = '/layouts/blank';
                $this->render('error', $error);
			}
        }
    }

    protected function performAjaxValidation($model) {
        try {
            if (isset($_POST['ajax'])) {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
        } catch (Exception $e) {
            Yii::log("Exception " . print_r($e, true), 'error');
            throw new CHttpException("Exception " . print_r($e, true));
        }
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {

        if(isset(Yii::app()->user->id))
            //$this->redirect(Yii::app()->createAbsoluteUrl('/site/dashboard'));
            $this->redirect(Yii::app()->createAbsoluteUrl('/member/sample'));

        $this->redirect(Yii::app()->createAbsoluteUrl('/site/login'));
    }

    public function actionCheckCompany() {
        $companyName = $_POST['companyName'];
        $isExist = $this->isCompanyNameExist($companyName);
        $tmp = array();
        if ($isExist) {
            $tmp = $this->getCompanyByName($companyName);
        }
        echo json_encode($tmp);
        die;
    }

    public function isCompanyNameExist($companyName) {
        $company = SsnCompany::model()->findAll("name = '{$companyName}'");
        $r = FALSE;
        if ($company)
            $r = TRUE;
        return $r;
    }

    public function getCompanyByName($companyName) {
        $company = SsnCompany::model()->findAll(array('condition' => "name = '{$companyName}'"));
        $tmp = array();
        if ($company) {
            foreach ($company as $k => $item) {
                $tmp[$item->id] = "<div style='display:absolute; margin-top: -41px; margin-left: 20px'>". "Company Name: ".$item->name . '</br>' .'Company Phone ' . $item->phone . '</br>' .'Company Address ' . $item->address."</div>";
            }
        }
        return $tmp;
    }

	//public function actionActivation($email, $code) {
    public function actionActivation($code) {

        //Yii::app()->securityManager->setEncryptionKey("blowfish");
        //echo $dataeny = utf8_encode(Yii::app()->securityManager->encrypt("gkmbinh@gmail.com"  ));
        //echo Yii::app()->securityManager->decrypt(utf8_decode($dataeny)  );
        //echo $dataeny = base64_encode("gkmbinh@gmail.com");
        //echo base64_decode($dataeny);

        //?email=Z2ttYmluaEBnbWFpbC5jb20%3D&code=291544

        $code = base64_decode($code);
        $data = explode("{X}", $code );
        if (empty($data) || count($data) !=2) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        $email = $data[0];
        $code = $data[1];
        //$u = Users::model()->findByAttributes(array('email' => trim($email)));
		$user = Users::model()->findByAttributes(array(
            'email' => trim($email),
			'verify_code' => trim($code),
			'status' => STATUS_INACTIVE
		));

		if (!$user) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		$user->status = STATUS_ACTIVE;
		$user->verify_code = '';
		$user->update(array('status', 'verify_code'));
		Yii::app()->user->setFlash('success', 'Activated successfully. You can login to the system');
		$this->redirect(array('login'));
	}

    public function actionRegister() {
        $this->pageTitle = 'Register';
        $this->layout='column1';
        $model = new Users('createMemberFE');

        $company = new SsnCompany('createMember');

        //Load user & company by email
        // Uncomment the following line if AJAX validation is needed

        $this->performAjaxValidation($model);
        if (isset($_POST['Users'])) {


            $model->attributes = $_POST['Users'];
            $model->email = $_POST['Users']['username'];//$model->username; //username
            $company->attributes = $_POST['SsnCompany'];
            $company->admin_email = $model->email;

            $userCheck = Users::model()->findByAttributes(array('email' => trim($model->email)));
            if($userCheck && $userCheck->status== STATUS_AUTOCREATE_INACTIVE){
                $model = $userCheck;
                $companyCheck = SsnCompany::model()->findByAttributes(array('admin_email' => trim($model->email)));

                if($companyCheck && $companyCheck->status == STATUS_AUTOCREATE_INACTIVE){
                    $company = $companyCheck;
                }
            }

            if ($model->validate() && $company->validate()) {

                $model->attributes = $_POST['Users'];
                $model->email = $_POST['Users']['username'];//$model->username; //username
                $company->attributes = $_POST['SsnCompany'];
                $company->admin_email = $model->email;

                $model->status = STATUS_INACTIVE;
                $model->role_id = ROLE_MEMBER;
                $model->application_id = FE;
                $model->password_hash = md5($model->temp_password);
                $model->verify_code = Users::model()->checkVerifyCode(rand(100000, 1000000)); // Gen verify code and send qua mail or sms
                if ($model->save()) {

                    $c = $company;
                    $c->user_id = $model->id;
                    if ($_POST['SsnCompany']['companyId'] == 0) {
                        $c->attributes = $_POST['SsnCompany'];
                        $c->status =  STATUS_INACTIVE;
                        $c->save();
                    } else {
                        $oldCompany = SsnCompany::model()->findByPk($_POST['SsnCompany']['companyId']);
                        if ($oldCompany) {
                            $c->attributes = $oldCompany->attributes;
                            $c->status =  STATUS_INACTIVE;
                            $c->user_id = $model->id;
                            $c->save();
//                            $c->saveImage($fieldName);
                        }
                    }

                    //if($c->company_code ==""){
                    preg_match_all("/[A-Z]/", ucwords(strtolower($c->name)), $matches);
                    $c->company_code = implode("",$matches[0]);
                    $c->company_code.=$c->id;
                    $c->save();
                    //}


					SendEmail::registerSucceedToUser($model);
					Yii::app()->user->setFlash('success', 'Register success. An activation link has been sent to your email');
                    $this->redirect(array('login'));
                }
            }else{
                Yii::app()->user->setFlash('error', $model->getFirstError());
                //Yii::app()->user->setFlash('error', $model->getErrors()[0][0]);
                //$this->redirect(array('login'));
                //var_dump($model->getErrors());
                //$model->get
                //var_dump($company->getErrors());
                //Yii::app()->user->setFlash('error', $model->getErrors());
                //$this->redirect(array('login'));
            }
        }
        $this->render('/partials/register', array(
            'model' => $model,
            'company' => $company
        ));
    }

    private function captchaRequired() {
        return Yii::app()->session->itemAt('captchaRequired') >= $this->attempts;
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {



        if(isset(Yii::app()->user->id))
            //$this->redirect(Yii::app()->createAbsoluteUrl('/site/dashboard'));
            //$this->redirect(array('/member/sample/index'));
            $this->redirect(Yii::app()->createAbsoluteUrl('/member/sample'));

        $this->layout='column1';
        $model = $this->captchaRequired() ? new LoginForm('captchaRequired') : new LoginForm();
        $model->login_by = 'username';
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            $user = new Users();
            $user->email = $model->login_by;
            $user->password_hash = $model->password;
            if ($model->validate()) {
                //$this->redirect(Yii::app()->createAbsoluteUrl('/site/dashboard'));
                $this->redirect(Yii::app()->createAbsoluteUrl('/member/sample'));
                if($user->checkUser()){
                    Yii::app()->session->add('captchaRequired', 0);
                    Yii::app()->end();
                }
            } else {
                //var_dump($model->getErrors());
				Yii::app()->user->setFlash('error', $model->getFirstError());
                $this->counter = Yii::app()->session->itemAt('captchaRequired') + 1;
                Yii::app()->session->add('captchaRequired', $this->counter);
            }
        }

        $this->render('/partials/login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->createAbsoluteUrl('site/login'));
    }

    /**
     * send an email to enable member to reset password - bb - 27/7/2014
     */
    public function actionForgotPassword() {
        $model = new Users('forgotPassword');
        if (isset($_POST['Users'])) {
            $model->email = $_POST['Users']['email'];
            if ($model->validate()) {
                SendEmail::forgotPasswordToUser($model);
                Yii::app()->user->setFlash('success', "Email sent! You'll receive an email with instructions on how to set a new password.");
                $this->redirect(array('forgotPassword'));
            }
        }
		$this->layout = '/layouts/blank';
        //$this->layout='/layouts/site';
        //tite : Forget Password – SSN
        $this->pageTitle ="Forget Password – SSN";

        $this->render('forgot_password', array('model' => $model));
    }

    public function actionReSendVerification() {
        $model = new Users('resendverification');
        if (isset($_POST['Users'])) {
            $model->email = $_POST['Users']['email'];
            if ($model->validate()) {

                SendEmail::reSendVerify($model);

                Yii::app()->user->setFlash('success', "Email sent! You'll receive an email with instructions on how to verify account.");

                $this->redirect(array('ReSendVerification'));
            }
        }

        $this->layout = '/layouts/blank';
        //$this->layout='/layouts/site';
        //tite : Forget Password – SSN
        $this->pageTitle ="Resend verification - SSN";
        $this->render('resend_verification', array('model' => $model));
    }

    //reSendVerification


    public function actionResetPassword() {
		$verify_code = trim($_GET['verify_code']);
		$model = Users::model()->findByAttributes(array('verify_code' => $verify_code));
		if (!$model)
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');

		$model->temp_password = StringHelper::getRandomString();
		$model->password_hash = md5($model->temp_password);
		$model->verify_code = '';
		$model->update(array('password_hash', 'verify_code', 'temp_password'));
		SendEmail::changePassToUser($model);
		Yii::app()->user->setFlash('success', 'Your password has been changed successfully. Please check your email.');
		$this->redirect(array('login'));
    }

    public function actionContactUs() {
        $contact_page_id = 87;
        $this->pageTitle = 'Liên hệ ';
        $general_enquiry = new ContactGeneralEnquiry('create');

        if (isset($_POST['ContactGeneralEnquiry'])) {
            $general_enquiry->attributes = $_POST['ContactGeneralEnquiry'];
            if ($general_enquiry->save()) {
                
            } else {
                Yii::app()->user->setFlash('error', 'General Enquiry cannot be submitted for some reasons');
                Yii::log(print_r($general_enquiry->getErrors(), true), 'error', 'SiteController.actionContact');
            }
        }


        $this->render('contact_us', array(
            'general_enquiry' => $general_enquiry,
        ));
    }

    public function actionUnderConstruction() {
        $this->render('underconstruction');
    }
    
    public function actionDashboard(){

        //Users::model()->createUserCompanyProfileWithOnlyEmail("emtestnhenhang@gmail.com");
        $this->redirect(Yii::app()->createAbsoluteUrl('/member/sample'));
        return;

        $this->pageTitle = 'Dashboard';
        $this->render('dashboard', array(
            
        ));
    }

}

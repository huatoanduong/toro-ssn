<?php

/**
 * This is the model class for table "{{_ssn_sample}}".
 *
 * The followings are the available columns in table '{{_ssn_sample}}':
 * @property integer $id
 * @property integer $item_id
 * @property integer $from_user_id
 * @property string $awbn
 * @property string $remark
 * @property string $document
 * @property string $attention
 * @property integer $status
 * @property string $send_date
 * @property string $created_date
 * @property string $updated_date
 */
class SsnSample extends _BaseModel {

	public $uploadFileFolder = 'upload/sample/files';
	public $maxUploadFileSize = 3145728;
	public $allowUploadType = 'doc,docx,xls,xlsx,pdf,jpg,png,bmp,jpeg,jpf,bitmap,gif';//'jpg','png','bmp','jpeg','jpf','bitmap','gif'

	const STATUS_SEND = 1;
	const STATUS_RECEIVED = 0;

	public static function _SEARCH_TIME_OPTIONS() {
		return array(
			'1' => 'Lastest 7 days',
			'2' => 'Current month',
			'3' => 'Lastest 3 months',
			'4' => 'Range',
		);
	}

	/*
	 * @var SsnSampleItem[]
	 */

	public $form_items = array();

	/*
	 * @var array
	 */
	public $toUserIds;

	/*
	 * @var string
	 */
	public $documentFile;

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{_ssn_sample}}';
	}


    public $sender_email ;
    public $seach_receiver, $search_courier;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('awbn, dateSentText, toUserIds', 'required', 'on' => 'create, update', 'message' => 'You can not leave this field empty.'),
			array('document', 'application.components.validator.FileUploadClientValidator',
				'types' => 'doc,docx,xls,xlsx,pdf,jpg,png,bmp,jpeg,jpf,bitmap,gif', 'allowEmpty' => true, 'maxSize' => 3000000,
				'on' => 'create, update'),
			array('item_id, from_user_id, status', 'numerical', 'integerOnly' => true),
			array('awbn', 'length', 'max' => 50),
			array('document', 'length', 'max' => 255),
			array('remark, created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, item_id, from_user_id, awbn, remark, document, status, created_date, updated_date', 'safe', 'on' => 'search'),
			array('sender_email,seach_receiver, seach_courier, _search_time, _search_start_date, _search_end_date, attention, courier_id', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'sender' => array(self::BELONGS_TO, 'Users', 'from_user_id'),

            //'company' => array(self::HAS_ONE, 'SsnCompany', 'user_id'),
            'company'=>array(
                self::HAS_ONE,'SsnCompany',array('id'=>'user_id'),
                'through'=>'sender'
            ),

			'receivers' => array(
                self::MANY_MANY, 'Users',
                                 '{{_ssn_sample_to_user}}(sample_id, user_id)' ),

            'students'=>array(
                self::HAS_MANY,'User',array('student_id'=>'id'),
                'through'=>'mentorships','joinType'=>'INNER JOIN'
            ),


			'items' => array(self::HAS_MANY, 'SsnSampleItem', 'sample_id'),
			'courier' => array(self::BELONGS_TO, 'SsnCourier', 'courier_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'courier_id' => Yii::t('translation', 'Courier'),
			'id' => Yii::t('translation', 'ID'),
			'item_id' => Yii::t('translation', 'Item'),
			'from_user_id' => 'Send From',
			'awbn' => Yii::t('translation', 'AWBN'),
			'remark' => Yii::t('translation', 'Remark'),
			'document' => Yii::t('translation', 'Document'),
			'attention' => Yii::t('translation', 'Attention'),
			'status' => Yii::t('translation', 'Status'),
			'created_date' => Yii::t('translation', 'Send Date'),
			'updated_date' => Yii::t('translation', 'Updated Date'),
			'dateSentText' => 'Send Date',
			'toUserIds' => 'Send To',
			'senderName' => 'Sender',
			'recipientNames' => 'Recipient',
		);
	}

	public function behaviors() {
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'created_date',
				'updateAttribute' => 'updated_date',
			)
		);
	}

	public function activate() {
		$this->status = 1;
		$this->update();
	}

	public function deactivate() {
		$this->status = 0;
		$this->update();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SsnSample the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function nextOrderNumber() {
		return SsnSample::model()->count() + 1;
	}

	public function showUploadFile($model) {
		if (!empty($model->document)) {
			return CHtml::link($model->document, Yii::app()->createAbsoluteUrl($model->uploadFileFolder . '/' . $model->id . '/' . $model->document), array('target' => '_blank'));
		} else {
			return '';
		}
	}

	protected function beforeSave() {
		$this->created_date = DateHelper::toDbDateFormat($this->created_date);
		return parent::beforeSave();
	}

	public function getDateSentText() {
		return date('d/m/Y', strtotime($this->send_date));
	}

    public function getRawDateSentText($data,$row) {
        return date('d/m/Y', strtotime($data->send_date));
    }


	public function setDateSentText($value) {
		$this->send_date = DateHelper::toDbDateFormat($value);
	}

	public function getSenderName() {
		if ($this->sender->company)
			return $this->sender->company->name;
		return '';
	}

	public function getRecipientNames() {
		$names = array();
		foreach ($this->receivers as $receiver) {
			$names[] = $receiver->company->name;
		}
		return implode('<br/>', $names);
	}

	public function getRemarkExcerpt() {
		return StringHelper::limitStringLength($this->remark, 30);
	}

	public function getDocumentDirectory() {
		return sprintf('%s/upload/sample/%s/', Yii::getPathOfAlias('webroot'), $this->id);
	}

	public function saveDocument() {
		if (!$this->documentFile)
			return;

		$saveName = $this->documentFile->getName();
		$savePath = $this->getDocumentDirectory() . $saveName;
		if (!file_exists(dirname($savePath))) {
			mkdir(dirname($savePath));
		}
		$this->documentFile->saveAs($savePath);
		$this->removeDocumentFile();
		$this->document = $saveName;
		$this->update('document');
	}

	public function getDocumentDownloadUrl() {
		$file = $this->getDocumentDirectory() . $this->document;
		$url = sprintf('%s/upload/sample/%s/%s', Yii::app()->baseUrl, $this->id, $this->document);
		return is_file($file) ? $url : null;
	}

	public function saveReceivers() {

		$sql = "delete from {{_ssn_sample_to_user}} where sample_id=" . $this->id;
		Yii::app()->db->createCommand($sql)->execute();

        $ids = array();

		foreach ($this->toUserIds as $userId) {

            //CheckUserID, and Check Email exits ?
            $user = Users::model()->findByPk($userId); //findByAttributes(array('email' => $userId));
            if(!$user) {
                if (filter_var($userId, FILTER_VALIDATE_EMAIL)) {

                    //Tao company user
                    $user = Users::model()->createUserCompanyProfileWithOnlyEmail($userId);
                    if($user!=null && $user->id !=null){
                        $ids[]= $user->id ;

                        $sql = "insert into {{_ssn_sample_to_user}}(sample_id, user_id) value({$this->id}, {$user->id})";
                        Yii::app()->db->createCommand($sql)->execute();

                        //Tao Partner..if not exits ?

                        $modelp = new SsnPartner('create');
                        $modelp->user_email = $user->email;
                        $modelp->status = SsnPartner::STATUS_APPROVE;

                        $modelp->approve_by =  Yii::app()->user->id;
                        $modelp->from_user_id = Yii::app()->user->id;

                        $modelp->to_user_id = $user ? $user->id : 0;
                        $modelp->partner_outsite_system = $user ? '' : $user->email;

                        if ($modelp->validate()) {
                            $modelp->created_date = date('Y-m-d H:i:s');
                            if ($modelp->save()) {
                                SendEmail::partnerPendingRequest($modelp);
                             }
                        }


                    }

                }

            }else {
                $ids[]= $userId;
                $sql = "insert into {{_ssn_sample_to_user}}(sample_id, user_id) value({$this->id}, {$userId})";

                Yii::app()->db->createCommand($sql)->execute();
            }
		}

        //Update list
        $this->toUserIds = $ids;

	}

	public function getTrackingLink() {
		if (!$this->courier)
			return null;
		$url = str_replace('{{NUMBER}}', $this->awbn, $this->courier->tracking_link_pattern);
		return $url;
	}

	public function canView($userId = null) {
		return $this->isSender($userId) || $this->isRecipient($userId);
	}

	public function canEdit($userId = null) {
		return $this->isSender($userId);
	}

	public function isSender($userId = null) {
		if (!$userId)
			$userId = Yii::app()->user->id;
		return $userId == $this->from_user_id;
	}

	public function isRecipient($userId = null) {
		if (!$userId)
			$userId = Yii::app()->user->id;
		if ($this->isSender($userId))
			return false;
		$criteria = new CDbCriteria;
		$criteria->compare('id', $this->id);
		$criteria->addCondition("exists (SELECT id "
			. "FROM {{_ssn_sample_to_user}} "
			. "WHERE sample_id=t.id and user_id=$userId)");
		return self::model()->exists($criteria);
	}

	protected function beforeDelete() {
		$this->removeDocumentFile();
		return true;
	}

	public function removeDocumentFile() {
		if (is_file($this->getDocumentDirectory() . $this->document))
			unlink($this->getDocumentDirectory() . $this->document);
	}

	public function safeDeleteAll($condition = '', $params = array()) {
		$models = self::model()->findAll($condition, $params);
		foreach ($models as $model) {
			$model->delete();
		}
	}

	public function getCourierName() {
		return $this->courier ? $this->courier->courier : 'Other';
	}

	public function getRecipientIds() {
		$ids = array();
		foreach ($this->receivers as $receiver) {
			$ids[] = $receiver->id;
		}
		return $ids;
		
	}
}

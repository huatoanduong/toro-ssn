<?php

/**
 * @inheritdoc
 */
class SsnShippingSearch extends SsnShipping
{
	const MODE_SENT = 'sent';
	const MODE_RECV = 'recv';
	
	public $mode;
	public $user_id;
	public $key;
	public $dateRange=1;
	public $fromDate;
	public $toDate;
	public $pagination;
	
	public static $DATE_RANGES = array(
		1 => 'Latest 7 days',
		2 => 'Current month',
		3 => 'Latest 3 months',
		4 => 'Range',
	);
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dateRange, user_id', 'numerical', 'integerOnly'=>true),
			array('key, fromDate, toDate', 'safe'),
		);
	}

	/**
	 * @return SsnShipping[]
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$fromDate = $toDate = null;
		
		switch ($this->dateRange) {
			case 1:	// 7 day
				$fromDate = date('Y-m-d', strtotime('-7 days'));
				$toDate = date('Y-m-d');
				break;

			case 2: // cur month
				$fromDate = date('Y-m-1');
				$toDate = date("Y-m-t");
				break;

			case 3: // 3 months
				$fromDate = date('Y-m-1', strtotime('-3 months'));
				$toDate = date("Y-m-t");
				break;

			case 4: // range
				if ($this->fromDate)
					$fromDate = DateHelper::toDbDateFormat($this->fromDate);
				
				if ($this->toDate)
					$toDate = DateHelper::toDbDateFormat($this->toDate);
				break;

			default:
				break;
		}

		if ($fromDate)
			$criteria->addCondition("date_load >= '$fromDate'");
		
		if ($toDate)
			$criteria->addCondition("date_load <= '$toDate'");
		
		if ($this->key) {
			$comp = SsnCompany::model()->tableName();
			$cond = "order_number LIKE :key OR remark LIKE :key ";
			if ($this->mode==self::MODE_RECV) {
				$cond .= "OR EXISTS (SELECT id FROM $comp c WHERE c.name LIKE :key AND c.user_id=t.from_user_id)";
			} else {
				$cond .= "OR EXISTS (SELECT id FROM $comp c WHERE c.name LIKE :key AND c.user_id=t.to_user_id)";
			}
			$criteria->addCondition($cond);
			$criteria->params[':key'] = "%{$this->key}%";
		}
		
		if ($this->mode==self::MODE_RECV) {
			$criteria->compare('to_user_id', $this->user_id);
		} else {
			$criteria->compare('from_user_id', $this->user_id);
		}
		
		$criteria->order = 'date_load DESC';
		
		$pagination = new CPagination(self::model()->count($criteria));
		$pagination->pageSize=20;
		$pagination->applyLimit($criteria);
		$this->pagination = $pagination;
		
		return self::model()->findAll($criteria);
	}
	
	public function searchAdmin() {
		$criteria = new CDbCriteria;

		$fromDate = '1970-1-1';
		$toDate = '2999-1-1';
		if ($this->fromDate) {
			$fromDate = DateHelper::toDbDateFormat($this->fromDate);
		}

		if ($this->toDate) {
			$toDate = DateHelper::toDbDateFormat($this->toDate);
		}
		$criteria->addCondition("date_load BETWEEN '$fromDate' AND '$toDate'");
		
		if ($this->key) {
			$comp = SsnCompany::model()->tableName();
			$cond = "order_number LIKE :key OR remark LIKE :key ";
			$cond .= "OR EXISTS (SELECT id FROM $comp c WHERE c.name LIKE :key AND c.user_id=t.from_user_id)";
			$cond .= "OR EXISTS (SELECT id FROM $comp c WHERE c.name LIKE :key AND c.user_id=t.to_user_id)";
			$criteria->addCondition($cond);
			$criteria->params[':key'] = "%{$this->key}%";
		}

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => Yii::app()->params['PageSize'],
			),
		));
	}}
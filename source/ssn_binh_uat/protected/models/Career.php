<?php

/**
 * This is the model class for table "{{_masterfile}}".
 *
 * The followings are the available columns in table '{{_masterfile}}':
 * @property integer $id
 * @property string $content
 * @property integer $status
 * @property integer $type
 * @property string $created_date
 */
class Career extends _BaseModel {
    
    
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{_masterfile}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('content, status', 'required'),
            array('status, type', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, content, status, type, created_date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Yii::t('translation', 'ID'),
            'content' => Yii::t('translation', 'Content'),
            'status' => Yii::t('translation', 'Status'),
            'type' => Yii::t('translation', 'Type'),
            'created_date' => Yii::t('translation', 'Created Date'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('content', $this->content, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('type', $this->type);
        $criteria->compare('created_date', $this->created_date, true);


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->params['PageSize'],
            ),
        ));
    }

    public function activate() {
        $this->status = 1;
        $this->update();
    }

    public function deactivate() {
        $this->status = 0;
        $this->update();
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Citizenship the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function nextOrderNumber() {
        return self::model()->count() + 1;
    }
    
    protected function beforeSave() {
        if (parent::beforeSave() && $this->isNewRecord) {
            $this->created_date = date('Y-m-d H:i:s', time());
            $this->type = CONTACT_US_CAREER;
        }

        return parent::beforeSave();
    }
    
    public function defaultScope() {
        return array(
            'condition' => 'type = "' . CONTACT_US_CAREER . '"',
        );
    }
    
    public function getList(){
        $criteria = new CDbCriteria();
        $criteria->compare('status', STATUS_ACTIVE);
        $models = self::model()->findAll();
        $list = CHtml::listData($models, 'id', 'content');
        return $list;
    }
	
	public function getListName(){
        $criteria = new CDbCriteria();
        $criteria->compare('status', STATUS_ACTIVE);
        $models = self::model()->findAll();
        $list = CHtml::listData($models, 'content', 'content');
        return $list;
    }
}

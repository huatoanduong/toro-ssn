<?php

/**
 * This is the model class for table "{{_banners}}".
 *
 * The followings are the available columns in table '{{_banners}}':
 * @property string $id
 * @property string $render_in_url
 * @property string $name
 * @property string $alias
 * @property string $created_date
 */
define('ADS_BANNER_TYPE', 2);

class Ads extends _BaseModel {

	public $maxImageFileSize = 3145728; //3MB
	public $allowImageType = 'jpg,gif,png,jpeg';
	public $uploadImageFolder = 'upload/banners'; //remember remove ending slash
	public $defineImageSize = array(
		'image' => array(
			array('alias' => 'thumb', 'size' => '300x150'),
			array('alias' => 'large', 'size' => '1400x248')
		)
	);
	public $imageSize = '1400px x 248px';

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{_banners}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('apply_for_page,status', 'required', 'on' => 'create, update'),
			array('order_display, status, type', 'numerical', 'integerOnly' => true),
			array('image', 'length', 'max' => 255),
			array('apply_for_page,title', 'length', 'max' => 50),
			array('id, title, image, order_display, status, type, created_date', 'safe'),
//            array('link', 'url', 'defaultScheme' => 'http'),
			array('image', 'file',
				'allowEmpty' => true,
				'types' => 'jpg,gif,png,jpeg',
				'wrongType' => 'Only jpg,gif,png,jpeg are allowed.',
				'maxSize' => FileHelper::getMaxFileSize(), // 3MB
				'tooLarge' => 'The file was larger than ' . (FileHelper::getMaxFileSize() / 1024) . ' KB. Please upload a smaller file.',
			),
			array(
				'image', 'match',
				'pattern' => '/^[^\\/?*:&;{}\\\\]+\\.[^\\/?*:;{}\\\\]{3,4}$/',
				'message' => 'Image files name cannot include special characters: &%$#',
			)
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => Yii::t('translation', 'ID'),
			'name' => Yii::t('translation', 'Title'),
//            'link' => Yii::t('translation', 'Link'),
			'image' => Yii::t('translation', 'Image'),
			'status' => Yii::t('translation', 'Status'),
			'type' => Yii::t('translation', 'Type'),
			'created_date' => Yii::t('translation', 'Created Date'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function activate() {
		$this->status = 1;
		$this->update();
	}

	public function deactivate() {
		$this->status = 0;
		$this->update();
	}

	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('status', $this->status, true);
		$criteria->compare('order_display', $this->order_display, true);
		$criteria->compare('type', ADS_BANNER_TYPE);
		$criteria->compare('created_date', $this->created_date, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 't.order_display desc, t.created_date desc'),
			'pagination' => array(
				'pageSize' => Yii::app()->params['PageSize'],
			),
		));
	}

	public function defaultScope() {
		return array(
			'condition' => 'type = "' . ADS_BANNER_TYPE . '"',
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Banner the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function nextOrderNumber() {
		return self::model()->count() + 1;
	}

	public function getActiveAdsBanner() {
		$criteria = new CDbCriteria();
		$criteria->compare('status', STATUS_ACTIVE);
		$criteria->order = "order_display ASC";
		return self::model()->findAll($criteria);
	}

	protected function beforeSave() {
		if (parent::beforeSave() && $this->isNewRecord) {
			$this->created_date = date('Y-m-d H:i:s', time());
			$this->type = ADS_BANNER_TYPE;
		}
		if ($this->order_display < 1) {
			$this->order_display = 1;
		}
		return parent::beforeSave();
	}

	public function getBannerById($id) {
		return self::model()->findByPk($id);
	}

	public function getApplyPageList() {
		return $this->getAdsPage() + $this->getArrayCmsPage();
	}
	
	public function getArrayCmsPage(){
		$init = new Page();
		$list = $init->getPageListData();
		return CHtml::listData($list, 'slug', 'title');
	}

	public function getAdsPage() {
		return array(
			'site/contactus' => 'Contact Us',
			'bimCap/index' => 'BIM Capalities',
			'service/index' => 'BIM Service',
		);
	}

	public function getApplyPageName($actionPage) {
		$list = $this->getApplyPageList();
		if (array_key_exists($actionPage, $list)) {
			return $list[$actionPage];
		}
		return 'Not Set';
	}

	public function getAdsBanners($apply_for_page) {
		$criteria = new CDbCriteria();
		$criteria->compare('status', STATUS_ACTIVE);
		$criteria->addCondition('apply_for_page = "' . $apply_for_page . '"');
		return self::model()->findAll($criteria);
	}

}

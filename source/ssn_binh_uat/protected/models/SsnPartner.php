<?php

/**
 * This is the model class for table "{{_ssn_partner}}".
 *
 * The followings are the available columns in table '{{_ssn_partner}}':
 * @property integer $id
 * @property integer $from_user_id
 * @property integer $to_user_id
 * @property string $created_date
 * @property integer $status
 * @property integer $approve_by
 */
class SsnPartner extends _BaseModel
{

    const STATUS_SEND = 0;
    const STATUS_PENDING = 1;
    const STATUS_APPROVE = 2;
    public static $STATUSES = array(
        self::STATUS_SEND => 'Waiting',
        self::STATUS_PENDING => 'Waiting',
        self::STATUS_APPROVE => 'Approved',
    );

    public $user_email;

    public $company;

    public $fromDate;
    public $toDate;

//array('t.id',
//"mind_users.email",
//"mind_users.full_name",
//"mind_ssn_company.name",
//"mind_ssn_company.id as company_id"

    public $email;
    public $full_name;
    public $company_name;
    public $company_id;
    public $address;
    public $user_id;


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{_ssn_partner}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('from_user_id, status, approve_by', 'numerical', 'integerOnly' => true),
            array('created_date', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_email, from_user_id, to_user_id, created_date, status, approve_by', 'safe', 'on' => 'search'),
            array('user_email', 'required'),
            array('user_email', 'checkUserExist', 'on' => 'create'),
        );
    }

    public function checkUserExist()
    {
        $email = $this->user_email;
        $fuid = $this->from_user_id;
        $tuid = $this->to_user_id;
        $getEmail = Users::checkUserByEmail($email);
        $partner = self::model()->count(array('condition' => "from_user_id = $fuid AND to_user_id = $tuid"));
        if ($partner > 0) {
            $this->addError('user_email', 'Your request had been sent before. Please don\'t resend it.');
        }
        if (strcmp($email, Yii::app()->user->email) == 0) {
            $this->addError('user_email', 'You can not add yourself');
        }
        if ($getEmail <= 0) {
            $this->addError('user_email', 'This email not exist, please try again with another email');
        }
    }

    public function checkEmailExist()
    {

    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'from_user_fk' => array(self::BELONGS_TO, 'Users', 'from_user_id'),
            'to_user_fk' => array(self::BELONGS_TO, 'Users', 'to_user_id'),

            'approve_user_fk' => array(self::BELONGS_TO, 'Users', 'approve_by'),
            'from_user' => array(self::BELONGS_TO, 'Users', 'from_user_id'),

            'to_user' => array(self::BELONGS_TO, 'Users', 'to_user_id'),
            'approve_user' => array(self::BELONGS_TO, 'Users', 'approve_by'),
////
//            'from_company'=>array(
//                self::HAS_ONE,'SsnCompany',array('id'=>'user_id'),
//                'through'=>'from_user_fk'
//            ),
//            'to_company'=>array(
//                self::HAS_ONE,'SsnCompany',array('id'=>'user_id'),
//                'through'=>'to_user_fk'
//            ),

        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('translation', 'ID'),
            'from_user_id' => Yii::t('translation', 'Company Name'),
            'to_user_id' => Yii::t('translation', 'To Company'),
            'created_date' => Yii::t('translation', 'Send Date'),
            'status' => Yii::t('translation', 'Status'),
            'statusText' => Yii::t('translation', 'Status'),
            'approve_by' => Yii::t('translation', 'Approve By'),
            'user_email'=>Yii::t('translation', 'Email'),
            'name'=>'Company Name',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGruserLinkidView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        //print_r ($_GET['SsnPartner']);


        $criteria = new CDbCriteria;

        if (Yii::app()->user->role_id != ROLE_ADMIN && Yii::app()->user->role_id != ROLE_MANAGER) {
            if ($this->status == self::STATUS_APPROVE) {
                $uid = Yii::app()->user->id;
                $criteria->addCondition("t.from_user_id  = '{$uid}' OR t.to_user_id = '{$uid}'");
            }
            if (!empty($this->from_user_id))
                $criteria->compare('from_user_id', $this->from_user_id);
            if (!empty($this->to_user_id))
                $criteria->compare('to_user_id', $this->to_user_id);
        }


        $criteria->compare('t.status', $this->status);

        if (Yii::app()->user->role_id == ROLE_ADMIN || Yii::app()->user->role_id == ROLE_MANAGER)
        {
            //$criteria->compare('created_date', $this->created_date, true);

            if(isset($_GET['SsnPartner']['fromDate'])){
                $fromDate = $_GET['SsnPartner']['fromDate'];
                $fromDate = DateHelper::toDbDateFormat($fromDate);
                $criteria->addCondition("t.created_date >= '{$fromDate}'");
            }
            if(isset($_GET['SsnPartner']['toDate'])){
                $toDate = $_GET['SsnPartner']['toDate'];
                $toDate = DateHelper::toDbDateFormat($toDate);
                $criteria->addCondition("t.created_date <= '{$toDate}'");
            }

        }else{
            $criteria->compare('t.created_date', $this->created_date, true);
        }

        //user_email
        //$criteria->with = array( 'from_user_fk');
        //$criteria->compare( 'from_user_fk.full_name', $this->company, true );

        $criteria->with = array( 'to_user_fk');
        $criteria->compare( 'to_user_fk.email', $this->user_email, true );

        //$this->company
        if($_GET['SsnPartner']['company']) {
            $criteria->condition = "MATCH (to_user_fk.email,to_user_fk.full_name)
             AGAINST('{$_GET['SsnPartner']['company']}' IN BOOLEAN MODE)";


            //$criteria->params[':key'] = "{$_GET['SsnPartner']['company']}";
        }
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.created_date DESC',
                'attributes' => array(
                    'user_email' => array(
                        // 'asc' => 'sender.full_name',
                        // 'desc' => 'sender.full_name DESC',
                        'asc' => 'to_user_fk.email',
                        'desc' => 'to_user_fk.email DESC',
                    ),
                    '*',
                ),
            ),
            'pagination' => array(
                'pageSize' =>  Yii::app()->params['PageSize'],
            ),
        ));
    }

    public function searchAdmin()
    {


        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $conds = array();
        $conds[] = 'from_user_id = :user_id';
        $conds[] = 'to_user_id = :user_id';
        $criteria->addCondition(implode(' OR ', $conds));
        $criteria->params[':user_id'] = $this->from_user_id;

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'created_date DESC',
            ),
            'pagination' => array(
                'pageSize' =>  Yii::app()->params['PageSize'],
            ),
        ));
    }


    //Custom Search...
    public function searchSQLModel($user_id){

        $uid = Yii::app()->user->id;
        $criteria = new CDbCriteria;
        if($user_id!=null){
            $uid = $user_id;
        }


        $criteria->select = array('t.id',
            "mind_users.id as user_id",
            "mind_users.email",
            "mind_users.full_name",
            "t.from_user_id" ,
            "t.to_user_id" ,
            "mind_ssn_company.name as company_name",
            "mind_ssn_company.address as address",
            "mind_ssn_company.id as company_id" );

        $criteria->join ="  join mind_users
                            on(t.from_user_id ='{$uid}' and
                            mind_users.id = t.to_user_id ) or(
                            t.to_user_id = '{$uid}' and
                            mind_users.id = t.from_user_id)
                            left join mind_ssn_company on mind_users.id = mind_ssn_company.user_id";

        if(isset($_REQUEST['SsnPartner']['company'])) {
            $match = addcslashes($_REQUEST['SsnPartner']['company'], '%_');

            $criteria->condition = "mind_users.email like :searchtext or
            mind_users.full_name like :searchtext or
            mind_ssn_company.name like :searchtext";
            $criteria->params[':searchtext'] = "%{$match}%";

        }
        //var_dump($this->company);
        //$criteria->addCondition = ' mind_users.full_name like  ';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.created_date DESC',
                'attributes' => array(
                    'email'=>array(
                        'asc' => 'mind_users.email',
                        'desc' => 'mind_users.email DESC',
                    ),
                    'full_name' => array(
                        'asc' => 'mind_users.full_name',
                        'desc' => 'mind_users.full_name DESC',
                    ),
                    'company_name' => array(
                        'asc' => 'mind_ssn_company.name',
                        'desc' => 'mind_ssn_company.name DESC',
                    ),
                    '*',
                ),
            ),
            'pagination' => array(
                'pageSize' => Yii::app()->params['PageSize'],
            ),
        ));






    }



    public function activate()
    {
        $this->status = 1;
        $this->update();
    }

    public function deactivate()
    {
        $this->status = 0;
        $this->update();
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SsnPartner the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function nextOrderNumber()
    {
        return SsnPartner::model()->count() + 1;
    }

    public static function getTotalPartner($t)
    {
        $criteria = new CDbCriteria;
        $uid = Yii::app()->user->id;
        switch ($t) {
            case SsnPartner::STATUS_APPROVE:
                $criteria->addCondition("t.from_user_id  = '{$uid}' OR t.to_user_id = '{$uid}'");
                break;
            case SsnPartner::STATUS_SEND:
                $criteria->compare('from_user_id', $uid);
                break;
            case SsnPartner::STATUS_PENDING:
                $criteria->compare('to_user_id', $uid);
                $t = SsnPartner::STATUS_SEND;
                break;
        }

        $criteria->compare('status', $t);

        return self::model()->count($criteria);
    }

    public static function getListPartner()
    {
        $criteria = new CDbCriteria;

        $uid = Yii::app()->user->id;
        $criteria->addCondition("from_user_id  = '{$uid}' OR to_user_id = '{$uid}'");
        $criteria->compare('status', self::STATUS_APPROVE);

        $data = self::model()->findAll($criteria);
        $arr = array();
        if ($data) {
            foreach ($data as $k => $i) {
                if ($i->to_user_id == Yii::app()->user->id)
                    $arr[$i->from_user_id] = $i->from_user_fk ? $i->from_user_fk->full_name . ' - ' . $i->from_user_fk->email : '';
                if ($i->from_user_id == Yii::app()->user->id)
                    $arr[$i->to_user_id] = $i->to_user_fk ? $i->to_user_fk->full_name . ' - ' . $i->to_user_fk->email : '';
            }
        }
        return $arr;
    }

    public function getStatusText()
    {

        return Yii::app()->format->formatEnum($this->status, self::$STATUSES);
    }



	public function getFromCompany() {
		return $this->from_user ? $this->from_user->company->name : '';
	}
	public function getToCompany() {
		return $this->to_user ? $this->to_user->company->name : '';
	}
	
	static public function getPartnerListData($userId) {
		$p = SsnPartner::model()->tableName();
		$u = $userId;
		$condition = "user_id in (
			select to_user_id
			from $p
			where from_user_id=$u
		) or user_id in (
			select from_user_id
			from $p
			where to_user_id=$u
		)";
		$comps = SsnCompany::model()->findAll($condition);
		$data = array();
		foreach ($comps as $comp) {
			$data[$comp->user->id] = "{$comp->name} ({$comp->user->email})";
		}
		return $data;
	}


    //$STATUSES

    public function renderStatusText($data, $row)
    {
        return Yii::app()->format->formatEnum($data->status, self::$STATUSES);

    }

    public function renderApprovedUserName($data,$row){
        if($data->approve_user) {
            return $data->approve_user->full_name;
            //renderApprovedUserName
        }
        return '-';
    }

    public function renderLinkApprove($data,$row){
        if( $data->status!=2 ) {
            $url = Yii::app()->createAbsoluteUrl('admin/partner/Approve') . '/id/' . $data->id;
            $link = '<a class="ajaxupdate" title="Click here to Approve" href="' . $url . '">Approve</a>';
            return $link;
        }
        return '-';
    }

    static public function getPartnerListDataPage($userId,$page) {
        $limit = 10;

        $p = SsnPartner::model()->tableName();
        $u = $userId;
        $offest = ($page-1)*$limit;
        //$limit  =" Limit ". $limit. ",". $numberrow;

        $condition = "user_id in (
			select to_user_id
			from $p
			where from_user_id=$u
		) or user_id in (
			select from_user_id
			from $p
			where to_user_id=$u
		)";


        $criteria = new CDbCriteria(array(
            'condition' => $condition,
            'order' => 'id ASC',
            //'limit' => $limit,
            //'offset' => $offest // if offset less, thah 0 - it starts from the beginning
        ));
        //$result = $model->findAll($criteria);

        $comps = SsnCompany::model()->findAll($criteria);

        $data = array();
        foreach ($comps as $comp) {
            $data[$comp->user->id] = "{$comp->name} ({$comp->user->email})";
        }
        return $data;
    }

}

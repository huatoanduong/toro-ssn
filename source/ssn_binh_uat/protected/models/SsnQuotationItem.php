<?php

/**
 * This is the model class for table "{{_ssn_quotation_item}}".
 *
 * The followings are the available columns in table '{{_ssn_quotation_item}}':
 * @property integer $id
 * @property integer $quotation_id
 * @property integer $item_id
 * @property double $price
 * @property string $unit
 * @property string $remark
 * @property string $documents
 */
class SsnQuotationItem extends _BaseModel
{
    //public $item_name;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SsnQuotationItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{_ssn_quotation_item}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('quotation_id,item_id', 'required'),
			array('quotation_id, item_id', 'numerical', 'integerOnly'=>true),
			array('price', 'numerical'),
			array('unit', 'length', 'max'=>45),
			array('documents', 'length', 'max'=>255),
			array('remark,updated_date,expired_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, refer_item_id,quotation_id, item_id, price, unit, remark, documents', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'itemdetail'=>  array(self::BELONGS_TO, 'SsnCompanyItem', 'item_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'quotation_id' => 'Quotation',
			'item_id' => 'Item',
			'price' => 'Price',
			'unit' => 'Unit',
			'remark' => 'Remark',
			'documents' => 'Documents',
            'refer_item_id'=>'Item Id',
            'item_name'=>'Item Name',
            'updated_date'=>'Update',
            'expired_date'=>'Item Exp Date'
		);
	}


    public function getDateSentText() {
        return date('d/m/Y', strtotime($this->expired_date));
    }

    public function getRawDateSentText($data,$row) {
        return date('d/m/Y', strtotime($data->expired_date));
    }



    public function setDateSentText($value) {
        $this->expired_date = DateHelper::toDbDateFormat($value);
    }


    public function getPrice(){
        $n = $this->price;
        $n_decimals = 3;

        return ((floor($n) == round($n, $n_decimals)) ? number_format($n) : number_format($n, $n_decimals));
    }

    public function getRefer_Item_Id() {
        return  $this->item_id;
    }

    public function getRawRefer_Item_Id($data,$row) {
        return $this->item_id;
    }

    public function getItem_Name() {
        return  $this->itemdetail->name;
    }

    public function getRawItem_Name($data,$row) {
        return $this->itemdetail->name;
    }

    /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('quotation_id',$this->quotation_id);
		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('price',$this->price);
		$criteria->compare('unit',$this->unit,true);
		$criteria->compare('remark',$this->remark,true);
		$criteria->compare('documents',$this->documents,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


    public function getDocumentDirectory() {
        return sprintf('%s/upload/quotations-items/item-%s/', Yii::getPathOfAlias('webroot'), $this->id);
        //sprintf('%s/upload/sample-item/%s/', Yii::getPathOfAlias('webroot'), $this->id);
    }

    public function removeDocumentFile($file) {
        if (is_file($this->getDocumentDirectory().$file))
            unlink ($this->getDocumentDirectory().$file);
    }

   //getDocumentDownloadUrl
    public function getDocumentDownloadUrl(){

        $listDocuments = array();
        $listfiles = explode("|@|",$this->documents);
        foreach($listfiles as $item) {
            $file = $this->getDocumentDirectory() . $item;

            $url = sprintf('%s/upload/quotations-items/item-%s/%s', Yii::app()->baseUrl, $this->id, $item);
            if(is_file($file)) {
                $listDocuments[] = array("name"=>$item, "url"=>$url);
            }
        }
        return $listDocuments;
    }



}
<?php

/**
 * This is the model class for table "{{_contactus}}".
 *
 * The followings are the available columns in table '{{_contactus}}':
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $content
 * @property string $type
 * @property string $file_upload
 * @property integer $status
 * @property string $company
 * @property integer $option
 */
class ContactGeneralEnquiry extends _ContactUsBaseModel {

    public $grVerifyCode;
    public $subject = 'General Enquiry';

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{_contactus}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, email', 'required'),
            array('status, type ', 'numerical', 'integerOnly' => true),
            array('name, email, option,  phone, file_upload', 'length', 'max' => 255),
            array('email', 'email'),
            array('content', 'safe'),
            array(
                'phone',
                'match', 'not' => true, 'pattern' => '/[^0-9\+\-\)\( ]/',
                'message' => 'Phone must be numerical and allow input (),+,-',
            ),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, email, phone, content, type, file_upload, status, option', 'safe', 'on' => 'search'),
            array('grVerifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements()),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Yii::t('translation', 'ID'),
            'name' => Yii::t('translation', 'Name'),
            'email' => Yii::t('translation', 'Email'),
            'phone' => Yii::t('translation', 'Phone'),
            'content' => Yii::t('translation', 'Enquiry'),
            'type' => Yii::t('translation', 'Type'),
            'file_upload' => Yii::t('translation', 'File Upload'),
            'status' => Yii::t('translation', 'Status'),
            'option' => Yii::t('translation', 'Option'),
            'grVerifyCode' => Yii::t('translation', 'Enter code here :')
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('content', $this->content, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('option', $this->option);


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->params['PageSize'],
            ),
        ));
    }

    public function activate() {
        $this->status = 1;
        $this->update();
    }

    public function deactivate() {
        $this->status = 0;
        $this->update();
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ContactUs the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function nextOrderNumber() {
        return self::model()->count() + 1;
    }

    protected function beforeSave() {
        if (parent::beforeSave() && $this->isNewRecord) {
            $this->status = STATUS_ACTIVE;
            $this->type = CONTACT_US_GENERAL_ENQUIRY;
            $this->created_date = date('Y-m-d H:i:s', time());
        }
        return parent::beforeSave();
    }

}

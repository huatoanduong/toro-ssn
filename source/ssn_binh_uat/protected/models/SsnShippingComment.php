<?php

/**
 * This is the model class for table "{{_ssn_shipping_comment}}".
 *
 * The followings are the available columns in table '{{_ssn_shipping_comment}}':
 * @property integer $id
 * @property integer $shipping_id
 * @property integer $user_id
 * @property string $comment
 * @property integer $order_by
 * @property string $created_date
 */
class SsnShippingComment extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SsnShippingComment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{_ssn_shipping_comment}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('shipping_id, user_id, order_by', 'numerical', 'integerOnly'=>true),
			array('comment, created_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, shipping_id, user_id, comment, order_by, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'user'=>array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'shipping_id' => 'Shipping',
			'user_id' => 'User',
			'comment' => 'Comment',
			'order_by' => 'Order By',
			'created_date' => 'Created Date',
		);
	}

	public function behaviors() {
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'created_date',
			)
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('shipping_id',$this->shipping_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('order_by',$this->order_by);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
    public function findByShipping($id) {
        $criteria = new CDbCriteria;
        $criteria->compare('shipping_id', $id);
        $criteria->order = 't.created_date';
        return self::model()->findAll($criteria);
    }
}
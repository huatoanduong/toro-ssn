<?php

/**
 * This is the model class for table "{{_ssn_quotation}}".
 *
 * The followings are the available columns in table '{{_ssn_quotation}}':
 * @property integer $id
 * @property integer $sender_id
 * @property integer $reciver_id
 * @property string $created_date
 * @property string $date_expired
 * @property string $remark
 * @property string $documents
 */
class SsnQuotation extends _BaseModel
{
    public $form_items = array();
    public $reciver_ids;
    public $recivers;
    public $senderName;
    public $reciverName;

    public $itemName;
    public $itemPrice;
    public $itemUnit;
    public $itemExp;
    public $typemode;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SsnQuotation the static model class
	 */

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{_ssn_quotation}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sender_id, dateSentText , reciver_id', 'required','message' => 'You can not leave this field empty.'),
			array('sender_id', 'numerical', 'integerOnly'=>true),
			array('documents', 'length', 'max'=>255),
			array('created_date,reciver_id, date_expired, remark', 'safe'),
			array('id, sender_id, reciver_id, created_date, date_expired, remark, documents', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'items' => array(self::HAS_MANY, 'SsnQuotationItem', 'quotation_id'),
            'sender'=> array(self::BELONGS_TO, 'SsnCompany', 'sender_id'),
            'reciver'=> array(self::BELONGS_TO, 'SsnCompany', 'reciver_id'),

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sender_id' => 'Sender',
			'reciver_id' => 'Reciver',
			'created_date' => 'Created Date',
			'date_expired' => 'Date Expired',
			'remark' => 'Remark',
			'documents' => 'Documents',
            'dateSentText' => 'Exp Date',
		);
	}


//
//    protected function beforeSave() {
//        $this->date_expired = DateHelper::toDbDateFormat($this->date_expired);
//        return parent::beforeSave();
//    }

    public function getDateSentText() {
        return date('d/m/Y', strtotime($this->date_expired));
    }

    public function getRawDateSentText($data,$row) {
        return date('d/m/Y', strtotime($data->date_expired));
    }



    public function setDateSentText($value) {
        $this->date_expired = DateHelper::toDbDateFormat($value);
    }

    public function getDocumentDirectory() {
        return sprintf('%s/upload/quotations/quot-%s/', Yii::getPathOfAlias('webroot'), $this->id);
        //sprintf('%s/upload/sample-item/%s/', Yii::getPathOfAlias('webroot'), $this->id);
    }

    public function removeDocumentFile($file) {
        if (is_file($this->getDocumentDirectory().$file))
            unlink ($this->getDocumentDirectory().$file);
    }


    public function getDocumentDownloadUrl(){

        $listDocuments = array();
        $listfiles = explode("|@|",$this->documents);
        foreach($listfiles as $item) {
            $file = $this->getDocumentDirectory() . $item;

            $url = sprintf('%s/upload/quotations/quot-%s/%s', Yii::app()->baseUrl, $this->id, $item);
            if(is_file($file)) {
                $listDocuments[] = array("name"=>$item, "url"=>$url);
            }
        }
        return $listDocuments;
    }



	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

        $mycompany = SsnCompany::model()->findByAttributes(array('user_id' => Yii::app()->user->id ));


//        if($this->typemode == 1){ //reciver
//
//            $criteria->compare('reciver_id', $mycompany->id);
//        }
//        if($this->typemode == 2){ //sent
//
//            $criteria->compare('sender_id', $mycompany->id);
//        }
        //if($this->typemode == 3){ //all
            //$criteria->compare('sender_id', $mycompany->id,true,"OR");
            //$criteria->compare('reciver_id', $mycompany->id,true,"OR");
        //}

        $criteria->with = array( 'sender','reciver');
        $criteria->together = true;
        //$criteria->compare('items.item_name', $_REQUEST['SsnQuotation']['senderName'], true);

        $criteria->join ="LEFT OUTER JOIN  mind_ssn_quotation_item `items` ON (`items`.`quotation_id`=`t`.`id`)
         LEFT OUTER JOIN  mind_ssn_company_item on(items.item_id = mind_ssn_company_item.id )";

        $and = "";

        if(isset($_REQUEST['SsnQuotation']['senderName']) && $_REQUEST['SsnQuotation']['senderName']!="") {

            $criteria->condition = "( reciver.name like :key OR sender.name like :key ) OR mind_ssn_company_item.name like :key ";
            $criteria->params[':key'] = "%{$_REQUEST['SsnQuotation']['senderName']}%";
            $and = " AND ";
        }

        if ($this->typemode == 1) {
            $criteria->condition .= $and. " reciver.id = :id";
        }
        if ($this->typemode == 2) {
            $criteria->condition .= $and. " sender.id =:id";
        }
        if ($this->typemode == 3) {
            $criteria->condition .= $and. " (reciver.id = :id or sender.id =:id)";
        }

        $criteria->params[':id'] = $mycompany->id;



        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.created_date DESC',
                'attributes' => array(
                    'senderName'=>array(
                        'asc' => 'sender.name',
                        'desc' => 'sender.name DESC',
                    ),
                    'reciverName'=>array(
                        'asc' => 'reciver.name',
                        'desc' => 'reciver.name DESC',
                    ),

                    '*',
                ),
            ),
            'pagination' => array(
                'pageSize' => Yii::app()->params['PageSize'],
            ),
        ));
	}
}
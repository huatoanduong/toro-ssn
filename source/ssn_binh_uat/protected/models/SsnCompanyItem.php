<?php

/**
 * This is the model class for table "{{_ssn_company_item}}".
 *
 * The followings are the available columns in table '{{_ssn_company_item}}':
 * @property integer $id
 * @property integer $company_id
 * @property string $code
 * @property string $name
 * @property string $color
 * @property integer $buyer_id
 * @property string $created_date
 * @property string $remark
 * @property string $documents
 */
class SsnCompanyItem extends _BaseModel
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SsnCompanyItem the static model class
	 */

    public $buyer_name;
    public $textsearch;


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{_ssn_company_item}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('company_id, code,color, name,company_id, buyer_id', 'required'),
			array('company_id, buyer_id', 'numerical', 'integerOnly'=>true),
			array('code, color', 'length', 'max'=>45),
			array('name, documents', 'length', 'max'=>255),
			array('created_date,expired_date,buyer_name, remark', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id,buyer_name, company_id, code, name, color, buyer_id, created_date, remark, documents', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'buyer'=> array(self::BELONGS_TO, 'SsnCompany', 'buyer_id'),
            'mycompany'=> array(self::BELONGS_TO, 'SsnCompany', 'company_id'),
		);
	}

    public function getDocumentDirectory() {
        return sprintf('%s/upload/company-items/com-%s/', Yii::getPathOfAlias('webroot'), $this->company_id);
         //sprintf('%s/upload/sample-item/%s/', Yii::getPathOfAlias('webroot'), $this->id);
    }

    public function removeDocumentFile($file) {
        if (is_file($this->getDocumentDirectory().$file))
            unlink ($this->getDocumentDirectory().$file);
    }



    public $listDocuments = array();

    public function getDocumentDownloadUrl(){

        $this->listDocuments = array();
        $listfiles = explode("|@|",$this->documents);
        foreach($listfiles as $item) {
            $file = $this->getDocumentDirectory() . $item;

            $url = sprintf('%s/upload/company-items/com-%s/%s', Yii::app()->baseUrl, $this->company_id, $item);
            if(is_file($file)) {
                $this->listDocuments[] = array("name"=>$item, "url"=>$url);
            }
        }
        return $this->listDocuments;
    }
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'company_id' => 'Company',
			'code' => 'Material Code',
			'name' => 'Name',
			'color' => 'Color',
			'buyer_id' => 'Buyer',
			'created_date' => 'Created Date',
            'expired_date'=>'Exp Date',
			'remark' => 'Remark',
			'documents' => 'Documents',
            'buyer' =>'Buyer Name',
            'buyer_name'=>'Buyer Name',
            'mycompany' =>'Supplier Name',
		);
	}

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
public function searchAdmin()   {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

//        $criteria->compare('id',$this->id);
//        $criteria->compare('company_id',$this->company_id);
//        $criteria->compare('code',$this->code,true);
//        $criteria->compare('name',$this->name,true);
//        $criteria->compare('color',$this->color,true);
//        $criteria->compare('buyer_id',$this->buyer_id);
//        $criteria->compare('created_date',$this->created_date,true);
//        $criteria->compare('remark',$this->remark,true);
//        $criteria->compare('documents',$this->documents,true);

        //$criteria->with('buyer');
        $criteria->with = array( 'mycompany');

        //SsnCompanyItem[textsearch]:item SsnCompanyItem[textsearch]:

        //if($_GET['SsnCompanyItem']['textsearch']) {
            //$text = $_GET['SsnCompanyItem']['textsearch'];

            // $criteria->condition = "MATCH (buyer.name,t.name,t.color,t.code)
            //  AGAINST(:key IN BOOLEAN MODE)";
            $criteria->condition = "mycompany.name like :buyer_name and
            t.name like :name and
            t.code like :code and
            t.color like :color";
            //$criteria->params[':searchtext'] = "%{$text}%";
            $criteria->params[':name'] = "%{$this->name}%";
            $criteria->params[':code'] = "%{$this->code}%";
            $criteria->params[':color'] = "%{$this->color}%";
            $criteria->params[':buyer_name'] = "%{$this->buyer_name}%";

            //$criteria->params[':key'] = "{$text}";
        //}
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.created_date DESC',
                'attributes' => array(
                    'buyer_name'=>array(
                        'asc' => 'mycompany.name',
                        'desc' => 'mycompany.name DESC',
                    ),
                    '*',
                ),
            ),
            'pagination' => array(
                'pageSize' => Yii::app()->params['PageSize'],
            ),
        ));


    }

    public function getDateSentText() {
        return date('d/m/Y', strtotime($this->expired_date));
    }

    public function getRawDateSentText($data,$row) {
        return date('d/m/Y', strtotime($data->expired_date));
    }



    public function setDateSentText($value) {
        $this->expired_date = DateHelper::toDbDateFormat($value);
    }


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

//		$criteria->compare('id',$this->id);
//		$criteria->compare('company_id',$this->company_id);
//		$criteria->compare('code',$this->code,true);
//		$criteria->compare('name',$this->name,true);
//		$criteria->compare('color',$this->color,true);
//		$criteria->compare('buyer_id',$this->buyer_id);
//		$criteria->compare('created_date',$this->created_date,true);
//		$criteria->compare('remark',$this->remark,true);
//		$criteria->compare('documents',$this->documents,true);

        //$criteria->with('buyer');
        $criteria->with = array( 'buyer');

        $criteria->condition = "buyer.name like :buyer_name and
            t.name like :name and
            t.code like :code and
            t.color like :color";
        //$criteria->params[':searchtext'] = "%{$text}%";
        $criteria->params[':name'] = "%{$this->name}%";
        $criteria->params[':code'] = "%{$this->code}%";
        $criteria->params[':color'] = "%{$this->color}%";
        $criteria->params[':buyer_name'] = "%{$this->buyer_name}%";


//        $criteria->condition = "buyer.name like :buyer_name";
//        $criteria->params[':buyer_name'] = "%{$this->buyer_name}%";

        //SsnCompanyItem[textsearch]:item SsnCompanyItem[textsearch]:

//        if($_GET['SsnCompanyItem']['textsearch']) {
//            $text = $_GET['SsnCompanyItem']['textsearch'];
//
//           // $criteria->condition = "MATCH (buyer.name,t.name,t.color,t.code)
//           //  AGAINST(:key IN BOOLEAN MODE)";
//
//            $criteria->condition = "buyer.name like :searchtext or
//            t.name like :searchtext or
//            t.code like :searchtext or
//            t.remark like :searchtext or
//            t.color like :searchtext";
//            $criteria->params[':searchtext'] = "%{$text}%";
//
//            //$criteria->params[':key'] = "{$text}";
//        }
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 't.created_date DESC',
                'attributes' => array(
                    'buyer_name'=>array(
                        'asc' => 'buyer.name',
                        'desc' => 'buyer.name DESC',
                    ),
                    '*',
                ),
            ),
            'pagination' => array(
                'pageSize' => Yii::app()->params['PageSize'],
            ),
        ));


	}
}
<?php
/* @var $model SsnSampleItem */
/* @var $index int */

$form= new CActiveForm;
$form->enableClientValidation = true;
?>
<style >
    .button_updoc{
        height: 30px;
    }
</style>
<tr class="item">
	<td colspan="6">
	<div style="width: 100%;padding-bottom:5px;">
		<h4 class="hide">Item <?= str_replace('n', '', $index) + 1 ?></h4>

        <?= $form->textField($model, 'item_name', array(
			'name'=>"SsnQuotationItem[item_name]",
			'class'=>'form-control input-sm',
			'placeHolder'=>'Name'
		)) ?>

        <?php if($model->id !=null){
            echo $form->hiddenField($model, 'id', array(
            'name'=>"SsnQuotationItem[id]",
            'class'=>'form-control input-sm',
            'placeHolder'=>'ID'
        )); } ?>

		<?= $form->error($model, 'item_name', array('id'=>"SsnQuotationItem_item_name_em_")) ?>
	</div>

        <div   class="row" style="margin-right:0px;">
		<div  class="col-md-3" style="float:left;padding-right:2px;">

            <input name="SsnQuotationItem[price]"
                   class="form-control input-sm" value="<?= $model->price; ?>"
                   placeholder="Price" htmloption="" id="SsnQuotationItem_price" type="number">

			<?= $form->error($model, 'price', array('id'=>"SsnQuotationItem_price_em_")) ?>
		</div>

		<div  class="col-md-3" style="float:left;padding-right:2px;">
			<?= $form->dropDownList($model, 'unit', SsnSampleItem::getUnitList(), array(
				'name'=>"SsnQuotationItem[unit]",
				'class'=>'form-control input-sm',
				'empty'=>'N/A',
			)) ?>
			<?= $form->error($model, 'unit', array('id'=>"SsnQuotationItem_unit_em_")) ?>
		</div>

        <div  class="col-md-3" style="float:left;padding-right:2px;">
            <div class="col-md-4" style="margin-top: 7px;padding-right: 0px;text-align: right;">
                <?php echo $form->labelEx($model,'dateSentText', array('class'=>'control-label','label'=>'Exp date')); ?>
            </div>
            <div class="col-md-8">
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                    'model' => $model,
                    //'value'=>$model->dateSentText,
                    'attribute' =>'dateSentText',
                    //'name'=>"SsnQuotationItem[dateSentText]",
                    'options' => array(
                        'dateFormat' => 'dd/mm/yy',
                    ),
                    'htmlOptions'=>array(
                        'class'=>'form-control'
                    ),
                )); ?>
                <?php echo $form->error($model,'dateSentText'); ?>
            </div>
        </div>

		<div class="col-md-3" style="float:left;padding-right:2px;">
			<?= $form->fileField($model, 'documents', array(
				'name'=>"SsnQuotationItem[documents]",
			)) ?>


			<?= $form->error($model, 'documents', array('id'=>"SsnQuotationItem_documents_em_")) ?>

            <div class=" notranslate item-document-url">
                <?php
                $urls = $model->getDocumentDownloadUrl();
                if($urls):
                    foreach($urls as $url){
                        ?>
                        <div class="row-container document-link">
                            <a class="document-link-url" href="<?= $url["url"] ?>"><?=$url["name"]?></a>
                        </div>

                    <?php }
                endif ?>
            </div>


		</div>

	</div>
	</td>
</tr>

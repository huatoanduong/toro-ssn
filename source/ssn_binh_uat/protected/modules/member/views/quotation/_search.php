<?php
/* @var $this QuotationController */
/* @var $model SsnQuotation */
/* @var $form CActiveForm */
?>


<?php
$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
    'htmlOptions' => array('class' => 'form-horizontal', 'role' => 'form', 'id' => 'search-form'),
));
?>
<div class="row">
    <div class="col-md-12" style="margin-left: 5px;">
        <div class="form-group">
            <div class="col-sm-10">
                <?php echo $form->textField($model,'senderName',array('class'=>'form-control','placeholder'=>'search')); ?>
                <input type="hidden" name="t" value="<?php echo $type ?>">
                <?php echo $form->error($model, 'senderName'); ?>
            </div>

            <div class="col-md-2">
                <button type="submit" class="btn btn-default ">Search</button>
            </div>
        </div>

    </div>

</div>
<?php $this->endWidget(); ?>


<?php
/* @var $model SsnSampleItem */
/* @var $index int */

$form= new CActiveForm;
$form->enableClientValidation = true;
?>
<style >
    .button_updoc{
        height: 30px;
    }
</style>
<tr class="item">
	<td colspan="6">
	<div style="width: 100%;padding-bottom:5px;">
		<h4 class="hide">Item <?= str_replace('n', '', $index) + 1 ?></h4>
        <?= $form->hiddenField($model, 'refer_item_id', array(
            'name'=>"SsnQuotationItem[$index][refer_item_id]",
            'class'=>'form-control input-sm',
            'placeHolder'=>'Name'
        )) ?>

        <?= $form->textField($model, 'item_name', array(
			'name'=>"SsnQuotationItem[$index][item_name]",
			'class'=>'form-control input-sm',
			'placeHolder'=>'Name'
		)) ?>

        <?php if($model->id !=null){
            echo $form->hiddenField($model, 'id', array(
            'name'=>"SsnQuotationItem[$index][id]",
            'class'=>'form-control input-sm',
            'placeHolder'=>'ID'
        )); } ?>


		<?= $form->error($model, 'item_name', array('id'=>"SsnQuotationItem_{$index}_item_name_em_")) ?>
	</div>

	<div class="row" style="margin-right:0px;">
		<div  class="col-md-3"  style="float:left;padding-right:2px;">

            <input name="SsnQuotationItem[<?= $index ?>][price]"
                   class="form-control input-sm" value="<?= $model->price; ?>"
                   placeholder="Price" htmloption="" id="SsnQuotationItem_<?= $index ?>_price" type="number" step="0.001">

			<?= $form->error($model, 'price', array('id'=>"SsnQuotationItem_{$index}_price_em_")) ?>
		</div>

		<div   class="col-md-3"  style="float:left;padding-right:2px;">
			<?= $form->dropDownList($model, 'unit', SsnSampleItem::getUnitList(), array(
				'name'=>"SsnQuotationItem[$index][unit]",
				'class'=>'form-control input-sm',
				'empty'=>'N/A',
			)) ?>
			<?= $form->error($model, 'unit', array('id'=>"SsnQuotationItem_{$index}_unit_em_")) ?>
		</div>


        <div  class="col-md-3" style="float:left;padding-right:2px;">
            <div class="col-md-4" style="margin-top: 7px;padding-right: 0px;text-align: right;">
                <label class="control-label"
                       for="SsnQuotationItem_dateSentText">Exp date</label>
            </div>
            <div class="col-md-8">
                <input class="form-control datepickerform" id="SsnQuotationItem_<?php echo $index; ?>_dateSentText"
                       type="text" value="<?php echo $model->dateSentText; ?>"
                       name="SsnQuotationItem[<?php echo $index; ?>][dateSentText]" />
                <div class="errorMessage"
                     id="SsnQuotationItem_dateSentText_em_"
                     style="display:none"></div>
            </div>
        </div>




		<div class="col-md-3" style="float:left;padding-right:2px;">
			<?= $form->fileField($model, 'documents', array(
				'name'=>"SsnQuotationItem[$index][documents]",
			)) ?>


			<?= $form->error($model, 'documents', array('id'=>"SsnQuotationItem_{$index}_documents_em_")) ?>

            <div class=" notranslate item-document-url">
                <?php
                $urls = $model->getDocumentDownloadUrl();
                if($urls):
                    foreach($urls as $url){
                        ?>
                        <div class="row-container document-link">
                            <a class="document-link-url" href="<?= $url["url"] ?>"><?=$url["name"]?></a>
                        </div>

                    <?php }
                endif ?>
            </div>


		</div>

	</div>
	</td>
</tr>

<?php
/* @var $this SampleController */
/* @var $model SsnSample */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerScriptFile(
	Yii::app()->theme->baseUrl.'/select2/select2.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(
	Yii::app()->theme->baseUrl.'/select2/select2.css');

$addItemUrl = $this->createUrl('addItem');
$autoCompleteUrl = $this->createUrl('partners');
Yii::app()->clientScript->registerScript(time(), "
	ssn.setupSampleDetailPage('{$addItemUrl}',{$this->actionPartnerTemplate()});
", CClientScript::POS_LOAD);

?>

<?php $this->widget('application.components.widget.Notification'); ?>				

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ssn-sample-form',
	'enableAjaxValidation'=>false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'validateOnChange'=>false,
	),
	'htmlOptions' => array(
		'role' => 'form', 
		'enctype' => 'multipart/form-data'
	),
)); ?>

<style>
    .item-form-hidden{
        display: none;
    }
</style>
	<div class="box-body">
		<div class="form-group">
			<?php echo $form->labelEx($model,'dateSentText', array('class'=>'control-label')); ?>
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'model' => $model,
				'attribute' => 'dateSentText',
				'options' => array(
					'dateFormat' => 'dd/mm/yy',
				),
				'htmlOptions'=>array(
					'class'=>'form-control'
				),
			)); ?>
			<?php echo $form->error($model,'dateSentText'); ?>
		</div>
		
		<?php if ($model->isRecipient()): ?>
		<!--send from-->
		<div class="form-group">
			<?php echo $form->labelEx($model,'from_user_id', array('class'=>'control-label')); ?>
			<p class="form-control-static">
				<a href="<?= $this->createUrl('partner/view', array('id'=> $model->from_user_id)) ?>"
				    class="notranslate"><?= $model->sender->company->name ?></a>
			</p>
		</div>
		<?php endif ?>


        <!-- End SendFrom New -->
		<?php if ($model->isSender()): ?>
		<!--send to-->
		<div class="form-group">
			<?php echo $form->labelEx($model,'toUserIds', array('class'=>'control-label')); ?>
            <div id="error_nofrom" class="errorMessage" style=" display: none"></div>

            <div class="">
				<?php echo $form->dropDownList(
                    $model,
                    'toUserIds',
					SsnPartner::getPartnerListDataPage(Yii::app()->user->id,1),
                    //SsnPartner::getPartnerListData(Yii::app()->user->id),
					array(
						'multiple'=>true,
						'class'=>'company-select form-control hide',
						'style'=>'width: 100%'
					)
				); ?>
			</div>
			<?php echo $form->error($model,'toUserIds'); ?>
		</div>
		<?php endif ?>
		
		<div class="form-group">
			<?php echo $form->labelEx($model,'attention', array('class'=>'control-label')); ?>
			<?php echo $form->textField($model,'attention',array('class'=>'form-control','maxlength'=>255)); ?>
			<?php echo $form->error($model,'attention'); ?>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<?php echo $form->labelEx($model,'awbn', array('class'=>'control-label')); ?>
					<?php echo $form->textField($model,'awbn',array('class'=>'form-control','maxlength'=>255)); ?>
					<?php echo $form->error($model,'awbn'); ?>
					<?php if ($model->getTrackingLink()): ?>
					<p><a href="<?= $model->getTrackingLink() ?>" target="_blank">Go to tracking page</a></p>
					<?php endif ?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<?php echo $form->labelEx($model,'courier_id', array('class'=>'control-label')); ?>
					<?php echo $form->dropDownList($model, 'courier_id', SsnCourier::getListData(),
						array('class' => 'form-control', 'empty'=>'Other')); ?>
					<?php echo $form->error($model,'courier_id'); ?>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label for="" class="control-label">Item</label>
			<div >
				<table class="item-list table table-bordered table-hover">
                    <tbody>
                    <?php
                            foreach ($model->form_items as $index => $item):
                                if( empty($item->id) ){
                                    break;
                                }
                                ?>
                                <tr class="item row-item-id" id="tr-row-item-id-<?= $item->id ?>" data-item-id="<?= $item->id ?>">
                                    <td>
                                        <div class="row-container">
                                            <p class="item-name"><?= $item->name ?></p>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4 notranslate  item-color">
                                                <?= $item->color ?>
                                            </div>
                                            <div class="col-xs-4 notranslate item-quantity">
                                                <?= $item->quantity ?>
                                            </div>
                                            <div class="col-xs-4 notranslate item-unit">
                                                <?= $item->unit ?>
                                            </div>

                                        </div>
                                    </td>
                                    <td style="vertical-align:middle; text-align: center">

                                        <div class="row">
                                        <a href="javascript:void(0);"  class="btn-edit">
                                            <span class="glyphicon glyphicon-pencil"></span>
                                        </a>
                                        <a href="javascript:void(0);" class="">
                                            <span style="color: red; margin-left: 10px;"
                                                  class="glyphicon glyphicon-remove btn-remove"></span>
                                        </a>
                                        </div>
                                        <div class="row-container">

                                            <div class=" notranslate item-document-url">
                                                <?php if ($url = $item->getDocumentDownloadUrl()): ?>
                                                    <a href="<?= $url ?>"><?= $item->document ?></a>
                                                <?php else: ?>
                                                    No document uploaded.
                                                <?php endif ?>
                                            </div>

                                        </div>

                                    </td>

                                </tr>
                            <?php endforeach ?>

                    </tbody>
				</table>


			</div>

            <label for="" class="control-label">Add Item</label>
            <div id="error_noitems" class="errorMessage" style=" display: none">Sample must have at least one item.</div>

            <div class="table-responsive" id="list-form-items">

                <?php
                //$model->form_items
                //$item = new SsnSampleItem();
                //$item->sample_id = $model->id;
                //$index = 0;
                foreach($model->form_items as $index => $item):
                    if( empty($item->id) ){
                        break;
                    }
                    ?>
                    <div class="row-container  div-row-add-new item-form-hidden" id="item-form-hidden-id-<?php echo $item->id ?>">
                        <?php $this->renderPartial('_sample-item', array(
                        'index' => $index,
                        'model' => $item,
                         )) ?>
                    </div>
                <?php endforeach ?>

                <?php

                $item = new SsnSampleItem();
                $item->sample_id = $model->id;
                $index = 0;//count($model->form_items);
                ?>
                <div class="row-container  div-row-add-new current_show_form" id="item-form-hidden-id-N_<?=$index ?>">
                    <?php $this->renderPartial('_sample-item', array(
                        'index' => 'N_'. $index, //start-new-item,
                        'model' => $item,
                    )) ?>
                </div>

            </div>
            <div class="table-responsive" style="margin-top: 10px;">
                <div class="row-container"><button type="button" id="AddItem" class=" btn btn-default btn-add">Add</button></div>
            </div>



        </div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'remark', array('class'=>'control-label')); ?>
			<?php echo $form->textArea($model,'remark',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'remark'); ?>
		</div>
		<div class="form-group">
			<?php echo $form->labelEx($model,'document', array('class'=>'control-label')); ?>
			<?php echo $form->fileField($model, 'document'); ?>
			<?php echo $form->error($model, 'document'); ?>
			<?php if ($url = $model->getDocumentDownloadUrl()): ?>
			<p><a href="<?= $url ?>" class="notranslate"><?= $model->document ?></a></p>
			<?php else: ?>
			<p class="form-control-static">No document uploaded.</p>
			<?php endif ?>
		</div>	
	</div>
	<!-- /.box-body -->

	<div class="box-footer">
		<button type="submit" class="btn btn-primary"><?php echo $model->isNewRecord ? 'Send' : 'Update' ?></button>
	</div>

<?php $this->endWidget(); ?>

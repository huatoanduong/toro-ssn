<table class="table table-hover sample-item">
    <tr>
        <th rowspan="2">Date Sent</th>
        <?php if($t == SsnSample::STATUS_RECEIVED):?>
            <th rowspan="2">Sender </th>
        <?php endif;?>
        <?php if($t == SsnSample::STATUS_SEND):?>
            <th rowspan="2">Receiver </th>
        <?php endif;?>
        <th rowspan="2">AWBN</th>
        <th colspan="3">Item</th>
        <th rowspan="2">Remark</th>
    </tr>
    <tr>
        <th>Name</th>
        <th>Color</th>
        <th>Quantity</th>
    </tr>
    <?php foreach($data as $k => $sample):?>
        <?php
            $item = SsnSampleItem::model()->findAll(array('condition' => "sample_id = {$sample->id}"));
            $itemName = '';
            $itemColor = '';
            $itemQuantity = '';
            if($item){
                foreach($item as $ik => $i){
                    $itemName .= $i->name.'</br>';
                    $itemColor .= $i->color.'</br>';
                    $itemQuantity .= $i->quantity.'</br>';
                }
            }
        ?>
        <?php
            $listPartner = SsnPartner::getListPartner();
        ?>
        <tr>
            <td>
                <a href="<?php echo Yii::app()->createAbsoluteUrl('member/sample/view', array('id' => $sample->id, 't'=>$t)) ?>"><?php echo date('d/m/Y', strtotime($sample->created_date))?></a>
            </td>
            <?php if($t == SsnSample::STATUS_RECEIVED):?>
                <td>
                    <?php if(key_exists($sample->from_user_fk ? $sample->from_user_fk->id:-1, $listPartner)):?>
                        <a href="<?php echo Yii::app()->createAbsoluteUrl('member/partner/view', array('id' => $sample->from_user_fk ? $sample->from_user_fk->id:"0"));?>">
                            <?php echo $sample->from_user_fk ? $sample->from_user_fk->full_name:"-"?>
                        </a>
                    <?php else:?>
                            <?php echo $sample->from_user_fk ? $sample->from_user_fk->full_name:"-"?>
                    <?php endif;?>
                </td>
            <?php endif;?>
            <?php if($t == SsnSample::STATUS_SEND):?>
                <td>
                    <?php
                        $sampleCompany = SsnSampleCompany::model()->findAll(array('condition' => "sample_id = '{$sample->id}'"));
                        if($sampleCompany){
                            foreach($sampleCompany as $sK => $sI){
                                if(key_exists($sI->to_user_id, $listPartner))
                                    echo '<a href="'.Yii::app()->createAbsoluteUrl('member/partner/view', array('id' => $sI->to_user_id)).'">'.($sI->company_fk ? $sI->company_fk->name:'').'</a></br>';
                                else
                                    echo ($sI->company_fk ? $sI->company_fk->name:'').'</br>';
                            }
                        }
                    ?>
                    
                </td>
            <?php endif;?>
            <td class="align-left"><?php echo $sample->awbn ?></td>
            <td class="align-left"><?php echo $itemName;?></td>
            <td class="align-left"><?php echo $itemColor;?></td>
            <td><?php echo $itemQuantity;?></td>
            <td class="align-left"><?php echo $sample->remark ?></td>
        </tr>
    <?php endforeach;?>
</table>
<style>
    .sample-item .align-left{
        text-align: left;
    }
</style>
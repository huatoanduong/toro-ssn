<?php
/* @var $model SsnSampleItem */
/* @var $index int */

$form= new CActiveForm;
$form->enableClientValidation = true;
?>
<style >
    .button_updoc{
        height: 30px;
    }
</style>
<tr class="item">
	<td colspan="6">
	<div style="width: 100%;padding-bottom:5px;">
		<h4 class="hide">Item <?= str_replace('n', '', $index) + 1 ?></h4>
		<?= $form->textField($model, 'name', array(
			'name'=>"SsnSampleItem[$index][name]",
			'class'=>'form-control input-sm',
			'placeHolder'=>'Name'
		)) ?>
		<?= $form->error($model, 'name', array('id'=>"SsnSampleItem_{$index}_name_em_")) ?>
	</div>
	<div style="width: 100%">
		<div style="width:20%;float:left;padding-right:2px;">
			<?= $form->textField($model, 'color', array(
				'name'=>"SsnSampleItem[$index][color]",
				'class'=>'form-control input-sm',
				'placeHolder'=>'Color'
			)) ?>
			<?= $form->error($model, 'color', array('id'=>"SsnSampleItem_{$index}_color_em_")) ?>
		</div>
		<div style="width:20%;float:left;padding-right:2px;">
			<?= $form->textField($model, 'quantity', array(
				'name'=>"SsnSampleItem[$index][quantity]",
				'class'=>'form-control input-sm qty',
				'placeHolder'=>'Quantity'
			)) ?>
			<?= $form->error($model, 'quantity', array('id'=>"SsnSampleItem_{$index}_quantity_em_")) ?>
		</div>
		<div style="width:20%;float:left;padding-right:2px;">
			<?= $form->dropDownList($model, 'unit', SsnSampleItem::getUnitList(), array(
				'name'=>"SsnSampleItem[$index][unit]",
				'class'=>'form-control input-sm',
				'empty'=>'N/A',
			)) ?>
			<?= $form->error($model, 'unit', array('id'=>"SsnSampleItem_{$index}_unit_em_")) ?>
		</div>
		<div style="width:20%;float:left;padding-right:2px;">
			<?= $form->fileField($model, 'document', array(
				'name'=>"SsnSampleItem[$index][document]",
			)) ?>
			<?= $form->error($model, 'document', array('id'=>"SsnSampleItem_{$index}_document_em_")) ?>
			<?php if ($url = $model->getDocumentDownloadUrl()): ?>
			<a href="<?= $url ?>"><?= $model->document ?></a> 
			<?php endif ?>
		</div>

	</div>
	</td>
</tr>

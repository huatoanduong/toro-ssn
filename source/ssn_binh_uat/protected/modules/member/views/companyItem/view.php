<?php
/* @var $this CompanyItemController */
/* @var $model SsnCompanyItem */

$this->breadcrumbs=array(
	'Item Detail'=>array('index'),
	$model->code,
);

?>


<style>

    .breakwordP{
        white-space: normal; word-wrap: break-word;
        word-break: break-all;
    }

</style>

<div class="box">
    <div class="box-body">
    <div class="form-group">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'ssn-company-item-form',
            'enableAjaxValidation'=>false,
            'enableClientValidation'=>true,
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
            ),
            'htmlOptions' => array(
                'role' => 'form',
                'enctype' => 'multipart/form-data'
            ),
        )); ?>

        <div class="form-group">
            <label class="control-label"> <?php echo CHtml::encode($model->getAttributeLabel('code')); ?></label>
            <p class="form-control-static"><?= $model->code ?></p>
        </div>


        <label class="control-label"> <?php echo CHtml::encode($model->getAttributeLabel('name')); ?></label>
        <p class="form-control-static breakwordP" style=""><?= $model->name ?></p>
        </div>


        <div class="form-group">
            <label class="control-label"> <?php echo CHtml::encode($model->getAttributeLabel('color')); ?></label>
            <p class="form-control-static"><?= $model->color ?></p>
        </div>

        <div class="form-group">
            <label class="control-label"> <?php echo CHtml::encode($model->getAttributeLabel('buyer')); ?></label>
            <p class="form-control-static"><?= $model->buyer->name ?></p>
        </div>

        <div class="form-group">
            <label class="control-label"> <?php echo CHtml::encode($model->getAttributeLabel('remark')); ?></label>
            <p class="form-control-static breakwordP"><?= $model->remark ?></p>
        </div>

        <div class="form-group">
            <label class="control-label"> <?php echo CHtml::encode($model->getAttributeLabel('documents')); ?></label>
            <p class="form-control-static">
                <?php if ($url = $model->getDocumentDownloadUrl()): ?>
                    <a href="<?= $url ?>"><?= $model->documents ?></a>
                <?php else: ?>
                    No document uploaded.
                <?php endif ?>
            </p>
        </div>

    <?php $this->endWidget(); ?>
    </div>
</div>
<!---->
<?php //$this->widget('zii.widgets.CDetailView', array(
//	'data'=>$model,
//	'attributes'=>array(
//		'id',
//		'company_id',
//		'code',
//		'name',
//		'color',
//		'buyer_id',
//		'created_date',
//		'remark',
//		'documents',
//	),
//)); ?>

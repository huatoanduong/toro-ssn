<?php
/* @var $this HelpController */
/* @var $model Help */
/* @var $form CActiveForm */


?>
<style>
.cbx-status{

}

</style>

<div class="box-body">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'help-form',
	'enableAjaxValidation'=>false,
)); ?>


	<div class="form-group">
		<?php echo $form->labelEx($model,'title', array('class'=>'control-label'));?>
		<?php echo $form->textField($model,'title',array('class'=>'form-control','maxlength'=>255)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="form-group">


        <?php echo $form->labelEx($model,'description', array('class'=>'control-label')); ?>
            <?php $this->widget('ext.ckeditor.CKEditorWidget',array(
               "model"=>$model,                 # Data-Model <------- THIS LINE HIGHLIGHTED
               "attribute"=>'description',          # Attribute in the Data-Model
               "defaultValue"=>$model->description,     # Optional
           ));
           ?>
        <?php echo $form->error($model,'description'); ?>

	</div>
<div class="row">
	<div class="form-group col-md-4">
		<?php echo $form->labelEx($model,'status', array('class'=>'control-label'));?>
		<?= $form->dropDownList($model, 'status', Help::getListStatus(), array(
				'name'=>"Help[$index][status]",
				'class'=>'form-control input-sm'
			)) ?>

		<?php echo $form->error($model,'status'); ?>
	</div>
</div>


	<div class="form-group">
	    <div class="box-footer" >
		      <button type="submit" class="btn btn-primary"><?php echo $model->isNewRecord ? 'Create' : 'Save' ?></button>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
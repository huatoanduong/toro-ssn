<?php
/* @var $this HelpController */
/* @var $data Help */
?>
<div class="box-body">

    <div class="form-group">
            <?php echo CHtml::encode($data->getAttributeLabel('id')); ?>
            <?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
    </div>

    <div class="form-group">
            <?php echo CHtml::encode($data->getAttributeLabel('title')); ?>
            <?php echo CHtml::encode($data->title); ?>
    </div>
    <div class="form-group">
            <?php echo CHtml::encode($data->getAttributeLabel('description')); ?>
            <?php echo CHtml::encode($data->description); ?>
    </div>
    <div class="form-group">
            <?php echo CHtml::encode($data->getAttributeLabel('status')); ?>
            <?php echo CHtml::encode($data->status); ?>
    </div>
</div>
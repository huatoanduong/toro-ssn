<?php
/* @var $model SsnShippingSearch */
$items = $model->search();
?>
<?php if ($items): ?>
<div class="table-responsive">
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th rowspan="2" class="text-center">Date Load</th>
				<th rowspan="2" class="text-center">Order Number</th>
				<th rowspan="2" class="text-center">
				<?= $model->mode == SsnShippingSearch::MODE_RECV ? 'Sender' : 'Receiver' ?>
				</th>
				<th colspan="3" class="text-center">Item</th>
				<th rowspan="2" class="text-center">Remark</th>
			</tr>
			<tr>
				<th class="text-center">Name</th>
				<th class="text-center">Color</th>
				<th class="text-center">Qty</th>
			</tr>
		</thead>
		
		<tbody>
		<?php foreach($items as $shipping): ?>
		<?php $rs = count($shipping->items)>0 ?count($shipping->items) : 1 ?>
		<tr>
			<td rowspan="<?= $rs ?>" class="text-center">
				<a href="<?= $this->createUrl('view', array('id'=> $shipping->id)) ?>">
					<?= $shipping->dateLoadText ?>
				</a>
			</td>
			<td rowspan="<?= $rs ?>" class="text-center notranslate"><?= $shipping->order_number ?></td>
			<td rowspan="<?= $rs ?>">
				<?php if ($model->mode == SsnShippingSearch::MODE_RECV): ?>
				<a href="<?= $this->createUrl('partner/view', array('id'=> $shipping->from_user_id)) ?>"
				   class="notranslate">
				<?= $shipping->senderName ?>
				</a>
				<?php else: ?>
				<a href="<?= $this->createUrl('partner/view', array('id'=> $shipping->to_user_id)) ?>"
				    class="notranslate">
				<?= $shipping->receiverName ?>
				</a>
				<?php endif ?>
			</td>
			
			<?php if (count($shipping->items)>0): ?>
			<?php $item = $shipping->items[0] ?>
				<td class="notranslate"><?= $item->name ?></td>
				<td class="notranslate"><?= $item->color ?></td>
				<td class="text-center"><?= $item->quantity ?></td>
			<?php else: ?>
				<td></td>
				<td></td>
				<td></td>
			<?php endif ?>
			
			<td rowspan="<?= $rs ?>">
				<span data-toggle="tooltip" data-placement="bottom"
					title="<?= $shipping->remark ?>" class="notranslate">
				<?= $shipping->remarkExcerpt ?></span>
			</td>
		</tr>
		
		<?php for($i=1; $i<count($shipping->items); $i++): ?>
		<?php $item = $shipping->items[$i] ?>
		<tr>
			<td class="notranslate"><?= $item->name ?></td>
			<td class="notranslate"><?= $item->color ?></td>
			<td class="text-center"><?= $item->quantity ?></td>
		</tr>
		<?php endfor ?>
		<?php endforeach ?>
		</tbody>
	</table>
</div>

<div class="clearfix">
<?php $this->widget('CLinkPager', array(
	'pages' => $model->pagination,
	'header' => '',
	'nextPageLabel' => 'Next',
	'prevPageLabel' => 'Previous',
	'firstPageLabel' => 'First',
	'lastPageLabel' => 'Last',
	'selectedPageCssClass'=>'active',
	'maxButtonCount' => 10,
	'htmlOptions'=>array(
		'class' => 'pagination pull-right',
	)
)) ?>
</div>

<?php else: ?>
<p>There are no items.</p>
<?php endif ?>

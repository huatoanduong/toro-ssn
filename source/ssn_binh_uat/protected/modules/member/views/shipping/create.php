<?php
/* @var $this ShippingController */
/* @var $model SsnShipping */
$this->pageTitle = 'New Shipping';
?>

<section class="content-header">
    <h1>New Shipping</h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo Yii::app()->homeUrl ?>"><i class="fa fa-dashboard"></i> Home page</a></li> 
        <li><a href="<?= $this->createUrl('index') ?>">Shipping Schedule</a></li>
        <li class="active">New Shipping</li>
    </ol>
</section>

<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Information</h3>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
	</div>
	<!-- /.box -->
</section>


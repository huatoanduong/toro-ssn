<?php
/* @var $this ShippingController */
$this->pageTitle = 'Received Shipping Schedules';
?>

<section class="content-header">
    <h1>Shipping Schedule</h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo Yii::app()->homeUrl ?>"><i class="fa fa-dashboard"></i> Home page</a></li> 
        <li class="active">Shipping Schedule</li>
    </ol>
</section>

<section class="content">
	
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li class="active"><a href="javascript:void(0)">Received</a></li>
			<li><a href="<?= $this->createUrl('sent') ?>">Sent</a></li>
			<li>
				<button type="button"
						onclick="window.location='<?php echo $this->createUrl('create') ?>'"
				   class="btn btn-block btn-primary btn-flat">Add New</button>
			</li>
		</ul>
		
		<?php $this->renderPartial('_search', array('model'=>$model)) ?>
		
		<div class="tab-content">
			<div class="tab-pane active">
				<?php $this->renderPartial('_shipping-grid', array('model'=>$model)) ?>
			</div><!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- nav-tabs-custom -->
</section>
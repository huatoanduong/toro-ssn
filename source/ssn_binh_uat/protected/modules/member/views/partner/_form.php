<?php $form=$this->beginWidget('CActiveForm', array(
        'id' => 'ssn-partner-form',
        'enableAjaxValidation'=>false,
        'htmlOptions' => array('class' => 'form-horizontal', 'role' => 'form', 'enctype' => 'multipart/form-data'),
)); ?>

<?php $this->widget('application.components.widget.Notification'); ?>

<div class="box-body">

    <div class="col-md-6">
        <div class='form-group'>
            <?php echo $form->labelEx($model,'user_email', array('label' => 'Please enter your contact’s email', 'class' => 'control-label')); ?>
			<?php echo $form->textField($model,'user_email', array('class' => 'form-control', 'maxlength' => 255)); ?>
			<?php echo $form->error($model,'user_email'); ?>
        </div>

        <div class="clr"></div>
        <div class='form-group'>
            <button class="btn btn-primary sbm" type="submit">Submit</button>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>
 
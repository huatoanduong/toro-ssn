<?php
/* @var $this QuotationItemController */
/* @var $model SsnQuotationItem */

$this->breadcrumbs=array(
	'Ssn Quotation Items'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SsnQuotationItem', 'url'=>array('index')),
	array('label'=>'Manage SsnQuotationItem', 'url'=>array('admin')),
);
?>

<h1>Create SsnQuotationItem</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this QuotationItemController */
/* @var $model SsnQuotationItem */

$this->breadcrumbs=array(
	'Ssn Quotation Items'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List SsnQuotationItem', 'url'=>array('index')),
	array('label'=>'Create SsnQuotationItem', 'url'=>array('create')),
	array('label'=>'Update SsnQuotationItem', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SsnQuotationItem', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SsnQuotationItem', 'url'=>array('admin')),
);
?>

<h1>View SsnQuotationItem #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'quotation_id',
		'item_id',
		'price',
		'unit',
		'remark',
		'documents',
	),
)); ?>

<?php
/* @var $this QuotationItemController */
/* @var $model SsnQuotationItem */

$this->breadcrumbs=array(
	'Ssn Quotation Items'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SsnQuotationItem', 'url'=>array('index')),
	array('label'=>'Create SsnQuotationItem', 'url'=>array('create')),
	array('label'=>'View SsnQuotationItem', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SsnQuotationItem', 'url'=>array('admin')),
);
?>

<h1>Update SsnQuotationItem <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this QuotationItemController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ssn Quotation Items',
);

$this->menu=array(
	array('label'=>'Create SsnQuotationItem', 'url'=>array('create')),
	array('label'=>'Manage SsnQuotationItem', 'url'=>array('admin')),
);
?>

<h1>Ssn Quotation Items</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

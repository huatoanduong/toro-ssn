<?php

class CompanyItemController extends MemberController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	#public $layout='//layouts/column2';

    public $pluralTitle = 'Company Item';
    public $singleTitle = 'Company Item';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','delete'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}




	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{

        $model = $this->loadModel($id);
        $this->pageTitle = 'Item ' . $model->code;

		$this->render('view',array(

			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new SsnCompanyItem;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

        $this->pageTitle = 'Add New Item';

        $mycompany = SsnCompany::model()->findByAttributes(array('user_id' => Yii::app()->user->id ));

        $length = 5;
        $randomString = substr(str_shuffle(md5(time())),0,$length);
        $model->code  =  strtoupper($mycompany->company_code.'-'.$randomString);

        $model->dateSentText = date('d/m/Y');

        $mess = "";

		if(isset($_POST['SsnCompanyItem']))
		{
            $path = sprintf('%s/upload/company-items/com-%s/', Yii::getPathOfAlias('webroot'), $mycompany->id);
            $oldumask = umask(0);
            mkdir($path, 755,TRUE); // or even 01777 so you get the sticky bit set
            umask($oldumask);

            $documents = CUploadedFile::getInstancesByName('documents');
            $filelist = array();
            if (isset($documents) && count($documents) > 0) {
                // go through each uploaded image
                foreach ($documents as $doc => $document) {
                    if ($document->saveAs($path .$document->name)) {
                        $filelist[] =$document->name;
                    }
                }
            }
            //buyer_id
            $buyer_list =$_POST['SsnCompanyItem']['buyer_id']; //$_POST['SsnPartner']['company_id'];


            foreach($buyer_list as $buyer_id){

                $company = SsnCompany::model()->findByAttributes(array('id' => $buyer_id));
                if($company) {
                    $model = new SsnCompanyItem();
                    $model->attributes=$_POST['SsnCompanyItem'];
                    $model->buyer_id = $company->id;
                    $model->company_id = $mycompany->id;
                    $model->setDateSentText($_POST['SsnCompanyItem']['dateSentText']);

                    //$model->code = $mycompany->company_code;
                    $model->documents = implode("|@|",$filelist);

                    if($model->save()){
                        ##UpdateItems...
                        //$model->code = $mycompany->company_code .'-'. $model->id;
                        $model->save();

                    }else{
                        foreach($model->getErrors() as $error){
                            $mess.= "<p>".$error[0]."</p>";
                        }
                        $eror = true;
                        break;
                    }
                }

            }

            if($mess){
                Yii::app()->user->setFlash('error', $mess );
            }else{

                $model=new SsnCompanyItem;
                Yii::app()->user->setFlash('success', 'Item has been created successfully.');
                $this->redirect(array('index'));
            }
		}

        $this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{

        if($id==null || !(is_numeric($id))){
            $this->redirect(array('index'));
            return;
        }
        $model=$this->loadModel($id);
        if ( $model!=null && $model->mycompany->user_id == Yii::app()->user->id  ){

        }else{
            Yii::app()->user->setFlash('error', "Cant update this item.");
            $this->redirect(array('index'));
            return;
        }


        $this->pageTitle = 'Item ' . $model->code;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SsnCompanyItem']))
		{
			$model->attributes=$_POST['SsnCompanyItem'];

            $model->setDateSentText($_POST['SsnCompanyItem']['dateSentText']);

            $model->buyer_id = $model->buyer_id[0];

            //$mycompany = $model->company_id;
            //SsnCompany::model()->findByAttributes(array('user_id' => Yii::app()->user->id ));

            $path = sprintf('%s/upload/company-items/com-%s/', Yii::getPathOfAlias('webroot'),  $model->company_id);


            $filelist = array();

            //remmove file./
            $old_listfile =explode("|@|",$model->documents);
            if(isset($_POST['remove-file'])) {
                foreach ($_POST['remove-file'] as $removef) {
                    //removeDocumentFile
                    for ($i = 0; $i < count($old_listfile); $i++) {
                        if ($old_listfile[$i] == $removef) {
                            unset($old_listfile[$i]);
                            $model->removeDocumentFile($old_listfile[$i]);
                        }
                    }

                }
            }

            for($i=0; $i < count($old_listfile);$i++){
                $filelist[] =$old_listfile[$i];
            }

            $documents = CUploadedFile::getInstancesByName('documents');

            if (isset($documents) && count($documents) > 0) {
                // go through each uploaded image
                foreach ($documents as $doc => $document) {
                    if ($document->saveAs($path .$document->name)) {
                        $filelist[] =$document->name;
                    }
                }

            }

            $model->documents = implode("|@|",$filelist);




            $mess = "";

            if($model->save()) {
                //$this->setNotifyMessage(NotificationType::Success, $this->singleTitle . ' has been updated');
                Yii::app()->user->setFlash('success', "Item has been updated.");
                $this->redirect(array('index'));
            }
            else{
                foreach($model->getErrors() as $error){
                    $mess.= "<p>".$error[0]."</p>";
                }
                Yii::app()->user->setFlash('error', $mess);
                //$this->setNotifyMessage(NotificationType::Error, $this->singleTitle . $mess);
            }

		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{

        $model = $this->loadModel($id);
        if ($model->mycompany->user_id == Yii::app()->user->id ){

            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));

        }else{
            //$this->redirect('/');
            Yii::app()->user->setFlash('error', "You cant delete this item.");
        }
        //SsnCompany::model()->findByAttributes(array('user_id' => Yii::app()->user->id ));


	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{

        $this->pageTitle = 'Items';

        $model = new SsnCompanyItem("searchAdmin");

        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['SsnCompanyItem']))
            $model->attributes=$_GET['SsnCompanyItem'];

        $this->render('index', array(
            'model' => $model,
        ));

	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new SsnCompanyItem('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SsnCompanyItem']))
			$model->attributes=$_GET['SsnCompanyItem'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return SsnCompanyItem the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=SsnCompanyItem::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param SsnCompanyItem $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ssn-company-item-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

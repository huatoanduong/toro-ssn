<?php

class PartnerController extends MemberController {

	public $pluralTitle = 'Partners';
	public $singleTitle = 'Partner';
	public $cannotDelete = array();

	/**
	 * Declares class-based actions.
	 */
	public function accessRules() {
		return array();
	}

	public function actions() {
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page' => array(
				'class' => 'CViewAction',
			),
		);
	}

	public function actionCheckEmailExist() {
		if (isset($_POST['email']) && !empty($_POST['email'])) {
			$user = Users::model()->findByAttributes(array('email' => $_POST['email']));
			if ($user) {
				echo '1';
			} else {
				echo '1';
			}
		}
	}

	public function actionCreate() {

		$model = new SsnPartner('create');
		if (isset($_POST['SsnPartner'])) {
			$model->attributes = $_POST['SsnPartner'];
			$email = trim($_POST['SsnPartner']['user_email']);

            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $error = "This ($email) email address is invalid.";
                //$this->setNotifyMessage(NotificationType::Error,  $error);
                Yii::app()->user->setFlash('error', $error);

            }else{

                $user = Users::model()->findByAttributes(array('email' => $email));

                if(!$user) {
                    $user = Users::model()->createUserCompanyProfileWithOnlyEmail($email);
                }

                $model->status = SsnPartner::STATUS_APPROVE;
                $model->approve_by =  Yii::app()->user->id;
                $model->from_user_id = Yii::app()->user->id;
                $model->to_user_id =  $user ? $user->id: 0;
                $model->partner_outsite_system = $user ? '' : $email;

                if ($model->validate()) {
                    $model->created_date = date('Y-m-d H:i:s');
                    if ($model->save()) {

                        SendEmail::partnerPendingRequest($model);

                        $this->redirect(Yii::app()->createAbsoluteUrl('member/partner/index'));
                    }
                }
            }

		}
		
		//$this->pageTitle = 'Make a Request';
        $this->pageTitle = 'Add new Contact';
		$this->breadcrumbs['Partner'] = array('index');
		$this->breadcrumbs[] = 'Add new Contact';
		$this->render('create', array(
			'model' => $model,
		));
	}

	public function actionApprove($status, $id) {
		$partner = SsnPartner::model()->findByPk($id);
		if ($partner) {
			$partner->status = SsnPartner::STATUS_APPROVE;
			$partner->approve_by = Yii::app()->user->id;
			if ($partner->update(array('status'))) {
				SendEmail::partnerApproveRequest($model);
			}

			$this->redirect(Yii::app()->createAbsoluteUrl('member/partner/index'));
		}
	}

	public function actionDelete($id) {
		try {
			if (Yii::app()->request->isPostRequest) {
				// we only allow deletion via POST request
				if (!in_array($id, $this->cannotDelete)) {
					if ($model = $this->loadModel($id)) {
						if ($model->delete())
							Yii::log("Delete record " . print_r($model->attributes, true), 'info');
					}

					// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
					if (!isset($_GET['ajax']))
						$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
				}
			} else {
				Yii::log("Invalid request. Please do not repeat this request again.");
				throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
			}
		} catch (Exception $e) {
			Yii::log("Exception " . print_r($e, true), 'error');
			throw new CHttpException($e);
		}
	}


	public function actionIndex($t = 2) {

        //hide cac function tab kia. nen $t luon bang =2;
        $t = 2;
		$t_approve = SsnPartner::getTotalPartner(SsnPartner::STATUS_APPROVE);
		$t_pending = SsnPartner::getTotalPartner(SsnPartner::STATUS_PENDING);
		$t_sent = SsnPartner::getTotalPartner(SsnPartner::STATUS_SEND);

        //$model =

        $model =new SsnPartner('searchSQLModel'); //SsnPartner::model()

        //die;

        $model->unsetAttributes();
		$view = '_approved';
		$status = 'Approved';

		if (isset($_GET['SsnPartner']))
			$model->attributes = $_GET['SsnPartner'];

        //change updat 27-10-2016
        $this->pageTitle = 'Contact';
        $this->pluralTitle = 'Contact';
        $this->breadcrumbs[] = 'Contact';

		$this->render('index', array(
			'model' => $model,
			'view' => $view,
			'status' => $status,
			't_approve' => $t_approve,
			't_pending' => $t_pending,
			't_sent' => $t_sent,
		));


	}


	public function actionView($id) {
		$this->pageTitle = 'Partner Info';
		try {
			$model = Users::model()->findByPk($id);
			$partner = SsnPartner::getListPartner();
			if (!key_exists($id, $partner))
				throw new CHttpException(404, 'The requested page does not exist.');
			$this->render('view', array(
				'model' => $model,
				'title_name' => $model->id));
		} catch (Exception $exc) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
	}

	public function actionViewCompany($id) {
		$partner = SsnPartner::getListPartner();
		if (!key_exists($id, $partner))
			throw new CHttpException(403, 'Access Denied.');
		
		$user = Users::model()->getInforUser($id);
		$comp = SsnCompany::model()->getCompanyByUser($id);
		
		$this->breadcrumbs[] = 'Partner Info';
		$this->pageTitle = 'Partner Info';
		$this->render('view_company', array(
			'comp' => $comp,
			'user' => $user,
		));
	}

	/*
	 * Bulk delete
	 * If you don't want to delete some specified record please configure it in global $cannotDelete variable
	 */

	public function actionDeleteAll() {
		$deleteItems = $_POST['ssn-partner-grid_c0'];
		$shouldDelete = array_diff($deleteItems, $this->cannotDelete);

		if (!empty($shouldDelete)) {
			SsnPartner::model()->deleteAll('id in (' . implode(',', $shouldDelete) . ')');
			$this->setNotifyMessage(NotificationType::Success, 'Your selected records have been deleted');
		} else
			$this->setNotifyMessage(NotificationType::Error, 'No records was deleted');

		if (!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	public function loadModel($id) {
		//need this define for inherit model case. Form will render parent model name in control if we don't have this line
		$initMode = new SsnPartner();
		$model = $initMode->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}


    /// gridfunctioncloumn

    protected function gridRepresentative($data,$row)
    {
        $current_user_id = $id=Yii::app()->user->id;
        if($current_user_id == $data->from_user_id) {
            return $data->to_user_fk->full_name;
        }
        return $data->from_user_fk->full_name;

    }

    protected function gridEmail($data,$row)
    {
        $current_user_id = $id=Yii::app()->user->id;
        if($current_user_id == $data->from_user_id) {
            return $data->to_user_fk->email;
        }
        return $data->from_user_fk->email;

    }

}

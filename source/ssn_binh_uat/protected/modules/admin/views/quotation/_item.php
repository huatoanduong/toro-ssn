<?php
/* @var $model SsnCompany */
?>
<div class="media company-thumb">
  <div class="media-body">
	  <h4><?= $model->name ?></h4>
	  <p><em><?= $model->color ?></em></p>
  </div>
</div>

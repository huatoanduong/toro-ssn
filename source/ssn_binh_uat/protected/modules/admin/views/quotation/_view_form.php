<?php
/* @var $this CompanyItemController */
/* @var $model SsnCompanyItem */
/* @var $form CActiveForm */


$addItemUrl = $this->createUrl('addItem');
$autoCompleteUrl = $this->createUrl('partners');

$modelpartner = new SsnPartner('searchSQLModel');
$dataa = $modelpartner->searchSQLModel()->getData();
$droplist = array();
$dataTemplate = array();

foreach ($dataa as $comp) {
    $dataTemplate[$comp->company_id] = $this->renderPartial('_company', array('model' => $comp), true);
    $droplist[$comp->company_id] = "{$comp->company_name} ({$comp->email})";
}
$templatdatataa = json_encode($dataTemplate);

$itemSsnCompanyItem = SsnCompanyItem::model();
$listcom_items =$itemSsnCompanyItem->findAllByAttributes(array("company_id"=>16));
$dataitems = array();
$droplistitem = array();


//
foreach ($listcom_items as $item) {
    $dataitems[$item->id] = array("id"=>$item->id,"name"=>$item->name,"color"=>$item->color);
    $droplistitem[$item->id] = "{$item->name} ({$item->color})";
}

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->theme->baseUrl . '/select2/select2.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(
    Yii::app()->theme->baseUrl . '/select2/select2.css');

Yii::app()->clientScript->registerScript(time(), "
	ssn.setupQuotaionDetailPage('{$addItemUrl}',{$templatdatataa});
	ssn.setupQuotaionIitems('{$addItemUrl}','{$templatdatataa}');
", CClientScript::POS_LOAD);

?>


<?php $this->widget('application.components.widget.Notification'); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'ssn-quotation-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => false,
    ),
    'htmlOptions' => array(
        'role' => 'form',
        'enctype' => 'multipart/form-data'
    ),
)); ?>


<style>
    .item-form-hidden{
        display: none;
    }
    .select2-container--default .select2-search--dropdown .select2-search__field {
        border: 1px solid #0868A3;
    }
    .input-sm , select.input-sm{
        height: 34px;
    }
</style>

<div class="box-body">


    <div class="form-group">
        <?php echo $form->labelEx($model,'created_date', array('class'=>'control-label')); ?>
        <?php  echo "<p>".date('d/m/Y', strtotime($model->created_date))."</p>" ?>
    </div>


    <div class="form-group">
        <?php echo $form->labelEx($model,'dateSentText', array('class'=>'control-label')); ?>
        <?php  echo "<p>{$model->dateSentText}</p>" ?>
    </div>


    <div class="form-group">

        <?php
        echo $form->labelEx($model, 'sender_id', array('label' => 'Send From', 'class' => 'control-label'));
        echo "<p><a href='".Yii::app()->createAbsoluteUrl('member/partner/viewCompany',array('id'=>$model->sender->id))."'>{$model->sender->name}</a></p>";
        ?>
    </div>

    <div class="form-group">

        <?php
        echo $form->labelEx($model, 'reciver_id', array('label' => 'Send To', 'class' => 'control-label'));
        echo "<p><a href='".Yii::app()->createAbsoluteUrl('member/partner/viewCompany',array('id'=>$model->reciver->id))."'>{$model->reciver->name}</a></p>";
        ?>
    </div>

        <div class="form-group">

                <label for="" class="control-label">Item</label>

                <div>
                    <table class="item-list table table-bordered table-hover">
                        <tbody>
                        <?php
                        foreach ($model->items as $index => $item):
                            if (empty($item->id)) {
                                break;
                            }
                            ?>
                            <tr class="item row-item-id" id="tr-row-item-id-<?= $item->id ?>"
                                data-item-id="<?= $item->id ?>">
                                <td>
                                    <div class="row-container">
                                        <p class="item-name"><?= $item->itemdetail->name ?></p>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-4 notranslate  item-color">
                                            <?= $item->itemdetail->color ?>
                                        </div>
                                        <div class="col-xs-4 notranslate  item-price">
                                            <?= $item->price ?>
                                        </div>
                                        <div class="col-xs-4 notranslate item-unit">
                                            <?= $item->unit ?>
                                        </div>

                                    </div>
                                </td>
                                <td style="vertical-align:middle; text-align: center">

                                    <div class="row-container">
                                        <div class=" notranslate item-document-url">
                                            <?php
                                            $urls = $item->getDocumentDownloadUrl();
                                            if($urls):
                                                foreach($urls as $url){
                                                    ?>
                                                    <div class="row-container document-link">
                                                        <a class="document-link-url" href="<?= $url["url"] ?>"><?=$url["name"]?></a>

                                                    </div>

                                                <?php }
                                            else: ?>
                                                No document uploaded.
                                            <?php endif ?>
                                        </div>

                                    </div>

                                </td>

                            </tr>
                        <?php endforeach ?>

                        </tbody>
                    </table>

                </div>

        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'remark', array('class' => 'control-label')); ?>
            <?php echo $form->textArea($model, 'remark', array('class' => 'form-control' ,'readony'=>true, 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'remark'); ?>
        </div>


        <div class="form-group">
            <div class="row" style="margin-left: 0px;">
                <?php echo $form->labelEx($model, 'documents', array('class' => 'control-label')); ?>
            </div>
            <div class="row" style="margin-left: 0px;">

                <div class="col-md-12" style=" float:left;padding-right:2px;">
                    <?= $form->error($model, 'documents', array('id'=>"SsnCompanyItem_{$index}_documents_em_")) ?>
                    <div class=" notranslate item-document-url">
                        <?php
                        $urls = $model->getDocumentDownloadUrl();
                        if($urls):
                            foreach($urls as $url){
                                ?>
                                <div class="row-container document-link">
                                    <a class="document-link-url" href="<?= $url["url"] ?>"><?=$url["name"]?></a>

                                </div>

                            <?php }
                        else: ?>
                            No document uploaded.
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div>
    <!-- form -->
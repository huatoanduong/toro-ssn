<?php
/* @var $this CompanyItemController */
/* @var $model SsnCompanyItem */
/* @var $form CActiveForm */


$addItemUrl = $this->createUrl('addItem');
$autoCompleteUrl = $this->createUrl('partners');

$modelpartner = new SsnPartner('searchSQLModel');
$dataa = $modelpartner->searchSQLModel()->getData();
$droplist = array();
$dataTemplate = array();

foreach ($dataa as $comp) {
    $dataTemplate[$comp->company_id] = $this->renderPartial('_company', array('model' => $comp), true);
    $droplist[$comp->company_id] = "{$comp->company_name} ({$comp->email})";
}
$templatdatataa = json_encode($dataTemplate);

$itemSsnCompanyItem = SsnCompanyItem::model();
$listcom_items =$itemSsnCompanyItem->findAllByAttributes(array("company_id"=>16));
$dataitems = array();
$droplistitem = array();


//
foreach ($listcom_items as $item) {
    $dataitems[$item->id] = array("id"=>$item->id,"name"=>$item->name,"color"=>$item->color);
    $droplistitem[$item->id] = "{$item->name} ({$item->color})";
}

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->theme->baseUrl . '/select2/select2.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(
    Yii::app()->theme->baseUrl . '/select2/select2.css');

Yii::app()->clientScript->registerScript(time(), "
	ssn.setupQuotaionDetailPage('{$addItemUrl}',{$templatdatataa});
	ssn.setupQuotaionIitems('{$addItemUrl}','{$templatdatataa}');
", CClientScript::POS_LOAD);

?>


<?php $this->widget('application.components.widget.Notification'); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'ssn-quotation-form',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => false,
    ),
    'htmlOptions' => array(
        'role' => 'form',
        'enctype' => 'multipart/form-data'
    ),
)); ?>


<style>
    .item-form-hidden{
        display: none;
    }
    .select2-container--default .select2-search--dropdown .select2-search__field {
        border: 1px solid #0868A3;
    }
    .input-sm , select.input-sm{
        height: 34px;
    }
</style>

<div class="box-body">

    <div class="form-group">
        <?php echo $form->labelEx($model,'dateSentText', array('class'=>'control-label')); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
            'model' => $model,
            'attribute' => 'dateSentText',
            'options' => array(
                'dateFormat' => 'dd/mm/yy',
            ),
            'htmlOptions'=>array(
                'class'=>'form-control'
            ),
        )); ?>
        <?php echo $form->error($model,'dateSentText'); ?>
    </div>


    <div class="form-group">

        <?php
        echo $form->labelEx($model, 'reciver_id', array('label' => 'Send to', 'class' => 'control-label')); // array('label'=>'CellPhone', 'class' => '')
        echo $form->dropDownList($model, 'reciver_id', $droplist,
            array(
                'multiple' => false,
                'class' => 'company-select form-control hide',
                'style' => 'width: 100%'
            )
        );

        ?>
    </div>

        <div class="form-group">

            <label for="" class="control-label">Edit Item</label>

            <div id="error_noitems" class="errorMessage" style=" display: none">Sample must have at least one item.
            </div>

            <table class="item-list table table-bordered table-hover">
                <tbody>
                    <tr class="item row-item-id" id="tr-row-item-id-1"></tr>
                </tbody>
            </table>


            <div class="table-responsive" id="list-form-items">

                <div class="row-container dropdown_select_item" style="position: absolute;margin-top: 0px;width: 100%;padding-right: 20px;">
                    <?php
                    $item = $model->items[0];
                    echo $form->dropDownList($item, 'refer_item_id', $droplistitem,
                        array(
                            'multiple' => false,
                            'class' => 'select_item_name form-control hide',
                            'style' => 'width: 100%'
                        )
                    );

                    ?>
                </div>
                <div class="row-container  div-row-add-new current_show_form"  >
                    <?php $this->renderPartial('_quotation-item-update', array(
                        'index' =>$item->id,
                        'model' => $item,
                    )) ?>
                </div>

            </div>

        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'remark', array('class' => 'control-label')); ?>
            <?php echo $form->textArea($model, 'remark', array('class' => 'form-control', 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'remark'); ?>
        </div>


        <div class="form-group">
            <div class="row" style="margin-left: 0px;">
                <?php echo $form->labelEx($model, 'documents', array('class' => 'control-label')); ?>
            </div>
            <div class="row" style="margin-left: 0px;">

                <?php
                $this->widget('CMultiFileUpload', array(
                    'model' => $model,
                    'name' => 'documents',
                    'attribute' => 'image',
                    'accept' => 'doc|docx|xls|xlsx|pdf|jpg|gif|png',
                    'max' => 4,
                    'remove' => 'Remove Image   ',
                    'duplicate' => 'Already Selected',
                ));
                ?>

                <div class="col-md-12" style=" float:left;padding-right:2px;">
                    <?= $form->error($model, 'documents', array('id'=>"SsnCompanyItem_{$index}_documents_em_")) ?>
                    <div class=" notranslate item-document-url">
                        <?php
                        $urls = $model->getDocumentDownloadUrl();
                        if($urls):
                            foreach($urls as $url){
                                ?>
                                <div class="row-container document-link">
                                    <a class="document-link-url" href="<?= $url["url"] ?>"><?=$url["name"]?></a>

                                </div>

                            <?php }
                        else: ?>
                            No document uploaded.
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">
                    <?php echo $model->isNewRecord ? 'Create' : 'Save' ?></button>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div>
    <!-- form -->
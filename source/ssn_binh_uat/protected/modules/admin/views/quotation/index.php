<?php
$this->breadcrumbs = array(
    $this->pluralTitle,
);

Yii::app()->clientScript->registerScript('search', "

$('#ssn-quotation-grid a.ajaxupdate').on('click', function() {
        $.fn.yiiGridView.update('ssn-quotation-grid', {
            type: 'POST',
            url: $(this).attr('href'),
            success: function() {
                $.fn.yiiGridView.update('ssn-quotation-grid');
            }
        });
        return false;
    });

$( document ).ajaxComplete(function() {
        $('#ssn-quotation-grid a.ajaxupdate').on('click', function() {
            $.fn.yiiGridView.update('ssn-quotation-grid', {
                type: 'POST',
                url: $(this).attr('href'),
                success: function() {
                    $.fn.yiiGridView.update('ssn-quotation-grid');
                }
            });
            return false;
        });
});


$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('ssn-quotation-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});

$('#clearsearch').click(function(){
	var id='search-form';
	var inputSelector='#'+id+' input, '+'#'+id+' select';
	$(inputSelector).each( function(i,o) {
		 $(o).val('');
	});
	var data=$.param($(inputSelector));
	$.fn.yiiGridView.update('ssn-quotation-grid', {data: data});
	return false;
});


");

?>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active" ><a href="<?= $this->createUrl('index') ?>">All</a></li>
    </ul>


<div class="box">

    <div class="box-body">
        <div class='search-form'>
            <?php $this->renderPartial('_search', array(
                'model' => $model,
            )); ?>
        </div>


        <div class="table-responsive">
            <?php

            $allowAction = in_array("delete", array()) ? 'CCheckBoxColumn' : '';
            $columnArray = array();
            if (in_array("Delete", array())) {
                $columnArray[] = array(
                    'value' => '$data->id',
                    'class' => "CCheckBoxColumn",
                );
            }

            $columnArray = array_merge($columnArray, array(
                array(
                    'header' => 'S/N',
                    'type' => 'raw',
                    'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                    'headerHtmlOptions' => array('width' => '30px', 'style' => 'text-align:center;'),
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),


                array(
                    'header'=>'Date Sent',
                    'name'=>'created_date',
                    'type' => 'date',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),

                array(
                    'header'=>'Sender Name',
                    'name'=>'senderName',
                    'value'=>'$data->sender->name',
                    'htmlOptions' => array('style' => 'text-align:left;')
                ),

                array(
                    'header'=>'Receiver Name',
                    'name'=>'reciverName',
                    'value'=>'$data->reciver->name',
                    'htmlOptions' => array('style' => 'text-align:left;')
                ),


                array(
                    'header'=>'Item Name',
                    'name'=>'itemName',
                    'value'=>'$data->items[0]->itemdetail->name',
                    'htmlOptions' => array('style' => 'text-align:left;')
                ),
                array(
                    'header'=>'Item Unit',
                    'name'=>'itemUnit',
                    'value'=>'$data->items[0]->unit',
                    'htmlOptions' => array('style' => 'text-align:left;')
                ),
                array(
                    'header'=>'Item Price',
                    'name'=>'itemPrice',
                    'value'=>'$data->items[0]->price',
                    'htmlOptions' => array('style' => 'text-align:left;')
                ),




                array(
                    'header'=>'Exp Date',
                    'name'=>'date_expired',
                    'type' => 'date',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),

                array(
                    'header'=>'Remark',
                    'name'=>'remark',
                    'htmlOptions' => array('class' => 'breakwordP')
                ),



                array(
                    'header' => 'Actions',
                    'class' => 'CButtonColumn',
                    'template' => '{delete}',
                ),
            ));

            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'ssn-quotation-grid-bulk',
                'enableAjaxValidation' => false,
                'htmlOptions' => array('enctype' => 'multipart/form-data')));

            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'ssn-quotation-grid',
                'dataProvider' => $model->search(),
                'selectionChanged'=>'function(id){ location.href = "'.$this->createUrl('view').'/id/"+$.fn.yiiGridView.getSelection(id);}',
                'selectableRows' => 2,
                'columns' => $columnArray,
                'cssFile'=>false,
                'summaryText'=>'',
                'itemsCssClass'=>'table table-striped table-bordered',
                'pagerCssClass'=>'pag-container',
                'pager' => array(
                    'header'=>'',
                    'cssFile' => false,
                    'nextPageLabel' => 'Next',
                    'prevPageLabel' => 'Previous',
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last',
                    'selectedPageCssClass'=>'active',
                    'maxButtonCount' => 10,
                    'htmlOptions'=>array('class' => 'pagination pull-right',)
                ),
            ));
            $this->endWidget();
            ?>
        </div>
    </div>
    <div>

</div>



<?php
/* @var $this ShippingController */
/* @var $model SsnShipping */
?>
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">Information</h3>
	</div>
	<!-- /.box-header -->
	<!-- form start -->

	 <div class="box-body">

        <div class="form-group">
                <label class="control-label" for="Help_status">ID</label>
                <div class="row-container">
                    <?php echo CHtml::link(CHtml::encode($model->id), array('view', 'id'=>$model->id)); ?>
                </div>
        </div>

        <div class="form-group">
                <label class="control-label" for="Help_title">Status</label>
                <div class="row-container">
                <?php echo $model->status ==1 ? "Active":"Disable"; ?>
                </div>
        </div>

        <div class="form-group">
                <label class="control-label" for="Help_title">Title</label>
                <div class="row-container">
                <?php echo CHtml::encode($model->title); ?>
                </div>
        </div>
        <div class="form-group">
                <label class="control-label" for="Help_description">Description</label>
                <div class="row-container">
                    <?php echo $model->description; ?>
                </div>
        </div>
       
    </div>


</div>
<!-- /.box -->
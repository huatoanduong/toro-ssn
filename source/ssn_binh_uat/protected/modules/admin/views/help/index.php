<?php
$this->breadcrumbs = array(
    $this->pluralTitle,
);


Yii::app()->clientScript->registerScript('search', "


$('#ssn-help-grid a.ajaxupdate').on('click', function() {
        $.fn.yiiGridView.update('ssn-help-grid', {
            type: 'POST',
            url: $(this).attr('href'),
            success: function() {
                $.fn.yiiGridView.update('ssn-help-grid');
            }
        });
        return false;
    });

$( document ).ajaxComplete(function() {
        $('#ssn-help-grid a.ajaxupdate').on('click', function() {
            $.fn.yiiGridView.update('ssn-help-grid', {
                type: 'POST',
                url: $(this).attr('href'),
                success: function() {
                    $.fn.yiiGridView.update('ssn-help-grid');
                }
            });
            return false;
        });
});


$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('ssn-help-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});

$('#clearsearch').click(function(){
	var id='search-form';
	var inputSelector='#'+id+' input, '+'#'+id+' select';
	$(inputSelector).each( function(i,o) {
		 $(o).val('');
	});
	var data=$.param($(inputSelector));
	$.fn.yiiGridView.update('ssn-help-grid', {data: data});
	return false;
});


");

?>

<style>

   .breakwordP{
       white-space: normal; word-wrap: break-word;
        word-break: break-all;
   }

</style>

<div class="box">
    <div class="box-body">
        <p>
            <a style="padding: 0px;" href="<?php echo Yii::app()->createAbsoluteUrl('/admin/help/create')?>" >
                <button type="button" class="btn btn-primary" style=" margin:5px;">Add New</button></a>

        </p>


        <div class="table-responsive">
            <?php

            $allowAction = in_array("delete", array()) ? 'CCheckBoxColumn' : '';
            $columnArray = array();
            if (in_array("Delete", array())) {
                $columnArray[] = array(
                    'value' => '$data->id',
                    'class' => "CCheckBoxColumn",
                );
            }

            $columnArray = array_merge($columnArray, array(
                array(
                    'header' => 'S/N',
                    'type' => 'raw',
                    'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                    'headerHtmlOptions' => array('width' => '30px', 'style' => 'text-align:center;'),
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),

                array(
                    'header'=>'Title',
                    'name'=>'title'
                ),
            array(
                    'header'=>'Status',
                    'name'=>'status',
                    'value'=>'$data->status==1?"Active":"Disable"'
                ),
                array(
                    'header' => 'Actions',
                    'class' => 'CButtonColumn',
                    'template' => '{view}{delete}',
                )
            ));

            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'ssn-help-grid-bulk',
                'enableAjaxValidation' => false,
                'htmlOptions' => array('enctype' => 'multipart/form-data')));

            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'ssn-help-grid',
                'dataProvider' => $model->search(),
                'selectableRows' => 2,
                'selectionChanged'=>'function(id){ location.href = "'.$this->createUrl('update').'/id/"+$.fn.yiiGridView.getSelection(id);}',
                'columns' => $columnArray,
                'cssFile'=>false,
                'summaryText'=>'',
                'itemsCssClass'=>'table table-striped table-bordered',
                'pagerCssClass'=>'pag-container',
                'pager' => array(
                    'header'=>'',
                    'cssFile' => false,
                    'nextPageLabel' => 'Next',
                    'prevPageLabel' => 'Previous',
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last',
                    'selectedPageCssClass'=>'active',
                    'maxButtonCount' => 10,
                    'htmlOptions'=>array('class' => 'pagination pull-right',)
                ),
            ));
            
            $this->endWidget();
            ?>
        </div>
    </div>
<div>
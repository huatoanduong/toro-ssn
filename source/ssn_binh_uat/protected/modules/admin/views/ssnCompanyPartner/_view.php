<?php
/* @var $this SsnCompanyPartnerController */
/* @var $data SsnCompanyPartner */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('from_user_id')); ?>:</b>
	<?php echo CHtml::encode($data->from_user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('to_user_id')); ?>:</b>
	<?php echo CHtml::encode($data->to_user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('from_company_id')); ?>:</b>
	<?php echo CHtml::encode($data->from_company_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('to_company_id')); ?>:</b>
	<?php echo CHtml::encode($data->to_company_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('partner_outsite_system')); ?>:</b>
	<?php echo CHtml::encode($data->partner_outsite_system); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('approve_by')); ?>:</b>
	<?php echo CHtml::encode($data->approve_by); ?>
	<br />

	*/ ?>

</div>
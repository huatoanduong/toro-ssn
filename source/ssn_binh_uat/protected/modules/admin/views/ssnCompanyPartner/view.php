<?php
/* @var $this SsnCompanyPartnerController */
/* @var $model SsnCompanyPartner */

$this->breadcrumbs=array(
	'Ssn Company Partners'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List SsnCompanyPartner', 'url'=>array('index')),
	array('label'=>'Create SsnCompanyPartner', 'url'=>array('create')),
	array('label'=>'Update SsnCompanyPartner', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SsnCompanyPartner', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SsnCompanyPartner', 'url'=>array('admin')),
);
?>

<h1>View SsnCompanyPartner #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'from_user_id',
		'to_user_id',
		'from_company_id',
		'to_company_id',
		'partner_outsite_system',
		'created_date',
		'status',
		'approve_by',
	),
)); ?>

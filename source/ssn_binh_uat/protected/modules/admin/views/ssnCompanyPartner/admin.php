<?php
/* @var $this SsnCompanyPartnerController */
/* @var $model SsnCompanyPartner */

$this->breadcrumbs=array(
	'Ssn Company Partners'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List SsnCompanyPartner', 'url'=>array('index')),
	array('label'=>'Create SsnCompanyPartner', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#ssn-company-partner-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Ssn Company Partners</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ssn-company-partner-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'from_user_id',
		'to_user_id',
		'from_company_id',
		'to_company_id',
		'partner_outsite_system',
		/*
		'created_date',
		'status',
		'approve_by',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

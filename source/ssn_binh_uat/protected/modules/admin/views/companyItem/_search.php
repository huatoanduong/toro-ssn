<?php
$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
    'htmlOptions' => array('class' => 'form-horizontal', 'role' => 'form', 'id' => 'search-form'),
));
?>
<div class="row">
    <div class="col-sm-12" style="margin-left: 5px;">
        <div class="form-group">
            <div class="col-sm-2">
                <?php echo $form->textField($model,'code',array('class'=>'form-control','placeholder'=>'System Code')); ?>
                <?php echo $form->error($model, 'code'); ?>
            </div>
            <div class="col-sm-3">
                <?php echo $form->textField($model,'name',array('class'=>'form-control','placeholder'=>'Item Name')); ?>
                <?php echo $form->error($model, 'name'); ?>
            </div>
            <div class="col-sm-2">
                <?php echo $form->textField($model,'color',array('class'=>'form-control','placeholder'=>'Color')); ?>
                <?php echo $form->error($model, 'color'); ?>
            </div>
            <div class="col-sm-3">
                <?php echo $form->textField($model,'buyer_name',array('class'=>'form-control','placeholder'=>'Buyer Name')); ?>
                <?php echo $form->error($model, 'buyer_name'); ?>
            </div>

            <div class="col-sm-2">
                <button type="submit" class="btn btn-default ">Search</button>
            </div>
        </div>

    </div>

</div>
<?php $this->endWidget(); ?>

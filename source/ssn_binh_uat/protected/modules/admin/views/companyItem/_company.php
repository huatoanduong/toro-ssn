<?php
/* @var $model SsnCompany */
?>
<div class="media company-thumb">
  <div class="media-body">
	  <h4><?= $model->company_name ?></h4>
	  <p><em><?= $model->email ?></em><br/><em><?= $model->address ?></em></p>
  </div>
</div>
<?php
/* @var $this CompanyItemController */
/* @var $model SsnCompanyItem */

$this->breadcrumbs=array(
	'Ssn Company Items'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List SsnCompanyItem', 'url'=>array('index')),
	array('label'=>'Create SsnCompanyItem', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#ssn-company-item-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Ssn Company Items</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ssn-company-item-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'company_id',
		'code',
		'name',
		'color',
		'buyer_id',
		/*
		'created_date',
		'remark',
		'documents',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

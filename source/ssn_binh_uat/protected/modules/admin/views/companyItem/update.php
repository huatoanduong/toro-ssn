
<style>
    .select2-container--default .select2-search--dropdown .select2-search__field {
        border: 1px solid #177389;
    }

</style>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Information</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <?php echo $this->renderPartial('_form', array('model' => $model)); ?>
</div>
<!-- /.box -->
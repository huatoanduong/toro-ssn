<?php
/* @var $this CompanyItemController */
/* @var $model SsnCompanyItem */
/* @var $form CActiveForm */

$modelpartner =new SsnPartner('searchSQLModel');

$dataa = $modelpartner->searchSQLModel($model->mycompany->user_id)->getData();
$droplist = array();
$dataTemplate = array();
foreach ($dataa as $comp) {
    $dataTemplate[$comp->company_id] = $this->renderPartial('_company', array('model' => $comp), true);
    $droplist[$comp->company_id] = "{$comp->company_name} ({$comp->email})";
}

$templatdatataa =json_encode($dataTemplate);

//Yii::app()->baseUrl

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->baseUrl.'/themes/select2/select2.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(
    Yii::app()->baseUrl.'/themes/select2/select2.css');


$addItemUrl = $this->createUrl('addItem');
$autoCompleteUrl = $this->createUrl('partners');

Yii::app()->clientScript->registerScript(time(), "
	ssn.setupSampleDetailPage('{$addItemUrl}',{$templatdatataa});
", CClientScript::POS_LOAD);

?>

<?php $this->widget('application.components.widget.Notification'); ?>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'ssn-company-item-form',
    'enableAjaxValidation'=>false,
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
        'validateOnChange'=>false,
    ),
    'htmlOptions' => array(
        'role' => 'form',
        'enctype' => 'multipart/form-data'
    ),
)); ?>

<div class="box-body">
    <div class="form-group">
        <?php

        //print_r($templatdatataa);

        echo $form->labelEx($model,'buyer_id', array('label'=>'Buyer','class'=>'control-label')); // array('label'=>'CellPhone', 'class' => '')

        echo $form->dropDownList($model,
            'buyer_id',
            $droplist ,
                array(
                    'multiple'=> $model->isNewRecord ? true : false,
                    'class'=>'company-select form-control hide',
                    'style'=>'width: 100%'
                )
            );

        ?>
    </div>

    <div class="form-group">
		<?php echo $form->labelEx($model,'name', array('class'=>'control-label'));?>
		<?php echo $form->textField($model,'name',array('class'=>'form-control','maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

    <div class="form-group">
		<?php echo $form->labelEx($model,'color', array('class'=>'control-label'));?>
		<?php echo $form->textField($model,'color',array('class'=>'form-control','maxlength'=>255)); ?>
		<?php echo $form->error($model,'color'); ?>
	</div>


    <div class="form-group">
		<?php echo $form->labelEx($model,'remark', array('class'=>'control-label'));?>
		<?php echo $form->textArea($model,'remark',array('class'=>'form-control','maxlength'=>255)); ?>
		<?php echo $form->error($model,'remark'); ?>
	</div>

<!--    <div class="form-group">-->
<!--		--><?php //echo $form->labelEx($model,'documents', array('class'=>'control-label'));?>
<!--		--><?php //echo $form->textField($model,'documents',array('class'=>'form-control','maxlength'=>255)); ?>
<!--		--><?php //echo $form->error($model,'documents'); ?>
<!--	</div>-->

    <div class="form-group">
        <div class="row" style="margin-left: 0px;">
            <?php  echo $form->labelEx($model,'documents', array('class'=>'control-label'));?>
        </div>
        <div class="row"  style="margin-left: 0px;">

            <div style="width:20%;float:left;padding-right:2px;">

                <?php
                $this->widget('CMultiFileUpload', array(
                    'model'=>$model,
                    'name'=>'documents',
                    'attribute'=>'image',
                    'accept'=>'doc|docx|xls|xlsx|pdf|jpg|gif|png',
                    'max'=>1,
                    'remove'=>'Remove file  ',
                    'duplicate'=>'Already Selected',
                ));
                ?>


                <?= $form->error($model, 'documents', array('id'=>"SsnCompanyItem_documents_em_")) ?>
                <?php
                $urls = $model->getDocumentDownloadUrl();
                if($urls) {
                    foreach ($urls as $url) {
                        ?>
                        <div class="row-container document-link">
                            <a class="document-link-url" href="<?= $url["url"] ?>"> <?= $url["name"] ?> </a>
                            <a href="javascript:void(0);" class=""><span data-item-filename="<?= $url["name"] ?>" style="color: red; margin-left: 10px;"
                                      class="glyphicon glyphicon-remove btn-remove-file"></span>

                            </a>
                        </div>

                    <?php }
                }
                else{ ?>
                    No document uploaded.
                <?php } ?>


            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="box-footer" >
            <button type="submit" class="btn btn-primary"><?php echo $model->isNewRecord ? 'Create' : 'Save' ?></button>
        </div>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
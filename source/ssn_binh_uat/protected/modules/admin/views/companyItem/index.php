<?php
$this->breadcrumbs = array(
    $this->pluralTitle,
);


Yii::app()->clientScript->registerScript('search', "


$('#ssn-company-item-grid a.ajaxupdate').on('click', function() {
        $.fn.yiiGridView.update('ssn-company-item-grid', {
            type: 'POST',
            url: $(this).attr('href'),
            success: function() {
                $.fn.yiiGridView.update('ssn-company-item-grid');
            }
        });
        return false;
    });

$( document ).ajaxComplete(function() {
        $('#ssn-company-item-grid a.ajaxupdate').on('click', function() {
            $.fn.yiiGridView.update('ssn-company-item-grid', {
                type: 'POST',
                url: $(this).attr('href'),
                success: function() {
                    $.fn.yiiGridView.update('ssn-company-item-grid');
                }
            });
            return false;
        });
});


$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('ssn-company-item-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});

$('#clearsearch').click(function(){
	var id='search-form';
	var inputSelector='#'+id+' input, '+'#'+id+' select';
	$(inputSelector).each( function(i,o) {
		 $(o).val('');
	});
	var data=$.param($(inputSelector));
	$.fn.yiiGridView.update('ssn-company-item-grid', {data: data});
	return false;
});


");

?>

<style>

    .breakwordP{
        white-space: normal; word-wrap: break-word;
        word-break: break-all;
    }

</style>

<div class="box">
    <div class="box-body">

        <div class='search-form'>
            <?php $this->renderPartial('_search', array(
                'model' => $model,
            )); ?>
        </div>


        <div class="table-responsive">
            <?php

            $allowAction = in_array("delete", array()) ? 'CCheckBoxColumn' : '';
            $columnArray = array();
            if (in_array("Delete", array())) {
                $columnArray[] = array(
                    'value' => '$data->id',
                    'class' => "CCheckBoxColumn",
                );
            }

            $columnArray = array_merge($columnArray, array(
                array(
                    'header' => 'S/N',
                    'type' => 'raw',
                    'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                    'headerHtmlOptions' => array('width' => '30px', 'style' => 'text-align:center;'),
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),

                array(
                    'header'=>'Material Code',
                    'name'=>'code',
                ),
                array(
                    'header'=>'Item Name',
                    'name'=>'name',
                    'htmlOptions' => array('class' => 'breakwordP')
                ),

                array(
                    'header'=>'Color',
                    'name'=>'color',
                ),

                array(
                    'header'=>'Company Owner',
                    'name'=>'buyer_name',
                    'value'=>'$data->mycompany->name',
                    'htmlOptions' => array('style' => 'text-align:left;')
                ),

                array(
                    'header'=>'Buyer Name',
                    'name'=>'buyer_name',
                    'value'=>'$data->buyer->name',
                    'htmlOptions' => array('style' => 'text-align:left;')
                ),

                array(
                    'header'=>'Created Date',
                    'name'=>'created_date',
                    'type' => 'date',
                    'htmlOptions' => array('style' => 'text-align:center;')
                ),
                array(
                    'header'=>'Remark',
                    'name'=>'remark',
                    'htmlOptions' => array('class' => 'breakwordP')
                ),
 
                array(
                    'header' => 'Actions',
                    'class' => 'CButtonColumn',
                    'template' => '{view}{delete}',
                ),

            ));

            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'ssn-company-item-grid-bulk',
                'enableAjaxValidation' => false,
                'htmlOptions' => array('enctype' => 'multipart/form-data')));

            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'ssn-company-item-grid',
                'dataProvider' => $model->searchAdmin(),
                'selectableRows' => 2,
                'selectionChanged'=>'function(id){ location.href = "'.$this->createUrl('update').'/id/"+$.fn.yiiGridView.getSelection(id);}',
                'columns' => $columnArray,
                'cssFile'=>false,
                'summaryText'=>'',
                'itemsCssClass'=>'table table-striped table-bordered',
                'pagerCssClass'=>'pag-container',
                'pager' => array(
                    'header'=>'',
                    'cssFile' => false,
                    'nextPageLabel' => 'Next',
                    'prevPageLabel' => 'Previous',
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last',
                    'selectedPageCssClass'=>'active',
                    'maxButtonCount' => 10,
                    'htmlOptions'=>array('class' => 'pagination pull-right',)
                ),
            ));
            
            $this->endWidget();
            ?>
        </div>
    </div>
<div>
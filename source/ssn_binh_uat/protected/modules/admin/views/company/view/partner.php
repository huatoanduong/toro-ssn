<div class="table-responsive">
<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'ssn-partner-grid',
	'dataProvider' => $model->searchModel->searchAdmin(),
	'columns' => array(
		array(
			'header' => 'S/N',
			'type' => 'raw',
			'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
			'headerHtmlOptions' => array('width' => '30px', 'style' => 'text-align:center;'),
			'htmlOptions' => array('style' => 'text-align:center;')
		),
		'fromCompany',
		'toCompany',
		array(
			'header' => 'Send Date',
			'type' => 'date',
			'name' => 'created_date',
		),
		'statusText',
		array(
			'header' => 'Actions',
			'class' => 'CButtonColumn',
			'template' => '{approve} {delete}',
			'buttons' => array(
				'delete' => array(
					'label'=>'',
					'imageUrl'=>'',
					'options' => array('class' => 'glyphicon glyphicon-remove', 'style' => 'color: red', 'title' => 'Decline'),
					'click' => 'function(e){
						e.preventDefault();
						if(!confirm("Are you sure you want to delete this item?")) return false;
						var th = this, afterDelete = function(){};
						jQuery("#ssn-partner-grid").yiiGridView("update", {
							type: "POST",
							url: jQuery(this).attr("href"),
							success: function(data) {
								$.fn.yiiGridView.update("ssn-partner-grid");
							},
							error: function(XHR) {
								alert("Some error occured");
							}
						});
						return false;}'
				),
				'approve' => array(
					'label'=>'',
					'imageUrl'=>'',
					'options' => array('class' => 'glyphicon glyphicon-ok', 'style' => 'color: green', 'title' => 'Approve'),
					'url'=>'Yii::app()->controller->createUrl("approve",array("id" => $data->id))',
					'visible'=>'$data->status==0',
					'click' => 'function(e){
						e.preventDefault();
						jQuery("#ssn-partner-grid").yiiGridView("update", {
							type: "POST",
							url: jQuery(this).attr("href"),
							success: function(data) {
								$.fn.yiiGridView.update("ssn-partner-grid");
							},
							error: function(XHR) {
								alert("Some error occured");
							}
						});
						return false;}'
				),
			)
		),
	),
	'cssFile'=>false,
	'itemsCssClass'=>'items table table-striped table-bordered',
	'pagerCssClass'=>'pag-container',
	'pager' => array(
		'header'=>'',
		'cssFile' => false,
		'nextPageLabel' => 'Next',
		'prevPageLabel' => 'Previous',
		'firstPageLabel' => 'First',
		'lastPageLabel' => 'Last',
		'selectedPageCssClass'=>'active',
		'maxButtonCount' => 10,
		'htmlOptions'=>array('class' => 'pagination pull-right',)
	),
));
?>
</div>

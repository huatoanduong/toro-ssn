<div class="table-responsive">
<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'ssn-sample-grid',
	'dataProvider' => $model->searchModel->searchCompanyProfile(),
	'columns' => array(
		array(
			'header' => 'S/N',
			'type' => 'raw',
			'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
			'headerHtmlOptions' => array('width' => '30px', 'style' => 'text-align:center;'),
			'htmlOptions' => array('style' => 'text-align:center;')
		),
		//'dateSentText',
	    array(
            'header'=>'Send Date',
            'name'=>'send_date',
            'value'=>'date("d/m/Y", strtotime($data->send_date))', //array($model,getRawDateSentText),
        ),
        //'senderName',
        //if ($this->sender->company) return $this->sender->company->name;
        //sender' => array(self::BELONGS_TO, 'Users', 'from_user_id'), 'receivers
        array('header'=>'Sender',
            'name'=>'sender_email',
            'value'=>' $data->sender->company ? $data->sender->company->name:"-"' ),


        array('header'=>'Recipient',
            'name'=>'seach_receiver',
            'type'=>'raw',
            'value'=>'$data->recipientNames'),

        /*array(
            'header'=>'Sender',
            'name'=>'send_date',
            'value'=>'date("d/m/Y", strtotime($data->send_date))', //array($model,getRawDateSentText),
        ),*/
        //'senderName',

		//'recipientNames:raw',
		'awbn',
		'remark',
		array(
			'header' => 'Actions',
			'class' => 'CButtonColumn',
			'template' => '{view} {delete}',
			'buttons' => array(
				'delete' => array(
					'label'=>'',
					'imageUrl'=>'',
					'options' => array('class' => 'glyphicon glyphicon-remove', 'style' => 'color: red', 'title' => 'Delete'),
					'url'=>'Yii::app()->controller->createUrl("deleteSample",array("id" => $data->id))',
					'click' => 'function(e){
						e.preventDefault();
						if(!confirm("Are you sure you want to delete this item?")) return false;
						var th = this, afterDelete = function(){};
						jQuery("#ssn-sample-grid").yiiGridView("update", {
							type: "POST",
							url: jQuery(this).attr("href"),
							success: function(data) {
								$.fn.yiiGridView.update("ssn-sample-grid");
							},
							error: function(XHR) {
								alert("Some error occured");
							}
						});
						return false;}'
				),
				'view' => array(
					'label'=>'',
					'imageUrl'=>'',
					'options' => array('class' => 'glyphicon glyphicon-search', 'style' => 'color: green', 'title' => 'View'),
					'url'=>'Yii::app()->controller->createUrl("sample/view",array("id" => $data->id))',
				),
			)
		),
	),
	'cssFile'=>false,
	'itemsCssClass'=>'items table table-striped table-bordered',
	'pagerCssClass'=>'pag-container',
	'pager' => array(
		'header'=>'',
		'cssFile' => false,
		'nextPageLabel' => 'Next',
		'prevPageLabel' => 'Previous',
		'firstPageLabel' => 'First',
		'lastPageLabel' => 'Last',
		'selectedPageCssClass'=>'active',
		'maxButtonCount' => 10,
		'htmlOptions'=>array('class' => 'pagination pull-right',)
	),

));
?>
</div>
<div class="box">
	<div class="box-body">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li class="active"><a href="javascript:void(0)">Information</a></li>
				<li><a href="<?= $this->createUrl('partner', array('id'=>$model->id)) ?>">Partner</a></li>
				<li><a href="<?= $this->createUrl('sample', array('id'=>$model->id)) ?>">Sample</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active container-fluid">
					<?php $this->renderPartial('view/info', array('model'=>$model)) ?>
				</div><!-- /.tab-pane -->
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- nav-tabs-custom -->
	</div>
</div>
	

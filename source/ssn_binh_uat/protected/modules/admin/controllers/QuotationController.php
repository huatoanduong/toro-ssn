<?php

class QuotationController extends AdminController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	#public $layout='//layouts/column2';


    public $pluralTitle = 'Quotation';
    public $singleTitle = 'Quotation';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','AddItem'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','delete','update','AddItem'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','AddItem'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new SsnQuotation;

        $model->dateSentText = date('d/m/Y');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

        $mycompany = SsnCompany::model()->findByAttributes(array('user_id' => Yii::app()->user->id ));

		if(isset($_POST['SsnQuotation'])) {
            //buyer_id
            $reciver_list = $_POST['SsnQuotation']['reciver_ids'];

            $mess ="";

            $documents = CUploadedFile::getInstancesByName('documents');


            foreach ($reciver_list as $reciver) {


                foreach ($_POST['SsnQuotationItem'] as $key=> $postitem) {


                    if($postitem['refer_item_id'] != null) {

                        $tmpquotation = new SsnQuotation;
                        $tmpquotation->attributes = $_POST['SsnQuotation'];
                        $tmpquotation->reciver_id = $reciver;
                        $tmpquotation->setDateSentText($tmpquotation->dateSentText);
                        $tmpquotation->sender_id = $mycompany->id;

                        //echo "\n {$reciver}-{$key}";

                        //var_dump($tmpquotation->attributes);

                        if ($tmpquotation->save()) {

                            $path = sprintf('%s/upload/quotations/quot-%s/', Yii::getPathOfAlias('webroot'), $tmpquotation->id);

                            //echo "\n save path:".$path;

                            mkdir($path, 0777, true);
                            $filelist = array();
                            //var_dump($documents);

                            if (isset($documents) && count($documents) > 0) {
                                foreach ($documents as $doc => $document) {
                                    //var_dump($document->name);

                                    if ($document->saveAs($path . $document->name, false)) {
                                        $filelist[] = $document->name;
                                    }


                                }

                                $tmpquotation->documents = implode("|@|", $filelist);


                                if ($tmpquotation->save()) {
                                    //print "\nquotationdoc:". $tmpquotation->documents;


                                } else {
                                    foreach ($tmpquotation->getErrors() as $error) {
                                        $mess .= "<p>" . $error[0] . "</p>";
                                    }
                                    //var_dump($tmpquotation->getErrors());
                                }
                            }

                            $item = new SsnQuotationItem();
                            $item->attributes = $postitem;
                            $item->quotation_id = $tmpquotation->id;
                            $item->item_id = $postitem['refer_item_id'];
                            $item->price = $item->price;

                            if ($item->save()) {

                                $path = sprintf('%s/upload/quotations-items/item-%s/', Yii::getPathOfAlias('webroot'), $item->id);
                                //$oldumask = umask(0);
                                mkdir($path); // or even 01777 so you get the sticky bit set
                                //umask($oldumask);
                                //
                                $document_items = CUploadedFile::getInstancesByName('SsnQuotationItem[' . $key . '][documents]');

                                $filelist = array();

                                if (isset($document_items) && count($document_items) > 0) {
                                    // go through each uploaded image
                                    foreach ($document_items as $doc => $document) {

                                        if ($document->saveAs($path . $document->name,false)) {
                                            $filelist[] = $document->name;
                                        }
                                    }
                                    $item->documents = implode("|@|", $filelist);


                                }

                                if ($item->save()) {

                                    SendEmail::newQuotationRecipientNotify($tmpquotation);

                                } else {
                                    //print_r($item->getErrors());
                                    foreach ($item->getErrors() as $error) {
                                        $mess .= "<p>" . $error[0] . "</p>";
                                    }
                                }


                            } else {
                                foreach ($item->getErrors() as $error) {
                                    $mess .= "<p>" . $error[0] . "</p>";
                                }                            }

                        }else{
                            foreach ($item->getErrors() as $error) {
                                $mess .= "<p>" . $error[0] . "</p>";
                            }

                        }

                    }

                }

            }


            if($mess) {
                Yii::app()->user->setFlash('error', $mess);

            }else{
                Yii::app()->user->setFlash('success',"Create new quotation successfull.");
                $this->redirect(array('index'));
            }
        }




        $this->pageTitle = 'New Quotation';

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

        if(isset($_POST['SsnQuotation'])) {
            //buyer_id
            $reciver = $_POST['SsnQuotation']['reciver_id'][0];
            $mess ="";
            $model->attributes = $_POST['SsnQuotation'];
            $model->reciver_id = $reciver;
            $model->setDateSentText($model->dateSentText);
            //dateSentText

            $path = sprintf('%s/upload/quotations/quot-%s/', Yii::getPathOfAlias('webroot'), $model->id);
            !mkdir($path, 0777, true);
            $documents = CUploadedFile::getInstancesByName('documents');

            $filelist = array();
            if (isset($documents) && count($documents) > 0) {
                // go through each uploaded image
                foreach ($documents as $doc => $document) {
                    if ($document->saveAs($path . $document->name)) {
                        $filelist[] = $document->name;
                    }
                }
                $model->documents = implode("|@|", $filelist);
            }



            if($model->save()) {

            } else {
                foreach ($model->getErrors() as $error) {
                    $mess .= "<p>" . $error[0] . "</p>";
                }
                var_dump($model->getErrors());
                die;
            }

            //If one quotation...

            $item = SsnQuotationItem::model()->findByPk( $_POST['SsnQuotationItem']["id"] );
            $item->attributes = $_POST['SsnQuotationItem'];
            $item->item_id = $_POST['SsnQuotationItem']['refer_item_id'][0];//$item->refer_item_id[0];


            $path = sprintf('%s/upload/quotations-items/item-%s/', Yii::getPathOfAlias('webroot'), $item->id);
            //$oldumask = umask(0);
            mkdir($path); // or even 01777 so you get the sticky bit set
            //umask($oldumask);
            //
            $document_items = CUploadedFile::getInstancesByName('SsnQuotationItem[documents]');
            $filelist = array();

            if (isset($document_items) && count($document_items) > 0) {
                // go through each uploaded image
                foreach ($document_items as $doc => $document) {

                    if ($document->saveAs($path . $document->name)) {
                        $filelist[] = $document->name;
                    }
                }
                if(count($filelist) > 0 ) {
                    $item->documents = implode("|@|", $filelist);
                }

            }

            if ($item->save()) {

                SendEmail::updateQuotationRecipientNotify($model);

            } else {
                foreach ($item->getErrors() as $error) {
                    $mess .= "<p>" . $error[0] . "</p>";
                }
            }

            if($mess) {
                Yii::app()->user->setFlash('error', $mess);

            }else{
                Yii::app()->user->setFlash('success',"Quotation have bean update successfull.");
                $this->redirect(array('index'));
            }

        }


		$this->render('update',array(
			'model'=>$model,
		));
	}


	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($t=5)
	{

        $model = new SsnQuotation("search");

        //Sent: $model->sender_id = current_company_id
        //Reciver: $model->reciver_id = current_company_id
        //All $model->reciver_id = current_company_id or $model->sender_id = current_company_id

        $model->senderName= $_REQUEST[' SsnQuotation']['senderName'];

        $model->typemode = $t;

        $this->pageTitle = 'Quotations';

        $this->render('index', array(
            'model' => $model,
            'type'=>$t,
        ));

	}

    public function actionAddItem (){

        $index = isset($_POST['index']) ? $_POST['index'] : time();
        $item = new SsnQuotationItem();
        $this->renderPartial('_quotation-item', array(
            'index' => $index,
            'model' => $item,
        ), false, true);
    }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new SsnQuotation('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SsnQuotation']))
			$model->attributes=$_GET['SsnQuotation'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return SsnQuotation the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=SsnQuotation::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param SsnQuotation $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ssn-quotation-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

<?php

class EventsController extends AdminController {

    public $pluralTitle = 'Events';
    public $singleTitle = 'Event';
    public $cannotDelete = array();

    public function actionCreate() {
        try {
            $model = new TbEvents('create');
            if (isset($_POST['TbEvents'])) {
                $model->attributes = $_POST['TbEvents'];
                $model->start_date = DateHelper::toDbDateTimeFormatV2($model->start_date);
                $model->end_date = DateHelper::toDbDateTimeFormatV2($model->end_date);
                if ($model->save()) {
                    $model->saveImage('feature_image');
                    $this->setNotifyMessage(NotificationType::Success, $this->singleTitle . ' has been created');
                    $this->redirect(array('view', 'id' => $model->id));
                } else
                    $this->setNotifyMessage(NotificationType::Error, $this->singleTitle . ' cannot be created for some reasons');
            }
            $this->render('create', array(
                'model' => $model,
                'actions' => $this->listActionsCanAccess,
            ));
        } catch (exception $e) {
            Yii::log("Exception " . print_r($e, true), 'error');
            throw new CHttpException($e);
        }
    }

    public function actionDelete($id) {
        try {
            if (Yii::app()->request->isPostRequest) {
                // we only allow deletion via POST request
                if (!in_array($id, $this->cannotDelete)) {
                    if ($model = $this->loadModel($id)) {
                        $model->removeImage(array('feature_image'), true);
                        if ($model->delete())
                            Yii::log("Delete record " . print_r($model->attributes, true), 'info');
                    }

                    // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                    if (!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
                }
            } else {
                Yii::log("Invalid request. Please do not repeat this request again.");
                throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
            }
        } catch (Exception $e) {
            Yii::log("Exception " . print_r($e, true), 'error');
            throw new CHttpException($e);
        }
    }

    public function actionIndex() {
        try {
            $model = new TbEvents('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['TbEvents']))
                $model->attributes = $_GET['TbEvents'];

            $this->render('index', array(
                'model' => $model, 'actions' => $this->listActionsCanAccess,
            ));
        } catch (Exception $e) {
            Yii::log("Exception " . print_r($e, true), 'error');
            throw new CHttpException($e);
        }
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $model->start_date = DateHelper::toDateTimePickerFormat($model->start_date);
        $model->end_date = DateHelper::toDateTimePickerFormat($model->end_date);
        if (isset($_POST['TbEvents'])) {
            $model->attributes = $_POST['TbEvents'];
            $model->start_date = DateHelper::toDbDateTimeFormatV2($model->start_date);
            $model->end_date = DateHelper::toDbDateTimeFormatV2($model->end_date);
            if ($model->save()) {
                $model->saveImage('feature_image');
                $this->setNotifyMessage(NotificationType::Success, $this->singleTitle . ' has been updated');
                $this->redirect(array('view', 'id' => $model->id));
            } else
                $this->setNotifyMessage(NotificationType::Error, $this->singleTitle . ' cannot be updated for some reasons');
        }
        //$model->beforeRender();
        $this->render('update', array(
            'model' => $model,
            'actions' => $this->listActionsCanAccess,
            'title_name' => $model->title));
    }

    public function actionView($id) {
        try {
            $model = $this->loadModel($id);
            $this->render('view', array(
                'model' => $model,
                'actions' => $this->listActionsCanAccess,
                'title_name' => $model->title));
        } catch (Exception $exc) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
    }

    /*
     * Bulk delete
     * If you don't want to delete some specified record please configure it in global $cannotDelete variable
     */

    public function actionDeleteAll() {
        $deleteItems = $_POST['tb-events-grid_c0'];
        $shouldDelete = array_diff($deleteItems, $this->cannotDelete);

        if (!empty($shouldDelete)) {
            TbEvents::model()->deleteAll('id in (' . implode(',', $shouldDelete) . ')');
            $this->setNotifyMessage(NotificationType::Success, 'Your selected records have been deleted');
        } else
            $this->setNotifyMessage(NotificationType::Error, 'No records was deleted');

        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    public function loadModel($id) {
        //need this define for inherit model case. Form will render parent model name in control if we don't have this line
        $initMode = new TbEvents();
        $model = $initMode->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionDetail($slug) {
        $model = News::model()->getDetailBySlug($slug);
        $this->pageTitle = $model->title . ' - ' . Yii::app()->params['title'];
        $this->render('news_detail', array(
            'model' => $model
                )
        );
    }
    
    public function actionGetCities(){
        $cityId = 0;
        if(!isset($_POST['areaCode']))
            echo '';
        $areaCode = $_POST['areaCode'];
        $citiesData = MfCity::getListData($areaCode);
        if(!isset($_POST['cityId']) && !empty($_POST['cityId']))
            $cityId = $_POST['cityId'];
        $this->renderPartial('_template/_cities', array('citiesData' => $citiesData, 'cityId' => $cityId));
    }

}

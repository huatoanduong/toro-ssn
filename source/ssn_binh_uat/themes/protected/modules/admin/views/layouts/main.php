<?php
$this->bodyClass .= 'sidebar-mini skin-purple sidebar-collapse ';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo CHtml::encode($this->pageTitle) . ' - ' . Yii::app()->params['projectName'] . ' Admin'; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/css/AdminLTE.min.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/css/skins/skin-purple.min.css">
	<!-- Site style -->
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<?php
	Yii::app()->clientScript->scriptMap = array(
		'jquery.js' => Yii::app()->theme->baseUrl.'/plugins/jQuery/jQuery-2.1.4.min.js',
		'jquery.min.js' => Yii::app()->theme->baseUrl.'/plugins/jQuery/jQuery-2.1.4.min.js',
	);
	Yii::app()->clientScript->coreScriptPosition = CClientScript::POS_BEGIN;
	Yii::app()->clientScript->defaultScriptFilePosition = CClientScript::POS_END;
	
	Yii::app()->getClientScript()->registerCoreScript('jquery');
	Yii::app()->clientScript->registerScriptFile(
		Yii::app()->theme->baseUrl.'/plugins/bootstrap/js/bootstrap.min.js');
	Yii::app()->clientScript->registerScriptFile(
		Yii::app()->theme->baseUrl.'/js/app.min.js');
	Yii::app()->clientScript->registerScriptFile(
		Yii::app()->theme->baseUrl.'/js/script.js');
	?>
  </head>
  <body class="<?= $this->bodyClass ?>">
    <!-- Site wrapper -->
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo Yii::app()->createAbsoluteUrl('admin/site/index'); ?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><?php echo Yii::app()->params['projectName']; ?></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><?php echo Yii::app()->params['projectName']; ?></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="hidden-xs"><?php echo Yii::app()->user->name; ?></span>
					<span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
					<li><a href="<?php echo Yii::app()->createAbsoluteUrl('admin/manageadmin/change_my_password'); ?>">Change password</a></li>
					<li><a href="<?php echo Yii::app()->createAbsoluteUrl('admin/site/logout'); ?>">Logout</a></li>                </ul>
				</li>
            </ul>
          </div>
        </nav>
      </header>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
           <!-- sidebar menu: : style can be found in sidebar.less -->
			<?php
			$menu = new ShowAdminMenu();
			echo $menu->showMenu();
			?>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo CHtml::encode($this->pageTitle) ?>
          </h1>
				<?php $this->widget('zii.widgets.CBreadcrumbs', array(
					'links' => $this->breadcrumbs,
					'separator' => '',
					'homeLink'=> CHtml::tag('li', array(), CHtml::link('<i class="fa fa-dashboard"></i> Home', $this->createurl('/admin'))),
					'tagName' => 'ol',
					'activeLinkTemplate' => '<li><a href="{url}">{label}</a></li>',
					'inactiveLinkTemplate' => '<li class="active">{label}</li>',
					'htmlOptions'=>array('class'=>'breadcrumb')
				)); ?>
        </section>

        <!-- Main content -->
        <section class="content">
			<?php $this->widget('application.components.widget.Notification'); ?>
			<?= $content ?>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     </div><!-- ./wrapper -->
  </body>
</html>

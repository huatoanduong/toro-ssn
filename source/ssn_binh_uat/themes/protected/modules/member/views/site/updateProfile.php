<section class="content-header">
    <h1>
        Profile
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo Yii::app()->homeUrl ?>"><i class="fa fa-dashboard"></i> Home page</a></li>
        <li class="active">Profile</li>
    </ol>
</section>

<?php if(Yii::app()->user->hasFlash('success')):?>
<section class="content-header">
    <div class="alert alert-success" role="alert">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
</section>
<?php endif; ?>

<section class="content">
	<div class="box">
		<div class="box-body">
		<?php $this->renderPartial("_form", array(
			'comp' => $comp, 
			'user' => $user, 
		))?> 
		</div>	
	</div>
</section>
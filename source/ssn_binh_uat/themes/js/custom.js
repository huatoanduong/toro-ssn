var  currentIndex= 0;

window.ssn = {
	
	setupSampleDetailPage: function (newItemUrl, partnerTemplate) {

		// display dropdown item in company list
		function formatCompany (company) {

			if (!company.id || !(company.id in partnerTemplate)) { return company.text; }
			
			var $company = $(partnerTemplate[company.id]);
			return $company;

		};


		// setup company dropdown
		$(".company-select").select2({
            tags:true,
			placeholder: "Select company",
            createTag: function (tag) {

                 console.log("Lenght:"+$(".select2-results__options li[id]").length);
                 if($(".select2-results__options li[id]").length >0) {
                 }else{

                     return {
                           id: tag.term,
                           text: tag.term,
                           isNew : true };
                }

            },
            'val':$('.select2 option:eq(1)').val(),
	    	templateResult: formatCompany

		});
        ssn.setupItemList(newItemUrl);
	},
	
	addValidateRule: function (rule) {
		$(rule.inputs).each(function () {
			var inputId = this.id;
			var inputName = '';
			inputId.split('_').forEach(function(value, index, arr) {
				if (index==0) {
					inputName = value;
				} else {
					inputName += '['+value+']';
				}
			});
			var yiiRule = {
				'id': inputId,
				'inputID': inputId,
				'errorID': inputId + '_em_',
				'enableAjaxValidation': false,
				'clientValidation': ssn.createValidateChain(rule.validators)
			};
			$(this).closest('form').data('settings').attributes.push(yiiRule);	
		});
	},
	
	removeValidateRule: function (inputs) {
		$(inputs).each(function () {
			var inputID = this.id;
			var attributes = $(this).closest('form').data('settings').attributes;
			attributes.forEach(function (item, index, object) {
				if (item.inputID == inputID) {
					attributes.splice(index, 1);
				}
			});
		});
	},
	
	setupShippingDetailPage: function (newItemUrl, autoCompleteUrl) {
		// custom render for dropdown auto complete
		$('#SsnShipping_to_company').data( "autocomplete" )._renderItem = function( ul, item ) {
			return $( "<li></li>" )
				.data( "item.autocomplete", item )
				.append(item.label)
				.appendTo(ul);
		};

		// add token when select on auto complate
		$('#SsnShipping_to_company').on( "autocompleteselect", function( event, ui ) {
			event.preventDefault();
			if ($('.user-token input[value="'+ui.item.value+'"]').size()>0)
				return;
			$(this).val('');// clear auto complete textbox

			var companyHtml = $('.user-token-tpl').clone(true);
			companyHtml.find('.comp-name').text(ui.item.name+' ('+ui.item.address+')');
			companyHtml.find('input[type="hidden"]')
				.prop('disabled', false).val(ui.item.value);
			companyHtml.removeClass('user-token-tpl').removeClass('hide');
			$('.user-ids').empty().append(companyHtml);
		});


		// remove token
		$('.user-token').on('click', '.btn-remove', function() {
			$(this).closest('.user-token').remove();


		});

		// add validator for token field
		ssn.addValidateRule({
			inputs: '#SsnShipping_to_company',
			validators: [ssn.tokenRequireValidator]
		});

		ssn.setupItemList(newItemUrl);
	},

	setupItemList: function (newItemUrl) {
		// add new item
        /*
		$(".btn-add").on('click',function(){
            //alert("click");
            return;

			var last_item=$("tr.item").length-1;
			if($('#SsnSampleItem_n'+last_item+'_name').val()==''){
			$('#SsnSampleItem_n'+last_item+'_name').focus();
			return;
			}
			var newNo = parseInt($('.item:last').find('h4').text().replace('Item ', ''));
			var item_number = 'n'+newNo;
			$.ajax({ 
				type: "POST", 
				url: newItemUrl,    
				data: { index : item_number}, 
				success: function(text) { 
					$('.item-list tbody').append(text);
					ssn.updateItemList();
                    $('.item .btn-remove').addClass('invisible').prop('disabled', false);
                    $('.item .btn-remove').removeClass('invisible');
                    $('.item:last .btn-remove').addClass('invisible').prop('disabled', true);
				}
			 });
		});

		// remove item
		$('.item-list').on('click', '.btn-remove', function () {
			$(this).closest('.item').remove();
			ssn.updateItemList();
		});
		ssn.updateItemList();
        */

        $(".btn-add").on('click',function(){


            $('#error_noitems').hide();

            console.log("click :"+currentIndex);

             //$('.item-list tbody tr').length;

            indexNumber =  'N_' + currentIndex;

            console.log("NextItem:"+indexNumber);



            if($(this).text()=="Update"){
                console.log("customjs AddItem :"+indexNumber);
                //item-form-hidden
                //Update DataToTableRow
                editAbleId  = $(".current_show_form").attr("id").replace("item-form-hidden-id-","");

                if(ssn.rquireValidated(editAbleId)==false){
                    return;
                }

                item_name = $('#SsnSampleItem_'+editAbleId+'_name').val();
                item_color = $('#SsnSampleItem_'+editAbleId+'_color').val();
                item_qty  = $('#SsnSampleItem_'+editAbleId+'_quantity').val();
                //SsnSampleItem_4_quantity
                item_unit = $('#SsnSampleItem_'+editAbleId+'_unit').val();

                item_doc_name = 'No document uploaded.';

                if($("#SsnSampleItem_"+editAbleId+"_document").val()!=""){
                    item_doc_name =$("#SsnSampleItem_"+editAbleId+"_document").val();
                }

                $("#tr-row-item-id-"+ editAbleId +" .item-name").text(item_name);
                $("#tr-row-item-id-"+ editAbleId +" .item-color").text(item_color);
                $("#tr-row-item-id-"+ editAbleId +" .item-quantity").text(item_qty);
                $("#tr-row-item-id-"+ editAbleId +" .item-unit").text(item_unit);

                $("#tr-row-item-id-"+ editAbleId +" .item-document-url").text(item_doc_name);

                $(".current_show_form").addClass('item-form-hidden');
                $(".current_show_form").removeClass('current_show_form');

                $("#item-form-hidden-id-"+ indexNumber).removeClass('item-form-hidden');
                $("#item-form-hidden-id-"+ indexNumber).addClass('current_show_form');
                //current_show_form

                $(this).text("Add");
                return;
            }

            if(ssn.rquireValidated(indexNumber)==false){
                return;
            }

            item_name = $('#SsnSampleItem_'+indexNumber+'_name').val();
            item_color = $('#SsnSampleItem_'+indexNumber+'_color').val();
            item_qty  = $('#SsnSampleItem_'+indexNumber+'_quantity').val();
            //SsnSampleItem_4_quantity
            item_unit = $('#SsnSampleItem_'+indexNumber+'_unit').val();

            item_doc_name = 'No document uploaded.';
            if($("#SsnSampleItem_"+indexNumber+"_document").val()!=""){
                item_doc_name =$("#SsnSampleItem_"+indexNumber+"_document").val();
            }


            text = '<tr class="item  row-item-id" id="tr-row-item-id-'+indexNumber+'" data-item-id="'+indexNumber+'"> ' +
            '<td> <div class="row-container"> ' +
            '<p class="item-name">' + item_name +
            '</p> </div> <div class="row"> ' +
            '<div class="col-xs-4 notranslate item-color"> '+item_color+' </div> ' +
            '<div class="col-xs-4 notranslate item-quantity"> '+item_qty+'  </div> ' +
            '<div class="col-xs-4 notranslate item-unit"> '+item_unit+'  </div> ' +
            '</div></td> <td style="vertical-align:middle; text-align: center"> ' +
            '<div class="row">' +
            '<a href="javascript:void(0);" class="btn-edit"> <span class="glyphicon glyphicon-pencil"></span> </a> ' +
            '<a href="javascript:void(0);" class="btn-remove">' +
            '<span style="color: red; margin-left: 10px;" class="glyphicon glyphicon-remove"></span> </a> ' +
            '</div><div class="row">' +
            '<div class=" notranslate item-document-url"> '+item_doc_name+' </div> </div> ' +

            '</td> </tr>';
             //AppendView----

            $('.item-list tbody').append(text);
            //AppendForm

            var new_item_number =  currentIndex + 1;

            $.ajax({
                type: "POST",
                url: newItemUrl,
                data: { index : 'N_' + new_item_number},
                success: function(text) {

                    //up_row
                    currentIndex = currentIndex + 1;

                    console.log("ajax :"+new_item_number);
                    //$('.item-list tbody').append(text);
                    newform ='<div class="row-container  div-row-add-new current_show_form" ' +
                    'id="item-form-hidden-id-N_'+new_item_number+
                    '">'+ text + '</div>';

                    //item-form-hidden
                    $("#item-form-hidden-id-"+ indexNumber).addClass('item-form-hidden');
                    //current_show_form
                    $("#item-form-hidden-id-"+ indexNumber).removeClass('current_show_form');

                    //current_show_form item-form-hidden

                    $("#list-form-items").append(newform);
                    ssn.updateItemList();
                    //$('.item .btn-remove').addClass('invisible').prop('disabled', false);
                    //$('.item .btn-remove').removeClass('invisible');
                    //$('.item:last .btn-remove').addClass('invisible').prop('disabled', true);
                }
            });

            //////////////


        });


        $('.btn-remove-file').on('click', function(){
            filename = $(this).data("item-filename");
            console.log(filename);
            $(this).closest('.document-link').remove();
            $("#ssn-company-item-form").append("<input type='hidden' name='remove-file[]' value='"+filename+"'>");

            return false;
        });

        var current_edit_id ="";

        $('.item-list').on('click', '.btn-edit', function () {

            $('.errorMessage').hide();
            //$(this).closest('.item').remove();
            //alert("edit");
            item_id = $(this).closest('.item').data("item-id");
            current_edit_id = item_id;
            //item-form-hidden-id-N_7
            //$("#item-form-hidden-id-N_7")
            //current_show_form
            $(".current_show_form").addClass('item-form-hidden');
            $(".current_show_form").removeClass('current_show_form');

            $("#item-form-hidden-id-"+ item_id).removeClass('item-form-hidden');
            $("#item-form-hidden-id-"+ item_id).addClass('current_show_form');
            $("#AddItem").text("Update");

            ssn.updateItemList();
        });

        $('.item-list').on('click', '.btn-remove', function () {
            item_id = $(this).closest('.item').data("item-id");
            $("#item-form-hidden-id-"+ item_id).remove();
            $(this).closest('.item').remove();
            ssn.updateItemList();
            if(current_edit_id == item_id){

                $(".btn-add").text("Add");
                new_item_number = currentIndex + 1;
                $.ajax({
                    type: "POST",
                    url: newItemUrl,
                    data: { index : 'N_' + new_item_number},
                    success: function(text) {
                        //up_row
                        currentIndex = currentIndex + 1;

                        console.log("ajax :"+new_item_number);
                        //$('.item-list tbody').append(text);
                        newform ='<div class="row-container  div-row-add-new current_show_form" ' +
                        'id="item-form-hidden-id-N_'+new_item_number+
                        '">'+ text + '</div>';

                        //item-form-hidden
                        $("#item-form-hidden-id-"+ indexNumber).addClass('item-form-hidden');
                        //current_show_form
                        $("#item-form-hidden-id-"+ indexNumber).removeClass('current_show_form');

                        $("#list-form-items").append(newform);

                        ssn.updateItemList();
                    }
                });
            }



        });

	},
	
	updateItemList: function () {
		var no = 1;
		$('.div-row-add-new').each(function () {
			$(this).find('h4').text('Item '+no);
			no++;
		});
		// $('.item:last .btn-remove').addClass('invisible').prop('disabled', true);
		// clear all input's rules of all items
		ssn.removeValidateRule('.div-row-add-new input');
		
		// add new rules
		var rules = [
			{
				inputs: '.div-row-add-new input[type="text"]',
				validators: [ssn.requireValidator]
			},
			{
				inputs: '.div-row-add-new .qty',
				validators: [ssn.numberValidator]
			},
			{
				inputs: '.div-row-add-new input[type="file"]',
				validators: [ssn.docTypeValidator]
			}
		];
		rules.forEach(function(value, index, arr) {
            //console.log(value);
			//ssn.addValidateRule(value );
            ssn.createValidateChain(value);
		});


	},

    rquireValidated:function(indexNumber){

        item_name = $('#SsnSampleItem_'+indexNumber+'_name').val();
        item_color = $('#SsnSampleItem_'+indexNumber+'_color').val();
        item_qty  = $('#SsnSampleItem_'+indexNumber+'_quantity').val();
        //SsnSampleItem_4_quantity
        item_unit = $('#SsnSampleItem_'+indexNumber+'_unit').val();

        item_doc_name = '';
        if($("#SsnSampleItem_"+indexNumber+"_document").val()!=""){
            item_doc_name =$("#SsnSampleItem_"+indexNumber+"_document").val();
        }

        //Validate Before Add To TableList
        error =0;
        if (item_name=='') {
            ssn.show_error_message('#SsnSampleItem_'+indexNumber+'_name',"You can not leave this field empty.");
            error =1;
        }
        if(item_color==''){
            ssn.show_error_message('#SsnSampleItem_'+indexNumber+'_color',"You can not leave this field empty.");
            error =1;
        }
        if (isNaN(item_qty) || item_qty =='' ) {
            ssn.show_error_message('#SsnSampleItem_'+indexNumber+'_quantity',"Minimum value is 1.");
            error =1;
        }

        /*
        if(item_unit==''){
            ssn.show_error_message('#SsnSampleItem_'+indexNumber+'_unit',"You can not leave this field empty.");
            error =1;
        }
        */

        if(item_doc_name!='') {
            var ext = item_doc_name.substr(item_doc_name.lastIndexOf('.') + 1);
            // Bo va cho up tat ca cac dinh dang.
            if ($.inArray(ext, ['doc', 'docx', 'xls', 'xlsx', 'pdf','jpg','png','bmp','jpeg','jpf','bitmap','gif']) == -1) {
                ssn.show_error_message('#SsnSampleItem_'+indexNumber+'_document',"Only allow file type: doc,docx,xls,xlsx,pdf,jpg,png,bmp,jpeg,jpf,bitmap,gif.");
                error=1;
            }

        }
        /*if(item_doc_name==''){

            ssn.show_error_message('#SsnSampleItem_'+indexNumber+'_document',"Only allow file type: *.doc, *.docx, *.xls, *.xlsx, *.pdf.");
            error =1;
        }*/

        if( error ==1){
            return false;
        }

        $('.errorMessage').hide();

        return true;

    },
    show_error_message: function(element_id,message){

        $(element_id).next('.errorMessage').html(message);
        $(element_id).next('.errorMessage').show();

    },

	requireValidator: function (value, messages, attribute) {
		if (value=='') {
			messages.push("You can not leave this field empty.");
			return false;
		}
		return true;
	},

	numberValidator: function (value, messages, attribute) {
		if (isNaN(value)) {
			messages.push("Minimum value is 1.");
			return false;
		}
		return true;
	},

	docTypeValidator: function (value, messages, attribute) {
		var input = $('#'+attribute.inputID);
		if (input.size()<1 || input[0].files.length<1) {
			return true;
		}
		var f = input[0].files[0];
		var ext = f.name.substr(f.name.lastIndexOf('.')+1);
        console.log(ext);
        if ($.inArray(ext, ['doc','docx','xls','xlsx','pdf','jpg','png','bmp','jpeg','jpf','bitmap','gif']) == -1) {
			messages.push('Only allow file type: doc,docx,xls,xlsx,pdf,jpg,png,bmp,jpeg,jpf,bitmap,gif.');
			return false;
		}
		
		return true;

        //'jpg','png','bmp','jpeg','jpf','bitmap','gif'
	},
	
	tokenRequireValidator: function (value, messages, attribute) {
		if ($('#'+attribute.inputID).parent().find('.user-token').size()<2) {
			messages.push('You have to choose at least one partner.');
			return false;
		}
	},

	createValidateChain: function (rules) {
		return function (value, messages, attribute) {
			rules.every(function (valFunc, index, arr) {
				return valFunc(value, messages, attribute);
			});
		};
	}

};

$(function() {


    $("#ssn-sample-form").submit(function() {


        indexNumber = 'N_' + currentIndex;
        //$("#item-form-hidden-id-"+ indexNumber).remove();
        //$(".current_show_form").remove();
        if( $('.item-list tbody tr').length == 0){
            $('#error_noitems').show();
            return false;
        }
        //valid email here
        var mess ="";
        $("#SsnSample_toUserIds option").each(function()
        {
            // Add $(this).val() to your listvar
            if(!$.isNumeric($(this).val())){
                pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
                //console.log($(this).val());
                //console.log(pattern.test($(this).val()));
                mess = mess + $(this).val() + " ";
            }

        });
        if(mess != "") {
            $("#error_nofrom").html(mess + " emails are invalid.");
            $("#error_nofrom").show();
            return false;
        }

    });

    $('.index-sample-row').hover( function(){
           $(this).css('background-color', '#d8d8d8');
            item_id = $(this).data("item-id");
            $('.linkable-sub-'+item_id).css('background-color', '#d8d8d8');
            //background-color: #d8d8d8;

        },
        function(){
            item_id = $(this).data("item-id");

            $('.linkable-sub-'+item_id).css('background-color', '#fff');
            $(this).css('background-color', '#fff');
            //background-color: #f5f5f5;

        });

    //
    $('.linkable-sub').hover( function(){
            // $(this).css('background-color', '#F00');
            item_id = $(this).data("item-id");
            $('.linkable-sub-'+item_id).css('background-color', '#d8d8d8');
            //background-color: #d8d8d8;
            $('.row-item-'+item_id).css('background-color', '#d8d8d8');
        },
        function(){
            item_id = $(this).data("item-id");
            $('.linkable-sub-'+item_id).css('background-color', '#fff');
            //$(this).css('background-color', '#000');
            //background-color: #f5f5f5;
            $('.row-item-'+item_id).css('background-color',  '#fff');

        });



});
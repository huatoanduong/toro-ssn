var  currentIndex= 0;

var listItem = null;
$(document).ready(function () {
    window.ssn = {

        setupQuotaionIitems: function (newItemUrl, itemTemplate) {
            // setup company dropdown

            selections = $(".select_item_name").select2({
                placeholder: "Select item",
                'val': $('.select2 option:eq(1)').val()
            });
            //var selections =  $(".select_item_name").select2('data') ;
            //
            //$.each(selections, function() {
            //    console.log(this.id);
            //    // I here set a hidden field with the ids in the order they were selected
            //});
        },

        setupQuotaionDetailPage: function (newItemUrl, partnerTemplate) {

            // display dropdown item in company list
            function formatCompany(company) {

                if (!company.id || !(company.id in partnerTemplate)) {
                    return company.text;
                }

                var $company = $(partnerTemplate[company.id]);
                return $company;

            };


            // setup company dropdown
            $(".company-select").select2({
                tags: true,
                placeholder: "Select company",
                createTag: function (tag) {

                    console.log("Lenght:" + $(".select2-results__options li[id]").length);
                    if ($(".select2-results__options li[id]").length > 0) {
                    } else {

                        return {
                            id: tag.term,
                            text: tag.term,
                            isNew: true
                        };
                    }

                },
                'val': $('.select2 option:eq(1)').val(),
                templateResult: formatCompany

            });
            ssn.setupItemList(newItemUrl);
        },

        addValidateRule: function (rule) {
            $(rule.inputs).each(function () {
                var inputId = this.id;
                var inputName = '';
                inputId.split('_').forEach(function (value, index, arr) {
                    if (index == 0) {
                        inputName = value;
                    } else {
                        inputName += '[' + value + ']';
                    }
                });
                var yiiRule = {
                    'id': inputId,
                    'inputID': inputId,
                    'errorID': inputId + '_em_',
                    'enableAjaxValidation': false,
                    'clientValidation': ssn.createValidateChain(rule.validators)
                };
                $(this).closest('form').data('settings').attributes.push(yiiRule);
            });
        },

        removeValidateRule: function (inputs) {
            //$(inputs).each(function () {
            //	var inputID = this.id;
            //	var attributes = $(this).closest('form').data('settings').attributes;
            //	attributes.forEach(function (item, index, object) {
            //		if (item.inputID == inputID) {
            //			attributes.splice(index, 1);
            //		}
            //	});
            //});

        },


        setupItemList: function (newItemUrl) {


            $(".btn-add").on('click', function () {
                //e.preventDefault();

                $('#error_noitems').hide();

                console.log("click :" + currentIndex);

                //$('.item-list tbody tr').length;

                indexNumber = 'N_' + currentIndex;

                console.log("NextItem:" + indexNumber);


                if ($(this).text() == "Update") {
                    console.log("AddItem:" + indexNumber);
                    //item-form-hidden
                    //Update DataToTableRow
                    editAbleId = $(".current_show_form").attr("id").replace("item-form-hidden-id-", "");

                    if (ssn.rquireValidated(editAbleId) == false) {
                        return;
                    }


                    item_name = $('#SsnQuotationItem_' + editAbleId + '_item_name').val();

                    item_price = $('#SsnQuotationItem_' + editAbleId + '_price').val();
                    //item_qty  = $('#SsnQuotationItem_'+editAbleId+'_quantity').val();
                    //SsnQuotationItem_
                    item_unit = $('#SsnQuotationItem_' + editAbleId + '_unit').val();

                    //SsnQuotationItem_dateSentText
                    item_exp = $('#SsnQuotationItem_' + editAbleId + '_dateSentText').val();
                    item_doc_name = 'No document uploaded.';


                    //item_id .select2("val", "value to select");
                    item_id = $('#SsnQuotationItem_' + editAbleId + '_refer_item_id').val();
                    selections = $(".select_item_name").select2('val', item_id);

                    $.each(selections, function () {
                        console.log(this.id);
                        console.log(this.text);
                        item_name = this.text;
                        item_id = this.id;
                        //$("#SsnQuotationItem_N_0_item_name").va
                        $('#SsnQuotationItem_' + editAbleId + '_item_name').val(item_name);
                        $('#SsnQuotationItem_' + editAbleId + '_refer_item_id').val(item_id);
                    });


                    $("#tr-row-item-id-" + editAbleId + " .item-name").text(item_name);
                    //$("#tr-row-item-id-"+ editAbleId +" .item-item_name").text(item_name);

                    $("#tr-row-item-id-" + editAbleId + " .item-price").text(item_price);
                    $("#tr-row-item-id-" + editAbleId + " .item-unit").text(item_unit);

                    $("#tr-row-item-id-" + editAbleId + " .item-dateSentText").text(item_exp);

                    //item_exp =  $('#SsnQuotationItem_'+editAbleId+'_dateSentText').val();


                    if ($("#SsnQuotationItem_" + editAbleId + "_documents").val() != "") {
                        item_doc_name = $("#SsnQuotationItem_" + editAbleId + "_documents").val();
                        $("#tr-row-item-id-" + editAbleId + " .item-document-url").text(item_doc_name);
                    }

                    $(".current_show_form").addClass('item-form-hidden');
                    $(".current_show_form").removeClass('current_show_form');

                    $("#item-form-hidden-id-" + indexNumber).removeClass('item-form-hidden');
                    $("#item-form-hidden-id-" + indexNumber).addClass('current_show_form');
                    //current_show_form

                    $(this).text("Add");
                    return;
                }

                if (ssn.rquireValidated(indexNumber) == false) {
                    return;
                }

                //item_name = $('#SsnQuotationItem_'+indexNumber+'_item_name').val();
                //item_name = $('#SsnQuotationItem_'+indexNumber+'_item_name').val();
                item_name = "";
                item_id = "";
                selections = $(".select_item_name").select2('data');
                $.each(selections, function () {
                    console.log(this.id);
                    console.log(this.text);
                    item_name = this.text;
                    item_id = this.id;
                    //$("#SsnQuotationItem_N_0_item_name").va
                    $('#SsnQuotationItem_' + indexNumber + '_item_name').val(item_name);
                    $('#SsnQuotationItem_' + indexNumber + '_refer_item_id').val(item_id);
                    // I here set a hidden field with the ids in the order they were selected
                });

                item_price = $('#SsnQuotationItem_' + indexNumber + '_price').val();
                item_unit = $('#SsnQuotationItem_' + indexNumber + '_unit').val();

                item_exp = $('#SsnQuotationItem_' + indexNumber + '_dateSentText').val();

                item_doc_name = 'No document uploaded.';
                if ($("#SsnQuotationItem_" + indexNumber + "_documents").val() != "") {
                    item_doc_name = $("#SsnQuotationItem_" + indexNumber + "_documents").val();
                }

                //$("#tr-row-item-id-"+ editAbleId +" .item-dateSentText").text(item_exp);

                text = '<tr class="item  row-item-id" id="tr-row-item-id-' + indexNumber + '" data-item-id="' + indexNumber + '"> ' +
                '<td> <div class="row-container"> ' +
                '<p class="item-item_name">' + item_name +
                '</p> </div> <div class="row"> ' +
                '<div class="col-xs-4 notranslate item-price"> ' + item_price + ' </div> ' +
                '<div class="col-xs-4 notranslate item-unit"> ' + item_unit + '  </div> ' +
                '<div class="col-xs-4 notranslate item-dateSentText"> ' + item_exp + '  </div> ' +
                '</div></td> <td style="vertical-align:middle; text-align: center"> ' +
                '<div class="row">' +
                '<a href="javascript:void(0);" class="btn-edit"> <span class="glyphicon glyphicon-pencil"></span> </a> ' +
                '<a href="javascript:void(0);" class="btn-remove">' +
                '<span style="color: red; margin-left: 10px;" class="glyphicon glyphicon-remove"></span> </a> ' +
                '</div><div class="row">' +
                '<div class=" notranslate item-document-url"> ' + item_doc_name + ' </div> </div> ' +

                '</td> </tr>';
                //AppendView----

                $('.item-list tbody').append(text);
                //AppendForm

                var new_item_number = currentIndex + 1;

                $.ajax({
                    type: "POST",
                    url: newItemUrl,
                    data: {index: 'N_' + new_item_number},
                    success: function (text) {

                        //up_row
                        currentIndex = currentIndex + 1;

                        console.log("ajax :" + new_item_number);
                        //$('.item-list tbody').append(text);
                        newform = '<div class="row-container  div-row-add-new current_show_form" ' +
                        'id="item-form-hidden-id-N_' + new_item_number +
                        '">' + text + '</div>';

                        //item-form-hidden
                        $("#item-form-hidden-id-" + indexNumber).addClass('item-form-hidden');
                        //current_show_form
                        $("#item-form-hidden-id-" + indexNumber).removeClass('current_show_form');

                        $("#list-form-items").append(newform);
                        ssn.updateItemList();
                        $('.datepickerform').datepicker({'dateFormat':'dd/mm/yy'});
                    }
                });

                //////////////


            });

            $('.item-list').on('click', '.btn-edit', function () {

                $('.errorMessage').hide();
                //$(this).closest('.item').remove();
                //alert("edit");
                item_id = $(this).closest('.item').data("item-id");

                //current_show_form
                $(".current_show_form").addClass('item-form-hidden');
                $(".current_show_form").removeClass('current_show_form');

                $("#item-form-hidden-id-" + item_id).removeClass('item-form-hidden');
                $("#item-form-hidden-id-" + item_id).addClass('current_show_form');
                $("#AddItem").text("Update");

                editAbleId = $(".current_show_form").attr("id").replace("item-form-hidden-id-", "");

                item_id = $('#SsnQuotationItem_' + editAbleId + '_refer_item_id').val();

                //$(".select_item_name").select2('val',item_id);
                // $(".select_item_name").select2('val',item_id);

                ssn.updateItemList();
            });

            $('.item-list').on('click', '.btn-remove', function () {
                item_id = $(this).closest('.item').data("item-id");
                $("#item-form-hidden-id-" + item_id).remove();
                $(this).closest('.item').remove();
                ssn.updateItemList();
            });

        },

        updateItemList: function () {
            var no = 1;
            $('.div-row-add-new').each(function () {
                $(this).find('h4').text('Item ' + no);
                no++;
            });
            // $('.item:last .btn-remove').addClass('invisible').prop('disabled', true);
            // clear all input's rules of all items
            ssn.removeValidateRule('.div-row-add-new input');

            // add new rules
            var rules = [
                {
                    inputs: '.div-row-add-new input[type="text"]',
                    validators: [ssn.requireValidator]
                },
                {
                    inputs: '.div-row-add-new .qty',
                    validators: [ssn.numberValidator]
                },
                {
                    inputs: '.div-row-add-new input[type="file"]',
                    validators: [ssn.docTypeValidator]
                }
            ];
            rules.forEach(function (value, index, arr) {
                //console.log(value);
                //ssn.addValidateRule(value );
                ssn.createValidateChain(value);
            });


        },

        rquireValidated: function (indexNumber) {


            item_name = $('#SsnQuotationItem_' + indexNumber + '_item_name').val();
            item_price = $('#SsnQuotationItem_' + indexNumber + '_price').val();
            //SsnSampleItem_4_quantity
            item_unit = $('#SsnQuotationItem_' + indexNumber + '_unit').val();

            //item_doc_name = '';

            //if($("#SsnQuotationItem_"+indexNumber+"_documents").val()!=""){
            //    item_doc_name =$("#SsnQuotationItem_"+indexNumber+"_documents").val();
            //}

            //Validate Before Add To TableList
            error = 0;
            //
            //if (item_name=='') {
            //    ssn.show_error_message('#SsnQuotationItem_'+indexNumber+'_item_name',"You can not leave this field empty.");
            //    error =1;
            //}

            if (item_price == '') {
                ssn.show_error_message('#SsnQuotationItem_' + indexNumber + '_price', "You can not leave this field empty.");
                error = 1;
            }

            if (item_unit == '') {
                ssn.show_error_message('#SsnQuotationItem_' + indexNumber + '_unit', "You can not leave this field empty.");
                error = 1;
            }


            //if(item_doc_name!='') {
            //    var ext = item_doc_name.substr(item_doc_name.lastIndexOf('.') + 1);
            //    // Bo va cho up tat ca cac dinh dang.
            //    if ($.inArray(ext, ['doc', 'docx', 'xls', 'xlsx', 'pdf','jpg','png','bmp','jpeg','jpf','bitmap','gif']) == -1) {
            //        ssn.show_error_message('#SsnQuotationItem_'+indexNumber+'_documents',"Only allow file type: doc,docx,xls,xlsx,pdf,jpg,png,bmp,jpeg,jpf,bitmap,gif.");
            //        error=1;
            //    }
            //
            //}

            /*if(item_doc_name==''){

             ssn.show_error_message('#SsnSampleItem_'+indexNumber+'_document',"Only allow file type: *.doc, *.docx, *.xls, *.xlsx, *.pdf.");
             error =1;
             }*/

            if (error == 1) {
                return false;
            }

            $('.errorMessage').hide();

            return true;

        },
        show_error_message: function (element_id, message) {

            $(element_id).next('.errorMessage').html(message);
            $(element_id).next('.errorMessage').show();

        },

        requireValidator: function (value, messages, attribute) {
            if (value == '') {
                messages.push("You can not leave this field empty.");
                return false;
            }
            return true;
        },

        numberValidator: function (value, messages, attribute) {
            if (isNaN(value)) {
                messages.push("Minimum value is 1.");
                return false;
            }
            return true;
        },

        docTypeValidator: function (value, messages, attribute) {
            var input = $('#' + attribute.inputID);
            if (input.size() < 1 || input[0].files.length < 1) {
                return true;
            }
            var f = input[0].files[0];
            var ext = f.name.substr(f.name.lastIndexOf('.') + 1);
            console.log(ext);
            if ($.inArray(ext, ['doc', 'docx', 'xls', 'xlsx', 'pdf', 'jpg', 'png', 'bmp', 'jpeg', 'jpf', 'bitmap', 'gif']) == -1) {
                messages.push('Only allow file type: doc,docx,xls,xlsx,pdf,jpg,png,bmp,jpeg,jpf,bitmap,gif.');
                return false;
            }

            return true;

            //'jpg','png','bmp','jpeg','jpf','bitmap','gif'
        },

        tokenRequireValidator: function (value, messages, attribute) {
            if ($('#' + attribute.inputID).parent().find('.user-token').size() < 2) {
                messages.push('You have to choose at least one partner.');
                return false;
            }
        },

        createValidateChain: function (rules) {
            return function (value, messages, attribute) {
                rules.every(function (valFunc, index, arr) {
                    return valFunc(value, messages, attribute);
                });
            };
        }

    };

    $(function () {


        $("#ssn-quotation-form").submit(function () {

            indexNumber = 'N_' + currentIndex;

            if ($('.item-list tbody tr').length == 0) {

                $('#error_noitems').show();
                return false;

            }

        });

        $('.index-sample-row').hover(function () {
                $(this).css('background-color', '#d8d8d8');
                item_id = $(this).data("item-id");
                $('.linkable-sub-' + item_id).css('background-color', '#d8d8d8');
                //background-color: #d8d8d8;

            },
            function () {
                item_id = $(this).data("item-id");

                $('.linkable-sub-' + item_id).css('background-color', '#fff');
                $(this).css('background-color', '#fff');
                //background-color: #f5f5f5;

            });

        //
        $('.linkable-sub').hover(function () {
                // $(this).css('background-color', '#F00');
                item_id = $(this).data("item-id");
                $('.linkable-sub-' + item_id).css('background-color', '#d8d8d8');
                //background-color: #d8d8d8;
                $('.row-item-' + item_id).css('background-color', '#d8d8d8');
            },
            function () {
                item_id = $(this).data("item-id");
                $('.linkable-sub-' + item_id).css('background-color', '#fff');
                //$(this).css('background-color', '#000');
                //background-color: #f5f5f5;
                $('.row-item-' + item_id).css('background-color', '#fff');

            });

    });

});
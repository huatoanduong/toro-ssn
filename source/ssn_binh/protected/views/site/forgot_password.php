<?php
$this->bodyClass .= 'login-page ';


?>

<div class="login-box">

    <div class="login-logo">
        <a href="<?php echo Yii::app()->createAbsoluteUrl('site/updateProfile')?>"><b>SSN</b></a>
    </div>

	<div class="login-box-body">
		<p class="login-box-msg">Type in your email to reset password</p>

		<?php $this->widget('application.components.widget.Notification'); ?>

		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'forgot_password-form',
			'enableClientValidation' => true,
			'enableAjaxValidation' => false,
			'clientOptions' => array( 'validateOnSubmit' => true, ),
			'htmlOptions'=>array('class'=>'form-horizontal', 'role'=>'form'),
		)); ?>
		<div class="form-group">
                <?php echo $form->labelEx($model,'email', array('class'=>'col-sm-3 control-label')); ?>

			<div class="col-sm-9">
				<?php echo $form->textField($model,'email', array('class'=>'form-control','placeholder'=>'Email')); ?>
				<?php echo $form->error($model,'email'); ?>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-9">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
		</div>
		<?php $this->endWidget(); ?>		
	</div>
</div>
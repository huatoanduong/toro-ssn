<div class="item clearfix toan">    
    <div class="image"><img src="<?php echo $data->getImageUrl()?>" alt="<?php echo $data->title;?>" /></div>
    <div class="description">
        <h4><a href="#"><?php echo $data->title;?></a></h4>
        <p><?php echo $data->short_content;?></p>
        <p class="more"><a href="<?php echo Yii::app()->createAbsoluteUrl('events/eventDetail', array('slug' => $data->slug))?>">read more</a></p>
    </div>
</div>
<?php

/*
 * @inheritdoc
 */

class SsnSampleSearch extends SsnSample {

	const MODE_SENT = 'sent';
	const MODE_RECV = 'recv';
	const MODE_ALL = 'all';

	public $mode;
	public $user_id;
	public $key;
	public $dateRange = 1;
	public $fromDate;
	public $toDate;
	public $pagination;
	public static $DATE_RANGES = array(
		1 => 'Latest 7 days',
		2 => 'Current month',
		3 => 'Latest 3 months',
		4 => 'Range',
	);

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dateRange, user_id', 'numerical', 'integerOnly' => true),
			array('key, fromDate, toDate, remark, created_date, updated_date', 'safe'),
		);
	}

	/**
	 * @return SsnSample[]
	 */
	public function search() {
		$criteria = new CDbCriteria;

		$fromDate = $toDate = null;
		switch ($this->dateRange) {
			case 1: // 7 day
				$fromDate = date('Y-m-d', strtotime('-7 days'));
				$toDate = date('Y-m-d');
				break;

			case 2: // cur month
				$fromDate = date('Y-m-1');
				$toDate = date("Y-m-t");
				break;

			case 3: // 3 months
				$fromDate = date('Y-m-1', strtotime('-3 months'));
				$toDate = date("Y-m-t");
				break;

			case 4: // range
				if ($this->fromDate)
					$fromDate = DateHelper::toDbDateFormat($this->fromDate);

				if ($this->toDate)
					$toDate = DateHelper::toDbDateFormat($this->toDate);
				break;

			default:
				break;
		}

		if ($fromDate)
			$criteria->addCondition("send_date >= '$fromDate'");

		if ($toDate && $this->dateRange==4) {
            $criteria->addCondition("send_date <= '$toDate'");

        }

		if ($this->key) {
			$comp = SsnCompany::model()->tableName();
			$conds = array("awbn LIKE :key OR remark LIKE :key");
			if ($this->canShowSender()) {
				$conds[] = "EXISTS (SELECT id FROM $comp c WHERE c.name LIKE :key AND c.user_id=t.from_user_id)";
			}

			if ($this->canShowRecipient()) {
				$conds[] = "EXISTS (
					select su.user_id 
					from {{_ssn_sample_to_user}} su
					where su.sample_id=t.id and su.user_id in (
						select c.user_id
						from $comp c
						where c.name like :key and c.user_id=su.user_id
					)
				)";
			}
			
			$criteria->addCondition(implode(' OR ', $conds));
			$criteria->params[':key'] = "%{$this->key}%";
		}

		// search by sender & recipient
		$userId = $this->user_id;
		$conds = array();
		if ($this->canShowSender()) {
			$conds[] = "exists (SELECT id "
					. "FROM {{_ssn_sample_to_user}} "
					. "WHERE sample_id=t.id and user_id=$userId)";
		}
		
		if ($this->canShowRecipient()) {
			$conds[] = "from_user_id=$userId";
		}
		
		$criteria->addCondition(implode(' OR ', $conds));
		$criteria->order = 'send_date DESC, created_date DESC';

		$pagination = new CPagination(self::model()->count($criteria));
		$pagination->pageSize = 20;
		$pagination->applyLimit($criteria);
		$this->pagination = $pagination;

		return self::model()->findAll($criteria);
	}

	public function searchAdmin() {
		$criteria = new CDbCriteria;

		$fromDate = '1970-1-1';
		$toDate = '2999-1-1';
		if ($this->fromDate) {
			$fromDate = DateHelper::toDbDateFormat($this->fromDate);
		}

		if ($this->toDate) {
			$toDate = DateHelper::toDbDateFormat($this->toDate);
		}
		$criteria->addCondition("send_date BETWEEN '$fromDate' AND '$toDate'");
		
		if ($this->key) {
			$comp = SsnCompany::model()->tableName();
			$cond = "awbn LIKE :key OR remark LIKE :key ";
			$cond .= "OR EXISTS (SELECT id FROM $comp c WHERE c.name LIKE :key AND c.user_id=t.from_user_id)";
			$cond .= "OR EXISTS (
				select su.user_id 
				from {{_ssn_sample_to_user}} su
				where su.sample_id=t.id and su.user_id in (
					select c.user_id
					from $comp c
					where c.name like :key and c.user_id=su.user_id
				)
			)";
			$criteria->addCondition($cond);
			$criteria->params[':key'] = "%{$this->key}%";
		}



       // $criteria->with = array( 'sender');
       // $criteria->compare( 'sender.email', $this->sender_email, true );

        $criteria->with = array( 'company');
        $criteria->compare( 'company.name', $this->sender_email, true );

        if( $_GET['SsnSampleSearch_sort']!='sender_email.desc' && $_GET['SsnSampleSearch_sort']!='sender_email'){

            $criteria->with = array( 'courier');
            $criteria->compare( 'courier.courier_id', $this->search_courier, true );
        }



        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'send_date DESC',
                'attributes' => array(
                    'search_courier'=>array(
                        'asc' => 'courier.courier',
                        'desc' => 'courier.courier DESC',
                    ),
                    'sender_email' => array(
                       // 'asc' => 'sender.full_name',
                       // 'desc' => 'sender.full_name DESC',
                          'asc' => 'company.name',
                          'desc' => 'company.name DESC',
                    ),
                    '*',
                ),
            ),
            'pagination' => array(
                'pageSize' => Yii::app()->params['PageSize'],
            ),
        ));

        /*
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => ' t.created_date DESC, t.awbn ASC, courier.courier ASC ',

            ),
			'pagination' => array(
				'pageSize' => Yii::app()->params['PageSize'],
			),
		));
        */

	}

	public function canShowSender() {
		return $this->mode==self::MODE_RECV || $this->mode==self::MODE_ALL;
	}
	
	public function canShowRecipient() {
		return $this->mode==self::MODE_SENT || $this->mode==self::MODE_ALL;
	}


    public function getRawDateSentText($data,$row) {
        return date('d/m/Y', strtotime($data->send_date));
    }



	public function searchCompanyProfile() {
		$criteria = new CDbCriteria;

		$conds = array();
		$conds[] = 'from_user_id = :user_id';
		$conds[] = "EXISTS (
			select su.user_id 
			from {{_ssn_sample_to_user}} su
			where su.sample_id=t.id and su.user_id=:user_id
		)";
		$criteria->addCondition(implode(' OR ', $conds));
		$criteria->params[':user_id'] = $this->user_id;

        $criteria->with = array( 'company');
        $criteria->compare( 'company.name', $this->sender_email, true );


        /*
        //[SsnSampleSearch_sort] => seach_receiver.desc
        if( $_GET['SsnSampleSearch_sort']!='sender_email.desc' && $_GET['SsnSampleSearch_sort']!='sender_email'){

           $criteria->with = array(
                'receivers' => array(  // this is for fetching data
                    'together' => true,
                    'select' => false,
                ),

            );

            $criteria->compare('receivers.full_name', $this->seach_receiver, true);

            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'sort' => array(
                    'defaultOrder' => 'send_date DESC',
                    'attributes' => array(
                        'seach_receiver' => array(
                            'asc' => 'receivers.full_name',
                            'desc' => 'receivers.full_name DESC',
                        ),

                        'sender_email' => array(
                            'asc' => 'company.name',
                            'desc' => 'company.name DESC',
                        ),
                        '*',
                    ),
                ),
                'pagination' => array(
                    'pageSize' => Yii::app()->params['PageSize'],
                ),
            ));

        }else{
            */
            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'sort' => array(
                    'defaultOrder' => 'send_date DESC',
                    'attributes' => array(
                       /* 'seach_receiver' => array(
                            'asc' => 'receivers.full_name',
                            'desc' => 'receivers.full_name DESC',
                        ),
                       */
                        'sender_email' => array(
                            'asc' =>'company.name',
                            'desc' => 'company.name DESC',
                        ),
                        '*',
                    ),
                ),
                'pagination' => array(
                    'pageSize' => Yii::app()->params['PageSize'],
                ),
            ));

       // }
	}
}

<?php

/**
 * This is the model class for table "{{_ssn_shipping_item}}".
 *
 * The followings are the available columns in table '{{_ssn_shipping_item}}':
 * @property integer $id
 * @property string $name
 * @property string $color
 * @property integer $quantity
 * @property string $document
 * @property integer $shipping_id
 */
class SsnShippingItem extends CActiveRecord
{
	public $documentFile;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SsnShippingItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{_ssn_shipping_item}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		Yii::import('application.components.validator.FileUploadClientValidator');
		
		return array(
			array('name, quantity, color', 'required', 'on' => 'create, update', 'message' => 'Please fill in this field.'),
			array('document', 'application.components.validator.FileUploadClientValidator',
				'types' => 'doc, docx, xls, xlsx, pdf',
				'types' => 'doc, docx, xls, xlsx, pdf', 'allowEmpty' => true, 
				'on'=>'create, update'),
			
			array('quantity, shipping_id', 'numerical', 'integerOnly'=>true),
			array('name, document', 'length', 'max'=>255),
			array('color', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, color, quantity, document, shipping_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'color' => 'Color',
			'quantity' => 'Quantity',
			'document' => 'Document',
			'shipping_id' => 'Shipping',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('color',$this->color,true);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('document',$this->document,true);
		$criteria->compare('shipping_id',$this->shipping_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getDocumentDirectory() {
		return Yii::getPathOfAlias('webroot') . '/upload/shipping-item/';
	}

	public function saveDocument() {
		if (!$this->documentFile) return;
		
		$saveName = time() . '-' . $this->documentFile->getName();
		$savePath = $this->getDocumentDirectory() . $saveName;
		if (!file_exists(dirname($savePath))) {
			mkdir(dirname($savePath));
		}
		$this->documentFile->saveAs($savePath);
		$this->removeDocumentFile();
		$this->document = $saveName;
		$this->update('document');
	}
	
	public function getDocumentDownloadUrl() {
		$file = $this->getDocumentDirectory() . $this->document;
		return is_file($file) ? Yii::app()->baseUrl .'/upload/shipping-item/' . $this->document : null;
	}
	
	protected function beforeDelete() {
		$this->removeDocumentFile();
		return true;
	}
	
	public function removeDocumentFile() {
		if (is_file($this->getDocumentDirectory().$this->document))
			unlink ($this->getDocumentDirectory().$this->document);
	}

	public function safeDeleteAll($condition='',$params=array()) {
		$models = self::model()->findAll($condition, $params);
		foreach($models as $model) {
			$model->delete();
		}
	}
}
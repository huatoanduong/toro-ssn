<?php

/**
 * This is the model class for table "{{_ssn_sample_item}}".
 *
 * The followings are the available columns in table '{{_ssn_sample_item}}':
 * @property integer $id
 * @property string $name
 * @property string $color
 * @property integer $quantity
 * @property integer $sample_id
 * @property string $document
 * @property string $unit
 */
class SsnSampleCompany extends _BaseModel {
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{_ssn_sample_company}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id, sample_id, to_company_id, to_user_id', 'safe')
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'company_fk' => array(self::BELONGS_TO, 'SsnCompany', 'to_company_id')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => Yii::t('translation', 'ID'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('sample_id', $this->sample_id);
        $criteria->compare('to_company_id', $this->to_company_id);


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->params['PageSize'],
            ),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SsnSampleItem the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function nextOrderNumber() {
        return SsnSampleItem::model()->count() + 1;
    }

}

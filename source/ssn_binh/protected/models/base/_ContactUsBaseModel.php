<?php
class _ContactUsBaseModel extends _BaseModel {

    public $contactType = array(
        CONTACT_US_GENERAL_ENQUIRY => 'General Enquiry',
        CONTACT_US_TRAINING_REQUEST => 'Training Request',
        CONTACT_US_BIM_SERVICE_ENQUIRY => 'BIM Service Enquiry',
        CONTACT_US_CAREER => 'Career',
    );
    public $allowUploadType = 'pdf, jpg, gif, png, jpeg';
    public $maxUploadFileSize = 5242880; //10MB
    public $uploadFileFolder = 'upload/contactus';

    public function getListGeneralEnquiry() {
        $init = new GeneralEnquiry();
        $list = $init->getListName();
        return $list;
    }

    public function getListTrainingRequest() {
        $init = new TrainingRequest();
        $list = $init->getListName();
        return $list;
    }

    public function getListBIMServiceEnquiry() {
        $init = new BIMServiceEnquiry();
        $list = $init->getListName();
        return $list;
    }

    public function getListCareer() {
        $init = new Career();
        $list = $init->getListName();
        return $list;
    }

    public function getOptionName($model) {
        switch ($model->type) {
            case CONTACT_US_GENERAL_ENQUIRY:
                $option = $this->getGeneralEnquiryName($model->option);
                break;
            case CONTACT_US_TRAINING_REQUEST:
                $option = $this->getTrainingRequestName($model->option);
                break;
            case CONTACT_US_BIM_SERVICE_ENQUIRY:
                $option = $this->getBIMServiceEnquiryName($model->option);
                break;
            case CONTACT_US_CAREER:
                $option = $this->getCareerName($model->option);
                break;
        }
        return $option;
    }

    public function getGeneralEnquiryName($option) {
        $list = $this->getListGeneralEnquiry();
        if (isset($list[$option])) {
            return $list[$option];
        } else {
            return 'Not set';
        }
    }

    public function getTrainingRequestName($option) {
        $list = $this->getListTrainingRequest();
        if (isset($list[$option])) {
            return $list[$option];
        } else {
            return 'Not set';
        }
    }

    public function getBIMServiceEnquiryName($option) {
        $list = $this->getListBIMServiceEnquiry();
        if (isset($list[$option])) {
            return $list[$option];
        } else {
            return 'Not set';
        }
    }

    public function getCareerName($option) {
        $list = $this->getListCareer();
        if (isset($list[$option])) {
            return $list[$option];
        } else {
            return 'Not set';
        }
    }

}

?>

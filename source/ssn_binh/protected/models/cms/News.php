<?php

class News extends _BasePost {

    public $uploadImageFolder = 'upload/cms'; //remember remove ending slash
    public $defineImageSize = array(
        'featured_image' => array(array('alias' => 'thumb1', 'size' => '130x130')),
    );
    public $pageType = 'news';
    public $categoryId;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function defaultScope() {
        return array(
            'condition' => "post_type='" . $this->pageType . "'",
        );
    }
	
	public function rules() {
		$rules = parent::rules();
		$rules[] = array('title, short_content, content', 'required', 'on' => 'News');
		return $rules;
	}

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        $return = array(
            'postsCategories' => array(self::HAS_MANY, 'PostsCategories', 'post_id', 'joinType' => 'INNER JOIN'),
            'category' => array(self::HAS_MANY, 'NewsCategory', array('category_id' => 'id'), 'through' => 'PostsCategories', 'joinType' => 'INNER JOIN'),
            'categoryLink' => array(self::MANY_MANY, 'NewsCategory', $this->tablePrefix() . '_posts_categories(post_id, category_id)', 'together' => true, 'joinType' => 'LEFT JOIN'),
        );
        return $return;
    }

    public function getSlugById($id) {
        return News::model()->findByPk((int) $id);
    }

    public function nextOrderNumber() {
        return News::model()->count() + 1;
    }
    public function getListNews() {
        $criteria = new CDbCriteria;
        $criteria->compare('status', 1);
        $criteria->order = 'created_date DESC';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 2,
            ),
        ));
    }
    public function getAllNews(){
        $criteria = new CDbCriteria;
        $criteria->compare('status', 1);
        $criteria->order = 'created_date DESC';
        $model = self::model()->findAll($criteria);
        if($model){
            return $model;
        }
        return;
    }
    public function searchNews()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('short_content', $this->short_content, true);
		$criteria->compare('content', $this->content, true);
		$criteria->compare('status', $this->status);
		$criteria->compare('posted_by', $this->posted_by, true);
		$criteria->compare('post_type', $this->post_type, true);
		$criteria->compare('title_tag', $this->title_tag, true);
		$criteria->compare('meta_keywords', $this->meta_keywords, true);
		$criteria->compare('meta_desc', $this->meta_desc, true);
		$criteria->compare('featured_image', $this->featured_image, true);
		$criteria->compare('display_order', $this->display_order);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('modified_date', $this->modified_date, true);
		$criteria->compare('slug', $this->slug, true);
		$sort = new CSort();
		$sort->attributes = array(
			'name' => array(
				'asc' => 't.title',
				'desc' => 't.title desc',
				'default' => 'asc',
			),
		);
		$sort->defaultOrder = 't.created_date DESC';


		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
                        'sort'=>$sort,
			'pagination' => array(
				'pageSize' => Yii::app()->params['PageSize'],
			),
		));
	}

}

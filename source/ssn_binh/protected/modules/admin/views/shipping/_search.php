<div class="panel panel-default">
    <div class="panel-body">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'method' => 'get',
            'htmlOptions' => array('class' => 'form-horizontal', 'role' => 'form', 'id' => 'search-form'),
        )); ?>
		<div class="form-group row">
			<div class="col-md-3">
				<?php echo $form->textField($model,'key',array('class'=>'form-control', 'placeHolder' => 'Text to search')); ?>
			</div>
			<div class="col-md-3">
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
					'model' => $model,
					'attribute' => 'fromDate',
					'options' => array(
						'dateFormat' => 'dd/mm/yy',
					),
					'htmlOptions'=>array(
						'class'=>'form-control fromDate',
						'placeHolder' => 'From'
					),
				)); ?>
			</div>
			<div class="col-md-3">
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
					'model' => $model,
					'attribute' => 'toDate',
					'options' => array(
						'dateFormat' => 'dd/mm/yy',
					),
					'htmlOptions'=>array(
						'class'=>'form-control toDate',
						'placeHolder' => 'To'
					),
				)); ?>			
			</div>
			<div class="col-md-1">
				<button type="submit" class="btn btn-default btn-sm">Search</button>
			</div>
		</div>
        <?php $this->endWidget(); ?>
    </div>
</div>


<div class="box">
	<div class="box-body">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li><a href="<?= $this->createUrl('view', array('id'=>$model->id)) ?>">Information</a></li>
				<li><a href="<?= $this->createUrl('partner', array('id'=>$model->id)) ?>">Partner</a></li>
				<li class="active"><a href="javascript:void(0)">Sample</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active container-fluid">
					<?php $this->renderPartial('view/sample', array('model'=>$model)) ?>
				</div><!-- /.tab-pane -->
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- nav-tabs-custom -->
	</div>
</div>
	

<h3>Company Information</h3>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-model-form',
	'enableAjaxValidation'=>false,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
		'htmlOptions' => array('role' => 'form', 'enctype' => 'multipart/form-data'),
)); ?>
	<div class="form-group">
		<?php echo $form->labelEx($model, 'image', array('class' => '')); ?>
		<div class="clearfix">
			<?php if (!$model->isNewRecord && $model->is_img_active == 1) : ?>
				<?php if (!empty($model->image)): ?>
					<div class="thumbnail" id="thumbnail-<?php echo $model->id; ?>">
					<?php echo CHtml::image($model->getLogoUrl(), '', array()); ?>
					</div>
				<?php endif; ?>
			<?php endif; ?>
			<?php if($model->is_img_active == 0):?>
				<img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo.png" alt="Blocked Image"/>
			<?php else:?>
			<?php echo $form->fileField($model, 'image', array('title' => "Upload " . $model->getAttributeLabel('image'))); ?>
			<div class='notes'>Recommended Size: <?php echo $model->imageSize ?> (width x height), Allow file type  <?php echo '*.' . str_replace(',', ', *.', $model->allowImageType); ?> - Maximum file size : <?php echo ($model->maxImageFileSize / 1024) / 1024; ?>M </div>
			<?php echo $form->error($model, 'image'); ?>
			<?php endif;?>
			<p><a href="<?= $this->createUrl('setDefaultLogo', array('id'=>$model->id)) ?>" class="def-logo">Change to default logo.</a></p>
		</div>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'name',array()); ?>
		<?php echo $form->textField($model,'name',array('class' => 'form-control')); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'address', array('class' => '')); ?>
		<?php echo $form->textField($model,'address',array('class' => 'form-control')); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'area_code_id', array('class' => '')); ?>
		<?php echo $form->dropDownList($model,'area_code_id', AreaCode::loadArrArea(),array('class' => 'form-control')); ?>
		<?php echo $form->error($model,'area_code_id'); ?>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'city_id', array('class' => '')); ?>
		<?php echo $form->dropDownList($model,'city_id', array(),array('class' => 'form-control')); ?>
		<?php echo $form->error($model,'city_id'); ?>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'website', array('class' => '')); ?>
		<?php echo $form->textField($model,'website',array('class' => 'form-control')); ?>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'phone', array('class' => '')); ?>
		<?php echo $form->textField($model,'phone',array('class' => 'form-control')); ?>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'size', array('class' => '')); ?>
		<?php echo $form->textField($model,'size',array('class' => 'form-control')); ?>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'about', array('class' => '')); ?>
		<?php echo $form->textArea($model,'about',array('class' => 'form-control')); ?>
	</div>

	<h3>Admin Information</h3>
	
	<div class="form-group">
		<label class="">Admin Name</label>
		<p class="control-static"><?= $model->adminName ?></p>
	</div>

	<div class="form-group">
		<label class="">Admin Email</label>
		<p class="control-static"><?= $model->adminEmail ?></p>
	</div>
	
	<div class="form-group">
		<input type="submit" name="yt0" value="Update" class="btn btn-primary">
	</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
	$('.def-logo').click(function (e) {
		var c = confirm('Are you sure to change this to default logo?');
		if (!c) {
			e.preventDefault();
		}
	});
</script>
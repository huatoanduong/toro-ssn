<?php
$this->breadcrumbs = array(
    $this->pluralTitle,
);

Yii::app()->clientScript->registerScript('search', "

$( document ).ajaxComplete(function() {


$('#ssn-sample-grid .table tbody tr td').click(function() {
        if ($(this).hasClass('checkbox-column') == false) {
            var row = $(this).parent(0);
            console.log(row);
            id = row.children('.checkbox-column').children('.select-on-check').val();
            location.href ='".$this->createUrl('view')."/id/'+id;
        }
    });

});

$('#ssn-sample-grid .table tbody tr td').click(function() {
        if ($(this).hasClass('checkbox-column') == false) {
            var row = $(this).parent(0);
            console.log(row);
            id = row.children('.checkbox-column').children('.select-on-check').val();
            location.href ='".$this->createUrl('view')."/id/'+id;
        }
    });


$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('ssn-sample-grid', {
                url : $(this).attr('action'),
		data: $(this).serialize()
	});
	return false;
});

$('#clearsearch').click(function(){
	var id='search-form';
	var inputSelector='#'+id+' input, '+'#'+id+' select';
	$(inputSelector).each( function(i,o) {
		 $(o).val('');
	});
	var data=$.param($(inputSelector));
	$.fn.yiiGridView.update('ssn-sample-grid', {data: data});
	return false;
});

$('.deleteall-button').click(function(){
        var atLeastOneIsChecked = $('input[name=\"ssn-sample-grid_c0[]\"]:checked').length > 0;
        if (!atLeastOneIsChecked)
        {
                alert('Please select at least one record to delete');
        }
        else if (window.confirm('Are you sure you want to delete the selected records?'))
        {
                document.getElementById('ssn-sample-grid-bulk').action='" . Yii::app()->createAbsoluteUrl('admin/' . Yii::app()->controller->id . '/deleteall') . "';
                document.getElementById('ssn-sample-grid-bulk').submit();
        }
});

");

Yii::app()->clientScript->registerScript('ajaxupdate', "
    $('#ssn-sample-grid a.ajaxupdate').on('click', function() {
        $.fn.yiiGridView.update('ssn-sample-grid', {
            type: 'POST',
            url: $(this).attr('href'),
            success: function() {
                $.fn.yiiGridView.update('ssn-sample-grid');
            }
        });
        return false;
    });
");
?>

<script>



</script>
<div class="box">
	<div class="box-body">
		<div class='search-form'>
		<?php $this->renderPartial('_search', array(
			'model' => $model,
		)); ?>
		</div>		
		
		<p>
			<?= CHtml::htmlButton('<span class="glyphicon glyphicon-trash"></span> Bulk Delete', array(
			'class' => 'btn btn-danger deleteall-button', 
			'type' => 'button')); ?>
		</p>
		
		<div class="table-responsive">
        <?php
        $allowAction = in_array("delete", $this->listActionsCanAccess) ? 'CCheckBoxColumn' : '';
        $columnArray = array();
        if (in_array("Delete", $this->listActionsCanAccess)) {
            $columnArray[] = array(
                'value' => '$data->id',
                'class' => "CCheckBoxColumn",
            );
        }
        $columnArray = array_merge($columnArray, array(
            array(
                'header' => 'S/N',
                'type' => 'raw',
                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                'headerHtmlOptions' => array('width' => '30px', 'style' => 'text-align:center;'),
                'htmlOptions' => array('style' => 'text-align:center;')
            ),
            array(
                'header'=>'Send Date',
                'name'=>'send_date',
                'value'=>'date("d/m/Y", strtotime($data->send_date))',
                'htmlOptions' => array('style' => 'text-align:left;')
            ),

            array('header'=>'Sender',
                'name'=>'sender_email',
                'value'=>' $data->sender->company ? $data->sender->company->name:"-"' ,
                'htmlOptions' => array('style' => 'text-align:left;'),
            ),
            array(
                'name' => 'awbn',
                'htmlOptions' => array('style' => 'text-align:left;')
            ),

            array(
                'header'=>'Courier Name',
                'name'=>'search_courier',
                'value'=>'$data->courierName',
                'htmlOptions' => array('style' => 'text-align:left;')
            ),

            /*
            array(
                'header' => 'Actions',
                'class' => 'CButtonColumn',
                'template' => '{view} {delete}',
            ),
            */

        ));
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'ssn-sample-grid-bulk',
            'enableAjaxValidation' => false,
            'htmlOptions' => array('enctype' => 'multipart/form-data')));

        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'ssn-sample-grid',
            'dataProvider' => $model->searchAdmin(),
			'selectableRows' => 2,
            'columns' => $columnArray,
			'cssFile'=>false,
			'itemsCssClass'=>'table table-striped table-bordered',
			'pagerCssClass'=>'pag-container',
			'pager' => array(
				'header'=>'',
				'cssFile' => false,
				'nextPageLabel' => 'Next',
				'prevPageLabel' => 'Previous',
				'firstPageLabel' => 'First',
				'lastPageLabel' => 'Last',
				'selectedPageCssClass'=>'active',
				'maxButtonCount' => 10,
				'htmlOptions'=>array('class' => 'pagination pull-right',)
			),
		));
        $this->endWidget();
        ?>			
		</div>
	</div>
<div>


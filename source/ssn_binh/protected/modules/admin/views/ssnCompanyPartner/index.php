<?php
/* @var $this SsnCompanyPartnerController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ssn Company Partners',
);

$this->menu=array(
	array('label'=>'Create SsnCompanyPartner', 'url'=>array('create')),
	array('label'=>'Manage SsnCompanyPartner', 'url'=>array('admin')),
);
?>

<h1>Ssn Company Partners</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

<select id="TbEvents_city_id" name="TbEvents[city_id]" class="form-control">
<option value="">--Select--</option>
<?php if(count($citiesData) > 0):?>
    <?php foreach($citiesData as $key => $city):?>
        <option value="<?php echo $key?>" <?php echo $key == $cityId ? 'Selected':''?>><?php echo $city;?></option>
    <?php endforeach;?>
<?php endif;?>
</select>
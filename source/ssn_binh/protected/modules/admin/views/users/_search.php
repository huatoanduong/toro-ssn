<?php $form = $this->beginWidget('CActiveForm', array(
	'method' => 'get',
	'htmlOptions' => array('class' => 'form-horizontal', 'role' => 'form', 'id' => 'search-form'),
)); ?>
<div class="form-group">
	<label class="control-label col-md-1">Search</label>
	<div class="col-md-10">
		<?php echo $form->textField($model,'email',array('class'=>'form-control', 'placeHolder' => 'Text to search')); ?>
	</div>
	<div class="col-md-1">
		<button type="submit" class="btn btn-default btn-sm">Search</button>
	</div>
</div>
<?php $this->endWidget(); ?>

<?php
$form = $this->beginWidget('CActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
	'htmlOptions' => array('class' => 'form-horizontal', 'role' => 'form', 'id' => 'search-form'),
));
?>
<div class="row">
	<div class="col-md-10">
		<div class="form-group">
			<?php echo $form->labelEx($model, 'status', array('class' => 'col-sm-1 control-label')); ?>
			<div class="col-sm-2">
				<?php echo $form->dropDownList($model, 'status', $model->optionApproved, array('class' => 'form-control')); ?>
				<?php echo $form->error($model, 'status'); ?>
			</div>

            <label class="col-sm-2 control-label" for="SsnPartner_created_date">From date</label>
            <div class="col-sm-2">
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                    'model' => $model,
                    'attribute' => 'fromDate',
                    'options' => array(
                        'dateFormat' => 'dd/mm/yy', //2015-09-17 11:11:43
                    ),
                    'htmlOptions'=>array(
                        'class'=>'form-control'
                    ),
                )); ?>
                <?php //echo $form->textField($model,'created_date',array('class'=>'form-control', 'placeHolder' => 'Send date')); ?>
            </div>
            <label class="col-sm-2 control-label" for="SsnPartner_created_date">To date</label>

            <div class="col-sm-2">
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                    'model' => $model,
                    'attribute' => 'toDate',
                    'options' => array(
                        'dateFormat' => 'dd/mm/yy',//2015-09-17 11:11:43
                    ),
                    'htmlOptions'=>array(
                        'class'=>'form-control'
                    ),
                )); ?>
                <?php //echo $form->textField($model,'created_date',array('class'=>'form-control', 'placeHolder' => 'Send date')); ?>
            </div>


		</div>

	</div>
	<div class="col-md-2">
        <button type="submit" class="btn btn-default btn-sm">Search</button>
	</div>
</div>
<?php $this->endWidget(); ?>

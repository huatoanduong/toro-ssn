<?php
$this->bodyClass .= 'hold-transition skin-purple ';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>
		<?php echo CHtml::encode($this->pageTitle) . ' - ' . Yii::app()->params['projectName'] . ' Admin'; ?></title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.5 -->
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/plugins/bootstrap/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/css/AdminLTE.min.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/css/style.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/plugins/iCheck/square/blue.css">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<?php
	Yii::app()->clientScript->coreScriptPosition = CClientScript::POS_BEGIN;
	Yii::app()->clientScript->defaultScriptFilePosition = CClientScript::POS_END;
	
	Yii::app()->clientScript->scriptMap = array(
		'jquery.js' => Yii::app()->theme->baseUrl.'/plugins/jQuery/jQuery-2.1.4.min.js',
		'jquery.min.js' => Yii::app()->theme->baseUrl.'/plugins/jQuery/jQuery-2.1.4.min.js',
	);
	Yii::app()->getClientScript()->registerCoreScript('jquery');
	Yii::app()->clientScript->registerScriptFile(
		Yii::app()->theme->baseUrl.'/plugins/bootstrap/js/bootstrap.min.js');
	?>
</head>

<body class="<?= $this->bodyClass ?>">
	<?= $content ?>
</body>

</html>
<?php

class MailchimpController extends AdminController
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
            array('allow',
                'actions' => array('index'),
                'users' => array('@'),
            ),
            array('allow',
                'actions' => array('create', 'view','delete','update','synchronize'),
                'users' => array('@'),
            ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	
        //import all subcriber to mailchimp
        public function actionSynchronize()
        {
            set_time_limit(7200);
            $criteria=new CDbCriteria;
            $mSubscriber = Subscriber::model()->findAll($criteria);
            $test=array();
            if(count($mSubscriber)>0)
            {
                Yii::import('ext.MailChimp.MailChimp', true); 
                foreach($mSubscriber as $item)
                {
                    $mailChimp = new MailChimp();
                    if($item->status == STATUS_ACTIVE)
                    {
                        $test[]= $mailChimp->addSubscriber($item->email);
                    }
                    else
                    {
                        $mailChimp->removeSubscriber($item->email);
                    }
                }
            }
            Yii::app()->user->setFlash('setting', "Synchronize Mailling list successfully!");
            $this->redirect ( Yii::app()->createAbsoluteUrl("admin/setting"));
        }
}

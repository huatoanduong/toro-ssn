<?php

class SiteController extends MemberController {

    /**
     * Declares class-based actions.
     */
    public function accessRules() {
        return array();
    }

    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function actionIndex() {
        $this->redirect(array('/site/dashboard'));
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
			} else {
				$this->layout = '/layouts/blank';
                $this->render('error', $error);
			}
        }
    }

    public function actionChangePassword() {
        $model = Users::getInforUser(Yii::app()->user->id);
        $model->scenario = 'changeMyPassword';

        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
            if ($model->validate()) {
                $model->password_hash = md5($model->newPassword);
                $model->temp_password = $model->newPassword;
                if ($model->update(array('password_hash', 'temp_password'))) {
                    SendEmail::changePassMailToUser($model);
                    Yii::app()->user->setFlash('successChangeMyPassword', "Your password has been successfully changed.");
                }
            }
        }

        $this->render('changePassword', array(
            'model' => $model,
        ));
    }

    public function actionUpdateProfile() {
		// company update
        $comp = SsnCompany::model()->getCompanyByUser(Yii::app()->user->id);
        if (isset($_POST['SsnCompany'])) {
            $comp->attributes = $_POST['SsnCompany'];
            $comp->user_id = Yii::app()->user->id;
            if($comp->validate()) {
                if ($comp->save()) {
                    $comp->saveImage('image');
                    Yii::app()->user->setFlash('success', "Your Company Information has been successfully changed.");
                }
            }
        }

		// profile update
		$user = Users::model()->getInforUser(Yii::app()->user->id);
        $user->scenario = 'updateMyProfile';
        if (isset($_POST['Users'])) {
            $user->attributes = $_POST['Users'];
            if($user->validate()){
                if(!empty($user->newPassword)) {
                    $user->password_hash = md5(trim($user->newPassword));
                    $user->temp_password = trim($user->newPassword);
				}

                if ($user->save()) {
                    $user->saveImage('image');
                    Yii::app()->user->setFlash('success', "Your Admin Information has been successfully changed.");
                }
            }
        }
		
        $this->pageTitle = 'Profile';
        $this->breadcrumbs[] = 'Profile';
        $this->render('updateProfile', array(
            'comp' => $comp,
            'user' => $user,
        ));
    }
	
	public function actionInvitePartner() {
        $model = SsnCompany::getCompanyByUser(Yii::app()->user->id);
        $model->scenario = 'updateMyProfile';

        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', "Your profile has been successfully changed.");
            }
        }
        $this->render('invitePartner', array(
            'model' => $model,
        ));
    }
	
    public function actionTestEmail(){
        $request = SsnPartner::model()->findByPk(30);
        SendEmail::partnerPendingRequest($request);
    }
    
    public function actionGetListByArea(){
        $area = $_POST['module'];
        $c = new CDbCriteria();
        $c->compare('area_code_id', $area);
        $data = SsnCity::model()->findAll($c);
        $html = '';
        foreach($data as $k => $i){
            $html .= '<option value="' . $i->id . '">' . $i->name . '</option>';
        }
        echo $html;
    }

}
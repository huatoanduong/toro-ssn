<?php
/* @var $this SampleController */
?>
<style>

    .table-hover>tbody>tr:hover {
        background-color: #d8d8d8;
    }

</style>
<div class="nav-tabs-custom">
	<ul class="nav nav-tabs">
		<li><a href="<?= $this->createUrl('index') ?>">Received</a></li>
		<li><a href="<?= $this->createUrl('sent') ?>">Sent</a></li>
		<li class="active"><a href="javascript:void(0)">All</a></li>
		<li>
			<button type="button"
					onclick="window.location='<?php echo $this->createUrl('create') ?>'"
			   class="btn btn-primary">Add New</button>
		</li>
	</ul>

	<?php $this->renderPartial('_search', array('model'=>$model)) ?>

	<div class="tab-content">
		<div class="tab-pane active">
			<?php $this->renderPartial('_sample-grid', array('model'=>$model)) ?>
		</div><!-- /.tab-pane -->
	</div>
	<!-- /.tab-content -->
</div>
<!-- nav-tabs-custom -->



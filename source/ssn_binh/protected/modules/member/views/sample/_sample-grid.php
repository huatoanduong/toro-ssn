<?php
/* @var $model SsnSampleSearch */
$items = $model->search();

?>
<?php if ($items): ?>
<div class="table-responsive">
	<table class="table table-bordered table-hover">
		<thead>
			<tr>

				<th rowspan="2" class="text-center">Date Sent</th>
				<?php if ($model->canShowSender()): ?>
				<th rowspan="2" class="text-center">Sender</th>
				<?php endif ?>
				
				<?php if ($model->canShowRecipient()): ?>
				<th rowspan="2" class="text-center">Recipient</th>
				<?php endif ?>
				<th rowspan="2" class="text-center">AWBN</th>

                <th colspan="3" class="text-center">Item</th>
                <th rowspan="2" class="text-center">Remark</th>
            </tr>
            <tr>
                <th class="text-center"> Name </th>
                <th class="text-center">Color</th>
                <th class="text-center">Qty</th>

			</tr>


		</thead>
		
		<tbody>
		<?php foreach($items as $sample): ?>
		<?php $rs = count($sample->items)>0 ? count($sample->items) : 1 ?>

        <tr class="linkable index-sample-row row-item-<?= $sample->id ?>"  data-item-id="<?= $sample->id ?>" onclick="window.location='<?php echo $this->createUrl('view', array('id'=> $sample->id)) ?>'">
			<td rowspan="<?= $rs ?>" class="text-center">
				<?= $sample->dateSentText ?>
			</td>
			
			<?php if ($model->canShowSender()): ?>
			<td rowspan="<?= $rs ?>">
				<span class="notranslate">
					<?= $sample->sender->id==Yii::app()->user->id ? '' : $sample->senderName ?>
				 </span>
			</td>
			<?php endif ?>

			<?php if ($model->canShowRecipient()): ?>
			<td rowspan="<?=  $rs ?>">
				<?php foreach($sample->receivers as $receiver): ?>
				<p class="notranslate">
					<?= $receiver->id==Yii::app()->user->id ? '' : $receiver->company->name ?>
				</p>
				<?php endforeach ?>
			</td>
			<?php endif ?>
				
			<td rowspan="<?=  $rs ?>" class="text-center notranslate">
				<?= $sample->awbn ?>
			</td>


            <?php if (count($sample->items)>0): ?>
                <?php
                $item = $sample->items[0];
                //foreach($sample->items as $item): ?>
                <td>
                    <div class="notranslate"
                         style="white-space: normal; word-wrap: break-word;
                          word-break: break-all;">
                        <?= $item->name ?>
                    </div>
                 </td>
                    <td class="notranslate"><?= $item->color ?></td>
                    <td class="text-center"><?= $item->quantity ?></td>

            <?php else: ?>
                <td></td>
                <td></td>
                <td></td>
            <?php endif ?>
			
			<td rowspan="<?= $rs ?>">
				<span data-toggle="tooltip" data-placement="bottom" title="<?= $sample->remark ?>"
					class="notranslate">
				<?= $sample->remarkExcerpt ?>
				</span>
			</td>

		</tr >

            <?php
            for($i = 1;  $i < count($sample->items);$i++){
                ?>
                <tr class="linkable linkable-sub linkable-sub-<?php echo $sample->id ?>"
                    data-item-id="<?= $sample->id ?>"
                    onclick="window.location='<?php echo $this->createUrl('view', array('id'=> $sample->id)) ?>'">
                    <?php
                    $item = $sample->items[$i];
                    //foreach($sample->items as $item): ?>
                    <td>
                        <div class="notranslate"
                             style="white-space: normal; word-wrap: break-word;
                              word-break: break-all;">
                            <?= $item->name ?>
                        </div>
                    </td>
                    <td class="notranslate"><?= $item->color ?></td>
                    <td class="text-center"><?= $item->quantity ?></td>
                </tr>
            <?php
            }
            ?>


		<?php endforeach ?>
		
		</tbody>
	</table>
</div>

<div class="clearfix">
<?php $this->widget('CLinkPager', array(
	'pages' => $model->pagination,
	'header' => '',
	'nextPageLabel' => 'Next',
	'prevPageLabel' => 'Previous',
	'firstPageLabel' => 'First',
	'lastPageLabel' => 'Last',
	'selectedPageCssClass'=>'active',
	'maxButtonCount' => 10,
	'htmlOptions'=>array(
		'class' => 'pagination pull-right',
	)
)) ?>
</div>

<?php else: ?>
<p>There are no items.</p>
<?php endif ?>

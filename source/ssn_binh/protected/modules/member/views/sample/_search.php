<?php
/* @var $this SampleController */
/* @var $model SsnSampleSearch */
/* @var $form CActiveForm */
?>
<div class="box box-default">
	<div class="box-header with-border">
		<h3 class="box-title">Search</h3>
	</div>
	<div class="box-body">
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'post',
)); ?>

	<div class="form-group row">
		<div class="col-md-3">
			<?php echo $form->textField($model,'key',array('class'=>'form-control',)); ?>
		</div>
		<div class="col-md-2">
			<?php echo $form->dropdownList($model,'dateRange', SsnSampleSearch::$DATE_RANGES, 
				array('class'=>'form-control dateRange')); ?>
		</div>
		<div class="col-md-2">
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'model' => $model,
				'attribute' => 'fromDate',
				'options' => array(
					'dateFormat' => 'dd/mm/yy',
				),
				'htmlOptions'=>array(
					'class'=>'form-control fromDate hide',
					'placeHolder' => 'From'
				),
			)); ?>
		</div>
		<div class="col-md-2">
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'model' => $model,
				'attribute' => 'toDate',
				'options' => array(
					'dateFormat' => 'dd/mm/yy',
				),
				'htmlOptions'=>array(
					'class'=>'form-control toDate hide',
					'placeHolder' => 'To'
				),
			)); ?>			
		</div>
		<div class="col-md-3">
			<button type="submit" class="btn btn-default">Search</button>
		</div>
	</div>
<?php $this->endWidget(); ?>
	</div>
</div>

<script>
$(window).load(function () {
	function updateSearchState() {
		if ($('.dateRange').val() == 4) {
			$('.fromDate,.toDate').removeClass('hide');
		} else {
			$('.fromDate,.toDate').addClass('hide');
		}
	}
	
	updateSearchState();
	$('.dateRange').change(updateSearchState);
});
</script>
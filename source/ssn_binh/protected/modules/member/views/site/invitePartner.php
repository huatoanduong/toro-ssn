<div class="wrapper container">
    <div class="header">
        <div class="row">
            <div class="col-md-7">
                <ol class="breadcrumb">
                    <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/'); ?>"><span class="icon icon-home"></span></a></li>
                    <li class="active">Update Profile</li>
                </ol>
                <h2 class="title-2">Update Profile</h2>
            </div>
        </div>
    </div>
    <div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'users-model-form',
    'enableAjaxValidation'=>false,
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
    ?>

    <div class="row form-group">
        <?php echo $form->labelEx($model,'full_name',array('label'=>'Full Name', 'class' => 'col-sm-2')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model,'full_name',array('class' => 'form-control')); ?>
            <?php echo $form->error($model,'full_name'); ?>
        </div>
    </div>
    <div class="row form-group">
        <?php echo $form->labelEx($model,'email', array('class' => 'col-sm-2')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model,'email',array('class' => 'form-control')); ?>
            <?php echo $form->error($model,'email'); ?>
        </div>
    </div>

    <div class="row form-group">
        <?php echo $form->labelEx($model,'phone_public', array('label'=>'CellPhone', 'class' => 'col-sm-2')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model,'phone_public',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
            <?php echo $form->error($model,'phone_public'); ?>
        </div>
    </div>
    <div class="row form-group">
        <?php echo $form->labelEx($model,'phone_private', array('label'=>'Phone', 'class' => 'col-sm-2')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model,'phone_private',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
            <?php echo $form->error($model,'phone_private'); ?>
        </div>
    </div>
    <div class="row form-group">
        <?php echo $form->labelEx($model,'skype', array('label'=>'Skype', 'class' => 'col-sm-2')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model,'skype',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
            <?php echo $form->error($model,'skype'); ?>
        </div>
    </div>
    <div class="row form-group">
        <?php echo $form->labelEx($model,'whatapps', array('label'=>'Whatapps', 'class' => 'col-sm-2')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model,'whatapps',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
            <?php echo $form->error($model,'whatapps'); ?>
        </div>
    </div>
    <div class="row form-group">
        <?php echo $form->labelEx($model,'line', array('label'=>'Line', 'class' => 'col-sm-2')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model,'line',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
            <?php echo $form->error($model,'line'); ?>
        </div>
    </div>
    <div class="row form-group">
        <?php echo $form->labelEx($model,'viber', array('label'=>'Viber', 'class' => 'col-sm-2')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model,'viber',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
            <?php echo $form->error($model,'viber'); ?>
        </div>
    </div>
    <div class="row form-group">
        <?php echo $form->labelEx($model,'wechat', array('label'=>'Wechat', 'class' => 'col-sm-2')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model,'wechat',array('size'=>18,'maxlength'=>30, 'class' => 'form-control')); ?>
            <?php echo $form->error($model,'wechat'); ?>
        </div>
    </div>


    <div class="row form-group">
        <div class="col-sm-offset-2 col-sm-6">
            
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>

</div>
    
</div>

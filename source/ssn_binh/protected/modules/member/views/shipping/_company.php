<?php
/* @var $model SsnCompany */
?>
<div class="media company-thumb">
  <div class="media-left">
    <a href="javascript:void(0)">
		<img alt="" src="<?= $model->getLogoUrl() ?>" class="img-responsive media-object" style="width: 50px"/>
    </a>
  </div>
  <div class="media-body">
	  <h4><?= $model->name ?></h4>
	  <p><i><?= $model->address ?></i></p>
  </div>
</div>
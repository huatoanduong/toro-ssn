<?php
/* @var $this ShippingController */
/* @var $comments SsnShippingComment[] */
/* @var $model SsnShipping */
$this->pageTitle = 'Shipping Comments';
?>

<section class="content-header">
    <h1>Shipping<small>Comment</small></h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo Yii::app()->homeUrl ?>"><i class="fa fa-dashboard"></i> Home page</a></li> 
        <li><a href="<?= $this->createUrl('index') ?>">Shipping Schedule</a></li>
        <li class="active">View Comment</li>
    </ol>
</section>

<section class="content">
	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs">
			<li><a href="<?= $this->createUrl('view', array('id'=>$model->id)) ?>">Detail</a></li>
			<li class="active"><a href="javascript:void(0)">View Comment</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active">
				<div class="box-body">
				<div class="direct-chat-messages comments">
					<?php foreach($comments as $k => $comment):?>
						<?php 
							$alignName = $comment->user_id != Yii::app()->user->id ? 'left':'right';
							$alignTime = $comment->user_id != Yii::app()->user->id ? 'right':'left';
						?>
						<div class="direct-chat-msg <?php echo $alignName?>">
							<div class="direct-chat-info clearfix">
								<span class="direct-chat-name notranslate pull-<?php echo $alignName?>"><?php echo $comment->user ? $comment->user->full_name:''?></span>
								<span class="direct-chat-timestamp pull-<?php echo $alignTime?>"><?php echo $comment->created_date?></span>
							</div>
							
							<img class="direct-chat-img" src="<?= $comment->user->getCompanyLogo() ?>" alt="" />
							<div class="direct-chat-text notranslate"><?php echo CHtml::encode($comment->comment)?></div>
						</div>
					<?php endforeach;?>
				</div>
				</div>
				<div class="box-footer">
					<form method="post" id="comment-form">
						<div class="input-group">
							<textarea name="message" id="message" class="form-control" style="height:34px"></textarea>
							<div class="input-group-btn">
								<button type="submit" class="btn btn-warning btn-flat">Send</button>
							</div>
						</div>						
					</form>
				</div>
			</div><!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>
	<!-- nav-tabs-custom -->
</section>
<script>
$(document).ready(function(){
	$('#comment-form').on('submit',function(ev){
		if ($('#message').val().trim()=='') {
			alert('Please input comment');
			ev.preventDefault();
		}
	});
});
</script>

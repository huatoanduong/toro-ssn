<?php
/* @var $this SsnCompanyPartnerController */
/* @var $model SsnCompanyPartner */

$this->breadcrumbs=array(
	'Ssn Company Partners'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SsnCompanyPartner', 'url'=>array('index')),
	array('label'=>'Create SsnCompanyPartner', 'url'=>array('create')),
	array('label'=>'View SsnCompanyPartner', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SsnCompanyPartner', 'url'=>array('admin')),
);
?>

<h1>Update SsnCompanyPartner <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
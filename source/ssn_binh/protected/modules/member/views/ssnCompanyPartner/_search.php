<?php
/* @var $this SsnCompanyPartnerController */
/* @var $model SsnCompanyPartner */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'from_user_id'); ?>
		<?php echo $form->textField($model,'from_user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'to_user_id'); ?>
		<?php echo $form->textField($model,'to_user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'from_company_id'); ?>
		<?php echo $form->textField($model,'from_company_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'to_company_id'); ?>
		<?php echo $form->textField($model,'to_company_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'partner_outsite_system'); ?>
		<?php echo $form->textField($model,'partner_outsite_system',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'approve_by'); ?>
		<?php echo $form->textField($model,'approve_by'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
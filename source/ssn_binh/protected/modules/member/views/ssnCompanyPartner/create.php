<?php
/* @var $this SsnCompanyPartnerController */
/* @var $model SsnCompanyPartner */

$this->breadcrumbs=array(
	'Ssn Company Partners'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SsnCompanyPartner', 'url'=>array('index')),
	array('label'=>'Manage SsnCompanyPartner', 'url'=>array('admin')),
);
?>

<h1>Create SsnCompanyPartner</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
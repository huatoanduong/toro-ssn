<?php
/* @var $this SsnCompanyPartnerController */
/* @var $model SsnCompanyPartner */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ssn-company-partner-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'from_user_id'); ?>
		<?php echo $form->textField($model,'from_user_id'); ?>
		<?php echo $form->error($model,'from_user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'to_user_id'); ?>
		<?php echo $form->textField($model,'to_user_id'); ?>
		<?php echo $form->error($model,'to_user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'from_company_id'); ?>
		<?php echo $form->textField($model,'from_company_id'); ?>
		<?php echo $form->error($model,'from_company_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'to_company_id'); ?>
		<?php echo $form->textField($model,'to_company_id'); ?>
		<?php echo $form->error($model,'to_company_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'partner_outsite_system'); ?>
		<?php echo $form->textField($model,'partner_outsite_system',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'partner_outsite_system'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
		<?php echo $form->error($model,'created_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'approve_by'); ?>
		<?php echo $form->textField($model,'approve_by'); ?>
		<?php echo $form->error($model,'approve_by'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
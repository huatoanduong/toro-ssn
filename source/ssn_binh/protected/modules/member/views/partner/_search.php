<?php
$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
    'htmlOptions' => array('class' => 'form-horizontal', 'role' => 'form', 'id' => 'search-form'),
));
?>
<div class="row">
    <div class="col-md-12" style="margin-left: 5px;">
        <div class="form-group">
            <div class="col-sm-3">
                <?php echo $form->textField($model,'company',array('class'=>'form-control','placeholder'=>'Search')); ?>
                <?php echo $form->error($model, 'company'); ?>
            </div>
            <div class="col-md-2">
                <button type="submit" class="btn btn-default ">Search</button>
            </div>
        </div>

    </div>

</div>
<?php $this->endWidget(); ?>

<?php

/**
 * All sendmail function should be placed here.
 * Custom For Each Project
 */
class SendEmail {
 
    //registering successfully, send email to Administrator - bb
    public static function registerSucceedToAdmin($mUser) {
        $param = $mUser->getParamArray();
        $param['{FULL_NAME}'] = !empty($mUser->full_name) ? $mUser->full_name : $mUser->first_name . ' ' . $mUser->last_name;
        $param['{PASSWORD}'] = $mUser->temp_password;
        $param['{COUNTRY}'] = $mUser->area_code->area_name;

        if (CmsEmail::sendmail(MAIL_REGISTER_SUCCEED_TO_ADMIN, $param, Yii::app()->params['adminEmail'])) {

        } else
            $mUser->addError('status', 'Can not send email');
    }

    //send email to User for forgetting password  - bb
    public static function forgotPasswordToUser($mUser) {

		$u = Users::model()->findByAttributes(array('email' => trim($mUser->email)));
		$u->verify_code = Users::model()->checkVerifyCode(rand(100000, 1000000)); // Gen verify code and send qua mail or sms
        $u->update('verify_code');
        $resetlink = '<a href="' . Yii::app()->createAbsoluteUrl("site/resetPassword", array('verify_code' => $u->verify_code)) . '">link</a>';
        $param = array(
            '{FULL_NAME}' => $u->full_name,
            '{RESET_LINK}' => $resetlink,
        );

        if (CmsEmail::sendmail('reset_password_user', $param, $param, $u->email)) {
            
        } else
            $mUser->addError('status', 'Can not send email');
    }

    //send email to User for changing password
    public static function changePassToUser($mUser) {
        $name = $mUser->full_name;
        $param = array(
            '{FULL_NAME}' => $name,
            '{PASSWORD}' => $mUser->temp_password,
            '{LINK_LOGIN}' => Yii::app()->createAbsoluteUrl("site/login"),
        );

        CmsEmail::sendmail('new_password_user', $param, $param, $mUser->email);
    }

    //Submitting contact form successfully, send email to confirm User
    public static function confirmContactMailToUser($contactM) {
        $param = array(
            '{NAME}' => $contactM->name,
            '{EMAIL}' => $contactM->email,
            '{PHONE}' => $contactM->phone,
            '{MESSAGE}' => $contactM->message,
        );

        $aSubject = array(
            '{NAME}' => $contactM->name,
            '{EMAIL}' => $contactM->email,
            '{PHONE}' => $contactM->phone,
            '{MESSAGE}' => $contactM->message,
        );

        if (CmsEmail::sendmail(MAIL_CONTACT_US_TO_USER, $aSubject, $param, Yii::app()->params['adminEmail'])) {
            
        } else
            $contactM->addError('email', 'Can not send email to: ' . $contactM->email);
    }

    //Submitting contact form successfully, send email to Administrator
    public static function noticeContactMailToAdmin($contactM) {
        $option = $contactM->type == CONTACT_US_GENERAL_ENQUIRY ? $contactM->option : $contactM->getOptionName($contactM);
        $param = array(
            '{NAME}' => $contactM->name,
            '{EMAIL}' => $contactM->email,
            '{PHONE}' => $contactM->phone,
            '{MESSAGE}' => $contactM->content,
            '{SUBJECT}' => $contactM->subject,
            '{OPTION}' => $option
        );

        $aSubject = array(
            '{NAME}' => $contactM->name,
            '{EMAIL}' => $contactM->email,
            '{PHONE}' => $contactM->phone,
            '{MESSAGE}' => $contactM->content,
            '{SUBJECT}' => $contactM->subject,
            '{OPTION}' => $option
        );

        if (CmsEmail::sendmail(MAIL_CONTACT_US_TO_ADMIN, $aSubject, $param, Yii::app()->params['adminEmail'])) {
            
        } else
            $contactM->addError('email', 'Can not send email to: ' . $contactM->email);
    }

    //mail from Forgot Password at BE
    public static function verifyResetPasswordToAdmin($mUser) {
        $name = $mUser->full_name;
        $key = ForgotPasswordForm::generateKey($mUser);
        $forgot_link = '<a href="' . Yii::app()->createAbsoluteUrl('/admin/site/resetPassword', array('id' => $mUser->id, 'key' => $key)) . '">' . Yii::app()->createAbsoluteUrl('/admin/site/ResetPassword', array('id' => $mUser->id, 'key' => $key)) . '</a>';

        $param = array(
            '{NAME}' => $name,
            '{USERNAME}' => $mUser->username,
            '{EMAIL}' => $mUser->email,
            '{LINK}' => $forgot_link,
        );
        $aSubject = array(
            '{NAME}' => $name,
            '{USERNAME}' => $mUser->username,
            '{EMAIL}' => $mUser->email,
            '{LINK}' => $forgot_link,
        );

        if (CmsEmail::sendmail(MAIL_VERIFY_TO_RESET_PASSWORD_TO_ADMIN, $aSubject, $param, Yii::app()->params['adminEmail']))
            Yii::app()->user->setFlash("success", "An email has sent to: $mUser->email. Please check email to verify this action.");
        else
            $mUser->addError('email', 'Can not send email.');
    }

    //mail to reset password after admin agreed verify email at BE
    public static function resetPasswordToAdmin($mUser) {
        $name = $mUser->full_name;
        $login_link = '<a href="' . Yii::app()->createAbsoluteUrl("admin/site/login") . '">' . Yii::app()->createAbsoluteUrl("admin/site/login") . '</a>';
        $param = array(
            '{NAME}' => $name,
            '{PASSWORD}' => $mUser->temp_password,
            '{LINK_LOGIN}' => $login_link,
        );

        $aSubject = array(
            '{NAME}' => $name,
            '{PASSWORD}' => $mUser->temp_password,
            '{LINK_LOGIN}' => $login_link,
        );

        if (CmsEmail::sendmail(MAIL_RESET_PASSWORD_TO_ADMIN, $aSubject, $param, Yii::app()->params['adminEmail']))
            Yii::app()->user->setFlash("success", "An email has sent to: $mUser->email. Please check email to get new password.");
        else
            $mUser->addError('email', 'Can not send email to: ' . $mUser->email);
    }

    //mail to change password successfully from "Change password form" at BE
    public static function noticeChangPasswordSucceedToAdmin($mUser) {
        $name = $mUser->full_name;
        $login_link = '<a href="' . Yii::app()->createAbsoluteUrl("admin/site/login") . '">' . Yii::app()->createAbsoluteUrl("admin/site/login") . '</a>';
        $param = array(
            '{NAME}' => $name,
            '{PASSWORD}' => $mUser->temp_password,
            '{LINK_LOGIN}' => $login_link,
        );

        $aSubject = array(
            '{NAME}' => $name,
            '{PASSWORD}' => $mUser->temp_password,
            '{LINK_LOGIN}' => $login_link,
        );

        if (CmsEmail::sendmail(MAIL_CHANGE_PASSWORD_TO_ADMIN, $aSubject, $param, Yii::app()->params['adminEmail']))
            Yii::app()->user->setFlash("success", "An email has sent to: $mUser->email. Please check email to get new password.");
        else
            $mUser->addError('email', 'Can not send email to: ' . $mUser->email);
    }

    /*
     * Bim Service: request for quote
     */

    public static function requestForQuote(BimRequestQuote $model) {
        $addresses = array($model->address_1);
        if (!empty($model->address_2))
            $addresses[] = $model->address_2;
        $param = array(
            '{NAME}' => $model->name,
            '{EMAIL}' => $model->email,
            '{ADDRESS}' => implode(', ', $addresses),
            '{CONTACT}' => $model->contact,
            '{SERVICE}' => $model->bim_service->title,
        );

        if (CmsEmail::sendmail(EmailTemplates::MAIL_REQUEST_FOR_QUOTE, array(), $param, Yii::app()->params['adminEmail'])) {
            
        } else
            $mUser->addError('email', 'Can not send email');
    }
    
    /*
	 * @author Lam Huynh
	 */
	public static function registerSucceedToUser($model) {


        //Yii::app()->securityManager->encrypt( $string, $key );
        //Yii::app()->securityManager->decrypt( $string, $key );
        //Yii::app()->securityManager->encrypt( $model->email, $model->verify_code )

		$params = array(
			'{{USER}}' => $model->full_name,
			'{{ACT_LINK}}' => Yii::app()->createAbsoluteUrl('site/activation', array(
				//'email'=>base64_encode($model->email),
				//'code'=>$model->verify_code
                'code'=>base64_encode($model->email."{X}".$model->verify_code),
				)),
		);

		return CmsEmail::sendmail('register_user',
			$params, $params, $model->email);
    }

    public  static function reSendVerify($model){


        $u = Users::model()->findByAttributes(array('email' => trim($model->email)));
        $u->verify_code = Users::model()->checkVerifyCode(rand(100000, 1000000)); // Gen verify code and send qua mail or sms
        $u->update('verify_code');

        $params = array(
            '{{COMPANY_NAME}}' => $u->getCompanyName(),
            '{{ACT_LINK}}' => Yii::app()->createAbsoluteUrl('site/activation', array(
                //'email'=>base64_encode($u->email),
                //'code'=>$u->verify_code,
                'code'=>base64_encode($u->email."{X}".$u->verify_code),
                )),
        );

        return CmsEmail::sendmail('resend_verification',
            $params, $params, $model->email);

    }
    
    public static function partnerPendingRequest($request){
        $link = '<a href="'.Yii::app()->createAbsoluteUrl('/site/login').'">Click here to view</a>';
        $param = array(
            '{SENDER}' => $request->from_user_fk ? $request->from_user_fk->full_name:'',
            '{RECEIVER}' => $request->to_user_fk ? $request->to_user_fk->full_name:'',
            '{LINK}' => $link,
        );
        $aSubject = array(
        );
        $email = $request->to_user_fk ? $request->to_user_fk->email:$request->partner_outsite_system;
        CmsEmail::sendmail(CmsEmail::PARTNER_PENDING_REQUEST, $aSubject, $param, $email);
    }
    
    public static function partnerRejectRequest($request){
        $link = '<a href="'.Yii::app()->createAbsoluteUrl('/site/login').'">Click here to view</a>';
        $param = array(
            '{SENDER}' => $request->from_user_fk ? $request->from_user_fk->full_name:'',
            '{RECEIVER}' => $request->to_user_fk ? $request->to_user_fk->full_name:'',
            '{LINK}' => $link,
        );
        $aSubject = array(
        );
        $email = $request->to_user_fk ? $request->to_user_fk->email:'mind3f@gmail.com';
        CmsEmail::sendmail(CmsEmail::PARTNER_REJECT_REQUEST, $aSubject, $param, $email);
    }
    
    public static function partnerApproveRequest($request){
        $param = array(
            '{SENDER}' => $request->from_user_fk ? $request->from_user_fk->full_name:'',
            '{RECEIVER}' => $request->to_user_fk ? $request->to_user_fk->full_name:'',
        );
        $aSubject = array(
        );
        $email = $request->to_user_fk ? $request->to_user_fk->email:'mind3f@gmail.com';
        CmsEmail::sendmail(CmsEmail::PARTNER_APPROVE_REQUEST, $aSubject, $param, $email);
    }
    
    public static function sampleSendToUser($request){
        $param = array(
        );
        $aSubject = array(
            '{NAME}' => '11111',
        );
        CmsEmail::sendmail(10, $aSubject, $param, 'mind3f@gmail.com');
    }
    

    public static function sendTestEmail() {
		EmailHelper::sendMail(array(
			'subject' => 'test',
			'from' => 'u1@m.mm',
			'to' => 'u2@m.mm',
			'message' => 'mail body'
		));
    }

	/*
	 * @author Lam Huynh
	 */
	public static function newShippingRecipientNotify(SsnShipping $model) {
		$params = array(
			'{{USER}}' => $model->receiver->full_name,
			'{{COMPANY_SENDER}}' => $model->getSenderName(),
			'{{DATE_LOAD}}' => $model->dateLoadText,
			'{{ORDER_NO}}' => $model->order_number,
			'{{REMARK}}' => $model->remark,
			'{{LINK}}' => Yii::app()->createAbsoluteUrl('member/shipping/view',array('id'=>$model->id)),
			'{{ITEMS}}' => Yii::app()->controller->renderPartial(
				'application.views.email-tpl.shipping-item-table', array('model'=>$model), true)
		);

		return CmsEmail::sendmail('new_shipping_receiver', 
			$params, $params, $model->receiver->email);
	}
	
	/*
	 * @author Lam Huynh
	 */
	public static function updateShippingRecipientNotify(SsnShipping $model) {
		$params = array(
			'{{USER}}' => $model->receiver->full_name,
			'{{COMPANY_SENDER}}' => $model->getSenderName(),
			'{{DATE_LOAD}}' => $model->dateLoadText,
			'{{ORDER_NO}}' => $model->order_number,
			'{{REMARK}}' => $model->remark,
			'{{LINK}}' => Yii::app()->createAbsoluteUrl('member/shipping/view',array('id'=>$model->id)),
			'{{ITEMS}}' => Yii::app()->controller->renderPartial(
				'application.views.email-tpl.shipping-item-table', array('model'=>$model), true)
		);

		return CmsEmail::sendmail('update_shipping_receiver', 
			$params, $params, $model->receiver->email);
	}
	
	/*
	 * @author Lam Huynh
	 *
	 *
    <p><img src="http://s5.postimg.org/kthgn6p3b/ssn_logo.png" alt=""></p>
    <p>From Company: {{COMPANY_SENDER}}</p>
    <p>User {{USER}}({{USER_EMAIL}})</p>
    <p>Attention: {{ATTENTION}}</p>
    <p>Send Date: {{SEND_DATE}}</p>
    <p>AWBN: {{AWBN}}</p>
    <p>Courier: {{COURIER}}</p>
    <p>Item:</p>

    <div>{{ITEMS}}</div>

    <p>Please check at:</p>
    <a href="{{LINK}}">{{LINK}}</a>
    <p>Regard,</p>
    */

	public static function newSampleRecipientNotify(SsnSample $model) {
		foreach ($model->receivers as $receiver) {
			$params = array(
				'{{USER}}' => $receiver->full_name,
                '{{USER_EMAIL}}'=>$receiver->email,
				'{{COMPANY_SENDER}}' => $model->getSenderName(),
                '{{ATTENTION}}' => $model->attention,
				'{{SEND_DATE}}' => $model->dateSentText,
				'{{AWBN}}' => $model->awbn,
				'{{COURIER}}' => $model->courierName,
				'{{LINK}}' => Yii::app()->createAbsoluteUrl('member/sample/view',array('id'=>$model->id)),
				'{{ITEMS}}' => Yii::app()->controller->renderPartial(
					'application.views.email-tpl.sample-item-table', array('model'=>$model), true)
			);


			CmsEmail::sendmail('new_sample_receiver', 
				$params, $params, $receiver->email);
		}
	}
	
	/*
	 * @author Lam Huynh
	 */
	public static function updateSampleRecipientNotify(SsnSample $model) {
		foreach ($model->receivers as $receiver) {
			$params = array(
				'{{USER}}' => $receiver->full_name,
                '{{USER_EMAIL}}'=>$receiver->email,
                '{{ATTENTION}}' => $model->attention,
				'{{COMPANY_SENDER}}' => $model->getSenderName(),
				'{{SEND_DATE}}' => $model->dateSentText,
				'{{AWBN}}' => $model->awbn,
				'{{COURIER}}' => $model->courierName,
				'{{LINK}}' => Yii::app()->createAbsoluteUrl('member/sample/view',array('id'=>$model->id)),
				'{{ITEMS}}' => Yii::app()->controller->renderPartial(
					'application.views.email-tpl.sample-item-table', array('model'=>$model), true)
			);


			CmsEmail::sendmail('update_sample_receiver', 
				$params, $params, $receiver->email);
		}
	}
	
	public static function sendInviteRequest($data) {
		$params = array(
			'{{FROM}}' => $data['from'],
			'{{TO}}' => $data['to'],
			'{{REGISTER_LINK}}' => Yii::app()->createAbsoluteUrl('site/register'),
		);

		return CmsEmail::sendmail('invite_user', 
			$params, $params, $data['to']);		
	}
}

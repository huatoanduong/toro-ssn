<?php

class BaseFormatter extends CFormatter {
            
    public function formatStatus($value) {
        if (is_array($value)) {
            return (($value['status'] == STATUS_INACTIVE) ?
                            CHtml::link(
                                    "Inactive", array("ajaxActivate", "id" => $value['id']), array(
                                "class" => "ajaxupdate",
                                "title" => "Click here to " . DeclareHelper::$statusFormat[STATUS_ACTIVE],
                                    )
                            ) :
                            CHtml::link(
                                    "Active", array("ajaxDeactivate", "id" => $value['id']), array(
                                "class" => "ajaxupdate",
                                "title" => "Click here to " . DeclareHelper::$statusFormat[STATUS_INACTIVE],
                                    )
                            )
                    );
        } else
            return $value == 0 ? DeclareHelper::$statusFormat[STATUS_INACTIVE] : DeclareHelper::$statusFormat[STATUS_ACTIVE];
    }

    public function formatPartner($value) {
        $r = '';
        switch ($value) {
            case SsnPartner::STATUS_SEND:
                $r = 'Sent';
                break;
            case SsnPartner::STATUS_APPROVE:
                $r = 'Approved';
                break;
            case SsnPartner::STATUS_PENDING:
                $r = 'Pending';
                break;
        }
        return $r;
    }

    public function formatApprovePartner($value) {
        $r = '';
        $partner = SsnPartner::model()->findByAttributes(array('id' => $value['id']));
        if ($partner) {
            if ($partner->status == SsnPartner::STATUS_SEND)
                $r = '<a href="' . Yii::app()->createUrl("/member/partner/approve", array("id" => $value['id'])) . '">Approve</a>';
        }
        return $r;
    }

    public function formatApprovePartnerByAdmin($value) {
        $r = '';
        $partner = SsnPartner::model()->findByAttributes(array('id' => $value['id']));
        if ($partner) {
            if ($partner->status == SsnPartner::STATUS_PENDING || $partner->status == SsnPartner::STATUS_SEND)
                $r = '<a href="' . Yii::app()->createUrl("/admin/partner/approve", array("id" => $value['id'])) . '">Approve</a>';
        }
        return $r;
    }

    public function formatLogoShow($value) {
        if (is_array($value)) {
            return (($value['status'] == STATUS_INACTIVE) ?
                            CHtml::link(
                                    "Hidden", array("showLogo", "id" => $value['id']), array(
                                "class" => "ajaxupdate",
                                "title" => "Click here to Show",
                                    )
                            ) :
                            CHtml::link(
                                    "Show", array("showLogo", "id" => $value['id']), array(
                                "class" => "ajaxupdate",
                                "title" => "Click here to Hide",
                                    )
                            )
                    );
        } else
            return $value == 0 ? DeclareHelper::$statusFormat[STATUS_INACTIVE] : DeclareHelper::$statusFormat[STATUS_ACTIVE];
    }

    public function formatDate($value, $formatF = '') {
        if (empty($formatF))
            $formatF = Yii::app()->params['dateFormat'];
        if ($value == '0000-00-00' || $value == '0000-00-00 00:00:00' || is_null($value))
            return '';
        if (is_string($value)) {
            $date = new DateTime($value);
            return $date->format($formatF);
        }
        return parent::formatDate($value);
    }

    public function formatTime($value, $formatF = '') {
        if (empty($formatF))
            $formatF = Yii::app()->params['timeFormat'];
        if ($value == '0000-00-00' || $value == '0000-00-00 00:00:00' || is_null($value))
            return '';
        if (is_string($value)) {
            $date = new DateTime($value);
            return $date->format($formatF);
        }
        return parent::formatDate($value);
    }

    public function formatDateTime($value, $formatF = '') {
        if (empty($formatF))
            $formatF = Yii::app()->params['dateFormat'] . ' ' . Yii::app()->params['timeFormat'];
        if ($value == '0000-00-00' || $value == '0000-00-00 00:00:00' || is_null($value))
            return '';
        if (is_string($value)) {
            $date = new DateTime($value);
            return $date->format($formatF);
        }
        return parent::formatDate($value);
    }

    /* formatYNStatus use for Yes/No */

    public static function formatYNStatus($value) {
        $return = DeclareHelper::$yesNoFormat;
        return isset($return[$value]) ? $return[$value] : "";
    }

    public function formatPrice($value, $country = 'sg') {
        if ($country == 'sg') {
            return 'S$' . number_format($value, 2);
        }
        return $value;
    }

    public function formatNumberCurrency($value, $country = 'sg') {
        if (is_array($value)) {
            if (empty($value['currencyType']))
                $currencyType = 'SGD';
            else
                $currencyType = $value['currencyType'];
            return number_format((float) $value['number'], 2) . " (" . $currencyType . ")";
        } else
            return $value = "";
    }

    public function formatGetBanner($data) {
        if ($data->place_holder_id == Banners::BANNER_HOLDER_HOME) {
            $width = Banners::IMAGE2_WIDTH_1;
            $height = Banners::IMAGE2_HEIGHT_1;
        } else {
            $width = Banners::IMAGE2_WIDTH_1;
            $height = Banners::IMAGE2_HEIGHT_2;
        }
        return '<img src="' . $data->getImageUrl($data->id, 'large_image', $width, $height) . '">';
    }

    public function formatImage($data) {
        $img = $data->getImageUrl();
        return sprintf('<img src="%s" alt="" style="max-width:400px" />', $img);
    }

    public function formatUsername($id) {
        $val = 'Undefined';
        if ($id == null)
            return $val;
        $user = Users::model()->findByPk($id);
        if ($user) {
            $val = $user->full_name;
        }
        return $val;
    }
    
    public function formatUserNameFormat($arr){
        
        $name = '';
        $id = '';
        $url = '';
        if($arr[0] == Yii::app()->user->id){
            $id = $arr[1];
            $name = $arr[3];
        }
        if($arr[1] == Yii::app()->user->id){
            $id = $arr[0];
            $name = $arr[2];
        }
        $company = SsnCompany::model()->findByAttributes(array("user_id" => $id));
        return '<a href="'.Yii::app()->createAbsoluteUrl('member/partner/viewCompany', array('id' => $id)).'">'.($company ? $company->name:"").'</a>';
    }
    
    public function formatUserLink($arr){
        $name = '';
        $id = '';
        $url = '';
        if($arr[0] == Yii::app()->user->id){
            $id = $arr[1];
            $name = $arr[3];
        }
        if($arr[1] == Yii::app()->user->id){
            $id = $arr[0];
            $name = $arr[2];
        }
        if($id == Yii::app()->user->id)
            return '<a href="'.Yii::app()->createAbsoluteUrl('member/site/updateProfile').'">'.$name.'</a>';
        return '<a href="'.Yii::app()->createAbsoluteUrl('member/partner/viewCompany', array('id' => $id)).'">'.$name.'</a>';
    }

	/*
	 * @author Lam Huynh
	 */
	public function formatEnum($value,$data=array()) {
		if (is_array($value)) {
			$data = $value[1];
			$value = $value[0];
		}
		return isset($data[$value]) ? $data[$value] : '';
	}
}

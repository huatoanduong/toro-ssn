<?php

class EventsWidget extends CWidget {

//    public $position;

    public function run() {
        $this->getEvents();
    }

    public function getEvents() {
        $event = new TbEvents();
        $data = $event->search();
        $this->render("events/_view", array('data' => $data));
    }

}

?>
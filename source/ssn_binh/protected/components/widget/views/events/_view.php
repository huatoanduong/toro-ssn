<?php
    $this->widget('zii.widgets.CListView', array(
        'dataProvider'=>$data,
        'itemView'=>'events/_item',
        'itemsCssClass' => 'events-list',
        'template' => '<div class="events-list-wrap">
                            {items}
                        </div>'
    ));
?>
<script>
    $(document).ready(function(){ 
        $('#TbEvents_start_date').on('change', function(){ 
            $.ajax({
                type: "POST",
                url: $('form').attr("action"),   
                data: $('#form0').serialize(),
                success: function (result) {
                   // do somthing here
                }
           });
        });
      });
    $('#clearsearch').click(function(){
        var id='search-form';
        var inputSelector='#'+id+' input, '+'#'+id+' select';
        $(inputSelector).each( function(i,o) {
                 $(o).val('');
        });
        var data=$.param($(inputSelector));
        $.fn.yiiGridView.update('tb-events-grid', {data: data});
        return false;
    });
</script>


<h2 class="title-1">OUR CLIENTS</h2>
<ul>
	<?php 
	foreach($models as $key=>$model) {
	$class = '';
	if($key+1 == count($models)) {
		$class='class="last"';
	} elseif($key+1 == 1) {
		$class='class="first"';
	} 
	?>
	<li <?php echo $class;?>>
		<img alt="<?php echo $model->name;?>" src="<?php echo $model->getImageUrl('image'); ?>" />
	</li>
	<?php } ?>
</ul>

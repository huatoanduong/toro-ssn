<?php

class FormForQuoteWidget extends CWidget {

    public $service_id;

    public function run() {
        $this->showFormQuote();
    }

    public function showFormQuote() {
        $model = new BimRequestQuote('create');
        $this->render("formquote/formquote", array(
            'model' => $model,
            'service_id' => $this->service_id
        ));
    }

}

?>
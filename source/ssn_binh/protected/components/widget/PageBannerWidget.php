<?php

class PageBannerWidget extends CWidget {

    public $apply_for_page;

    public function run() {
        $this->getPageBanners();
    }

    public function getPageBanners() {
        $init = new Ads();
        $models = $init->getAdsBanners($this->apply_for_page);
        $this->render("banners/page_banners", array(
            'models' => $models,
        ));
    }

}

?>
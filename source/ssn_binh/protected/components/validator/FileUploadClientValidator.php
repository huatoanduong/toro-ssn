<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FileUploadClientValidator
 *
 * @author Lam
 */
class FileUploadClientValidator extends CFileValidator {
	
	/**
	 * Returns the JavaScript needed for performing client-side validation.
	 * @param CModel $object the data object being validated
	 * @param string $attribute the name of the attribute to be validated.
	 * @return string the client-side validation script.
	 * @see CActiveForm::enableClientValidation
	 */
	public function clientValidateAttribute($object, $attribute) {
		$types = explode(',', $this->types);
		array_walk($types, function (&$value)  { 
			$value = "'".trim($value)."'"; 
		});
		$exts = '['. implode(',',$types) .']';
		
		return "
var input = $('#'+attribute.inputID);
if (input.size()<1 || input[0].files.length<1) {
	return;
}
var f = input[0].files[0];
var ext = f.name.substr(f.name.lastIndexOf('.')+1);
if ($.inArray(ext, $exts) == -1) {
	messages.push('Only allow file type: *.doc, *.docx, *.xls, *.xlsx, *.pdf.');
	return;
}
";
	}

}

Gửi partner:
 - Thêm 1 cột company_email trong table company, có giá trị bằng user_email chủ của company đó
 - Khi tạo company -> status = 1
 - Khi gửi partner request -> Nếu không có trong hệ thống thì tạo company & user với status = 0 (Chưa kích hoạt, không đăng nhập được)
 - Sau khi gửi Partner Invitation -> status = approved
 
Đăng kí:
 - Nếu company & user email có trong database progess -> Gửi email validate -> Sau khi validate thì update status company & user = 1
 - Add company_email = user_email khi đăng kí xong

New Sample:
 - Remove header of item table (Sửa nhẹ trong view)
 - Cho phép gửi đến email bất kì (Hiện tại là chỉ gửi được cho partner của mình)  -> Nếu email user không có trong hệ thống -> Tạo company & user với status = 0 (Chưa kích hoạt, không đăng nhập được)
 - Sau khi gửi Sample -> Add Company vào Partner luôn -> Status = approved
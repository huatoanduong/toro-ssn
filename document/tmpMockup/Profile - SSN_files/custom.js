window.ssn = {
	
	setupSampleDetailPage: function (newItemUrl, partnerTemplate) {
		// display dropdown item in company list
		function formatCompany (company) {
			if (!company.id || !(company.id in partnerTemplate)) { return company.text; }
			
			var $company = $(partnerTemplate[company.id]);
			return $company;
		};
		
		// setup company dropdown
		$(".company-select").select2({
			placeholder: "Select company",
	    	templateResult: formatCompany
		});
				
		ssn.setupItemList(newItemUrl);
	},
	
	addValidateRule: function (rule) {
		$(rule.inputs).each(function () {
			var inputId = this.id;
			var inputName = '';
			inputId.split('_').forEach(function(value, index, arr) {
				if (index==0) {
					inputName = value;
				} else {
					inputName += '['+value+']';
				}
			});
			var yiiRule = {
				'id': inputId,
				'inputID': inputId,
				'errorID': inputId + '_em_',
				'enableAjaxValidation': false,
				'clientValidation': ssn.createValidateChain(rule.validators)
			};
			$(this).closest('form').data('settings').attributes.push(yiiRule);	
		});
	},
	
	removeValidateRule: function (inputs) {
		$(inputs).each(function () {
			var inputID = this.id;
			var attributes = $(this).closest('form').data('settings').attributes;
			attributes.forEach(function (item, index, object) {
				if (item.inputID == inputID) {
					attributes.splice(index, 1);
				}
			});
		});
	},
	
	setupShippingDetailPage: function (newItemUrl, autoCompleteUrl) {
		// custom render for dropdown auto complete
		$('#SsnShipping_to_company').data( "autocomplete" )._renderItem = function( ul, item ) {
			return $( "<li></li>" )
				.data( "item.autocomplete", item )
				.append(item.label)
				.appendTo(ul);
		};

		// add token when select on auto complate
		$('#SsnShipping_to_company').on( "autocompleteselect", function( event, ui ) {
			event.preventDefault();
			if ($('.user-token input[value="'+ui.item.value+'"]').size()>0)
				return;
			$(this).val('');	// clear auto complete textbox
			
			var companyHtml = $('.user-token-tpl').clone(true);
			companyHtml.find('.comp-name').text(ui.item.name+' ('+ui.item.address+')');
			companyHtml.find('input[type="hidden"]')
				.prop('disabled', false).val(ui.item.value);
			companyHtml.removeClass('user-token-tpl').removeClass('hide');
			$('.user-ids').empty().append(companyHtml);
		});

		// remove token
		$('.user-token').on('click', '.btn-remove', function() {
			$(this).closest('.user-token').remove();
		});
		
		// add validator for token field
		ssn.addValidateRule({
			inputs: '#SsnShipping_to_company',
			validators: [ssn.tokenRequireValidator]
		});
		
		ssn.setupItemList(newItemUrl);
	},

	setupItemList: function (newItemUrl) {
		// add new item
		$(".btn-add").on('click',function(){
			var last_item=$("tr.item").length-1;
			if($('#SsnSampleItem_n'+last_item+'_name').val()==''){
			$('#SsnSampleItem_n'+last_item+'_name').focus();
			return;
			}
			var newNo = parseInt($('.item:last').find('h4').text().replace('Item ', ''));
			var item_number = 'n'+newNo;
			$.ajax({ 
				type: "POST", 
				url: newItemUrl,    
				data: { index : item_number}, 
				success: function(text) { 
					$('.item-list tbody').append(text);
					ssn.updateItemList();
$('.item .btn-remove').addClass('invisible').prop('disabled', false);
$('.item .btn-remove').removeClass('invisible');

$('.item:last .btn-remove').addClass('invisible').prop('disabled', true);
				}
			 });
		});

		// remove item
		$('.item-list').on('click', '.btn-remove', function () {
			$(this).closest('.item').remove();
			ssn.updateItemList();
		});
		
		ssn.updateItemList();
	},
	
	updateItemList: function () {
		var no = 1;
		$('.item').each(function () {
			$(this).find('h4').text('Item '+no);
			no++;
		});
		$('.item:last .btn-remove').addClass('invisible').prop('disabled', true);
		
		// clear all input's rules of all items
		ssn.removeValidateRule('.item input');
		
		// add new rules
		var rules = [
			{
				inputs: '.item input[type="text"]',
				validators: [ssn.requireValidator]
			},
			{
				inputs: '.item .qty',
				validators: [ssn.numberValidator]
			},
			{
				inputs: '.item input[type="file"]',
				validators: [ssn.docTypeValidator]
			}
		];
		rules.forEach(function(value, index, arr) {
			ssn.addValidateRule(value);
		});
	},
		
	requireValidator: function (value, messages, attribute) {
		if (value=='') {
			messages.push("You can not leave this field empty.");
			return false;
		}
		return true;
	},

	numberValidator: function (value, messages, attribute) {
		if (isNaN(value)) {
			messages.push("Minimum value is 1.");
			return false;
		}
		return true;
	},

	docTypeValidator: function (value, messages, attribute) {
		var input = $('#'+attribute.inputID);
		if (input.size()<1 || input[0].files.length<1) {
			return true;
		}
		var f = input[0].files[0];
		var ext = f.name.substr(f.name.lastIndexOf('.')+1);
		if ($.inArray(ext, ['doc','docx','xls','xlsx','pdf']) == -1) {
			messages.push('Only allow file type: *.doc, *.docx, *.xls, *.xlsx, *.pdf.');
			return false;
		}
		
		return true;
	},
	
	tokenRequireValidator: function (value, messages, attribute) {
		if ($('#'+attribute.inputID).parent().find('.user-token').size()<2) {
			messages.push('You have to choose at least one partner.');
			return false;
		}
	},

	createValidateChain: function (rules) {
		return function (value, messages, attribute) {
			rules.every(function (valFunc, index, arr) {
				return valFunc(value, messages, attribute);
			});
		};
	}
	
};